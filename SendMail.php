<?php

    include("config/config.php");

    //inclui a classe de envio
    require 'lib/phpmailer/PHPMailerAutoload.php';


    /*### DADOS DO E-MAIL DE CONTATO ###*/
    if($_REQUEST['Type']=="contato") {

        $mensagem = "Dados da Mensagem:<br>";
        $mensagem .= "<br><strong>Nome: </strong>".$_REQUEST['nome'];
        $mensagem .= "<br><strong>Telefone(s): </strong>".$_REQUEST['telefone'];
        $mensagem .= "<br><strong>E-mail: </strong>".$_REQUEST['email'];
        $mensagem .= "<br><strong>Mensagem: </strong><br>".$_REQUEST['mensagem'];

        if($_REQUEST['email']<>"") $remetente = $_REQUEST['email'];
        else $remetente = $remetente_padrao_contato;

        $email_destino = $email_administrador;
        $FromName = $_REQUEST['nome'];
        $Subject = "Formulário de Contato site Qualycare";
        $MsgRetorno = "OK";
    }
    /*### DADOS DO E-MAIL DE CONTATO ###*/




    /*### DADOS DO E-MAIL DE ENVIO DE CURRÍCULO ###*/
    elseif($_REQUEST['Type']=="curriculo") {

        $mensagem = "Dados da Mensagem:<br>";
        $mensagem .= "<br><strong>Nome: </strong>".$_REQUEST['nome'];
        $mensagem .= "<br><strong>E-mail: </strong>".$_REQUEST['email'];
        $mensagem .= "<br><strong>Telefone(s): </strong>".$_REQUEST['telefone'];
        $mensagem .= "<br><strong>Data Nascimento: </strong>".$_REQUEST['nascimento'];

        if($_REQUEST['email']<>"") $remetente = $_REQUEST['email'];
        else $remetente = $remetente_padrao_contato;

        $email_destino = $email_administrador;
        $FromName = $_REQUEST['nome'];
        $Subject = "Envio de currículo pelo site Qualycare";
        $MsgRetorno = "OK";
    }
    /*### DADOS DO E-MAIL DE ENVIO DE CURRÍCULO ###*/



    //CHAMA A CLASSE PHPMailer
    $Email = new PHPMailer();

    /*### OPÇÕES DE DEBUG ###*/
    //$Email->SMTPDebug = 2;
    //$Email->Debugoutput = 'html';
    /*### OPÇÕES DE DEBUG ###*/


    /*### OPÇÕES PARA LOGON AUTENTICADO TLS (GMAIL/LOCAWEB) ###*/
    $Email->IsSMTP();
    $Email->Mailer = "smtp";
    $Email->SMTPSecure = 'tls';
    $Email->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    /*### OPÇÕES PARA LOGON AUTENTICADO TLS (GMAIL/LOCAWEB) ###*/


    /*### DADOS DE AUTENTICAÇÃO ###*/
    $Email->SMTPAuth = true;
    $Email->SMTP_PORT = $remetente_padrao_contato_porta;
    $Email->Port = $remetente_padrao_contato_porta;
    $Email->Host = $remetente_padrao_contato_host;
    $Email->Helo = $remetente_padrao_contato_helo;
    $Email->Username = $remetente_padrao_contato_usuario;
    $Email->Password = $remetente_padrao_contato_senha;
    /*### DADOS DE AUTENTICAÇÃO ###*/

    //IDIOMA PADRÃO PARA BR
    $Email->SetLanguage("br");

    //E-MAILS EM FORMATO HTML
    $Email->IsHTML(true);

    //CHARSET UTF-8
    $Email->CharSet = 'UTF-8';

    //E-MAIL PADRÃO DO REMETENTE
    $Email->From = $remetente_padrao_contato;

    //NOME DO REMENTENTE
    $Email->FromName = $FromName;

    //E-MAIL PADRÃO DO REMETENTE (NECESSÁRIO SER E-MAIL VÁLIDO)
    $Email->Sender = $remetente_padrao_contato;

    //DESTINO DA MENSAGEM
    $Email->AddAddress(trim($email_destino));

    if (isset($_FILES['curriculo']) && $_FILES['curriculo']['error'] == UPLOAD_ERR_OK) {

        $Email->AddAttachment($_FILES['curriculo']['tmp_name'], $_FILES['curriculo']['name']); 
    }
    
    //OPÇÃO PARA CÓPIA OCULTA
    //$Email->AddBCC(trim($email_administrador));

    //SE EXISTIR E-MAIL DIGITADO, INFORMA PARA REPLY-TO
    if($remetente<>"") $Email->AddReplyTo(trim($remetente));

    //ASSUNTO DA MENSAGEM
    $Email->Subject = $Subject;

    //CORPO COM O TEXTO/HTML DA MENSAGEM
    $Email->Body = $mensagem;

    /*### ENVIA O E-MAIL OU RETORNA ERRO ###*/
    if($Email->Send())
        {
            $result["sended"] = "OK";
            $result["msg"] = $MsgRetorno;
        }
    else
        {
            $result["sended"] = "NOK";
            $result["msg"] = "<p class='bg-danger' style='padding:4px;'>Sua mensagem n&atilde;o foi enviada. Favor entrar em contato pelo e-mail $email_suporte e informar o erro: <strong>".$Email->ErrorInfo."</strong></p>";
        }
    /*### ENVIA O E-MAIL OU RETORNA ERRO ###*/

    echo json_encode($result);
?>