
    
    <div class="site-container-internal home-box clearfix">
            <div class="home-box-container">


			    <div class="page relative noborder cleafix">  
                      
                    <!--### HOME BOX ###-->
                    <ul class="home-box-content clearfix">
                        <li class="home_box light_blue">
                            <h2>
                                <a href="#" title="Trabalhe Conosco">
                                    Trabalhe Conosco
                                </a>
                            </h2>
                            <div class="news clearfix">
                                <p class="text">Quer fazer parte do nosso time? Clique no botão abaixo e envie o seu currículo. Agradecemos desde já o seu interesse e desejamos boa sorte!</p>
                                <a class="more light icon_small_arrow margin_right_white" href="<?php echo $BaseUrl; ?>?page=trabalhe-conosco" title="Veja mais">Veja mais</a>
                            </div>
                        </li>
                        <li class="home_box blue">
                            <h2>
                                <a href="#" title="Serviços 24 horas">
                                    Serviços 24 horas
                                </a>
                            </h2>
                            <div class="news clearfix">
                                <p class="text">Atendimento às mais diversas intercorrências clínicas com ambulâncias modernas e equipe treinada para atender a situações de urgência e emergência.</p>
                                <a class="more light icon_small_arrow margin_right_white" href="<?php echo $BaseUrl; ?>?page=servicos" title="Veja mais">Veja mais</a>
                            </div>
                        </li>
                        <li class="home_box dark_blue">
                            <h2>
                                Horário de funcionamento
                            </h2>
                            <ul class="items_list thin dark_blue opening_hours">
                                <li class="clearfix">
                                    <span>
                                        Segunda - Sexta
                                    </span>
                                    <div class="value">
                                        8h às 12h
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <span>
                                        Segunda - Sexta
                                    </span>
                                    <div class="value">
                                        13h às 18h
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <span>
                                        Sábado
                                    </span>
                                    <div class="value">
                                        8h às 12h
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!--### HOME BOX ###-->
                    
                </div>

                <div class="clearfix"></div>

            </div>

        </div>

		<div class="site-container-internal home-icon-services clearfix">
			<div class="page relative noborder clearfix">  



                <!--### ICON SERVICES BOX ###-->
                <div class="page relative noborder home_icon_services clearfix">
                    <div class="columns columns_4 page_margin_top_section clearfix">
                        <ul class="column">
                            <li class="item_content clearfix">
                                <a class="features_image" href="<?php echo $BaseUrl; ?>?page=servicos#tab-01" title="Home Care">
                                    <img src="<?php echo $BaseUrl; ?>images/ico-home-care.png" alt="" class="animated_element animation-scale" />
                                </a>
                                <div class="text">
                                    <a href="<?php echo $BaseUrl; ?>?page=servicos#tab-01">
                                        <h3>Home Care</h3>
                                        Assistência de enfermagem domiciliar em diversas modalidades e atendimento da equipe multiprofissional.
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <ul class="column">
                            <li class="item_content clearfix">
                                <a class="features_image" href="<?php echo $BaseUrl; ?>?page=servicos#tab-02" title="">
                                    <img src="<?php echo $BaseUrl; ?>images/ico-remocao.png" alt="" class="animated_element animation-scale" />
                                </a>
                                <div class="text">
                                    <a href="<?php echo $BaseUrl; ?>?page=servicos#tab-03">
                                        <h3>Remoção Terrestre</h3>
                                        Remoções simples e avançadas com o acompanhamento de equipe specializada e UTI Móvel.
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <ul class="column">
                            <li class="item_content clearfix">
                                <a class="features_image" href="<?php echo $BaseUrl; ?>?page=servicos#tab-05" title="">
                                    <img src="<?php echo $BaseUrl; ?>images/ico-eventos.png" alt="" class="animated_element animation-scale" />
                                </a>
                                <div class="text">
                                    <a href="<?php echo $BaseUrl; ?>?page=servicos#tab-05">
                                        <h3>Eventos</h3>
                                        Assistência médica para eventos corporativos, culturais, desportivos e sociais. Somos referência nacional em eventos de massa.
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <ul class="column">
                            <li class="item_content clearfix">
                                <a class="features_image" href="<?php echo $BaseUrl; ?>?page=servicos#tab-04" title="">
                                    <img src="<?php echo $BaseUrl; ?>images/ico-prescricao.png" alt="" class="animated_element animation-scale" />
                                </a>
                                <div class="text">
                                    <a href="<?php echo $BaseUrl; ?>?page=servicos#tab-02">
                                        <h3>Atendimento Domiciliar</h3>
                                        Acompanhamento e visitas de uma equipe multidisciplinar completa e integrada. O cuidado de um hospital com o conforto de casa.
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--### ICON SERVICES BOX ###-->



		    </div>
        </div>



        <!--### OFFSHOOT BOX ###-->
        <div class="site_container offshoot-home-box-container">
            <div class="page relative noborder offshoot-home-box clearfix">
                <h3 class="box_header page_margin_top_section">Unidades</h3>

                <div class="page relative noborder">                   
                    <div id="offshoot-carousel" class="offshoot-home-box-carousel">
                        <div class="clearfix page_margin_top">
                            <div class="header_right">
                                <a href="#" id="offshoot_carousel_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
                                <a href="#" id="offshoot_carousel_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>
                            </div>
                        </div>
                        <ul class="gallery horizontal_carousel carousel offshoot_carousel">
                            <?php
                                foreach($Unidades as $key => $Prop) {
                            ?>
                                    <li class="gallery_box">
                                        <a href="<?php echo $BaseUrl; ?>images/<?php echo $Unidades[$key]["foto"]; ?>" class="fancybox open_lightbox"><img src="<?php echo $BaseUrl; ?>images/<?php echo $Unidades[$key]["foto"]; ?>" alt="<?php echo $Unidades[$key]["titulo"]; ?>" /></a>
                                        <div class="description">
                                            <h3><?php echo $Unidades[$key]["titulo"]; ?></h3>
                                        </div>
                                    </li>
                            <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <!--### OFFSHOOT BOX ###-->
        
        
        


        <!--### NEWS HOME BOX ###-->
		<div class="site-container-internal home-box-news-container">

            <div class="page relative noborder">               
                
                <div class="home-box-news-feature-container">
                    <div class="home-box-news-feature">
                        <div class="description">
                            <!-- <h3>Espaço para notícias importantes da Qualycare</h3> -->
                        </div>
                    </div>
                    <div class="home-box-news-feature">
                        <div class="description">
                            <!-- <h3>Espaço para notícias importantes da Qualycare</h3> -->
                            <!-- <h5>Titulo para abrir a matéria</h5> -->
                        </div>
                    </div>
                </div>

                <div id="news-carousel" class="home-news-box">
                    <div class="clearfix page_margin_top">
                        <div class="header_left">
                            <h3 class="">Últimas notícias</h3>
                            <h2 class="">Seja o primeiro a ler</h2>
                        </div>
                        <div class="header_right">
                            <a href="#" id="news_carousel_prev" class="scrolling_list_control_left icon_small_arrow left_black"></a>
                            <a href="#" id="news_carousel_next" class="scrolling_list_control_right icon_small_arrow right_black"></a>
                        </div>
                    </div>
                    <ul class="gallery horizontal_carousel carousel news_carousel">
                        <?php
                            foreach($Noticias as $key => $Prop) {
                        ?>
                            <li class="gallery_box">
                                <a href="<?php echo $BaseUrl; ?>?page=mostra&titulo=<?php echo t5f_sanitize_filename($Noticias[$key]["titulo"]); ?>&id=<?php echo $key; ?>">
                                    <img src="<?php echo $BaseUrl; ?>images/<?php echo $Noticias[$key]["foto"]; ?>" alt="" />
                                    <div class="description">
                                        <h3><?php echo $Noticias[$key]["titulo"]; ?></h3>
                                        <h5><?php echo $Noticias[$key]["resumo"]; ?></h5>
                                    </div>
                                </a>
                            </li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>


            </div>
            <div class="clearfix"></div>
        </div>
        <!--### NEWS HOME BOX ###-->
        
        
        
    


        <!--### SOCIAL MEDIA BOX ###-->
        <div class="site_container social-media-box-container">
            <div class="page relative noborder social-media-box clearfix">
                <h3 class="box_header page_margin_top_section">Qualycare nas redes</h3>
                <ul class="social_gallery">
                    <li><img src="<?php echo $BaseUrl; ?>images/foto-media-01-01.jpg" alt="" /></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/foto-media-01-02.jpg" alt="" /></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/foto-media-01-03.jpg" alt="" /></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/foto-media-02-01.jpg" alt="" /></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/foto-media-02-02.jpg" alt="" /></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/foto-media-02-03.jpg" alt="" /></li>
                </ul>
            </div>
        </div>
        <!--### SOCIAL MEDIA BOX ###-->
        
        
        


        <!--### PARTNERS BOX ###-->
		<div class="site-container-internal home-partners-box-container">
            
            <div class="page relative noborder home-partners-box">
                <ul class="partners_gallery">
                    <li><h3>Grupo Econômico</h3></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/parceiro-01.png" alt="" /></li>
                    <li><img src="<?php echo $BaseUrl; ?>images/parceiro-02.png" alt="" /></li>
                </ul>
            </div>
            <div class="clearfix"></div>
            
        </div>
        <!--### PARTNERS BOX ###-->


