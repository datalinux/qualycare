
<div class="site-container-internal contact-page clearfix">
    <div class="clearfix"><p>&nbsp;</p></div>

    <div class="page relative noborder cleafix">
        <h2>Fale conosco</h2>
        <!-- <h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </h3> -->
        <div class="clearfix"></div>
        <div class="page relative noborder home-contact-area">
            <div class="columns columns_3 page_margin_top_section clearfix">
                <ul class="column">
                    <li class="item_content clearfix">
                        <a class="features_image" title="">
                            <img src="<?php echo $BaseUrl; ?>images/ico-marker.png" alt="" class="" />
                        </a>
                        <div class="text">
                            Av. Miguel Sutil, 10000
                            <br>Jardim Mariana - Cuiabá, MT
                            <br>CEP 78040-305
                        </div>
                    </li>
                </ul>
                <ul class="column">
                    <li class="item_content clearfix">
                        <a class="features_image" title="">
                            <img src="<?php echo $BaseUrl; ?>images/ico-celular.png" alt="" class="" />
                        </a>
                        <div class="text">
                            0800 603 1012
                            <br>(65) 3626 – 1011
                        </div>
                    </li>
                </ul>
                <ul class="column">
                    <li class="item_content clearfix">
                        <a class="features_image" title="">
                            <img src="<?php echo $BaseUrl; ?>images/ico-carta.png" alt="" class="" />
                        </a>
                        <div class="text">
                            contato@qualycare.com.br
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        <div id="page-send">
            <form class="contact_form" id="contact_form" method="post" action="">
                <div class="columns full_width page_margin_top clearfix">
                    <div class="column_left">
                        <fieldset>
                            <label>Nome</label>
                            <div class="block">
                                <input class="text_input" name="nome" id="nome" type="text" value="" />
                            </div>
                        </fieldset>
                    </div>
                    <div class="column_right">
                    <fieldset>
                            <label>Telefone</label>
                            <div class="block">
                                <input class="text_input" name="telefone" id="telefone" type="text" value="" />
                            </div>
                        </fieldset>
                    </div>
                </div>
                <fieldset>
                    <label>E-mail</label>
                    <div class="block">
                        <input class="text_input" type="text" name="email" id="email" value="" />
                    </div>
                </fieldset>
                <fieldset style="clear:both;">
                    <label>Mensagem</label>
                    <div class="block">
                        <textarea name="mensagem" id="mensagem" rows="5"></textarea>
                    </div>
                    <input type="hidden" name="Type" value="contato" />
                </fieldset>
                <input type="submit" name="submit" value="Enviar Mensagem" class="more blue" />
            </form>
        </div>
        <div class="clearfix"><p>&nbsp;</p></div>

    </div>
</div>
<script src="<?php echo $BaseUrl; ?>lib/jquery-validation/dist/jquery.validate.js"></script>
<script type="text/javascript">

    FormSendMail("#contact_form", "#page-send", "contato");

</script>
