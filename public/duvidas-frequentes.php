
<div class="site-container-internal faq-page clearfix">
    <div class="clearfix"><p>&nbsp;</p></div>

    <div class="page relative noborder cleafix">
        <div class="header">
            <h2>Dúvidas relacionadas aos nossos serviços e atendimento:</h2>
        </div>
        <div class="clearfix"></div>
        <ul class="accordion medium page_margin_top clearfix">
            <?php
                foreach($Duvidas as $key => $Prop) {
            ?>
                    <li>
                        <div id="accordion-accordiontab-notifications-<?php echo t5f_sanitize_filename($Duvidas[$key]["pergunta"]); ?>">
                            <h3><?php echo $Duvidas[$key]["pergunta"]; ?></h3>
                        </div>
                        <ul class="clearfix">
                            <blockquote><?php echo $Duvidas[$key]["resposta"]; ?></blockquote>
                        </ul>
                    </li>
            <?php
                }
            ?>
        </ul>
        <div class="clearfix"><p>&nbsp;</p></div>
    </div>

</div>
