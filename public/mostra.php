<?php
        $titulo_compartilhar = "QUALYCARE: ".$Noticias[$_REQUEST["id"]]["titulo"];
        $link_compartilhar = $Parameters["Url"];
        $resumo_compartilhar = $Noticias[$_REQUEST["id"]]["resumo"];
?>
<meta property="og:title" content="<? echo $titulo_compartilhar; ?>" />
<meta property="og:description" content="<?php echo $resumo_compartilhar; ?>" />
<meta property="og:url" content="<? echo $link_compartilhar; ?>" />
<meta property="og:image" content="<? echo $http_thumb_redes; ?>" />
<meta property="og:type" content="article" />

<div class="site-container-internal show-news-page clearfix">
    <div class="clearfix"><p>&nbsp;</p></div>

    <div class="page relative noborder cleafix">
        <!-- <h3>7 DE FEVEREIRO DE 2018 / DESTAQUE / MÍDIA / MÚSICA</h3> -->
        <h2><?php echo $Noticias[$_REQUEST["id"]]["titulo"]; ?></h2>
        <a href="<?php echo $BaseUrl; ?>images/<?php echo $Noticias[$_REQUEST["id"]]["foto"]; ?>" class="fancybox open_lightbox"><img src="<?php echo $BaseUrl; ?>images/<?php echo $Noticias[$_REQUEST["id"]]["foto"]; ?>" class="img-responsive"/></a>
        <div class="txt-justify"><?php echo $Noticias[$_REQUEST["id"]]["texto"]; ?></div>
        <div class="clearfix"><p>&nbsp;</p></div>
        <h4>Compartilhar</h4>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_inline_share_toolbox" data-tile="<? echo $titulo_compartilhar; ?>" data-description="<?php echo $resumo_compartilhar; ?>" data-media="<? echo $http_thumb_redes; ?>" data-url="<? echo $link_compartilhar; ?>"></div>
        <div class="clearfix"><p>&nbsp;</p></div>
    </div>

</div>
