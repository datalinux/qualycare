
<div class="site-container-internal services-page clearfix">
    <div class="clearfix"><p>&nbsp;</p></div>

    <div class="page relative noborder cleafix">
        <div class="header_left">
            <h2>Conheça nossos serviços</h2>
        </div>
        <div class="clearfix"></div>
        <div class="tabs page_margin_top">
            <ul class="clearfix tabs_navigation">
                <li>
                    <a href="#tab-01" title="Home Care">Home Care</a>
                </li>
                <li>
                    <a href="#tab-02" title="SAD - Serviço de Atendimento Domiciliar">SAD - Serviço de Atendimento Domiciliar</a>
                </li>
                <li>
                    <a href="#tab-03" title="Serviços de Remoção">Serviços de Remoção</a>
                </li>
                <li>
                    <a href="#tab-04" title="Área Protegida">Área Protegida</a>
                </li>
                <li>
                    <a href="#tab-05" title="Cobertura de eventos">Cobertura de eventos</a>
                </li>
            </ul>
            <ul id="tab-01">
                <div class="columns page_margin_top full_width clearfix">
                    <div class="column_left">
                        <a href="<?php echo $BaseUrl; ?>images/foto-servico-home-care.jpg" rel="photostream" class="fancybox open_lightbox"><img src="<?php echo $BaseUrl; ?>images/foto-servico-home-care.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="column_right txt-justify no-padding long-text">
                        <p>Acreditamos na desospitalização precoce, promovendo assim a reinserção dos pacientes no meio familiar, pois favorece um melhor convívio familiar e social, prevenindo o acometimento de outras co-morbidades que podem ocorrer durante a internação hospitalar, promovendo uma melhor qualidade de vida ao indivíduo.</p>
                        <p>Estabelecido de acordo com o PAD – Plano de Atenção Domiciliar, com base nas condições de saúde e suporte profissional de que o paciente necessita, podendo ser de baixa, média ou alta complexidade, para que receba assistência de enfermagem domiciliar na modalidade de 6, 12 ou 24 horas por dia, respectivamente, além do atendimento da equipe multiprofissional.</p>
                    </div>
                </div>
            </ul>
            <ul id="tab-02">
                <div class="columns page_margin_top full_width clearfix">
                    <div class="column_left">
                        <img src="<?php echo $BaseUrl; ?>images/foto-servico-prescricao.jpg" class="img-responsive" />
                    </div>
                    <div class="column_right txt-justify no-padding long-text">
                        <p>Acompanhamento e visitas de uma equipe multidisciplinar completa e integrada (fonoaudiólogo, fisioterapeuta, nutricionista, terapeuta ocupacional e psicólogo, entre outros).</p>
                        <br><p><strong>Atendimento Pontual</strong>
                        <p>Com duração de até duas horas, para realização de medicamentos e curativos. O atendimento pode ser realizado até 3x ao dia, conforme a necessidade de cada paciente.</p>
                        <p>Nesta modalidade, a atuação da equipe multiprofissional é definida conforme necessidade do paciente e autorização do convênio.</p>
                    </div>
                </div>
            </ul>
            <ul id="tab-03">
                <div class="columns page_margin_top full_width clearfix">
                    <div class="column_left">
                        <img src="<?php echo $BaseUrl; ?>images/foto-servico-remocao.jpg" class="img-responsive" />
                    </div>
                    <div class="column_right txt-justify no-padding long-text">
                        <p><strong>Remoção Simples e Avançada - UTI Móvel</strong>
                        <p>Remoções eletivas de pacientes de baixa a alta complexidade com o acompanhamento de equipe altamente especializada e ambulâncias com equipamentos para o suporte avançado à vida.</p>
                        <br><p><strong>Viagens intermunicipais e interestaduais</strong></p>
                        <p>Transferência inter-hospitalar de pacientes em estado grave ou não, com total segurança, em todo território nacional.</p>
                    </div>
                </div>
            </ul>
            <ul id="tab-04">
                <div class="columns page_margin_top full_width clearfix">
                    <div class="column_left">
                        <img src="<?php echo $BaseUrl; ?>images/foto-servico-aph.jpg" class="img-responsive" />
                    </div>
                    <div class="column_right txt-justify no-padding long-text">
                        <p><strong>APH – Atendimento Pré-Hospitalar</strong></p>
                        <p>Atendimento às mais diversas intercorrências clínicas com ambulâncias modernas e equipe treinada para atender a situações de urgência e emergência. O paciente é avaliado e estabilizado onde estiver, e nos casos em que não há melhora do quadro, ocorre a transferência para uma unidade hospitalar.</p>
                        <p>Atendimento em situações de urgência e emergência para colaboradores, clientes e frequentadores que estiverem na área protegida contratada. É só ligar e, em poucos minutos, a área protegida recebe o atendimento de uma de nossas unidades móveis.</p>
                        <p>A prestação de socorro resguarda de prejuízos materiais e imateriais de uma eventual omissão, além de trazer tranquilidade e segurança para colaboradores, clientes e frequentadores do espaço.</p>
                        <br><strong>Vantagens</strong>
                        <ul class="list">
                            <li class="icon_small_arrow right_black">Atendimento 24 horas</li>
                            <li class="icon_small_arrow right_black">Ambulâncias modernas e equipadas</li>
                            <li class="icon_small_arrow right_black">Equipe médica e de enfermagem</li>
                            <li class="icon_small_arrow right_black">Pacotes diferenciados de acordo com a necessidade avalidada</li>
                            <li class="icon_small_arrow right_black">Serviço sem carência, passando a vigorar a partir da assinatura do contrato</li>
                        </ul>
                        
                    </div>
                </div>
            </ul>
            <ul id="tab-05">
                <div class="columns page_margin_top full_width clearfix">
                    <div class="column_left">
                        <img src="<?php echo $BaseUrl; ?>images/foto-servico-evento.jpg" class="img-responsive" />
                    </div>
                    <div class="column_right txt-justify no-padding long-text">
                        <p>Segurança para eventos corporativos, culturais, desportivos e sociais.</p>
                        <p>Para atender às normas do Conselho Federal e às legislações vigentes de Medicina e garantir a assistência médica em eventos, a equipe QualyCare é altamente capacitada para atender às situações de riscos, emergências ou acidentes, além de oferecer uma estrutura completa para os primeiros socorros e também intervenções mais complexas.</p>
                        <br><p><strong>Com posto médico</strong></p>
                        <p>Atendimento desde o suporte básico (posto médico simples) ao avançado (posto médico com leito vermelho1).</p>
                        <br><p><strong>Com Ambulância</strong></p> 
                        <p>Disponibilização de equipes: USB – Unidade de Suporte Básico com condutor e enfermeiro. USA – Unidade de Suporte Avançado com condutor enfermeiro e médico intervencionista.</p>
                    </div>
                </div>
            </ul>
    </div>

</div>
