
<div class="site-container-internal clearfix">
    <div class="clearfix"><p>&nbsp;</p></div>

    <div class="page relative noborder cleafix">
        <div class="columns page_margin_top full_width clearfix">
            <div class="column_left">
                <img src="<?php echo $BaseUrl; ?>images/foto-quem-somos.jpg" class="img-responsive" />
            </div>
            <div class="column_right txt-justify no-padding long-text">
                <p>A Qualycare foi criada em 2008 para oferecer os melhores cuidados e toda atenção que a saúde requer quando está em risco, durante uma recuperação ou emergência. Além do preparo técnico, nossa equipe multidisciplinar é constantemente treinada para oferecer atendimento humanizado e um olhar atento à qualidade de vida.</p>
                <p>Prestamos atendimentos de remoções para diversos hospitais, convênios, clínicas e instituições públicas em algumas das principais cidades de Mato Grosso (Cuiabá, Várzea Grande, Sinop, Sorriso, Primavera do Leste e Tangará da Serra) e contamos com o serviço de home care junto a várias operadoras de planos de saúde, além da modalidade de serviço particular.</p>
                <p>Além disso, possuímos uma ampla experiência profissional no segmento de remoção (Simples e Unidade de Tratamento Intensivo - UTI), onde já atuamos em grandes eventos, como os Jogos Estudantis Brasileiros, carnaval de Cuiabá, Jogos dos Povos Indígenas, eventos testes da Arena Pantanal, Corrida de Reis, Jogos Universitários Brasileiros, entre outros.</p>
            </div>
        </div>
        <div class="clearfix"><p>&nbsp;</p></div>
        <div class="columns_3 page_margin_top full_width clearfix">
            <div class="column">
                <h3 class="box_header slide">Missão</h3>
                <p>Oferecer soluções em saúde com segurança onde você estiver.</p>
            </div>
            <div class="column">
                <h3 class="box_header slide">Visão</h3>
                <p>Ser a maior e mais segura empresa em soluções de saúde do Centro-Oeste.</p>
            </div>
            <div class="column">
                <!-- <h3 class="box_header slide">Valores</h3> -->
                <p></p>
            </div>
        </div>
        <div class="clearfix"><p>&nbsp;</p></div>
    </div>

</div>
