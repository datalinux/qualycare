
<div class="site-container-internal news-page clearfix">
    <div class="clearfix"><p>&nbsp;</p></div>

    <div class="page relative noborder cleafix">
        <div class="header_left">
            <h2>Fique por dentro</h2>
        </div>
        <div class="clearfix"></div>
        <?php
            foreach($Noticias as $key => $Prop) {
        ?>
                <div class="columns page_margin_top full_width news-box clearfix">
                    <div class="column_left">
                        <a href="<?php echo $BaseUrl; ?>?page=mostra&titulo=<?php echo t5f_sanitize_filename($Noticias[$key]["titulo"]); ?>&id=<?php echo $key; ?>"><img src="<?php echo $BaseUrl; ?>images/<?php echo $Noticias[$key]["foto"]; ?>" class="img-responsive" /></a>
                    </div>
                    <div class="column_right txt-justify no-padding long-text clearfix">
                        <!-- <h4>7 DE FEVEREIRO DE 2018 / DESTAQUE / MÍDIA / MÚSICA</h4> -->
                        <h3><?php echo $Noticias[$key]["titulo"]; ?></h3>
                        <p><?php echo $Noticias[$key]["resumo"]; ?></p>
                        <a class="more light icon_small_arrow margin_right_gray" href="<?php echo $BaseUrl; ?>?page=mostra&titulo=<?php echo t5f_sanitize_filename($Noticias[$key]["titulo"]); ?>&id=<?php echo $key; ?>" title="Veja mais">Veja mais</a>
                    </div>
                </div>
                <div class="clearfix"></div>
        <?php
            }
        ?>

        <!-- <ul class="pagination page_margin_top">
            <li class="selected">
                <a href="#" title="">
                    1
                </a>
            </li>
            <li>
                <a href="#" title="">
                    2
                </a>
            </li>
            <li>
                <a href="#" title="">
                    3
                </a>
            </li>
        </ul> -->
        <div class="clearfix"><p>&nbsp;</p></div>
    </div>

</div>
