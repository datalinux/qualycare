<?php

    include("functions.php");

    /*### ROTAS ###*/
    $Routes["home"] = array("file" => "home.php");
    $Routes["quem-somos"] = array("file" => "quem-somos.php");
    $Routes["servicos"] = array("file" => "servicos.php");
    $Routes["servicos2"] = array("file" => "servicos2.php");
    $Routes["na-midia"] = array("file" => "na-midia.php");
    $Routes["mostra"] = array("file" => "mostra.php");
    $Routes["duvidas-frequentes"] = array("file" => "duvidas-frequentes.php");
    $Routes["fale-conosco"] = array("file" => "fale-conosco.php");
    $Routes["trabalhe-conosco"] = array("file" => "trabalhe-conosco.php");
    /*### ROTAS ###*/




    /*### SLIDER ###*/
    $Slider["home"][] = array("img" => "slider-home-01.jpg");
    // $Slider["home"][] = array("img" => "slider-home-02.jpg");
    // $Slider["home"][] = array("img" => "slider-home-03.jpg");
    
    $Slider["quem-somos"][] = array("img" => "slider-interno.jpg",
                                    "title" => "Quem Somos");
                                    // "description" => "Ut in tortor ac nunc ullamcorper vestibulum. Vivamus augue turpis, malesuada sed ex in, pretium eleifend tortor.");

    $Slider["servicos"][] = array("img" => "slider-interno.jpg",
                                    "title" => "Serviços");
                                    // "description" => "Quisque gravida eros viverra ex mattis tempus. Nulla fringilla faucibus eleifend.");

    $Slider["servicos2"][] = array("img" => "slider-interno.jpg",
                                    "title" => "Serviços");
                                    // "description" => "Quisque gravida eros viverra ex mattis tempus. Nulla fringilla faucibus eleifend.");

    $Slider["na-midia"][] = array("img" => "slider-interno.jpg",
                                    "title" => "Na mídia");
                                    // "description" => "Quisque gravida eros viverra ex mattis tempus. Nulla fringilla faucibus eleifend.");

    $Slider["duvidas-frequentes"][] = array("img" => "slider-interno.jpg",
                                            "title" => "Dúvidas Frequentes");
                                            // "description" => "Quisque gravida eros viverra ex mattis tempus. Nulla fringilla faucibus eleifend.");

    $Slider["fale-conosco"][] = array("html" => "<div class='contact_map page_margin_top html' id='map'></div>");
    $Slider["trabalhe-conosco"][] = array("html" => "<div class='contact_map page_margin_top html' id='map'></div>");
    /*### SLIDER ###*/



    

    /*### UNIDADES ###*/
    $Unidades[1]["titulo"] = "Cuiabá/MT";
    $Unidades[1]["foto"] = "foto-und-cuiaba.jpg";
    $Unidades[2]["titulo"] = "Rondonópolis/MT";
    $Unidades[2]["foto"] = "foto-und-rondonopolis.jpg";
    $Unidades[3]["titulo"] = "Sorriso/MT";
    $Unidades[3]["foto"] = "foto-und-sorriso.jpg";
    $Unidades[4]["titulo"] = "Primavera/MT";
    $Unidades[4]["foto"] = "foto-und-primavera.jpg";
    $Unidades[5]["titulo"] = "Sinop/MT";
    $Unidades[5]["foto"] = "foto-und-sinop.jpg";
    // $Unidades[6]["titulo"] = "Tangará da Serra/MT";
    // $Unidades[6]["foto"] = "foto-und-tangara.jpg";
    /*### UNIDADES ###*/




    /*### NOTÍCIAS INICIAIS ###*/
    $Noticias[1]["titulo"] = "Circunstâncias emergenciais exigem preparo de empresas, instituições e órgãos";
    $Noticias[1]["foto"] = "foto-noticia-01-g.jpg";
    $Noticias[1]["resumo"] = "As remoções de urgência e emergência não só agitam o dia a dia das equipes intervencionistas que se dedicam a atender às chamadas, como também daqueles que têm a sua rotina interrompida um acidente.";
    $Noticias[1]["texto"] = "<p>As remoções de urgência e emergência não só agitam o dia a dia das equipes intervencionistas que se dedicam a atender às chamadas, como também daqueles que têm a sua rotina interrompida por uma descarga elétrica, um incêndio, uma queda ou qualquer outro acidente.</p>
    <p>Um bom atendimento de resgate exige preparo e agilidade. Na abordagem da vítima, a equipe deve ser capaz de identificar os danos ocasionados e os possíveis riscos de agravos, para assim atuar rapidamente conforme os protocolos de Atendimento Pré-Hospitalar (APH).</p>
    <p>No entanto, segundo a médica Flávia Monteiro, coordenadora do setor de remoções da Qualycare – empresa especializada em home care, remoções e atendimento pré-hospitalar –, existe um grande despreparo das instituições e unidades de saúde em relação às operações de resgate ou transporte.</p>
    <p>“A falta de um elevador em prédios de médio porte, a ausência de uma rede de oxigênio em clínicas médicas e laboratoriais, uma maca ou sala de espera adequada para pacientes acamados ou um carrinho de emergência em instituições que atendem grande fluxo de pessoas acabam por dificultar as nossas atividades”, revela.</p>
    <p>De acordo com a especialista, é indiscutível a percepção de que falta comprometimento na melhoria da segurança dos pacientes – sobretudo no quesito de prevenção e planos de ação em situações emergenciais. Tais fatos acarretam prejuízo na assistência ao paciente e a preocupação da equipe intervencionista.</p>
    <br><p><strong>SIMULAÇÕES</strong></p>
    <p>Com o objetivo de auxiliar as equipes profissionais dos mais variados setores, existem simulações de evacuação para casos de incêndio ou cursos de amparo ao resgate de vítimas. Flávia explica que qualquer estabelecimento que aglomere pessoas tem que estar preparado para as situações de emergência.</p>
    <p>Ao visar o melhor preparo de sua equipe hospitalar, o Hospital Santa Rosa realizou um Plano de Atendimento a Desastres e Incidentes com Múltiplas Vítimas (PAD). O treinamento contou com a participação de uma equipe multidisciplinar, bem como com o apoio das ambulâncias da Qualycare. </p>
    <p>“É fantástico saber que algumas instituições renomadas, como o Hospital Santa Rosa, se preocupam em preparar a equipe para situações de risco. Precisamos que toda e qualquer unidade de saúde esteja precavida e preparada para as mais variadas circunstâncias”, avaliou Flávia Monteiro.</p>
    <p>A médica explica que, diante do despreparo relacionado às situações emergenciais, quadros muito simples podem ser agravados. Por este motivo, escolas, creches, órgãos públicos e empresas de médio ou grande porte devem estar preparados e atualizados da ideal conduta de resgate.</p>
    <p>Alguns estabelecimentos optam, inclusive, pela Área Protegida, que é um serviço permanente de apoio e atendimento aos funcionários da empresa.</p>";
    
    $Noticias[2]["titulo"] = "“Todos os dias eu me lembro de que ganhei um presente especial”, diz mãe de paciente com síndrome rara";
    $Noticias[2]["foto"] = "foto-noticia-02-g.jpg";
    $Noticias[2]["resumo"] = "O piscar dos olhos, o apertar de uma mão ou a simples emissão de um som são algumas das pequenas vitórias que marejam os olhos e abrilhantam a vida de Jocilaine Zomer, mãe do pequenino Valdelir Floriano.";
    $Noticias[2]["texto"] = "<p>O piscar dos olhos, o apertar de uma mão ou a simples emissão de um som são algumas das pequenas vitórias que marejam os olhos e abrilhantam a vida de Jocilaine Zomer, mãe do pequenino Valdelir Floriano. Aos dois anos e cinco meses de vida, Florianinho – como é carinhosamente chamado pela família – tem, na figura da mãe, o seu maior e mais feliz suporte.</p>
    <p>Diagnosticado com a rara Síndrome de Pierre Robin, doença genética que acarreta a má formação dos sistemas craniofacial, ósseo e circulatório, o garoto vive sob os cuidados home care; é traqueostomizado em ventilação mecânica contínua associada ao oxigênio.</p>
    <p>Em seu quarto, além do barulho constante da ventilação e da presença de cilindros de oxigênio, os aparelhos de medição vital podem ser percebidos bem ao lado da televisão e de uma cadeirinha multicolorida. “Ele gosta de assistir desenho sentado na cadeira”, explica a mãe.</p>
    <p>Há dois anos em Cuiabá, a família Zomer é natural de Aripuanã, cidade ao norte do estado. Esposa de Gilmar Zomer e também mãe de Sabrina, de 19 anos, Nicole, de 15 e Luiz Gustavo, de 9, Jocilaine confessa que Valdelir é o ‘xodó’ de todos na casa. A família se mudou para a capital exclusivamente para priorizar o tratamento do caçula.</p>
    <p>“Sempre fazemos o acompanhamento com consultas médicas e exames para entender completamente o quadro dele. Também precisamos do cuidado periódico para saber quando vai ser possível a realização de procedimentos cirúrgicos que o beneficiarão, como o de fenda palatina”, explica a mãe.</p>
    <br><p><strong>Home care</strong></p>
    <p>Valdelir Floriano nasceu e foi imediatamente encaminhado para a UTI – onde esteve internado por longos 11 meses, até conseguir o direito ao atendimento hospitalar em domicílio por meio de liminar judicial. Com quase um ano de vida, o pequeno pôde ir para casa graças ao amparo do home care. O serviço, além de oportunizar a convivência com a família, apresentou um novo e encantador universo ao garoto: o seu lar.</p>
    <p>“Pela manhã, não gosta de ficar no berço e fica bravo se não coloca ele na cadeirinha. Adora ficar no colo também”, comenta Jocilaine. Florianinho já se acostumou com a rotina da casa e gosta muito de assistir à televisão pela manhã. Quando o quarto está mais cheio que o usual, a inquietude do pequeno denuncia o incômodo com a movimentação e a curiosidade para enxergar a todos que chegam ao local – sabendo disso, a enfermeira cuidadosamente ajusta a cama para que ele fique mais inclinado e veja a todos que adentram o seu universo. “Antes ele não tinha muita mobilidade nos olhos, hoje ele já consegue direcionar o olhar e observar tudo”, pondera a mãe.</p>
    <br><p><strong>Equipe multidisciplinar</strong></p>
    <p>Além do acompanhamento periódico por parte da equipe médica, Valdelir conta com o auxílio da fisioterapeuta, da fonoaudióloga e de enfermeiras. A mãe explica que as sessões de fisioterapia, por exemplo, são diárias; a frequência de exercícios possibilitou avanços consideráveis na mobilidade do garoto.</p>
    <p>O coordenador médico da Qualycare – empresa especializada em home care, remoções e atendimento pré-hospitalar –, Heleno Strobel, acompanha a evolução do quadro de Valdelir e ressalta os bons resultados proporcionados pelo atendimento em domicílio.</p>
    <p>“Desde que está sob os cuidados home care, ele cresceu e houve relevante melhora do desenvolvimento articular e sensorial. Também é importante destacar que o Valdelir é um paciente que raramente passa por intervenções clínicas, fato que evidencia o quão bem assistido ele é”, avaliou.</p>
    <p>Com carinho, Heleno diz que a motivação, enquanto médico, está justamente nas pequenas vitórias do dia-a-dia. O aperto de mão ou as respostas físicas a um carinho são alguns dos momentos felizes entre o doutor e o paciente – declaradamente o ‘queridinho’ da equipe multidisciplinar.</p>
    <p>Ellen Carla Zorzi, fonoaudióloga que atende o Valdelir há seis meses, reitera que o trabalho em equipe possibilita resultados mais significativos e eficazes para o paciente. “É muito gratificante ver as respostas do Valdelir aos estímulos. Trabalhamos em contato direto com a equipe médica ou com o fisioterapeuta e realizamos um trabalho realmente multidisciplinar. Há sempre o acompanhamento de todos os profissionais envolvidos”, destacou.</p>
    <p>Diante de todos os avanços de Valdelir Floriano, a equipe de especialistas está cada vez mais motivada em trabalhar o desenvolvimento do pequeno. A mãe, por sua vez, transborda no olhar a felicidade que é poder acompanhar de perto as conquistas de Florianinho – sempre grandiosas para ela. Com doses de esperança e amor, o caminho reserva ao garoto maiores superações.  </p>
    <p>“Por ele ter uma lesão cerebral, é criada a ideia de que o quadro dele estagnou, mas na verdade vemos os avanços acontecendo todos os dias. No tempo dele, ele está se desenvolvendo e eu sinto essa evolução. A minha esperança é que ele cresça e se desenvolva cada vez mais. Todos os dias eu me lembro que ganhei um presente especial para eu cuidar”, diz Jocilaine, emocionada.</p>";
    
    
    $Noticias[3]["titulo"] = "Assistência médica é tida como prioridade para o setor de eventos";
    $Noticias[3]["foto"] = "foto-noticia-03-g.jpg";
    $Noticias[3]["resumo"] = "Imprevistos, acidentes ou ocorrências que envolvam mal súbito são algumas das fatalidades que podem acontecer em eventos de pequeno, médio ou grande porte.";
    $Noticias[3]["texto"] = "<p>Imprevistos, acidentes ou ocorrências que envolvam mal súbito são algumas das fatalidades que podem acontecer em eventos de pequeno, médio ou grande porte. A presença de uma equipe de atendimento médico, além de ser regulamentada por lei, sela uma relação de confiança entre o realizador do evento e o público.</p>
    <p>De acordo com o artigo primeiro da Lei Municipal nº 4.984, é obrigatória a presença de unidades de atendimento compostas de médico, enfermeiros e ambulância com suporte de UTI, nos locais públicos e privados quando há a realização de shows, espetáculos artísticos ou qualquer evento congênere.</p>
    <p>A lei regulamenta a disponibilização de uma unidade de assistência médica para os eventos com participação entre um mil e cinco mil pessoas e uma unidade adicional a cada vez que o número de participantes representar o dobro do limite estabelecido neste parágrafo.</p>
    <p>A gerente comercial da Qualycare – empresa especializada em home care, remoções e atendimento pré-hospitalar –, Érika Alves de Lima, alerta para o fato de que, mesmo com a existência da lei, o ideal é a elaboração de um mapeamento personalizado, com o objetivo de garantir o serviço e estrutura adequados para cada tipo de evento e cliente. </p>
    <p>“Quanto maior o evento, maior deve ser a cobertura proporcionada pela equipe de assistência, com ambulância ou posto médico; pois também será maior a possibilidade de intercorrências. Optamos sempre pela aplicação do questionário de classificação de risco e a análise do perfil do evento. Eventos esportivos de até 1.000 pessoas, apesar de considerados de pequeno porte, devem contar com uma ambulância com UTI”, esclareceu.</p>
    <br><p><strong>PROPORÇÃO DO EVENTO</strong></p>
    <p>Como explicado por Érika, a magnitude dos eventos implica em estudos sobre as necessidades do público. Em eventos realizados com a assistência da Qualycare, é possível constatar a variação na estrutura disponibilizada, baseada na demanda e na análise estipulada pela empresa.</p>
    <p>“Levamos em consideração o porte, o tipo e o tempo de duração do evento, se é em local aberto ou fechado, a faixa etária do público e a distribuição de bebidas alcoólicas. Este planejamento também é um diferencial”, explica.</p>
    <p>No caso de uma festa de quinze anos, que reuniu 500 pessoas, foi necessária uma única ambulância para fazer a cobertura médica. Já uma formatura, realizada para mil pessoas, precisou de uma UTI móvel à disposição.</p>
    <p>A organização do Festival Braseiro, que contou com a presença de 3.000 pessoas, ressaltou a importância de se ter uma equipe de saúde de prontidão caso aconteça algo durante o evento. Marco Túlio, idealizador do Braseiro, explica a razão pela qual o festival está cada vez mais empenhado em assegurar o bem-estar do público.</p>
    <p>“O Braseiro busca ser referência em todos os aspectos. Procuramos pensar em todos os detalhes e, dentre eles, está a questão da estrutura em saúde – que é primordial. O conforto e, sobretudo, a segurança do nosso público são fatores que julgamos como dos mais importantes”, declarou Marco Túlio.</p>
    <p>Na Expoagro, um evento de grande porte que aglomera uma média de 10.000 pessoas por dia, a estrutura de emergência foi composta por um posto médico, duas UTIs móveis e uma ambulância simples.</p>
    <p>Thiago Alves, gestor de eventos da ZF Xperience – empresa responsável pela organização da última edição da Expoagro – ressalta a responsabilidade da produção em instruir o idealizador na contratação de uma equipe de assistência médica.</p>
    <p>“Chegamos a um público de 50 mil pessoas durante os shows da exposição. Disponibilizar uma estrutura compatível a essa demanda, com posto médico e UTI móvel, é responsabilidade do organizador. O papel da empresa que coordena eventos é justamente alertar o idealizador para esses cuidados e, desta forma, respaldamos os nossos clientes dos riscos”, explicou.</p>
    <br><p><strong>PRIORIDADE</strong></p>
    <p>A assistência de saúde em eventos deve ser tida como prioridade para os realizadores, fator que motivou a publicação da Portaria nº 1.139, de 10 de junho de 2013, do Ministério da Saúde, que regulamenta os critérios relacionados aos eventos e de que forma deve ocorrer a cobertura médica.</p>
    <p>A Resolução do Conselho Federal de Medicina nº 2012/13 também foi criada para regulamentar a presença de médicos em eventos de qualquer natureza.</p>
    <p>Importante alertar que, para cumprir os requisitos da legislação, é primordial que a cobertura médica esteja acertada no prazo máximo de três semanas antes da realização da programação.</p>";    
    /*### NOTÍCIAS INICIAIS ###*/




    /*### DUVIDAS INICIAIS ###*/
    $Duvidas[1]["pergunta"] = "O que é desospitalização?";
    $Duvidas[1]["resposta"] = "Os cuidados oferecidos pelo serviço de atendimento domiciliar, mais conhecido como home care, contam com um processo de extrema importância: a desospitalização. Paciente que possui condições de receber o tratamento na própria casa ou mesmo aquele que necessita apenas de tratamento paliativo pode ter a oportunidade de estar no conforto do lar e próximo dos familiares no momento de dificuldade com a saúde.";
    $Duvidas[2]["pergunta"] = "Quem define a introdução do paciente na Internação Domiciliar?";
    $Duvidas[2]["resposta"] = "O médico assistente é o responsável por designar o tratamento, com o consentimento do paciente e família, por meio de um relatório médico. Este relatório será analisado pela operadora do plano de saúde, responsável pelos custos de tratamento.";
    $Duvidas[3]["pergunta"] = "Qual a condição mínima necessária exigida pelo plano de saúde para implantação da internação domiciliar?";
    $Duvidas[3]["resposta"] = "Além do relatório médico com o quadro clínico do paciente e sua indicação médica para internação domiciliar, é necessário que esteja estável. Também será necessário a indicação pela família ou responsável de um cuidador informal que ficará responsável em receber e fornecer todas as informações necessárias para a implantação da internação domiciliar.";
    $Duvidas[4]["pergunta"] = "A assistência médica domiciliar para pacientes terminais é uma boa opção?";
    $Duvidas[4]["resposta"] = "Quando a família opta em tratar o paciente em casa, a equipe do home care realiza os cuidados necessários como se estivessem no ambiente hospitalar. Além de se sentir aconchegado por estar na própria casa, o paciente pode contar com a presença constante de seus entes queridos, o que proporcionará a ele dignidade e uma melhor qualidade nos seus derradeiros dias de vida.";
    $Duvidas[5]["pergunta"] = "Para quem não é indicada a Internação Domiciliar?";
    $Duvidas[5]["resposta"] = "Pacientes que não estão em condições de saúdes estáveis, com internações constantes, ou que precisem de equipamentos que não podem ser levados para seu domicílio devem permanecer em hospitais, já que nem sempre os cuidados de enfermeiros, entre outros profissionais da saúde, supre as necessidades médicas e hospitalares de um paciente.";
    $Duvidas[6]["pergunta"] = "O que é o desmame na Internação Domiciliar?";
    $Duvidas[6]["resposta"] = "Consiste na redução progressiva do tempo de assistência, de forma gradual e de acordo com a evolução do quadro clínico do paciente. Desta forma, os familiares serão treinados para assumirem os cuidados gradualmente até a saída definitiva da equipe de assistência e alta do paciente do programa.";
    /*### DUVIDAS INICIAIS ###*/





    /*### CONFIGURAÇÕES DO SERVIÇO DE E-MAIL ###*/
    $email_administrador = "contato@qualycare.com.br"; //email que o site vai usar como rementente em todos os formularios
    $email_suporte = "guilherme@datalinux.com.br"; //email suporte tecnico
    $remetente_padrao_contato = "contato@qualycare.com.br";
    $remetente_padrao_contato_host = "smtp.qualycare.com.br";
    $remetente_padrao_contato_porta = 587;
    $remetente_padrao_contato_helo = "QUALYCARE";
    $remetente_padrao_contato_usuario = "contato@qualycare.com.br";
    $remetente_padrao_contato_senha = "senhadoemail";
    /*### CONFIGURAÇÕES DO SERVIÇO DE E-MAIL ###*/


