# REPOSITÓRIO QUALYCARE
Repositório com os arquivos para o site inicial estático da Qualycare

## INFORMAÇÕES BÁSICAS
- O arquivo index.php contém o esqueleto principal da página, com menus e banner de topo.
- As páginas internas estão dentro da pasta public/, se desejar alterar os textos ou layout basta editar os arquivos.
- As principais configurações e os registros de itens recorrentes podem ser modificadas nas variáveis dentro do arquivo config/config.php:
    - a rotas das páginas ($Routes)
    - os dados dos banners de topo de cada uma das páginas ($Slider)
    - as configurações de remetente de e-mail ($remetente_*)
    - as notícias ($Noticias)
    - as dúvidas frequentes ($Duvidas)
    - as unidades ($Unidades)

## REQUISITOS TÉCNICOS DO SERVIDOR WEB
1. Versão mínima do PHP: 5.6
2. Servidor Web: Apache 2.4
3. Necessário o mod_rewrite do Apache estar ativo e funcionando
4. Certifique-se que o arquivo .htaccess incluído neste repositório esteja corretamente alocado na raiz da pasta public_html (ou equivalente)


## INSTALAÇÃO VIA GIT
1. Clonar o conteúdo do repositório remoto na pasta public_html (ou equivalente)
2. Rodar a instalação/atualização do composer (composer install ou composer update)


## INSTALAÇÃO VIA FTP
1. Baixar o arquivo compactado
2. Extratir o conteúdo do arquivo dentro da pasta public_html (ou equivalente)
