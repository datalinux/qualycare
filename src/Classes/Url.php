<?php
    namespace Classes;

    class Url {

        public static $Url = null;
        public static $BaseUrl = null;
        private $RequestURI;
        private $RedirectURL;
        private $PageUrl;

        public static function getBaseUrl() {
                
            global $_SERVER;

            if(is_dir($_SERVER['DOCUMENT_ROOT']."/qualycare"))
                {
                    self::$BaseUrl = "/qualycare/"; //sem barra no final
                }
            else
                {
                    self::$BaseUrl = "/"; //sem barra no final
                }

            return self::$BaseUrl;
        }

        public function getPage() {

            $this->RequestURI = $_SERVER["REQUEST_URI"];
            $this->RedirectURL = $_SERVER["REDIRECT_URL"];

            $PageUrl = str_replace(self::$BaseUrl, "", $this->RequestURI);
            $SplitUrl = explode("/",$PageUrl);

            $Page = ($SplitUrl[0]=="") ? "home" : $SplitUrl[0];

            if(count($SplitUrl)>1) {

                foreach($SplitUrl as $key => $parameter) {

                    if($key>0) {

                        $Parameters[] = $parameter;
                    }
                }
            }
            $Parameters["Url"] = "//".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];

            return array("page" => $Page, "parameters" => $Parameters);
        }

    }