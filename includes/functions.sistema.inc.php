<?
/*###################################################################
|																	|
|	DESCRIÇÃO: Arquivo que contém funções e variáveis comuns a 		|
|	todos os sistemas. Arquivo incluído dentro do sistema.cfg.php	|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guicastro@gmail.com										|
|	Data: 20/07/2014												|
|																	|
###################################################################*/





	/*###################################################################
	|																	|
	|	Variável $array_key_colunas_grid é a rotina para armazenar a	|
	|	correspondencia de key numérica da sequência da GRID			|
	|																	|
	###################################################################*/

	$array_key_colunas_grid = array();
	foreach($array_colunas_grid as $nome_coluna => $dados_coluna)
		{
			array_push($array_key_colunas_grid, $nome_coluna);
		}


	/*###################################################################
	|																	|
	|	Variável $array_colunas_key_grid é a rotina para armazenar a	|
	|	correspondencia de key textual com key numérida do GRID			|
	|																	|
	###################################################################*/

	unset($num_array_colunas_key_grid);
	$array_colunas_key_grid = array();
	foreach($array_colunas_grid as $nome_coluna => $dados_coluna)
		{
			if($num_array_colunas_key_grid=="") $num_array_colunas_key_grid = 0;
			$array_colunas_key_grid[$nome_coluna] = $num_array_colunas_key_grid;
			$num_array_colunas_key_grid++;
		}


	//VARIAVEL QUE ARMAZENA O NÚMERO TOTAL DE COLUNAS DO GRID
	$total_colunas_grid = count($array_colunas_grid);


	//VARIAVEL QUE ARMAZENA O NÚMERO TOTAL DE COLUNAS DO ARRAR DE ORDENAÇÃO DO GRID
	$total_ordenacao_grid = count($array_ordenacao_grid);
?>