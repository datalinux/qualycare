<?php
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);

############################## ABSOLUTE PATH ##############################
$pasta_root = $_SERVER['DOCUMENT_ROOT']."/qualycare"; //sem barra no final
$pasta_root_relativa = "/qualycare"; //sem barra no final
############################## ABSOLUTE PATH ##############################

############################## HTTP ROOT ##############################
$http_root = "http://".$_SERVER["HTTP_HOST"]."/qualycare"; // sem barra no final
$http_root = ($_SERVER['HTTPS']=="on") ? "https://".$_SERVER["HTTP_HOST"]."/qualycare" : "http://".$_SERVER["HTTP_HOST"]."/qualycare"; // sem barra no final
############################## HTTP ROOT ##############################

############################## REPONSIVE FILE MANAGER PATHS ##############################
$rfm_current_path = "../../../uploaded-images/"; // COM barra no final
$rfm_thumbs_base_path = "../../uploaded-images-thumbs/"; // COM barra no final
############################## REPONSIVE FILE MANAGER PATHS ##############################

$server_type = "DATALINUX";

?>