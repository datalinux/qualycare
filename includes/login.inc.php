<?php
//aqui eu nao deixo a pessoa acessar o arquivo diretamente
if (basename($_SERVER["PHP_SELF"]) == "login.inc.php") {
	die("Este arquivo n&atilde;oo pode ser acessado diretamente.");
}

$pagina_http = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
// echo "<br>pagina_http: ".$pagina_http;

// echo "<pre>REQUEST: "; print_r($_REQUEST); echo "</pre>";
// echo "<pre>SERVER: "; print_r($_SERVER); echo "</pre>";
// echo "<pre>SESSION: "; print_r($_SESSION); echo "</pre>";
// echo "<pre>SESSION: "; print_r($_SESSION["login"]); echo "</pre>";
// exit;

if(($_SESSION["login"]["usuario"]=="")||($_SESSION["login"]["id"]<=0)||($_SESSION["login"]["sessao_registrada"]<>true)||($_SESSION["login"]["session_name"]<>"admin".$key_senha_login))
	{
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Erro - Página Protegida</title>

		<meta name="description" content="Erro - Página Protegida" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="<? echo $http_root; ?>/font-awesome-4.7.0/css/font-awesome.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace.css" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace-skins.css" />
		<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace-rtl.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="<? echo $http_ace; ?>/assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<? echo $http_ace; ?>/assets/js/html5shiv.js"></script>
		<script src="<? echo $http_ace; ?>/assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- /section:basics/sidebar -->
			<div class="main-content">

				<!-- /section:basics/content.breadcrumbs -->
				<div class="page-content">

					<!-- /section:settings.box -->
					<div class="page-content-area">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<!-- #section:pages/error -->
								<div class="error-container">
									<div class="well">
										<h1 class="grey lighter smaller">
											<span class="blue bigger-125">
												<i class="ace-icon fa fa-lock"></i>
												Erro
											</span>
											Página Protegida
										</h1>

										<hr />
										<h3 class="lighter smaller">
											A página que você está acessando é protegida.
										</h3>

										<div class="space"></div>

										<div>
											<h4 class="lighter smaller">É necessário fazer o login para acessar</h4>

										</div>

										<hr />
										<div class="space"></div>

										<form action="<?php echo $http_admin.'/index.php'; ?>" method="post" enctype="multipart/form-data">
											<div class="center">
												<a href="javascript:history.back()" class="btn btn-grey">
													<i class="ace-icon fa fa-arrow-left"></i>
													Voltar
												</a>

												<button type="submit" class="btn btn-primary">
													<i class="ace-icon fa fa-key"></i>
													Login
												</button>

												<input type="hidden" name="pagina_http" id="pagina_http">
											</div>
										</form>
									</div>
								</div>

								<!-- /section:pages/error -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content-area -->
				</div><!-- /.page-content -->
			</div><!-- /.main-content -->


			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<? echo $http_ace; ?>/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<? echo $http_ace; ?>/assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<? echo $http_ace; ?>/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");

			$('#pagina_http').val(window.location.href);

		</script>
		<script src="<? echo $http_ace; ?>/assets/js/bootstrap.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="<? echo $http_ace; ?>/assets/js/ace-elements.js"></script>
		<script src="<? echo $http_ace; ?>/assets/js/ace.js"></script>

		<!-- inline scripts related to this page -->

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="<? echo $http_ace; ?>/assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="<? echo $http_ace; ?>/docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
	</body>
</html>

<?
		exit;
	}
?>
