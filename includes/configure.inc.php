<?

// echo "<br>DOCUMENT_ROOT: ".$_SERVER['DOCUMENT_ROOT'];
// echo "<br>HTTP_HOST: ".$_SERVER['HTTP_HOST'];
// echo "<br>$_SERVER: <pre>";print_r($_SERVER);echo "</pre>";
header('Content-Type: text/html; charset=utf-8');

session_start();
############################## CONFIGURAÇÕES DOS CAMINHOS DO SERVIDOR ##############################
include("configure.server.inc.php");
############################## CONFIGURAÇÕES DOS CAMINHOS DO SERVIDOR ##############################



############################## INFORMACOES DE BANCO DE DADOS ##############################
include("configure.db.inc.php");
############################## INFORMACOES DE BANCO DE DADOS ##############################



























############################## VARIÁVEIS INTERNAS PARA DEFINIR O CAMINHO DAS PASTAS ##############################
$pasta_fotos = $pasta_root."/banco_de_fotos"; //sem barra no final
$pasta_includes = $pasta_root."/includes"; //sem barra no final
$pasta_lib = $pasta_root."/lib"; //sem barra no final
$pasta_ace = $pasta_lib."/ace"; //sem barra no final
$pasta_admin = $pasta_root."/admin"; //sem barra no final
$pasta_imagens_admin = $pasta_root."/admin/imagens"; //sem barra no final
$pasta_imagens = $pasta_root."/images"; //sem barra no final
$pasta_imagens_templates_newsletters = $pasta_root."/imagens_templates_newsletters"; //sem barra no final
$caminho_imagens_templates_newsletters_cabecalho = $pasta_imagens_templates_newsletters."/cabecalho_"; //sem barra no final
$caminho_imagens_templates_newsletters_rodape = $pasta_imagens_templates_newsletters."/rodape_"; //sem barra no final
$pasta_fotos_galeria = $pasta_root."/galerias/fotos"; //sem barra no final
$pasta_galerias = $pasta_root."/galerias"; //sem barra no final
$pasta_fotos_rodape = $pasta_root."/fotos_rodape"; //sem barra no final
$pasta_fotos_rodape_fotos = $pasta_fotos_rodape."/fotos"; //sem barra no final
$pasta_arquivos = $pasta_root."/arquivos"; //sem barra no final
$pasta_dicas = $pasta_root."/dicas"; //sem barra no final
$pasta_documentos = $pasta_root."/documentos"; //sem barra no final
$pasta_videos = $pasta_root."/videos"; //sem barra no final
$pasta_destaques_topo = $pasta_root."/destaques_topo"; //sem barra no final
############################## VARIÁVEIS INTERNAS PARA DEFINIR O CAMINHO DAS PASTAS ##############################



############################## VARIÁVEIS INTERNAS PARA DEFINIR O AS URLS ##############################
$http_fotos = $http_root."/banco_de_fotos"; // sem barra no final
$http_includes = $http_root."/includes"; // sem barra no final
$http_lib = $http_root."/lib"; // sem barra no final
$http_ace = $http_lib."/ace"; // sem barra no final
$http_admin = $http_root."/admin"; // sem barra no final
$http_imagens_admin = $http_root."/admin/imagens"; // sem barra no final
$http_imagens = $http_root."/images"; // sem barra no final
$http_imagens_templates_newsletters = $http_root."/imagens_templates_newsletters"; // sem barra no final
$http_imagens_templates_newsletters_cabecalho = $http_imagens_templates_newsletters."/cabecalho_"; //sem barra no final
$http_imagens_templates_newsletters_rodape = $http_imagens_templates_newsletters."/rodape_"; //sem barra no final
$http_fotos_galeria = $http_root."/galerias/fotos"; // sem barra no final
$http_galerias = $http_root."/galerias"; // sem barra no final
$http_fotos_rodape = $http_root."/fotos_rodape"; // sem barra no final
$http_fotos_rodape_fotos = $http_fotos_rodape."/fotos"; // sem barra no final
$http_arquivos = $http_root."/arquivos"; // sem barra no final
$http_dicas = $http_root."/dicas"; // sem barra no final
$http_documentos = $http_root."/documentos"; // sem barra no final
$http_videos = $http_root."/videos"; // sem barra no final
$http_destaques_topo = $http_root."/destaques_topo"; // sem barra no final
$http_img_video_youtube = "http://img.youtube.com/vi"; // sem barra no final
############################## VARIÁVEIS INTERNAS PARA DEFINIR O AS URLS ##############################













############################## CONFIGURACOES GERAIS DO SITE ##############################
$email_administrador = "guilherme@datalinux.com.br"; //email que o site vai usar como rementente em todos os formularios

$email_suporte = "guilherme@datalinux.com.br"; //email suporte tecnico

//Configuracoes de listagem
$limite_registros_pagina = "20"; //limite de registros por pagina
$corlinha[0] = "FFFFFF"; //cor linha 1 doa paginacao
$corlinha[1] = "F0F0F0"; //cor linha 2 doa paginacao
$corlinha[2] = "CCFF99"; //cor do fundo da linha do mouseover
$corlinha[3] = "CEF3FF"; //cor do fundo da linha do mouseover
$corlinha[4] = "FEFFCA"; //cor do fundo da linha do mouseover


$pagina_login = "home.php"; //pagina de redirecionamento no login
$key_senha_login = "qualycare"; //chave para criptografia da senha de login no ADMIN

$filial_matriz = "qualycare"; //filial que podera ter acesso as informacoes de todas as outras filiais
$separador_string = "#"; //separador de informacoes usados para gravar em banco de dados - multiplas secoes

$tabela_usuarios = "usuarios"; //nome da tabela de usuarios
$tabela_usuarios_prefixo_campo = "usuario_"; //prefixo dos campos da tabela usuario
$tabela_permissoes = "usuarios_permissoes"; //nome da tabela de usuarios
$tabela_permissoes_prefixo_campo = "usuario_permissao_"; //nome da tabela de usuarios

//Funcao que retorna o limite maximo do tamanho do upload de arquivos feito pelo PHP
$upload_max_filesize = ini_get(post_max_size);
$tamanho_string_upload = strlen($upload_max_filesize);
$kbyte = 1024; //tamanho do KBYTE em BYTES
$megabyte = ceil($kbyte*$kbyte);
$limite_upload_php = (substr($upload_max_filesize,0,($tamanho_string_upload-1)))*$megabyte;

$titulo_site = "Qualycare"; //titulo do site
$titulo_admin = "Qualycare - Admin"; //titulo do site - secao admin


//Mensagens do sistema de administracao
$registro_inserido = "Registro inserido com sucesso."; //cadastro inserido
$registro_alterado = "Registro alterado com sucesso."; //cadastro alterado
$registro_excluido = "Registro excluído com sucesso."; //cadastro excluido
$registro_campos = "&Eacute; preciso preencher todos os campos com (*)"; //aviso de preenchimento de campos com *
$registro_todos_campos = "Todos os campos s&atilde;o de preenchimento obrigat&oacute;rio."; //aviso de preenchimento de todos os campos
$registro_erro = "Houve um erro ao efetuar a opera&ccedil;&atilde;o. Tente novamente ou entre com contato com o suporte t&eacute;cnico <a href='$http_admin/suporte.php' class='texto'>clicando aqui.</a> ".mysql_error(); //Mensagem de erro
$registro_exclusao = "ATEN&Ccedil;&Atilde;O!!! Esta opera&ccedil;&atilde;o &eacute; imediata e irrevers&iacute;vel."; //aviso de exclusao imediata


$sistema_nao_autorizado = "Usuário não autorizado a acessar este módulo ou operação."; //aviso de acesso ao modulo nao autorizada para o usuario
############################## CONFIGURACOES GERAIS DO SITE ##############################
























############################## INFORMACAO DAS GALERIAS DE FOTOS ##############################
//Outras configuracoes das galerias
$limite_caracteres_descricao_galeria = "450"; //limite de caracteres da descricao da galeria
############################## INFORMACAO DAS GALERIAS DE FOTOS ##############################













############################## CONFIGURACOES DO SISTEMA DE NOTICIAS/DESTAQUES ##############################
//Limites dos caracteres nos campos
$limite_caracteres_resumo = "450"; //limite de caracteres do resumo da noticia
$limite_caracteres_subtitulo = "150"; //limite de caracteres do subtitulo da noticia

//limite de numero de fotos de rodape de noticia
$limite_fotos_rodape = 6;

//limite de numero de arquivos que podem ser anexados na noticia
$limite_arquivos_noticia = 6;

//lagrura e altura da imagem do sistema de destaques
$largura_destaque_topo = 1903;
$altura_destaque_topo = 720;

//lagrura e altura da imagem do sistema de destaques (para tela de recorte)
$largura_recorte_destaque_topo = 855;
$altura_recorte_destaque_topo = 323;
$destaque_topo_export_zoom = '2.22';
############################## CONFIGURACOES DO SISTEMA DE NOTICIAS ##############################











############################## CONFIGURACOES DE DATA ##############################
$timezone = "America/Cuiaba";
date_default_timezone_set($timezone); //Define o TIMEZONE

//Define as variaveis para data atual
$hoje_dia = date("d"); //dia atual
$hoje_mes = date("m"); //mes atual
$hoje_mes_n = date("n"); //mes atual sem zero
$hoje_ano = date("Y"); //ano atual
$hoje_hora = date("H"); //hora atual
$hoje_minuto = date("i"); //minuto atual
$hoje_segundo = date("s"); //segundo atual
$hoje_semana = date("w"); //dia da semana atual (formato numerico)


//Variaveis de data atual completa
//echo "<br>".$hoje_dia."/".$hoje_mes."/".$hoje_ano." ".$hoje_hora.":".$hoje_minuto.":".$hoje_segundo;
$hoje_data_mk = mktime($hoje_hora,$hoje_minuto,$hoje_segundo,$hoje_mes,$hoje_dia,$hoje_ano); //data atual no foramto MK
//echo "<br>hoje_data_mk: ".$hoje_data_mk;
//echo "<br>hoje_data_mk: ".converte_timestamp_br_completa($hoje_data_mk);
$hoje_data_us = $hoje_ano."-".$hoje_mes."-".$hoje_dia." ".$hoje_hora.":".$hoje_minuto.":".$hoje_segundo; //data atual no foramto US (0000-00-00 00:00:00)
$hoje_data_br = $hoje_dia."/".$hoje_mes."/".$hoje_ano." ".$hoje_hora."h".$hoje_minuto; //data atual no foramto BR (00/00/000 00h00)

//Variaveis para usar na conversao de data em MK
$dia_mk = 86400; //valor numerico do MK para 1 dia
$mes_mk = $dia_mk*30; //valor numerico do MK para 1 mês de 30 dias
$hora_mk = $dia_mk/24; //valor numerico do MK para 1 hora
$minuto_mk = $hora_mk/60; //valor numerico do MK para 1 minuto

//ARRAY PARA MOSTRAR O DIA DO MES E DA SEMANA EM PORTUGUES
$dia_da_semana = array('0' => 'Domingo',
						'1' => 'Segunda-Feira',
						'2' => 'Ter&ccedil;a-Feira',
						'3' => 'Quarta-Feira',
						'4' => 'Quinta-Feira',
						'5' => 'Sexta-Feira',
						'6' => 'S&aacute;bado');

$array_meses = array('01' => 'Janeiro',
					'02' => 'Fevereiro',
					'03' => 'Mar&ccedil;o',
					'04' => 'Abril',
					'05' => 'Maio',
					'06' => 'Junho',
					'07' => 'Julho',
					'08' => 'Agosto',
					'09' => 'Setembro',
					'10' => 'Outubro',
					'11' => 'Novembro',
					'12' => 'Dezembro');

$array_meses_n = array('1' => 'Janeiro',
					'2' => 'Fevereiro',
					'3' => 'Mar&ccedil;o',
					'4' => 'Abril',
					'5' => 'Maio',
					'6' => 'Junho',
					'7' => 'Julho',
					'8' => 'Agosto',
					'9' => 'Setembro',
					'10' => 'Outubro',
					'11' => 'Novembro',
					'12' => 'Dezembro');

$hoje_data_completa_br = $hoje_dia." de ".$array_meses[$hoje_mes]." de ".$hoje_ano." - ".$hoje_hora."h".$hoje_minuto; //data atual completa no foramto BR
$hoje_data_abrev_br = $hoje_dia." ".substr($array_meses[$hoje_mes],0,3)." ".$hoje_ano." - ".$hoje_hora."h".$hoje_minuto; //data atual completa no foramto BR com nome de mes abreviado
############################## CONFIGURACOES DE DATA ##############################
















################ BUSCA INFORMACOES NO BANCO DE CONFIGURACOES ################
if($_SESSION['guarda_config']=="")
	{
		//echo "<br><strong>guardou</strong>";
		$array_config_filial = array();
		$array_config_categoria = array();
		$array_config_titulo = array();
		$array_config_valor = array();

		$sql_config = "SELECT
							configuracao_filial,
							configuracao_categoria,
							configuracao_titulo,
							configuracao_valor
						FROM
							configuracoes
						WHERE
							configuracao_excluido <> 'sim'
						ORDER BY
							configuracao_filial ASC,
							configuracao_categoria ASC,
							configuracao_titulo ASC,
							configuracao_valor ASC";
		$exe_config = mysql_query($sql_config) or die("Erro do MySQL [config]: ".mysql_error());
		while($ver_config = mysql_fetch_array($exe_config))
			{
				$_SESSION["guarda_config"]["titulo"][$ver_config["configuracao_filial"]][$ver_config["configuracao_categoria"]][$ver_config["configuracao_valor"]] = $ver_config["configuracao_titulo"];
				$_SESSION["guarda_config"]["valor"][$ver_config["configuracao_filial"]][$ver_config["configuracao_categoria"]][$ver_config["configuracao_titulo"]] = $ver_config["configuracao_valor"];
			}
	}
################ BUSCA INFORMACOES NO BANCO DE CONFIGURACOES ################










############################## CONFIGURACOES DE ENVIO DE E-MAILS ##############################
//remetente padrao para envio de formularios de contato
$remetente_padrao_contato = $_SESSION["guarda_config"]["titulo"]["todas"]["dados-email"]["email"];
$remetente_padrao_contato_host = $_SESSION["guarda_config"]["titulo"]["todas"]["dados-email"]["host"];
$remetente_padrao_contato_porta = $_SESSION["guarda_config"]["titulo"]["todas"]["dados-email"]["porta"];;
$remetente_padrao_contato_usuario = $_SESSION["guarda_config"]["titulo"]["todas"]["dados-email"]["usuario"];
$remetente_padrao_contato_senha = $_SESSION["guarda_config"]["titulo"]["todas"]["dados-email"]["senha"];
############################## CONFIGURACOES DE ENVIO DE E-MAILS ##############################










#########################MENSAGEM DE MANUTENCAO#########################
$habilitar_mensagem_manutencao = "nao";
$msg_manutencao = "<p class='erro_fundo' align='center' style='line-height:20px;'>";
$msg_manutencao .= "Voc&ecirc; est&aacute; acessando a se&ccedil;&atilde;o: <span id='titulo_secao_manutencao'>&nbsp;</span>.";
$msg_manutencao .= "<br><br>Informamos no dia 18/02/2011 at&eacute; &agrave;s 11h30 estaremos realizando uma manuten&ccedil;&atilde;o no sistema Newsletter Senai Cursos. Durante teste per&iacute;odo o sistema pode apresentar instabilidade.<br/>D&uacute;vidas ou problemas, entrar em contato pelo e-mail EMAIL ou telefone TELEFONE.";
$msg_manutencao .= "<br><br>Se&ccedil;&otilde;es atingidas:  Newsletter Senai Cursos";
$msg_manutencao .= "<br><br>Previs&atilde;o de t&eacute;rmino: 18/02/2011 - 11h30.";
$msg_manutencao .= "</p>";
#########################MENSAGEM DE MANUTENCAO#########################













############################## INCLUSAO DOS ARQUIVOS DAS FUNCOES ##############################
$pasta_atual = $_SERVER['REQUEST_URI'];
if($include_functions<>"nao")
	{
		include($pasta_includes."/functions.common.inc.php");
		include($pasta_includes."/functions.datas.inc.php");
	}
############################## INCLUSAO DOS ARQUIVOS DAS FUNCOES ##############################




?>