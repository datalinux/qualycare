



/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - CPF ###*/
//v = objeto do campo de formulário a ser tratado
function MaskCPF(v){

    var value = v.value;

    value = value.replace(/\D/g,"")                    //Remove tudo o que não é dígito
    value = value.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
    value = value.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos de novo (para o segundo bloco de números)
    value = value.replace(/(\d{2})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - CPF ###*/




/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - CNPJ ###*/
//v = objeto do campo de formulário a ser tratado
function MaskCNPJ(v){

    var value = v.value;

    value = value.replace(/\D/g,"")                    //Remove tudo o que não é dígito
    value = value.replace(/(\d{2})(\d)/,"$1.$2")       //Coloca um ponto entre o segundo e o terceiro dígitos
    value = value.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos de novo (para o segundo bloco de números)
    value = value.replace(/(\d{3})(\d)/,"$1/$2")       //Coloca uma ponto entre o terceiro e o quarto dígitos de novo (para o segundo bloco de números)
    value = value.replace(/(\d{4})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos (no último bloco)

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - CNPJ ###*/





/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - TELEFONE ###*/
//v = objeto do campo de formulário a ser tratado
function MaskTel(v, event){

    var value = v.value;

    if(value.length==14) {
        value = value.replace(/\D/g,"")                    //Remove tudo o que não é dígito
        value = value.replace(/(\d{2})(\d)/,"($1) $2")     //Coloca os parenteses nos dois primeiros dígitos
        value = value.replace(/(\d{4})(\d)/,"$1-$2")       //Se tiver 14 dígitos separa o telefone com 4 - 4 dígitos
    } else {

        value = value.replace(/\D/g,"")                    //Remove tudo o que não é dígito
        value = value.replace(/(\d{2})(\d)/,"($1) $2")     //Coloca um ponto entre o terceiro e o quarto dígitos
        value = value.replace(/(\d{5})(\d)/,"$1-$2")       //Se tiver 15 dígitos separa o telefone com 5 - 4 dígitos
    }

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - TELEFONE ###*/



/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - DATA (DD/MM/YYYY) ###*/
//v = objeto do campo de formulário a ser tratado
function MaskData(v){

    var value = v.value;

    value = value.replace(/\D/g,"")                     //Remove tudo o que não é dígito
    value = value.replace(/(\d{2})(\d)/,"$1/$2")        //Coloca uma barra entre o segundo e terceiro dígito
    value = value.replace(/(\d{2})(\d)/,"$1/$2")        //Coloca uma barra entre o quarto e quinto dígito

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - DATA (DD/MM/YYYY) ###*/




/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - CEP ###*/
//v = objeto do campo de formulário a ser tratado
function MaskCep(v){

    var value = v.value;

    value = value.replace(/\D/g,"")                     //Remove tudo o que não é dígito
    value = value.replace(/(\d{5})(\d)/,"$1-$2")        //Coloca um traço entre o quinto e o sexto dígito

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - CEP ###*/





/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - LOGIN ###*/
//v = objeto do campo de formulário a ser tratado
function MaskLogin(v){

    var value = v.value;

    value = value.replace(/[^a-zA-Z0-9.]/g,'') //Permite apenas letras, números e ponto

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - LOGIN ###*/





/*### NOTE: FUNÇÃO DE MÁSCARA DE VALORES - MOEDA ###*/
function MaskCurrency(v){

    var value = v.value;

    value = value.replace(/\D/g,"");//Remove tudo o que não é dígito
    value = value.replace(/(\d)(\d{8})$/,"$1.$2");//coloca o ponto dos milhões
    value = value.replace(/(\d)(\d{5})$/,"$1.$2");//coloca o ponto dos milhares

    value = value.replace(/(\d)(\d{2})$/,"$1,$2");//coloca a virgula antes dos 2 últimos dígitos

    v.value = value;
}
/*### FUNÇÃO DE MÁSCARA DE VALORES - MOEDA ###*/




/*### NOTE: FUNÇÃO QUE PROCESSA A URL IDENTIFICANDO A PÁGINA DIGITADA ###*/
//Url = URL completa pelo método window.location.href
function GetStartPageUrl(Url) {

    var StartPage = 1;

    var SplitUrl = Url.split('#page');

    if(parseInt(SplitUrl[1])>0) {

        StartPage = parseInt(SplitUrl[1]);
    }

    var StartPageUrl = [];
    StartPageUrl['StartPage'] = StartPage;
    StartPageUrl['Url'] = SplitUrl[0];

    return StartPageUrl;
}
/*### FUNÇÃO QUE PROCESSA A URL IDENTIFICANDO A PÁGINA DIGITADA ###*/




/*### NOTE: FUNÇÃO QUE CARREGA OS DADOS DO BANCO DE DADOS ###*/
//Module = módulo para referência de tabela de dados
//Section = seção para filtro dos dados (se Action = detail o Section recebe o ID do registro)
//Action = ação para retorno dos dados (list = listagem | detail = tdos os dados de um registro)
//Page = número da página atual
//Start = registro inicial
//End = registro final / limite de registros por página
//Condition = condição de busca adicional
//LoadingArea = nome do elemento onde será carregado os dados
//Search = palavra-chave para busca em todos os campos
function LoadDatabase(Module, Section, Action, Page, Start, End, Condition, LoadingArea, Search) {

    // console.log('\n-------------\nLoadDatabase');
    // console.log(Module);
    // console.log(Section);
    // console.log(Action);
    // console.log(Page);
    // console.log(Start);
    // console.log(End);
    // console.log(Condition);
    // console.log(LoadingArea);
    // console.log(Search);

    /*### MOSTRA A DIV DE CARREGANDO ###*/
    var contentArea = $(LoadingArea);
    contentArea.css('opacity', 0.25)

    var loader = $('<div class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
    var offset = contentArea.offset();
    // console.log(offset);

    if(Search=="") {

        loader.css({top: offset.top, left: offset.left});
        loader.css({width: contentArea.width(), height: contentArea.height()});
    }
    else {
        $(".ajax-loading-overlay").css({position: 'relative'});
        $(".icone-carregar").css({'margin-top': '0'});
    }
    // console.log(loader.css());
    /*### MOSTRA A DIV DE CARREGANDO ###*/


    /*### NOTE: AJAX PARA POSTAGEM DO FORMULÁRIO (ActionForm) ###*/
    return $.ajax({
        type: "POST",
        dataType: "json",
        url: 'ReturnData.php',
        data: {
            Module: Module,
            Section: Section,
            Action: Action,
            Page: Page,
            Start: Start,
            End: End,
            Condition: Condition,
            Search: Search
        },
        success: function(data, textStatus, jqXHR) {

            // console.log("success");
        },
        error: function (jqXHR, status, error) {

            // console.log("error");
            /*### ESCONDE A DIV DE CARREGANDO ###*/
            contentArea.css('opacity', 1);
            contentArea.prevAll('.ajax-loading-overlay').remove();
            /*### ESCONDE A DIV DE CARREGANDO ###*/

            var error = $('<div class="ajax-loading-overlay"><div class="bg-danger" style="padding:6px;"><h2>Houve um erro ao processar os dados, por favor, recarregue a página.</h2></div></div>').insertBefore(contentArea);
            var offset = contentArea.offset();
            error.css({top: offset.top, left: offset.left})
            error.css({width: contentArea.width(), height: contentArea.height()})
        }
    });
    /*### AJAX PARA POSTAGEM DO FORMULÁRIO ###*/
}
/*### FUNÇÃO QUE CARREGA OS DADOS DO BANCO DE DADOS ###*/




/*### NOTE: FUNÇÃO QUE GERA O HTML DA PAGINA DE DADOS ###*/
//Module = módulo para referência de tabela de dados
//FirstPage = número da primeira página
//LastPage = número da última página
//ListPages = array com as páginas internas
//Page = número da página atual
//StartPageUrl = array do resultado da função GetStartPageUrl
function Pagination(Module, FirstPage, LastPage, ListPages, Page) {

    var HtmlPagination = '<ul id="pag-'+Module+'" class="pagination">';

    /*### PRIMEIRA PÁGINA ###*/
    if(FirstPage) {

        HtmlPagination += '<li><a href="#'+FirstPage+'" data-page="'+FirstPage+'" class="troca-pagina">&laquo;</a></li>';
    }
    /*### PRIMEIRA PÁGINA ###*/


    /*### INDICADORES INTERNOS DE CADA PÁGINA ###*/
    $.each(ListPages, function(key, PageNum ) {

        var CurrentPage = PageNum==Page ? 'class="active"' : '';
        var HrefPage = PageNum==Page ? '#page0' : '#page'+PageNum;

        HtmlPagination += '<li '+CurrentPage+'><a href="'+HrefPage+'" data-page="'+PageNum+'" class="troca-pagina">'+PageNum+'</a></li>';
    });
    /*### INDICADORES INTERNOS DE CADA PÁGINA ###*/


    /*### ÚLTIMA PÁGINA ###*/
    if(LastPage) {

        HtmlPagination += '<li><a href="#page'+LastPage+'" data-page="'+LastPage+'" class="troca-pagina">&raquo;</a></li>';
    }
    /*### ÚLTIMA PÁGINA ###*/

    HtmlPagination += '</ul>';

    return HtmlPagination;
}
/*### FUNÇÃO QUE GERA O HTML DA PAGINA DE DADOS ###*/





/*### NOTE: FUNÇÃO PARA CARREGAMENTO DOS DADOS DO BANCO PARA A PÁGINA ###*/
//Database = objeto de dados gerado pela função LoadDatabase
//Module = módulo para referência de tabela de dados
//Section = seção para filtro dos dados (se Action = detail o Section recebe o ID do registro)
//Action = ação para retorno dos dados (list = listagem | detail = tdos os dados de um registro)
//LoadingArea = nome do elemento onde será carregado os dados
//StartPageUrl = array do resultado da função GetStartPageUrl
function ListDatabase(Database, Module, Section, Action, LoadingArea, StartPageUrl) {

    // console.log('\n-------------\nListDatabase');
    // console.log(Database);
    // console.log(Module);
    // console.log(Section);
    // console.log(Action);
    // console.log(LoadingArea);
    // console.log(StartPageUrl);

    if(Database) {

        /*### PROCESSA O JSON DE DADOS DO BANCO ###*/
        Database.success(function(data, textStatus, jqXHR) {


            var HtmlDataContent = "";


            /*### MONTA O RETORNO DOS DADOS ###*/
            if(data.Total>0) {












                /*### MODULO DE ARQUIVOS ###*/
                if(Module=='arquivos') {

                    /*### DADOS DA QTD DE REGISTROS ###*/
                    // HtmlDataContent += '<div class="course-list-header box clearfix">\
                    //                             <div class="summary pull-left"><strong>'+data.Total+'</strong> registros | exibindo <strong>'+data.Start+' a '+data.End+'</strong></div>\
                    //                         </div>';
                    /*### DADOS DA QTD DE REGISTROS ###*/

                    /*### CABEÇALHO DO ACCORDION ###*/
                    HtmlDataContent = '<div class="faq-wrapper">\
                                        <div class="panel-group" id="accordion-'+Module+'">';
                    /*### CABEÇALHO DO ACCORDION ###*/


                    /*### CABEÇALHO DA TABELA DE DADOS ###*/
                    // HtmlDataContent += '<div class="course-item-header row-divider hidden-sm hidden-xs">\
                    //                         <div class="row">\
                    //                             <div class="col-md-1"><strong>Data</strong></div>\
                    //                             <div class="col-md-8"><strong>Nome</strong></div>\
                    //                             <div class="col-md-3"><strong>Tipo</strong></div>\
                    //                         </div>\
                    //                     </div>';
                    /*### CABEÇALHO DA TABELA DE DADOS ###*/

                    // $.each(data.Data, function(key, DataObject ) {
                    /*### HTML DE CADA ARQUIVO ###*/
                    // HtmlDataContent += '<article class="course-item row-divider">\
                    //                         <div class="details row">\
                    //                             <div class="col-md-1">'+DataObject.arquivo_data+'</div>\
                    //                             <div class="col-md-8"><a href="'+HrefArquivo+'" target="_blank">'+DataObject.arquivo_titulo+'</a></div>\
                    //                             <div class="col-md-3">'+DataObject.arquivo_classificacao_nome+'</div>\
                    //                         </div>\
                    //                     </article>';
                    /*### HTML DE CADA ARQUIVO ###*/



                    /*### CRIA O HTML COM OS RESULTADOS DA BUSCA NO FORMATO ACCORDION ###*/
                    $.each(data.Data["ClassName"], function(Class, ClassName ) {

                        // console.log(Class);
                        // console.log(DataClassObject);

                        /*### HTML DA ESTRUTURA DE CADA PAINEL DO ACCORDION ###*/
                        HtmlDataContent += '<div class="panel panel-default">\
                                                <div class="panel-heading">\
                                                    <h4 class="panel-title">\
                                                        <a data-toggle="collapse" data-parent="#accordion-'+Module+'" class="collapsed" href="#collapse-'+Module+'-'+Class+'">'+ClassName+'</a>\
                                                    </h4>\
                                                </div>\
                                                <div id="collapse-'+Module+'-'+Class+'" class="panel-collapse collapse">\
                                                    <div class="panel-body">';

                        /*### HTML DO CONTEÚDO DE CADA PAINEL ACCORDION ###*/
                        $.each(data.Data["ClassData"][Class], function(key, DataObject ) {

                            //DEFINE O HREF DO ARQUIVO
                            var HrefArquivo = DataObject.arquivo_tipo=="link" ? DataObject.arquivo_link : data.http_arquivos+"/"+DataObject.arquivo_arquivo;

                            HtmlDataContent += '<div class="col-md-12" style="padding:4px;"><a href="'+HrefArquivo+'" target="_blank" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="'+DataObject.arquivo_descricao+'" title="Descrição do arquivo">'+DataObject.arquivo_titulo+'</a></div>';
                        });
                        /*### HTML DO CONTEÚDO DE CADA PAINEL ACCORDION ###*/

                        HtmlDataContent += '</div>\
                                                </div>\
                                            </div>';
                        /*### HTML DA ESTRUTURA DE CADA PAINEL DO ACCORDION ###*/

                    });
                    /*### CRIA O HTML COM OS RESULTADOS DA BUSCA NO FORMATO ACCORDION ###*/


                    /*### RODAPÉ DO ACCORDION ###*/
                    HtmlDataContent += '</div></div>';
                    /*### RODAPÉ DO ACCORDION ###*/

                }
                /*### MODULO DE ARQUIVOS ###*/















                /*### MODULO DE NOTÍCIAS ###*/
                else if(Module=='noticias') {

                    /*### DADOS DA QTD DE REGISTROS ###*/
                    HtmlDataContent += '<div class="course-list-header box clearfix">\
                                                <div class="summary pull-left"><strong>'+data.Total+'</strong> registros | exibindo <strong>'+data.Start+' a '+data.End+'</strong></div>\
                                            </div><br>';
                    /*### DADOS DA QTD DE REGISTROS ###*/


                    $.each(data.Data, function(key, DataObject ) {

                        //DEFINE O HTML QUANDO EXISTE FOTO
                        var Image = DataObject.noticia_foto>0 ? '<figure class="thumb col-md-2 col-sm-3 col-xs-4">\
                                                                    <img class="img-responsive" src="'+DataObject.arquivo_foto_url+'" alt="" />\
                                                                </figure>' : "";

                        //DEFINE O HTML QUANDO EXISTE RESUMO
                        var Resumo = DataObject.noticia_resumo!="" ? '<p>'+DataObject.noticia_resumo+'</p>' : "";

                        /*### HTML DE CADA NOTÍCIA ###*/
                        HtmlDataContent += '<article class="news-item page-row has-divider clearfix row">\
                                                '+Image+'\
                                                <div class="details col-md-10 col-sm-9 col-xs-8">\
                                                    <h3 class="title"><a href="mostra.php?secao='+Section+'&id='+DataObject.noticia_id+'">'+DataObject.noticia_titulo+'</a></h3>\
                                                    '+Resumo+'\
                                                    <a class="btn btn-theme read-more" href="mostra.php?secao='+Section+'&id='+DataObject.noticia_id+'">Ler mais<i class="fa fa-chevron-right"></i></a>\
                                                </div>\
                                            </article>';
                        /*### HTML DE CADA NOTÍCIA ###*/
                    });


                }
                /*### MODULO DE NOTÍCIAS ###*/















                /*### MODULO DE EVENTOS (GALERIA DE MÍDIA) ###*/
                else if(Module=='eventos') {

                    //DIV DE INÍCIO DA LINHA COM TODOS OS REGISTROS
                    HtmlDataContent += '<div class="row page-row">';

                    /*### DADOS DA QTD DE REGISTROS ###*/
                    HtmlDataContent += '<div class="course-list-header box clearfix">\
                                                <div class="summary pull-left"><strong>'+data.Total+'</strong> registros | exibindo <strong>'+data.Start+' a '+data.End+'</strong></div>\
                                            </div><br>';
                    /*### DADOS DA QTD DE REGISTROS ###*/


                    $.each(data.Data, function(key, DataObject ) {

                        /*### HTML DE CADA EVENTO (GALERIA/VÍDEO) ###*/
                        HtmlDataContent += '<div class="col-md-6 col-sm-6 col-xs-12 text-center">\
                                                <div class="album-cover">\
                                                    <a class="'+DataObject.CSS+'" '+DataObject.DATATAG+' '+DataObject.RELTAG+' title="'+DataObject.TITULO+'" href="'+DataObject.LINK+'"><img class="img-responsive" src="'+DataObject.FOTO+'" alt="" /></a>\
                                                    <div class="desc">\
                                                        <h4><small><a class="'+DataObject.CSS+'" '+DataObject.DATATAG+' '+DataObject.RELTAG+' title="'+DataObject.TITULO+'" href="'+DataObject.LINK+'">'+DataObject.TITULO+'</a></small></h4>\
                                                        <p>'+DataObject.DESCRICAO+'</p>\
                                                    </div>\
                                                </div>\
                                            </div>';
                        /*### HTML DE CADA EVENTO (GALERIA/VÍDEO) ###*/
                    });

                    //DIV DE FECHAMENTO DA LINHA COM TODOS OS REGISTROS
                    HtmlDataContent += '</div>';

                }
                /*### MODULO DE EVENTOS (GALERIA DE MÍDIA) ###*/
















                /*### MODULO DE ÁREA RESTRITA (ARQUIVOS) ###*/
                else if(Module=='area-restrita') {

                    /*### DADOS DA ÁREA RESTRITA / (INÍCIO/TRANSPARÊNCIA) ###*/
                    if($(data.Data["ClassName"]).length>0) {

                        /*### DADOS DA QTD DE REGISTROS ###*/
                        // HtmlDataContent += '<div class="course-list-header box clearfix">\
                        //                             <div class="summary pull-left"><strong>'+data.Total+'</strong> registros | exibindo <strong>'+data.Start+' a '+data.End+'</strong></div>\
                        //                         </div>';
                        /*### DADOS DA QTD DE REGISTROS ###*/


                        /*### CABEÇALHO DO ACCORDION ###*/
                        HtmlDataContent = '<div class="faq-wrapper">\
                                            <div class="panel-group" id="accordion">';
                        /*### CABEÇALHO DO ACCORDION ###*/


                        /*### CABEÇALHO DA TABELA DE DADOS ###*/
                        // HtmlDataContent += '<div class="course-item-header row-divider hidden-sm hidden-xs">\
                        //                         <div class="row">\
                        //                             <div class="col-md-1"><strong>Data</strong></div>\
                        //                             <div class="col-md-8"><strong>Nome</strong></div>\
                        //                             <div class="col-md-3"><strong>Tipo</strong></div>\
                        //                         </div>\
                        //                     </div>';
                        /*### CABEÇALHO DA TABELA DE DADOS ###*/

                        // $.each(data.Data, function(key, DataObject ) {

                        //     /*### HTML DE CADA ARQUIVO ###*/
                        //     HtmlDataContent += '<article class="course-item row-divider">\
                        //                             <div class="details row">\
                        //                                 <div class="col-md-1">'+DataObject.DATA+'</div>\
                        //                                 <div class="col-md-8"><a href="ReadFile.php?arquivo='+DataObject.ID+'" target="_blank">'+DataObject.TITULO+'</a></div>\
                        //                                 <div class="col-md-3">'+DataObject.CLASSIFICACAO+'</div>\
                        //                             </div>\
                        //                         </article>';
                        //     /*### HTML DE CADA ARQUIVO ###*/
                        // });



                        /*### CRIA O HTML COM OS RESULTADOS DA BUSCA NO FORMATO ACCORDION ###*/
                        $.each(data.Data["ClassName"], function(Class, ClassName ) {

                            // console.log(Class);
                            // console.log(DataClassObject);

                            /*### HTML DA ESTRUTURA DE CADA PAINEL DO ACCORDION ###*/
                            HtmlDataContent += '<div class="panel panel-default">\
                                                    <div class="panel-heading">\
                                                        <h4 class="panel-title">\
                                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-'+Class+'">'+ClassName+'</a>\
                                                        </h4>\
                                                    </div>\
                                                    <div id="collapse-'+Class+'" class="panel-collapse collapse">\
                                                        <div class="panel-body">';

                            /*### HTML DO CONTEÚDO DE CADA PAINEL ACCORDION ###*/
                            $.each(data.Data["ClassData"][Class], function(key, DataObject ) {

                                HtmlDataContent += '<div class="col-md-12" style="padding:4px;"><a href="ReadFile.php?arquivo='+key+'" target="_blank" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="'+DataObject.DESCRICAO+'" title="Descrição do arquivo">'+DataObject.TITULO+'</a></div>';
                            });
                            /*### HTML DO CONTEÚDO DE CADA PAINEL ACCORDION ###*/

                            HtmlDataContent += '</div>\
                                                    </div>\
                                                </div>';
                            /*### HTML DA ESTRUTURA DE CADA PAINEL DO ACCORDION ###*/

                        });
                        /*### CRIA O HTML COM OS RESULTADOS DA BUSCA NO FORMATO ACCORDION ###*/


                    }
                    /*### DADOS DA ÁREA RESTRITA / (INÍCIO/TRANSPARÊNCIA) ###*/


                }
                /*### MODULO DE ÁREA RESTRITA (ARQUIVOS) ###*/















                /*### MODULO DE CALENDÁRIO (AGENDA) ###*/
                else if(Module=='calendario') {

                    /*### DADOS DA QTD DE REGISTROS ###*/
                    HtmlDataContent += '<div class="course-list-header box clearfix">\
                                                <div class="summary pull-left"><strong>'+data.Total+'</strong> registros | exibindo <strong>'+data.Start+' a '+data.End+'</strong></div>\
                                            </div><br>';
                    /*### DADOS DA QTD DE REGISTROS ###*/


                    $.each(data.Data, function(key, DataObject ) {


                        //DEFINE O HTML QUANDO EXISTE HORÁRIO DO EVENTO
                        var HorarioEvento = DataObject.HORARIO_EVENTO ? '<span class="time"><i class="fa fa-clock-o"></i>'+DataObject.HORARIO_EVENTO+'</span>' : "";

                        //DEFINE O HTML QUANDO EXISTE A LOCALIZAÇÃO
                        var Localizacao = DataObject.agenda_evento_local!="" ? '<span class="location"><i class="fa fa-map-marker"></i><a href="#local">'+DataObject.agenda_evento_local+'</a></span>' : "";

                        /*### HTML DE CADA EVENTO DO CALENDÁRIO ###*/
                        HtmlDataContent += '<article class="events-item page-row has-divider clearfix">\
                                                <div class="date-label-wrapper col-md-1 col-sm-2">\
                                                    <p class="date-label">\
                                                        <span class="month">'+DataObject.MES+'</span>\
                                                        <span class="date-number">'+DataObject.DIA+'</span>\
                                                    </p>\
                                                </div>\
                                                <div class="details col-md-11 col-sm-10">\
                                                    <h3 class="title">'+DataObject.agenda_evento_titulo+'</h3>\
                                                    <p class="meta">\
                                                        '+HorarioEvento+'\
                                                        '+Localizacao+'\
                                                    </p>\
                                                    <p class="desc">'+DataObject.agenda_evento_descricao+'</p>\
                                                </div>\
                                            </article>';
                        /*### HTML DE CADA EVENTO DO CALENDÁRIO ###*/
                    });


                }
                /*### MODULO DE CALENDÁRIO (AGENDA) ###*/










                /*### MODULO DO PERFIL DO USUÁRIO (ASSOCIE) ###*/
                else if(Module=='associe') {

                    $("#form-associacao #nome").val(data.Data[0].arq_usuario_nome);
                    $("#form-associacao #rg").val(data.Data[0].arq_usuario_rg);
                    $("#form-associacao #cpf").val(data.Data[0].arq_usuario_cpf);
                    $("#form-associacao #nascimento").val(data.Data[0].arq_usuario_nascimento);
                    $("#form-associacao #endereco").val(data.Data[0].arq_usuario_endereco);
                    $("#form-associacao #bairro").val(data.Data[0].arq_usuario_bairro);
                    $("#form-associacao #complemento").val(data.Data[0].arq_usuario_complemento);
                    $("#form-associacao #cep").val(data.Data[0].arq_usuario_cep);
                    $("#form-associacao #cidade").val(data.Data[0].arq_usuario_cidade);
                    $("#form-associacao #uf").val(data.Data[0].arq_usuario_uf);
                    $("#form-associacao #email").val(data.Data[0].arq_usuario_email);
                    $("#form-associacao #emailAntigo").val(data.Data[0].arq_usuario_email);
                    $("#form-associacao #telefones").val(data.Data[0].arq_usuario_telefones);
                    $("#form-associacao #PHPBB_user_id").val(data.Data[0].arq_usuario_phpbb_user_id);
                }
                /*### MODULO DO PERFIL DO USUÁRIO (ASSOCIE) ###*/


            }
            /*### MONTA O RETORNO DOS DADOS ###*/

            /*### HTML DA PAGINAÇÃO ###*/
            if(data.ShowPagination==true) {

                HtmlDataContent += Pagination(Module, data.FirstPage, data.LastPage, data.ListPages, data.Page);
            }
            /*### HTML DA PAGINAÇÃO ###*/

            console.log(Module);
            console.log(data.Total);
            console.log(data.Search);
            /*### EXIBE MENSAGEM QUANDO NÃO HÁ REGISTRO ###*/
            if((data.Total==0)||(data.Total==null)) {

                if(data.ErroMsg) {

                    var HtmlDataContent = data.ErroMsg;
                }
                else {

                    var HtmlDataContent = data.Search=="" ? "<h3>Não há registros cadastrados</h3>" : "<h3>Não há registros com a palavra pesquisada</h3>";
                }

            }
            /*### EXIBE MENSAGEM QUANDO NÃO HÁ REGISTRO ###*/


            if(Module!='associe') {

                //CARREGA O CONTEÚDO NA ÁREA DO SITE
                $(LoadingArea).html(HtmlDataContent);
            }

            //INICIALIZA O PRETTYPHOTO
            $('a.prettyphoto').prettyPhoto();

            /*### ESCONDE A DIV DE CARREGANDO ###*/
            var contentArea = $(LoadingArea);
            contentArea.css('opacity', 1);
            contentArea.prevAll('.ajax-loading-overlay').remove();
            /*### ESCONDE A DIV DE CARREGANDO ###*/

            // console.log(data);
        });
        /*### PROCESSA O JSON DE DADOS DO BANCO ###*/


        /*### AÇÕES QUANDO CLICA PARA TROCAR DE PÁGINA ###*/
        $(LoadingArea).on('click', '.troca-pagina', function(event) {

            var Database = LoadDatabase(Module, Section, Action, $(this).data('page'), '', '', '', LoadingArea);

            ListDatabase(Database, Module, Section, Action, LoadingArea, StartPageUrl);

            window.location.href = StartPageUrl['Url']+'#page'+$(this).data('page');

            event.preventDefault();
        });
        /*### AÇÕES QUANDO CLICA PARA TROCAR DE PÁGINA ###*/


        /*### CHAMADA PARA POPOVER ###*/
        $(LoadingArea).on('mouseover', function(event) {

            // console.log($(this));
            $('[data-toggle="popover"]').popover({container:'body', html:true});

        });
        /*### CHAMADA PARA POPOVER ###*/


    }
}
/*### FUNÇÃO PARA CARREGAMENTO DOS DADOS DO BANCO PARA A PÁGINA ###*/








/*### NOTE: FUNÇÃO PARA POSTAGEM DO FORMULÁRIO DE E-MAIL (ASSOCIE E CONTATO) ###*/
//FormID = nome do formulário
//LoadingArea = nome do elemento onde será carregado os dados
//FormType = tipo de formulário (associe | contato)
function FormSendMail (FormID, LoadingArea, FormType) {

    $(FormID).validate({

            submitHandler: function (form) {

                /*### MOSTRA A DIV DE CARREGANDO ###*/
                var contentArea = $(LoadingArea);
                contentArea.css('opacity', 0.25)

                var loader = $('<div class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                loader.css({top: offset.top, left: offset.left})
                loader.css({width: contentArea.width(), height: contentArea.height()})
                /*### MOSTRA A DIV DE CARREGANDO ###*/

                //CRIA O FORMDATA
                var formData = new FormData(form);

                //ADICIONA O VALOR DO TIPO DE MENSAGEM
                formData.append('Type',FormType);

                $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: 'SendMail.php',
                        data: formData,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        success: function(data, textStatus, jqXHR) {

                            // console.log("success");
                            // console.log(data);

                            //CARREGA A MENSAGEM DE RETORNO NA ÁREA DO FORMULÁRIO
                            $(LoadingArea).html(data.msg)

                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                            contentArea.css('opacity', 1);
                            contentArea.prevAll('.ajax-loading-overlay').remove();
                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                        },
                        error: function (jqXHR, status, error) {

                            // console.log("error");
                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                            contentArea.css('opacity', 1);
                            contentArea.prevAll('.ajax-loading-overlay').remove();
                            /*### ESCONDE A DIV DE CARREGANDO ###*/

                            var error = $('<div class="ajax-loading-overlay"><div class="bg-danger" style="padding:6px;"><h2>Houve um erro ao processar os dados, por favor, recarregue a página.</h2></div></div>').insertBefore(contentArea);
                            var offset = contentArea.offset();
                            error.css({top: offset.top, left: offset.left})
                            error.css({width: contentArea.width(), height: contentArea.height()})
                        }
                    });
            },
            invalidHandler: function (form) {
            }
    });
}
/*### FUNÇÃO PARA POSTAGEM DO FORMULÁRIO DE E-MAIL (ASSOCIE E CONTATO) ###*/









/*### NOTE: FUNÇÃO PARA POSTAGEM DO FORMULÁRIO DE LOGIN DA ÁREA RESTRITA ###*/
//FormID = nome do formulário
//LoadingArea = nome do elemento onde será carregado os dados
//MsgArea = nome do elemento onde será carregada a mensagem de retorno
function FormLogin (FormID, LoadingArea, MsgArea) {


    $(FormID).validate({

            submitHandler: function (form) {

                /*### MOSTRA A DIV DE CARREGANDO ###*/
                var contentArea = $(LoadingArea);
                contentArea.css('opacity', 0.25)

                var loader = $('<div class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                loader.css({top: offset.top, left: offset.left})
                loader.css({width: contentArea.width(), height: contentArea.height()})
                /*### MOSTRA A DIV DE CARREGANDO ###*/

                //LIMPA A MENSAGEM DE RETORNO NA ÁREA DO FORMULÁRIO
                $(MsgArea).html('');

                //CRIA O FORMDATA
                var formData = new FormData(form);

                $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: 'LoginAreaRestrita.php',
                        data: formData,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        success: function(data, textStatus, jqXHR) {

                            // console.log("success");
                            // console.log(data);

                            if(data.login=="OK") {

                                window.location.replace('area-restrita-transparencia.php');
                            }
                            else {

                                //CARREGA A MENSAGEM DE RETORNO NA ÁREA DO FORMULÁRIO
                                $(MsgArea).html(data.msg);

                                /*### ESCONDE A DIV DE CARREGANDO ###*/
                                contentArea.css('opacity', 1);
                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                /*### ESCONDE A DIV DE CARREGANDO ###*/
                            }

                        },
                        error: function (jqXHR, status, error) {

                            // console.log("error");
                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                            contentArea.css('opacity', 1);
                            contentArea.prevAll('.ajax-loading-overlay').remove();
                            /*### ESCONDE A DIV DE CARREGANDO ###*/

                            var error = $('<div class="ajax-loading-overlay"><div class="bg-danger" style="padding:6px;"><h2>Houve um erro ao processar os dados, por favor, recarregue a página.</h2></div></div>').insertBefore(contentArea);
                            var offset = contentArea.offset();
                            error.css({top: offset.top, left: offset.left})
                            error.css({width: contentArea.width(), height: contentArea.height()})
                        }
                    });
            },
            invalidHandler: function (form) {
            }
    });

}
/*### FUNÇÃO PARA POSTAGEM DO FORMULÁRIO DE LOGIN DA ÁREA RESTRITA ###*/














/*### NOTE: FUNÇÃO PARA CHECAGEM DO E-MAIL NO CADASTRO DO USUÁRIO (ASSOCIE) ###*/
//Section = identifica a seção de origem (associar | perfil)
//FormID = nome do formulário
//LoadingArea = nome do elemento para mostrar área de carregando
//MsgArea = nome do elemento onde será carregada a mensagem de retorno
function CheckEmail (Section, FormID, LoadingArea, MsgArea) {


    /*### MOSTRA A DIV DE CARREGANDO ###*/
    var contentArea = $(LoadingArea);
    contentArea.css('opacity', 0.25)

    var loader = $('<div class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
    var offset = contentArea.offset();
    loader.css({top: offset.top, left: offset.left})
    loader.css({width: contentArea.width(), height: contentArea.height()})
    /*### MOSTRA A DIV DE CARREGANDO ###*/

    //LIMPA A MENSAGEM DE RETORNO NA ÁREA DO FORMULÁRIO
    $(MsgArea).html('');

    //LIMPA O CAMPO DE CheckEmail (VALIDAÇÃO)
    $("#CheckEmail").val('');

    $.ajax({
            type: "POST",
            dataType: "json",
            url: 'ReturnData.php',
            data: {
                Module: 'associe',
                Section: Section,
                Action: 'CheckEmail',
                Email: $("#email").val(),
                EmailAntigo: $("#emailAntigo").val()
            },
            success: function(data, textStatus, jqXHR) {

                // console.log("success");
                // console.log(data);

                if(data.CheckEmail=="NOK") {

                    $(MsgArea).html(data.ErrorMsg);
                    $("#email").focus();
                }

                if(data.CheckEmail=="OK") {

                    $("#CheckEmail").val(data.CheckEmail);
                }

                /*### ESCONDE A DIV DE CARREGANDO ###*/
                contentArea.css('opacity', 1);
                contentArea.prevAll('.ajax-loading-overlay').remove();
                /*### ESCONDE A DIV DE CARREGANDO ###*/

            },
            error: function (jqXHR, status, error) {

                // console.log("error");
                /*### ESCONDE A DIV DE CARREGANDO ###*/
                contentArea.css('opacity', 1);
                contentArea.prevAll('.ajax-loading-overlay').remove();
                /*### ESCONDE A DIV DE CARREGANDO ###*/

                var error = $('<div class="ajax-loading-overlay"><div class="bg-danger" style="padding:6px;"><h2>Houve um erro ao processar os dados, por favor, recarregue a página.</h2></div></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                error.css({top: offset.top, left: offset.left})
                error.css({width: contentArea.width(), height: contentArea.height()})
            }
        });

}
/*### FUNÇÃO PARA CHECAGEM DO E-MAIL NO CADASTRO DO USUÁRIO (ASSOCIE) ###*/















/*### NOTE: FUNÇÃO PARA POSTAGEM DO FORMULÁRIO DE ASSOCIAR E CADASTRAR COMO USUÁRIO ###*/
//Section = identifica a seção de origem (associar | perfil)
//FormID = nome do formulário
//LoadingArea = nome do elemento onde será carregado os dados
//MsgArea = nome do elemento onde será carregada a mensagem de retorno
function FormAssocie (Section, FormID, LoadingArea, MsgArea) {

    $(FormID).validate({

            submitHandler: function (form) {

                /*### MOSTRA A DIV DE CARREGANDO ###*/
                var contentArea = $(LoadingArea);
                contentArea.css('opacity', 0.25)

                var loader = $('<div class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                loader.css({top: offset.top, left: offset.left})
                loader.css({width: contentArea.width(), height: contentArea.height()})
                /*### MOSTRA A DIV DE CARREGANDO ###*/

                //CRIA O FORMDATA
                var formData = new FormData(form);

                //ADICIONA O MÓDULO
                formData.append('Module', 'associe');

                //ADICIONA A SECTION
                formData.append('Section', Section);

                //ADICIONA A ACTION
                formData.append('Action', 'associar');

                $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: 'ReturnData.php',
                        data: formData,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        success: function(data, textStatus, jqXHR) {

                            // console.log("success");
                            // console.log(data);

                            if(Section=="perfil") {

                                //CARREGA A MENSAGEM DE RETORNO NA ÁREA DO FORMULÁRIO (QUANDO PERFIL)
                                $(MsgArea).html(data.msg)
                                if(data.emailAntigo) {

                                    $("#form-associacao #emailAntigo").val(data.emailAntigo);
                                }
                            }
                            else {

                                //CARREGA A MENSAGEM DE RETORNO NA ÁREA DO FORMULÁRIO
                                $(LoadingArea).html(data.msg)
                            }

                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                            contentArea.css('opacity', 1);
                            contentArea.prevAll('.ajax-loading-overlay').remove();
                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                        },
                        error: function (jqXHR, status, error) {

                            // console.log("error");
                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                            contentArea.css('opacity', 1);
                            contentArea.prevAll('.ajax-loading-overlay').remove();
                            /*### ESCONDE A DIV DE CARREGANDO ###*/

                            var error = $('<div class="ajax-loading-overlay"><div class="bg-danger" style="padding:6px;"><h2>Houve um erro ao processar os dados, por favor, recarregue a página.</h2></div></div>').insertBefore(contentArea);
                            var offset = contentArea.offset();
                            error.css({top: offset.top, left: offset.left})
                            error.css({width: contentArea.width(), height: contentArea.height()})
                        }
                    });
            },
            invalidHandler: function (form) {
            }
    });
}
/*### FUNÇÃO PARA POSTAGEM DO FORMULÁRIO DE ASSOCIAR E CADASTRAR COMO USUÁRIO ###*/













/*### NOTE: FUNÇÃO QUE GERA OS DADOS DO GOOGLE SEARCH CUSTOM API ###*/
//Page = número da página atual
//LoadingArea = nome do elemento onde será carregado os dados
function LoadDatabaseGoogle(Page, LoadingArea, Keywords) {

    // console.log('LoadDatabaseGoogle');
    // console.log(Page);
    // console.log(LoadingArea);

    var IndexKeywords = Keywords=='' ? 'assae+mt' : Keywords;

    if(LoadingArea) {

        /*### MOSTRA A DIV DE CARREGANDO ###*/
        var contentArea = $(LoadingArea);
        contentArea.css('opacity', 0.25)

        var loader = $('<div class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
        var offset = contentArea.offset();
        loader.css({top: offset.top, left: offset.left})
        loader.css({width: contentArea.width(), height: contentArea.height()})
        /*### MOSTRA A DIV DE CARREGANDO ###*/
    }

    var startIndex = Page>1 ? Math.round((Number(Page)*10)-10) : 1;

    // console.log(startIndex);

    return $.ajax({
        type: "GET",
        dataType: "json",
        url: 'https://www.googleapis.com/customsearch/v1',
        data: {
            "key":"AIzaSyAQ5nl3l85q2IPGqY3_NojaVM_ny2ztcYI",
            "cx":"003393477488779502250:3puadhb8lj0",
            "q":IndexKeywords,
            "sort":"date:DESC",
            "start":startIndex
        },
        success: function(data, textStatus, jqXHR) {

            // console.log('success');
            // console.log(data);
            // console.log(textStatus);
            // console.log(jqXHR);
        },
        error: function (jqXHR, status, error) {

            // console.log('error');
            // console.log(jqXHR);
            // console.log(status);
            // console.log(error);
        }
    });
}
/*### FUNÇÃO QUE GERA OS DADOS DO GOOGLE SEARCH CUSTOM API ###*/



/*### NOTE: FUNÇÃO PARA TRATAMENTO E DEMONSTRAÇÃO DOS DADOS DO GOOGLE SEARCH CUSTOM API ###*/
//GoogleData = objeto de dados gerado pela função LoadDatabaseGoogle
//Page = número da página atual
//LoadingArea = nome do elemento onde será carregado os dados
//StartPageUrl = array do resultado da função GetStartPageUrl
function ListGoogle(GoogleData, Page, LoadingArea, StartPageUrl) {


    GoogleData.success(function(data, textStatus, jqXHR) {

        var HtmlDataContent = "";

        var StartRegister = Page>1 ? Math.round((Number(Page)*10)-10) : 1;
        var LastRegister = Page>1 ? Math.round(Number(Page)*10) : 10;

        var LastRegisterShow = data.searchInformation["totalResults"]>10 ? LastRegister : data.searchInformation["totalResults"];

        // console.log('success');
        // console.log(data);
        // console.log(textStatus);
        // console.log(jqXHR);
        // console.log(data.items);

        /*### DADOS DA QTD DE REGISTROS ###*/
        HtmlDataContent += '<div class="course-list-header box clearfix">\
                                    <div class="summary pull-left"><strong>'+data.searchInformation["totalResults"]+'</strong> registros | exibindo <strong>'+StartRegister+' a '+LastRegisterShow+'</strong></div>\
                                </div><br>';
        /*### DADOS DA QTD DE REGISTROS ###*/


        $.each(data.items, function(key, DataObject ) {

            // console.log('data.items');
            // console.log(key);
            // console.log(DataObject);

            /*### HTML DE CADA NOTÍCIA ###*/
            HtmlDataContent += '<article class="news-item page-row has-divider clearfix row">\
                                    <div class="details col-md-10 col-sm-9 col-xs-8">\
                                        <h3 class="title"><a href="'+DataObject.link+'" target="_blank">'+DataObject.title+'</a></h3>\
                                        '+DataObject.snippet+'\
                                        <br><a class="btn btn-theme read-more" href="'+DataObject.link+'" target="_blank">Ler mais<i class="fa fa-chevron-right"></i></a>\
                                    </div>\
                                </article>';
            /*### HTML DE CADA NOTÍCIA ###*/
        });

        if(data.searchInformation["totalResults"]>10) {

            HtmlDataContent += '<p>&nbsp;</p><a class="btn btn-theme read-more troca-pagina" href="#page'+Math.round(Number(Page)+1)+'" data-page="'+Math.round(Number(Page)+1)+'">Próxima página <i class="fa fa-angle-double-right"></i></a>';
        }


        //CARREGA O CONTEÚDO NA ÁREA DO SITE
        $(LoadingArea).html(HtmlDataContent);


        /*### ESCONDE A DIV DE CARREGANDO ###*/
        var contentArea = $(LoadingArea);
        contentArea.css('opacity', 1);
        contentArea.prevAll('.ajax-loading-overlay').remove();
        /*### ESCONDE A DIV DE CARREGANDO ###*/
    });


    /*### AÇÕES QUANDO CLICA PARA TROCAR DE PÁGINA ###*/
    $(LoadingArea).on('click', '.troca-pagina', function(event) {

        var GoogleData = LoadDatabaseGoogle($(this).data('page'), '#page-load');

        ListGoogle(GoogleData, $(this).data('page'), '#page-load', StartPageUrl);

        window.location.href = StartPageUrl['Url']+'#page'+$(this).data('page');

        event.preventDefault();
    });
    /*### AÇÕES QUANDO CLICA PARA TROCAR DE PÁGINA ###*/


}
/*### FUNÇÃO PARA TRATAMENTO E DEMONSTRAÇÃO DOS DADOS DO GOOGLE SEARCH CUSTOM API ###*/













/*### FUNÇÃO PARA CONTROLAR O UPLOAD DAS IMAGENS ###*/
//Form = nome do formulário do campo de upload
//UploadField = nome do campo de upload
//System = sistema para referência das operações
//maxFileCount = limite de arquivos que podem ser enviados
//allowedFileExtensions = array com as extensões de arquivos permitidos
//maxFileSize = tamanho máximo dos arquivos
//uploadExtraData = objeto ExtraData com informações extras para o upload
function BootstrapFileInput(Form, UploadField, System, maxFileCount, allowedFileExtensions, maxFileSize, uploadExtraData) {

    $('#'+Form+' #'+UploadField).fileinput();
    $('#'+Form+' #'+UploadField).fileinput('destroy');


    $('#'+Form+' #'+UploadField).fileinput({
        language: "pt-BR",
        uploadUrl: System+"/upload.php",
        uploadAsync: true,
        overwriteInitial: true,
        validateInitialCount: true,
        showRemove: false,
        maxFileCount: maxFileCount,
        inputname: UploadField,
        showUpload: false,
        allowedFileExtensions: allowedFileExtensions,
        maxFileSize: maxFileSize,
        autoReplace: true,
        uploadExtraData: uploadExtraData,
        deleteUrl: System+"/upload.php",
        required: true,
    }).on('fileuploaded', function(event, data, previewId, index) {
        // console.log('fileuploaded');
        $('#'+Form+' #arquivo_enviado').val(parseInt(parseInt($('#'+Form+' #arquivo_enviado').val()) + 1));
        $('#'+Form+' #arquivo_filename').val(data.files[0]['name']);
        // console.log(data);

    }).on('filedeleted', function(event, key) {
        // console.log('filedeleted');
        $('#'+Form+' #arquivo_enviado').val(parseInt(parseInt($('#'+Form+' #arquivo_enviado').val()) - 1));

    }).on('filebatchselected', function(event, files) {
        // console.log('filebatchselected');
        $('#'+Form+' #'+UploadField).fileinput('upload');
        // console.log(files);

    }).on('filebatchuploadsuccess', function(event, data) {
        // console.log('filebatchuploadsuccess');
        // console.log(data);

    }).on('filesuccessremove', function(event, id) {
        // console.log('filesuccessremove');
        $('#'+Form+' #arquivo_enviado').val(parseInt(parseInt($('#'+Form+' #arquivo_enviado').val()) - 1));

    }).on('filedeleted', function(event, key, jqXHR, data) {
        // console.log('filedeleted');
        // console.log(data);
    });


}
/*### FUNÇÃO PARA CONTROLAR O UPLOAD DAS IMAGENS ###*/






