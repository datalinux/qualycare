<?
/*###################################################################
|																	|
|	DESCRIÇÃO: Arquivo com as funções PHP específicas para tratar	|
|	datas e conversões de tempo										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guicastro@gmail.com										|
|	Data: 18/07/2014												|
|																	|
###################################################################*/

//funcao que converte data de timestamp para BR
function converte_timestamp_br($timestamp)
	{
		$data_br = strftime("%d/%m/%Y",$timestamp);
		return $data_br;
	}

//funcao que converte data completa de timestamp para BR
function converte_timestamp_br_completa($timestamp)
	{
		global $timezone;

		date_default_timezone_set($timezone);

		$data_br_completa = strftime("%d/%m/%Y - %H:%M",$timestamp);
		return $data_br_completa;
	}

//funcao que converte data completa de timestamp para BR e retorna os valores individuais
function converte_timestamp_br_individual($timestamp)
	{
		//echo "<br>timestamp: ".$timestamp;
		$dia = strftime("%d",$timestamp);
		$mes = strftime("%m",$timestamp);
		$ano = strftime("%Y",$timestamp);
		$hora = strftime("%H",$timestamp);
		$minuto = strftime("%M",$timestamp);
		$segundo = strftime("%S",$timestamp);
		return array($dia,$mes,$ano,$hora,$minuto,$segundo);
	}

//funcao que converte data de timestamp para o formato US
function converte_timestamp_us($timestamp)
	{
		$data = strftime("%Y-%m-%d",$timestamp);
		return $data;
	}

//funcao que converte data de timestamp para o formato US completo
function converte_timestamp_us_completa($timestamp)
	{
		$data = strftime("%Y-%m-%d %H:%M:%S",$timestamp);
		return $data;
	}

//funcao que converte data de US (Y-M-D) para timestamp
function converte_us_timestamp($data)
	{
		$dia = substr($data,8,2);
		$mes = substr($data,5,2);
		$ano = substr($data,0,4);
		//echo $dia."/".$mes."/".$ano;
		$data_timestamp = mktime(23,59,59,$mes,$dia,$ano);
		//echo "<br>data_timestamp: ".$data_timestamp;
		return $data_timestamp;
	}

//funcao que converte data de US (Y-M-D H:M:S) para timestamp
function converte_us_timestamp_completa($data)
	{
		$dia = substr($data,8,2);
		$mes = substr($data,5,2);
		$ano = substr($data,0,4);
		$hora = substr($data,11,2);
		$minuto = substr($data,14,2);
		$segundo = substr($data,17,2);
		if($segundo=="") $segundo = 00;
		//echo $dia."/".$mes."/".$ano." ".$hora.":".$minuto.":".$segundo;
		$data_timestamp = mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
		//echo "<br>data_timestamp: ".$data_timestamp;
		return $data_timestamp;
	}

//funcao que converte data de BR para timestamp
function converte_br_timestamp($dia,$mes,$ano)
	{
		$data_timestamp = mktime(23,59,59,$mes,$dia,$ano);
		//echo "<br>data_timestamp: ".$data_timestamp;
		return $data_timestamp;
	}

//funcao que converte data completa de BR para timestamp
function converte_br_timestamp_completa($dia,$mes,$ano,$hora,$minuto,$segundo)
	{
		$data_timestamp_completa = mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
		return $data_timestamp_completa;
	}

//funcao que converte data completa de timestamp para BR
function converte_timestamp_br_reduzida($timestamp)
	{
		$data_br_reduzida = strftime("%d/%m - %H:%M",$timestamp);
		return $data_br_reduzida;
	}

//funcao que converte a data de BR para US
function monta_data_us($dia,$mes,$ano)
	{
		if(($dia<>"")&&($mes<>"")&&($ano<>"")) $monta_data_us = $ano."-".$mes."-".$dia;
		return $monta_data_us;
	}

//funcao que converte a data completa de BR para US
function monta_data_completa_us($dia,$mes,$ano,$hora,$minuto,$segundo)
	{
		$monta_data_completa_us = $ano."-".$mes."-".$dia." ".$hora.":".$minuto.":".$segundo;
		return $monta_data_completa_us;
	}

//funcao que converte a data de US para BR
function mostra_data_br($data)
	{
		if($data<>"0000-00-00")
			{
				$dia = substr($data, 8, 2);
				$mes = substr($data, 5, 2);
				$ano = substr($data, 0, 4);

				if(($dia<>"")&&($mes<>"")&&($ano<>"")) $mostra_data_br = $dia."/".$mes."/".$ano;
			}

		return $mostra_data_br;
	}

//funcao que converte a data de US2 para BR
function mostra_data_us2_br($data)
	{
		$dia = substr($data, 3, 2);
		$mes = substr($data, 0, 2);
		$ano = substr($data, 6, 2);

		if(($dia<>"")&&($mes<>"")&&($ano<>"")) $mostra_data_br = $dia."/".$mes."/".$ano;

		return $mostra_data_br;
	}

//funcao que converte a data de BR para US
function mostra_data_us($data)
	{
		$dia = substr($data,0,2);
		$mes = substr($data,3,2);
		$ano = substr($data,6,4);

		if(($dia<>"")&&($mes<>"")&&($ano<>"")) $data_us = $ano."-".$mes."-".$dia;

		return $data_us;
	}



//funcao que converte a data completa de US para BR
function mostra_data_completa_br($data)
	{
		if(($data<>"0000-00-00 00:00")&&($data<>"0000-00-00 00:00:00")&&($data<>""))
			{
				$dia = substr($data, 8, 2);
				$mes = substr($data, 5, 2);
				$ano = substr($data, 0, 4);
				$hora = substr($data, 11, 2);
				$minuto = substr($data, 14, 2);
				$mostra_data_completa_br = $dia."/".$mes."/".$ano." - ".$hora."h".$minuto;
			}
		else
			{
				$mostra_data_completa_br = "";
			}
		return $mostra_data_completa_br;
	}



//funcao que converte a data completa de US para BR no formato TIMEPICKER
function mostra_data_completa_br_timepicker($data)
	{
		if(($data<>"0000-00-00 00:00")&&(($data<>"0000-00-00 00:00:00")))
			{
				$dia = substr($data, 8, 2);
				$mes = substr($data, 5, 2);
				$ano = substr($data, 0, 4);
				$hora = substr($data, 11, 2);
				$minuto = substr($data, 14, 2);
				$mostra_data_completa_br = $dia."/".$mes."/".$ano." ".$hora.":".$minuto;
			}
		return $mostra_data_completa_br;
	}



//funcao que converte a data completa de BR para US
function mostra_data_completa_us($data)
	{
		if(($data<>"0000-00-00 00:00")&&(($data<>"0000-00-00 00:00:00")))
			{
				$dia = substr($data, 0, 2);
				$mes = substr($data, 3, 2);
				$ano = substr($data, 6, 4);
				$hora = substr($data, 11, 2);
				$minuto = substr($data, 14, 2);
				$mostra_data_completa_us = $ano."-".$mes."-".$dia." ".$hora.":".$minuto;
			}
		return $mostra_data_completa_us;
	}

//funcao que retorna o numero de meses no intervalo da consulta
function num_meses_intervalo($data_inicial,$data_final)
	{
		//echo "<br>data_inicial: ".$data_inicial;
		//echo "<br>data_final: ".$data_final;
		$dia_inicial = substr($data_inicial, 8, 2);
		//echo "<br>dia_inicial: ".$dia_inicial;
		$dia_final = substr($data_final, 8, 2);
		//echo "<br>dia_final: ".$dia_final;

		$mes_inicial = substr($data_inicial, 5, 2);
		//echo "<br>mes_inicial: ".$mes_inicial;
		$mes_final = substr($data_final, 5, 2);
		//echo "<br>mes_final: ".$mes_final;

		$ano_inicial = substr($data_inicial, 0, 4);
		//echo "<br>ano_inicial: ".$ano_inicial;
		$ano_final = substr($data_final, 0, 4);
		//echo "<br>ano_final: ".$ano_final;

		$data_inicial_mk = mktime(0,0,1,$mes_inicial,$dia_inicial,$ano_inicial);
		$data_final_mk = mktime(23,59,59,$mes_final,$dia_final,$ano_final);
		$num_meses = ($data_final_mk-$data_inicial_mk)/2592000;
		//echo "<br>num_meses: ".$num_meses;
		return array($num_meses, $mes_inicial, $mes_final, $ano_inicial, $ano_final);
	}

//funcao que retorna o intervalo do mes a ser mostrado
function retorna_intervalo_mes_pesquisa($data_inicial,$mes_consulta,$ano_consulta,$data_final)
	{
		//echo "<br>data_inicial: ".$data_inicial;
		//echo "<br>mes_consulta: ".$mes_consulta;
		//echo "<br>ano_consulta: ".$ano_consulta;
		//echo "<br>data_final: ".$data_final;
		$dia_inicial = substr($data_inicial, 8, 2);
		//echo "<br>dia_inicial: ".$dia_inicial;
		$dia_final = substr($data_final, 8, 2);
		//echo "<br>dia_final: ".$dia_final;
		$mes_inicial = substr($data_inicial, 5, 2);
		//echo "<br>mes_inicial: ".$mes_inicial;
		$mes_final = substr($data_final, 5, 2);
		//echo "<br>mes_final: ".$mes_final;
		$ano_inicial = substr($data_inicial, 0, 4);
		//echo "<br>ano_inicial: ".$ano_inicial;
		$ano_final = substr($data_final, 0, 4);
		//echo "<br>ano_final: ".$ano_final;
		if(($mes_consulta==$mes_inicial)&&($ano_consulta==$ano_inicial))
			{
				//echo "<br>mes_consulta = mes_inicial E ano_consulta = ano_inicial";
				$inicio_pesquisa = mktime(0,0,1,$mes_inicial,$dia_inicial,$ano_inicial);
			}
		else
			{
				$inicio_pesquisa = mktime(0,0,1,$mes_consulta,1,$ano_consulta);
			}
		if(($mes_consulta==$mes_final)&&($ano_consulta==$ano_final))
			{
				//echo "<br>mes_consulta = mes_final E ano_consulta = ano_final";
				$fim_pesquisa = mktime(23,59,59,$mes_final,$dia_final,$ano_final);
			}
		else
			{
				$fim_pesquisa = mktime(23,59,59,($mes_consulta+1),0,$ano_consulta);
			}

		//echo "<br>inicio_pesquisa: ".$inicio_pesquisa;
		//echo "<br>fim_pesquisa: ".$fim_pesquisa;
		//echo "<br>";

		return array($inicio_pesquisa, $fim_pesquisa);
	}


//funcao que retorna o nome do mes e o ano usando uma data timestamp
function retorna_mes_ano_timestamp($timestamp,$array_meses)
	{
		$mes = strftime("%m",$timestamp);
		$ano = strftime("%Y",$timestamp);
		$nome_mes = $array_meses[$mes];
		return $nome_mes."/".$ano;
	}

//funcao que retorna o ultimo dia do mes no formato US
function retorna_ultimo_dia_us($mes,$ano)
	{
		//echo $mes."/".$ano;
		$data_timestamp = mktime(00,00,01,($mes+1),00,$ano);
		$data_us = strftime("%Y-%m-%d",$data_timestamp);
		return $data_us;
	}

//funcao que converte data para o formato US na pagina da agenda de eventos
function converte_data_eventos_us($data)
	{
		$dia = substr($data,0,2);
		$mes = substr($data,2,2);
		$ano = substr($data,4,4);
		$data_us = $ano."-".$mes."-".$dia;

		return $data_us;
	}

//funcao que converte data para o formato US na pagina da agenda de eventos
function converte_data_eventos_br($data)
	{
		$dia = substr($data,0,2);
		$mes = substr($data,2,2);
		$ano = substr($data,4,4);
		$data_br = $dia."/".$mes."/".$ano;

		return $data_br;
	}


//funcao que converte a data de US para BR
function mostra_data_br_newsletters($data)
	{
		global $array_meses;

		$dia = substr($data, 8, 2);
		$mes = substr($data, 5, 2);
		$ano = substr($data, 0, 4);
		$mostra_data_br_newsletters = "Cuiab&aacute;, ".$dia." de ".$array_meses[$mes]." de ".$ano;
		return $mostra_data_br_newsletters;
	}


//Funcao que retorna um array com intervalo de datas
//$IntervaloDiasMK = intervalo em dias MK
//retorna array(meses, dias, horas, minutos, legenda)
function FormataIntervaloDatasMK($IntervaloDiasMK)
	{
		global $mes_mk, $dia_mk, $hora_mk, $minuto_mk;

		//echo "<br><br>IntervaloDiasMK: ".$IntervaloDiasMK;

		//echo "<br>mes_mk: ".$mes_mk;
		//echo "<br>dia_mk: ".$dia_mk;
		//echo "<br>hora_mk: ".$hora_mk;
		//echo "<br>minuto_mk: ".$minuto_mk;

		$qtd_meses = round($IntervaloDiasMK/$mes_mk,2);
		$qtd_dias = round($IntervaloDiasMK/$dia_mk,2);
		$qtd_horas = round($IntervaloDiasMK/$hora_mk,2);
		$qtd_minutos = round($IntervaloDiasMK/$minuto_mk,2);
		//echo "<br><br>qtd_meses: ".$qtd_meses;
		//echo "<br>qtd_dias: ".$qtd_dias;
		//echo "<br>qtd_horas: ".$qtd_horas;
		//echo "<br>qtd_minutos: ".$qtd_minutos;

		//SE TEMPO FOR MAIOR QUE 1 MÊS, RETORNA XX MESES E XX DIAS
		if($IntervaloDiasMK>$mes_mk)
		  {
			  $legenda = $qtd_meses % $mes_mk." mes(es)";
			  if($qtd_dias>1) $legenda .= " e ".($qtd_dias - (($qtd_meses % $mes_mk)*30)) % $dia_mk." dia(s)";
		  }

		//SE TEMPO FOR MAIOR QUE 1 DIA, RETORNA XX DIAS E XX HORAS
		elseif($IntervaloDiasMK>$dia_mk)
		  {
			  $legenda = $qtd_dias % $dia_mk." dia(s)";
			  if($qtd_horas>1) $legenda .= " e ".($qtd_horas - (($qtd_dias % $dia_mk)*24)) % $hora_mk." hora(s)";
		  }

		//SE TEMPO FOR MAIOR QUE 1 HORA, RETORNA XX HORAS E XX MINUTOS
		elseif($IntervaloDiasMK>$hora_mk)
		  {
			  $legenda = $qtd_horas % $hora_mk." hora(s)";
			  if($qtd_minutos>1) $legenda .= " e ".($qtd_minutos - (($qtd_horas % $hora_mk)*60)) % $minuto_mk." minuto(s)";
		  }

		//SE TEMPO FOR MENOR QUE 1 HORA, RETORN XX MINUTOS
		else
		  {
			  $legenda = $qtd_minutos % $minuto_mk." minutos";
		  }

		//echo "<br>legenda: ".$legenda;

		return array($qtd_meses, $qtd_dias, $qtd_horas, $qtd_minutos, $legenda);

	}


//funcao que formata a data para aparecer na capa
//$data = data no formato US - YYYY-MM-DD HH:MM
function mostra_data_listagens($data)
	{
		$dia = substr($data, 8, 2);
		$mes = substr($data, 5, 2);
		$ano = substr($data, 0, 4);
		$hora = substr($data, 11, 2);
		$minuto = substr($data, 14, 2);
		$mostra_data_capa = $dia."/".$mes."&nbsp;".$hora."h".$minuto;
		return $mostra_data_capa;
	}

?>