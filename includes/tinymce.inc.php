    <script type="text/javascript" src="../lib/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        var IniciaTinyMCE = tinymce.init({
            //selector: "#noticia_materia",
            mode : "specific_textareas",
            editor_selector : "mceEditor",
            menubar: true,
            language: "pt_BR",
            theme: "modern",
            height:300,
            convert_urls: false,
            relative_urls: false,
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking table contextmenu",
                "textcolor colorpicker textpattern"
            ],
            paste_as_text: true,
            // paste_word_valid_elements: "b,strong,i,em,h1,h2,h3,u,table",
            // paste_word_valid_elements: "*[*]",
            // paste_enable_default_filters: false,
            // paste_retain_style_properties: "all",
            // paste_strip_class_attributes: "none",
            // valid_elements: "*[*]",
            // extended_valid_elements: "*[*]",
            image_list: "<?php echo $GLOBALS['http_root'].'/admin/banco-fotos/lista-fotos.php'; ?>",
            toolbar1: "paste | undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            toolbar2: "searchreplace | link image media | fullscreen preview code",
            image_advtab: true,
            external_filemanager_path:"../lib/responsive_filemanager/filemanager/",
            filemanager_title:"Enviar imagem" ,
            external_plugins: {
                "filemanager" : "../../../responsive_filemanager/filemanager/plugin.min.js"
            }
        });
    </script>
