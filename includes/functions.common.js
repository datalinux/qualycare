// JavaScript Document

	jQuery(document).ready(function () {
		(function ($) {

	    $(".fancybox").fancybox({
	        padding : 0
	    });

	    $(".fancyboxGaleria").fancybox({
	        'width': '90%',
	        'height': '98%',
	        'autoSize' : false,
	        padding : 0
	    });


		} )(jQuery);
	} );