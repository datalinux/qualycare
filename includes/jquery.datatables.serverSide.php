<?
/*###################################################################
|																	|
|	Arquivo que faz as consultas SQL, busca e filtragem para o		|
|	grid dataTable													|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guicastro@gmail.com										|
|	Data: 20/07/2014												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES
	$include_functions_js = "nao";
	include("configure.inc.php");

	//INCLUSÃO DO ARUQIVO DE PERMISSÕES
	$include_sistema = "nao";
	include($pasta_includes."/auth.inc.php");

	//print_r($_GET);
	//print_r($_POST);


	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include($pasta_admin."/".$_GET["sistema"]."/sistema.cfg.php");

	//INCLUSÃO DO ARQUIVO ADICIONAL DE CONFIGURAÇÕES DO MÓDULO
	$include_functions_sistema_esp = "nao";
	if($_GET["sistema_adicional"]<>"") include($pasta_admin."/".$_GET["sistema"]."/sistema".$_GET["sistema_adicional"].".cfg.php");





	/*### OBTER INFORMCAOES DO GET QUANTO AS ORDENACAO DOS REGISTROS NO GRID E CONSTRUIR O COMPLEMENTO DA QUERY ####*/
	$ordenacao_grid_ss = $_GET["order"];
	$sql_ordenacao = "ORDER BY ";
	$total_ordenacao_grid_ss = count($ordenacao_grid_ss);
	foreach($ordenacao_grid_ss as $order_sequencia => $order_info)
		{
			//echo "\n".$order_sequencia;
			//echo "\n     ".$order_info["column"]." => ".$order_info["dir"];
			//echo "\n     ".$array_key_colunas_grid[$order_info["column"]]." => ".$order_info["dir"];
			$num_ordenacao_grid_ss++;
			$sql_ordenacao .= $sistema_prefixo_campos_grid.$array_key_colunas_grid[$order_info["column"]];
			$sql_ordenacao .= " ".$order_info["dir"];
			if($num_ordenacao_grid_ss<$total_ordenacao_grid_ss) $sql_ordenacao .= ", ";
		}
	/*### OBTER INFORMCAOES DO GET QUANTO AS ORDENACAO DOS REGISTROS NO GRID E CONSTRUIR O COMPLEMENTO DA QUERY ####*/




	//OBTER INFORMCAOES DO GET QUANTO A PAGINACAO DOS REGISTROS NO GRID E CONSTRÓI A LINHA
	if(isset($_GET["start"]) && $_GET["length"] != -1) $limit = "LIMIT ".$_GET["start"].", ".$_GET["length"];





	/*### OBTER INFORMCAOES DO GET QUANTO A PALAVRA-CHAVE DA BUSCA ESPECIFICA DE UMA COLUNA E CONSTRUIR O COMPLEMENTO DA QUERY ####*/
	$array_colunas_grid_search = $_GET["columns"];
	$sql_palavra_chave_coluna = "( ";
	foreach($array_colunas_grid_search as $num_coluna => $array_config_coluna)
		{
			//echo "\n\ncoluna[".$num_coluna."] => ".$array_key_colunas_grid[$num_coluna];
			//echo "\nvalue[".$num_coluna."] => ".$array_config_coluna["search"]["value"];
			//echo "\nregex[".$num_coluna."] => ".$array_config_coluna["search"]["regex"];
			if((($array_config_coluna["search"]["regex"]=="")||($array_config_coluna["search"]["regex"]=="false"))&&($array_config_coluna["search"]["value"]<>""))
				{
					$num_array_colunas_grid_search++;
					if($num_array_colunas_grid_search>1) $sql_palavra_chave_coluna .= " AND ";
					$sql_palavra_chave_coluna .= $sistema_prefixo_campos_grid.$array_key_colunas_grid[$num_coluna]." LIKE '%".addslashes($array_config_coluna["search"]["value"])."%' ";
				}
		}
	$sql_palavra_chave_coluna .=  " ) AND ";
	if($num_array_colunas_grid_search<=0) unset($sql_palavra_chave_coluna);
	//echo "\n".$sql_palavra_chave_coluna;
	/*### OBTER INFORMCAOES DO GET QUANTO A PALAVRA-CHAVE DA BUSCA ESPECIFICA DE UMA COLUNA E CONSTRUIR O COMPLEMENTO DA QUERY ####*/




	/*### OBTER INFORMCAOES DO GET QUANTO A PALAVRA-CHAVE DA BUSCA GERAL E CONSTRUIR O COMPLEMENTO DA QUERY ####*/
	$palavra_chave = $_GET["search"]["value"];
	if($palavra_chave<>"")
		{
			$sql_palavra_chave = "( ";
			foreach($array_colunas_grid as $key_coluna => $config_coluna)
				{
					$num_coluna_busca++;
					$separa_palavra_chave[$key_coluna] = explode(" ",$palavra_chave)	;
					$sql_palavra_chave .= "( ";
					foreach($separa_palavra_chave[$key_coluna] as $key => $palavra_chave_sep)
						{
							//echo "\n".$key." => ".$palavra_chave_sep;
							$num_palavra_chave_sep[$key_coluna]++;
							$sql_palavra_chave .= $sistema_prefixo_campos_grid.$key_coluna." LIKE '%".addslashes($palavra_chave_sep)."%'";
							if($num_palavra_chave_sep[$key_coluna]<count($separa_palavra_chave[$key_coluna])) $sql_palavra_chave .= " AND ";
						}
					$sql_palavra_chave .= " )";
					if($num_coluna_busca<$total_colunas_grid) $sql_palavra_chave .= " OR ";
				}
			$sql_palavra_chave .= " ) AND ";
		}
	//echo "\n\nsql_palavra_chave: ".$sql_palavra_chave;
	/*### OBTER INFORMCAOES DO GET QUANTO A PALAVRA-CHAVE DA BUSCA GERAL E CONSTRUIR O COMPLEMENTO DA QUERY ####*/






	/*### OBTER INFORMCAOES DO GET QUANTO AO USO DE DE FILTRO AVANÇADO E CONSTRUIR COMPLEMENTO DA QUERY ####*/
	$array_colunas_grid_search_fl_av = $_GET["columns"];
	//print_r($array_colunas_grid_search_fl_av);
	$sql_palavra_chave_coluna_fl_av = "( ";
	foreach($array_colunas_grid_search_fl_av as $num_coluna => $array_config_coluna_fl_av)
		{
			if(($array_config_coluna_fl_av["search"]["value"]<>"")&&($array_config_coluna_fl_av["search"]["regex"]<>"")&&($array_config_coluna_fl_av["search"]["regex"]<>"false"))
				{
					//echo "\n\nnum_array_colunas_grid_search_fl_av: ".$num_array_colunas_grid_search_fl_av;
					//echo "\n[".$num_coluna."] => ".$array_key_colunas_grid[$num_coluna];
					//echo "\n[".$num_coluna."] => ".$array_config_coluna_fl_av["search"]["value"];
					//echo "\n[".$num_coluna."] => ".$array_config_coluna_fl_av["search"]["regex"];
					$num_array_colunas_grid_search_fl_av++;
					if($num_array_colunas_grid_search_fl_av>1) $sql_palavra_chave_coluna_fl_av .= " AND ";


					/*### FILTRO daterangepicker COM BASE NO INTERVALO DE DATAS DE INÍCIO E FIM NO CAMPO FILTRADO ####*/
					if($array_config_coluna_fl_av["search"]["regex"]=="daterangepicker")
						{
							$separa_data = explode(" - ",$array_config_coluna_fl_av["search"]["value"]);
							$sql_palavra_chave_coluna_fl_av .= $sistema_prefixo_campos_grid.$array_key_colunas_grid[$num_coluna]." BETWEEN '".mostra_data_us($separa_data[0])." 00:00:00' AND '".mostra_data_us($separa_data[1])." 23:59:59' ";
						}
					/*### FILTRO daterangepicker COM BASE NO INTERVALO DE DATAS DE INÍCIO E FIM NO CAMPO FILTRADO ####*/


					/*### FILTRO daterangepicker COM BASE NO INTERVALO DE DATAS COM DOIS CAMPOS DIFERENTES DE INÍCIO E FIM ####*/
					if((substr_count($array_config_coluna_fl_av["search"]["regex"],"daterangepicker")>0)&&(substr_count($array_config_coluna_fl_av["search"]["regex"],$separador_string)>0))
						{
							$separa_campos = explode($separador_string,$array_config_coluna_fl_av["search"]["regex"]);
							$separa_data = explode($separador_string,$array_config_coluna_fl_av["search"]["value"]);
							$sql_palavra_chave_coluna_fl_av .= $sistema_prefixo_campos_grid.$separa_campos[1]." >= '".mostra_data_us($separa_data[0])." 00:00:00' AND ".$sistema_prefixo_campos_grid.$separa_campos[2]." <= '".mostra_data_us($separa_data[1])." 23:59:59' ";
						}
					/*### FILTRO daterangepicker COM BASE NO INTERVALO DE DATAS COM DOIS CAMPOS DIFERENTES DE INÍCIO E FIM ####*/
				}
		}
	$sql_palavra_chave_coluna_fl_av .=  " ) AND ";
	//echo "\nsql_palavra_chave_coluna_fl_av: ".$sql_palavra_chave_coluna_fl_av;
	if($num_array_colunas_grid_search_fl_av<=0) unset($sql_palavra_chave_coluna_fl_av);
	/*### OBTER INFORMCAOES DO GET QUANTO AO USO DE DE FILTRO AVANÇADO E CONSTRUIR COMPLEMENTO DA QUERY ####*/






	/*### CONSTRUIR A QUERY DOS CAMPOS E VALORES BASEADOS NAS PERMISSÕES DO USUÁRIO ####*/
	if((is_array($array_campos_filtragem_permissoes))&&($_GET["filial"]<>"geral"))
		{
			foreach($array_campos_filtragem_permissoes as $campo_nome => $array_campo_valor)
				{
					// echo "\nfilial: ".$_GET["filial"];
					// echo "\nfilial_matriz: ".$filial_matriz;
					// echo "\n".$campo_nome." => ".$array_campo_valor;
					if(count($array_campo_valor)>0)
						{
							$array_sql_campos_filtragem_permissoes[$campo_nome] = array();
							foreach($array_campo_valor as $campo_valor_key => $campo_valor_value)
								{
									if($campo_valor_value<>"")
										{
											// echo "\n      ".$campo_valor_key." => ".$campo_valor_value;
											if($_GET["filial"]<>$filial_matriz)
												{
													// echo "\n      filial <> matriz";
													// echo "\n      substr: ".substr_count($campo_valor_value, "filial".$_GET["filial"]);
													// echo "\n      campo_nome: ".$campo_nome;
													if(substr_count($campo_valor_value, "filial".$_GET["filial"])>0) array_push($array_sql_campos_filtragem_permissoes[$campo_nome], $campo_nome." LIKE '%".$separador_string.$campo_valor_value.$separador_string."%' ");
												}
											elseif($_GET["filial"]==$filial_matriz)
												{
													// echo "\n      filial == matriz";
													array_push($array_sql_campos_filtragem_permissoes[$campo_nome], $campo_nome." LIKE '%".$separador_string.$campo_valor_value.$separador_string."%' ");
												}
										}
								}
						}
				}

			// echo "\narray_sql_campos_filtragem_permissoes: ";
			// print_r($array_sql_campos_filtragem_permissoes);

			if(count($array_sql_campos_filtragem_permissoes)>0)
				{
					$sql_campos_filtragem_permissoes = "( ";
					$total_array_sql_campos_filtragem_permissoes = count($array_sql_campos_filtragem_permissoes);
					foreach($array_sql_campos_filtragem_permissoes as $sql_campo => $arary_sql_query)
						{
							$num_sql_campo++;
							$sql_campos_filtragem_permissoes .= "( ";
							$total_arary_sql_query[$sql_campo] = count($arary_sql_query);
							foreach($arary_sql_query as $sql_query_key => $sql_query_value)
								{
									$num[$sql_campo]++;
									//echo "\n            ".$sql_query_key." => ".$sql_query_value;
									$sql_campos_filtragem_permissoes .= $sql_query_value;
									if($num[$sql_campo]<$total_arary_sql_query[$sql_campo]) $sql_campos_filtragem_permissoes .= " OR ";
								}
							if($num_sql_campo<$total_array_sql_campos_filtragem_permissoes) $sql_campos_filtragem_permissoes .= ") AND ";
						}
					$sql_campos_filtragem_permissoes .= " ) ) AND ";
				}
		}
	//echo "\nsql_campos_filtragem_permissoes: ".$sql_campos_filtragem_permissoes;
	//echo "\n\n";
	/*### CONSTRUIR A QUERY DOS CAMPOS E VALORES BASEADOS NAS PERMISSÕES DO USUÁRIO ####*/







	/*### CONSTRUIR A LINHA PARA CONDIÇÃO DE EXCLUÍDOS, QUANDO EXISTIR ####*/
	if($sistema_campo_excluido<>"") $linha_sql_excluido = $sistema_campo_excluido." <> 'sim'";
	else $linha_sql_excluido = $sistema_chave_primaria_grid." <> ''";
	/*### CONSTRUIR A LINHA PARA CONDIÇÃO DE EXCLUÍDOS, QUANDO EXISTIR ####*/







	/*### CONSTRUIR E EXECUTAR A QUERY PARA CONTAGEM TOTAL DE REGISTROS DA PAGINA ####*/
	$sql_todos_registros = "SELECT * FROM
										".$sistema_nome_da_tabela_grid."
									WHERE
										".$sistema_sql_adicional.
										$sql_campos_filtragem_permissoes.
										$linha_sql_excluido."
									".$sql_ordenacao;
	// echo "\n\nsql_todos_registros: ".$sql_todos_registros;
	$exe_todos_registros = mysql_query($sql_todos_registros, $con) or die("Erro do MySQL[exe_todos_registros]: ".mysql_error());
	$num_rows_todos_registros = mysql_num_rows($exe_todos_registros);
	/*### CONSTRUIR E EXECUTAR A QUERY PARA CONTAGEM TOTAL DE REGISTROS DA PAGINA ####*/






	/*### CONSTRUIR E EXECUTAR A QUERY PARA CONTAGEM TOTAL DE REGISTROS DE UM FILTRO ####*/
	$sql_registros_filtro = "SELECT * FROM
									".$sistema_nome_da_tabela_grid."
								WHERE
									".$sistema_sql_adicional.
									$sql_campos_filtragem_permissoes.
									$sql_palavra_chave.
									$sql_palavra_chave_coluna.
									$sql_palavra_chave_coluna_fl_av.
									$linha_sql_excluido."
									".$sql_ordenacao;
	//echo "\n\nsql_registros_filtro: ".$sql_registros_filtro;
	$exe_registros_filtro = mysql_query($sql_registros_filtro, $con) or die("Erro do MySQL[exe_registros_filtro]: ".mysql_error());
	$num_rows_registros_filtro = mysql_num_rows($exe_registros_filtro);
	/*### CONSTRUIR E EXECUTAR A QUERY PARA CONTAGEM TOTAL DE REGISTROS DE UM FILTRO ####*/








	/*### CONSTRUIR E EXECUTAR A QUERY PARA EXIBIÇÃO DOS REGISTROS SOLICITADOS PELO GRID ####*/
	$sql_registros = "SELECT * FROM
									".$sistema_nome_da_tabela_grid."
								WHERE
									".$sistema_sql_adicional.
									$sql_campos_filtragem_permissoes.
									$sql_palavra_chave.
									$sql_palavra_chave_coluna.
									$sql_palavra_chave_coluna_fl_av.
									$linha_sql_excluido."
								".$sql_ordenacao."
								".$limit;

	// echo "\n\nsql_registros: ".$sql_registros;
	if($_GET['carregar']<>"nao")
		{
			$exe_registros = mysql_query($sql_registros, $con) or die("Erro do MySQL[exe_registros]: ".mysql_error());
			$num_rows_registros = mysql_num_rows($exe_registros);


			while($ver_registros = mysql_fetch_array($exe_registros))
				{
					if($num_registro=="") $num_registro = 0;

					$row[$num_registro]["DT_RowId"] = $ver_registros[$sistema_chave_primaria_grid];

					foreach($array_colunas_grid as $key_coluna => $config_coluna)
						{
							//echo "\n".$key_coluna." => ".$config_coluna;
							//echo "\n     ".$config_coluna["formatacao"];
							if(is_array($config_coluna["formatacao"]))
								{
									$row[$num_registro][$key_coluna] = $config_coluna["formatacao"]["formatacao"]($ver_registros[$sistema_prefixo_campos_grid.$key_coluna]);
								}
							else
								{
									$row[$num_registro][$key_coluna] = $ver_registros[$sistema_prefixo_campos_grid.$key_coluna];
								}
						}


					$num_registro++;
				}
		}
	/*### CONSTRUIR E EXECUTAR A QUERY PARA EXIBIÇÃO DOS REGISTROS SOLICITADOS PELO GRID ####*/


	//SE NÃO HOUVE REGISTROS, RETORNA ZERO
	if($num_rows_registros==0) $row[$num_registro] = 0;




	//unset($row);
	/*$row[0] = array("Tiger","Nixon","System Architect","Edinburgh","25th Apr 11","$320,800");
	$row[1] = array("Garrett","Winters","Accountant","Tokyo","25th Jul 11","$170,750");
	$row[2] = array("Ashton","Cox","Junior Technical Author","San Francisco","12th Jan 09","$86,000");
	$row[3] = array("Garrett","Winters","Accountant","Tokyo","25th Jul 11","$170,750");
	$row[4] = array("Ashton","Cox","Junior Technical Author","San Francisco","12th Jan 09","$86,000");
	$row[5] = array("Tiger","Nixon","System Architect","Edinburgh","25th Apr 11","$320,800");
	$row[6] = array("Garrett","Winters","Accountant","Tokyo","25th Jul 11","$170,750");*/

	//CONDIÇÃO PARA VARIÁVEL DE TOTALIZADO, SE COM OU SEM FILTRO
	if($palavra_chave<>"") $num_rows_filtro = $num_rows_registros;
	else $num_rows_filtro = $num_rows_todos_registros;




	/*### CONSTRÓI O ARRAY PARA RETORNAR AO GRID ####*/
	$result = array("draw"            => intval($_GET["draw"]),
					"recordsTotal"    => intval($num_rows_todos_registros),
					"recordsFiltered" => intval($num_rows_registros_filtro),
					"sql_registros" => $sql_registros,
					"data"            => $row);
	/*### CONSTRÓI O ARRAY PARA RETORNAR AO GRID ####*/



	//echo print_r($result);

	//echo "\n\nprint_r(_GET)\n";
	//echo print_r($_GET);


	//echo "\n\nprint_r(array_colunas_grid)\n";
	//echo print_r($array_colunas_grid);

	//sleep(3);

	//echo "\n\njson_encode(result)\n";
	//print_r($result);


	//RETORNA O ARRAY DO GRID CODIFICANDO EM JSON
	echo json_encode($result);
?>





