<?
/*###################################################################
|																	|
|	DESCRIÇÃO: Arquivo com as funções PHP comuns a todas as seções 	|
|	do site															|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guicastro@gmail.com										|
|	Data: 18/07/2014												|
|																	|
###################################################################*/

//funcao anti-injection para comandos SQL
//$sql_in = comando SQL para ser protegido
function anti_injection($sql_in)
	{
  		$sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/ie","",$sql_in);
  		$sql = trim($sql);
		$sql = strip_tags($sql);
		$sql_out = addslashes($sql);
		return $sql_out;
	}

//funcao que monta o conteudo do campo das filiais (no cadastro de usuarios)
//$filiais = conteudo do campo filiais
function monta_array_filiais($filiais)
	{
		global $separador_string;

		//echo "<br>casas: ".$casas;

		$array_filiais = array();
		$separa_filiais = explode($separador_string,$filiais);
		foreach($separa_filiais as $filial)
			{
				if($filial<>"") array_push($array_filiais,$filial);
			}

		//echo "<br>array_filiais: ";
		//print_r($array_filiais);

		return $array_filiais;
	}

//funcao que corta a frase sem cortar a palavra ao meio
//$string = texto para ser cortado
//$len = tamanho maximo da frase (ponto de corte)
function corta_palavra($string, $len)
	{
		$i = $len;
		while ($i < strlen($string))
			{
				if ($string[$i] == " ")
					{
						$string = substr($string, 0, $i)."...";
						return $string;
					}
				$i++;
			}
		return $string;
	}


//funcao que exibe somente o primeiro e ultimo nome de uma string
//$nome = string com o nome completo
//$opcao = opção de retorno (primeiro -> somente primeiro nome / ultimo -> somente último nome / em branco -> mostra primeiro e último)
function mostra_primeiro_ultimo_nome($nome,$opcao)
	{
		$separa_nomes = explode(" ",$nome);
		$num_nomes = count($separa_nomes);

		if($opcao=="primeiro") return $separa_nomes[0];
		elseif($opcao=="ultimo") return $separa_nomes[$num_nomes-1];
		else return $separa_nomes[0]." ".$separa_nomes[$num_nomes-1];
	}


//funcao que adiciona/retira os tracos, pontos e barras no cpf/cnpj
//$numero = numero do CPF/CNPJ sem pontos ou tracos
//$operacao = define se a operacao sera adicionar ou retirar os caracteres (adiciona/retira);
function formata_cpf_cnpj($numero,$operacao)
	{
		if($numero<>"")
			{
				$numero = str_replace(".","",$numero);
				$numero = str_replace("/","",$numero);
				$numero = str_replace("-","",$numero);
				if($operacao=="adiciona")
					{
						$tamanho_campo = strlen($numero);
						if($tamanho_campo==11)
							{
								$resultado = substr($numero,0,3);
								$resultado .= ".".substr($numero,3,3);
								$resultado .= ".".substr($numero,6,3);
								$resultado .= "-".substr($numero,9,2);
							}
						else
							{
								$resultado = substr($numero,0,2);
								$resultado .= ".".substr($numero,2,3);
								$resultado .= ".".substr($numero,5,3);
								$resultado .= "/".substr($numero,8,4);
								$resultado .= "-".substr($numero,12,2);
							}
					}
				elseif($operacao=="retira")
					{
						$resultado = $numero;
					}
			}

		return $resultado;
	}



//funcao que adiciona/retira o traco e parenteses no telefone
//$numero = numero do telefone sem pontos ou tracos formato DDTTTTTTTTT (DD) é o DDD sem o ZERO
//$operacao = define se a operacao sera adicionar ou retirar os caracteres (adiciona/retira);
function formata_telefone($numero,$operacao)
	{
		if(($numero<>"")&&($numero<>" "))
			{
				$numero = str_replace("(","",$numero);
				$numero = str_replace(")","",$numero);
				$numero = str_replace(" ","",$numero);
				$numero = str_replace("-","",$numero);
				if($operacao=="adiciona")
					{
						$resultado = "(";
						$resultado .= substr($numero,0,2);
						$resultado .= ")";
						$resultado .= " ";
						if(strlen($numero==11))
							{
								$resultado .= substr($numero,2,5);
								$resultado .= "-".substr($numero,7,4);
							}
						else
							{
								$resultado .= substr($numero,2,4);
								$resultado .= "-".substr($numero,6,4);
							}
					}
				elseif($operacao=="retira")
					{
						$resultado = $numero;
					}
			}

		return $resultado;
	}


//funcao que retira do nome os caracteres acentuados
//$string = texto em que as caracteres serao substituidos
function retira_acentos_arquivos($string) {

	$letras = array(' ' => '_',
					':' => '',
					'(' => '',
					')' => '',
					'/' => '',
					'$' => '',
					'&' => '',
					'%' => '',
					'#' => '',
					'|' => '',
					'º' => '',
					'ª' => '',
					'ç' => 'c',
					'Ç' => 'C',
					'á' => 'a',
					'Á' => 'A',
					'ó' => 'o',
					'Ó' => 'O',
					'é' => 'e',
					'É' => 'E',
					'í' => 'i',
					'Í' => 'I',
					'ú' => 'u',
					'Ú' => 'U',
					'ã' => 'a',
					'Ã' => 'A',
					'õ' => 'o',
					'Õ' => 'O',
					'ü' => 'u',
					'Ü' => 'U',
					'â' => 'a',
					'Â' => 'A',
					'ê' => 'e',
					'Ê' => 'E',
					'ô' => 'o',
					'Ô' => 'O',
					'à' => 'a',
					'À' => 'A',
					'ç' => 'c',
					'Ç' => 'C');
	foreach ($letras as $original => $mudanca)
		{
			// echo "<br>tem $original decode: ".substr_count($string, utf8_decode($original));
			// echo "<br>tem $original encode:".substr_count($string, utf8_encode($original));
			$string = str_replace($original,$mudanca,$string);
		}
	return $string;
}



//funcao que retira do nome os caracteres acentuados
//$string = texto em que as caracteres serao substituidos
function retira_acentos($string) {

	$letras = array('$' => '',
					':' => '',
					'&' => '',
					'%' => '',
					'#' => '',
					'|' => '',
					'ç' => 'c',
					'Ç' => 'C',
					'á' => 'a',
					'Á' => 'A',
					'ó' => 'o',
					'Ó' => 'O',
					'é' => 'e',
					'É' => 'E',
					'í' => 'i',
					'Í' => 'I',
					'ú' => 'u',
					'Ú' => 'U',
					'ã' => 'a',
					'Ã' => 'A',
					'õ' => 'o',
					'Õ' => 'O',
					'ü' => 'u',
					'Ü' => 'U',
					'â' => 'a',
					'Â' => 'A',
					'ê' => 'e',
					'Ê' => 'E',
					'ô' => 'o',
					'Ô' => 'O',
					'à' => 'a',
					'À' => 'A',
					'ç' => 'c',
					'Ç' => 'C');
	foreach ($letras as $original => $mudanca)
		{
			$string = str_replace($original,$mudanca,$string);
		}
	return $string;
}


//funcao que gera o SQL para filtragem de informações baseado na permissão do usuário
//$campos_bd = nome dos campos no Banco de Dados que serão filtrados (múltiplos campos separados por $)
//$valores_bd = valores dos campos no Banco de Dados que serão filtrados (múltiplos campos separados por $)
//$posicao_op = indicar se a posição do operador AND será antes ou depois do SQL (antes / depois / vazio-sem operador)
function filtra_permissao($campos_bd,$valores_bd,$posicao_op)
	{
		global $separador_string;

		if(substr_count($campos_bd,'$')>0)
			{
				$separa_campos = explode("$",$campos_bd);
				$separa_valores_inicial = explode("$",$valores_bd);

				foreach($separa_campos as $key => $nome_campo)
					{
						if($nome_campo<>"")
							{
								//echo "<br>nome_campo: ".$nome_campo;
								//echo "<br>valor_campo: ".$separa_valores[$key];
								if(substr_count($separa_valores_inicial[$key],$separador_string)>0)
									{
										$separa_valores_final[$key] = explode($separador_string,$separa_valores_inicial[$key]);
										foreach($separa_valores_final[$key] as $key2 => $valor_campo)
											{
												if($valor_campo<>"") $sql_filtra2 .= $nome_campo." LIKE '%".$valor_campo."%' AND ";
											}
									}
								else
									{
										$sql_filtra .= $nome_campo." LIKE '%".$separa_valores_inicial[$key]."%' AND ";
									}
							}
					}
			}
		else
			{
				if(substr_count($valores_bd,$separador_string)>0)
					{
						$separa_valores_final = explode($separador_string,$valores_bd);
						foreach($separa_valores_final as $key => $valor_campo)
							{
								if($valor_campo<>"") $sql_filtra .= $campos_bd." LIKE '%".$valor_campo."%' AND ";
							}
					}
				else
					{
						$sql_filtra .= $campos_bd." LIKE '%".$valores_bd."%' AND ";
					}
			}

		if($sql_filtra<>"") $sql_final .= "(".substr($sql_filtra,0,-4).")";
		if($sql_filtra2<>"") $sql_final .= " AND (".substr($sql_filtra2,0,-4).")";

		if($posicao_op=="antes") $sql_final = " AND (".$sql_final.")";
		elseif($posicao_op=="depois") $sql_final = "(".$sql_final.") AND ";

		 return $sql_final;
	}

//funcao que retorna a extensao do arquivo
//$nome_do_arquivo = nome do arquivo completo
function extensao_arquivo($nome_do_arquivo)
	{
		$tamanho_string = strlen($nome_do_arquivo);
		//echo "<br>tamanho_string: ".$tamanho_string;
		$extensao = substr($nome_do_arquivo,$tamanho_string-3,$tamanho_string);
		//echo "<br>extensao: ".$extensao;
		return strtolower($extensao);
	}


//função que exibe o nome da Brach, HASH e Data do úlitmo commit do GIT
function branch_version_br()
	{
		global $pasta_root;

		$branchs_tags = `git log -1 --pretty=format:"%D"`;
		$release = explode(",",explode("tag: ",$branchs_tags[1]));
		$hash = `git log -1 --pretty=format:"%h"`;
		$author_name = `git log -1 --pretty=format:"%an"`;
		$author_email = `git log -1 --pretty=format:"%ae"`;
		$committer_date = `git log -1 --date=short --pretty=format:"%cd"`;
		$commit_subject = `git log -1 --date=short --pretty=format:"%s"`;

		return $release[0]." - ".$hash." - <a href='mailto:".$author_email."'>".$author_name."</a> - ".mostra_data_br($committer_date);
	}

//função que cria as versões de fotos em alta resolução, grande e miniaturas
//foto_pid = pid da foto
//foto_id = ID da foto no banco de dados
//foto_extensao = extensão do arquivo da foto (jpg | png | etc)
function criar_miniaturas($foto_pid, $foto_id, $foto_extensao)
	{
		global $_SESSION;

		//GUARDA AS INFORMAÇÕES E DIMENSÕES DA IMAGEM TEMPORÁRIA
		$foto_temporaria = $GLOBALS['pasta_fotos']."/tmp/".$foto_pid;
		$tamanho_imagem = getimagesize($foto_temporaria);
		//echo "<br>mime: ".$tamanho_imagem[2];


		foreach($_SESSION["guarda_config"]["valor"]["todas"]["miniaturas_prefixo"] as $miniatura_nome)
			{
				//APAGA A FOTO, SE JÁ EXISTIR
				@unlink($GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"][$miniatura_nome].$foto_id.'.'.$foto_extensao);

				//echo "<br><br>".$miniatura_nome;
				/*VERIFICA AS DIMENSÕES DA IMAGEM E CRIA A VERSÃO EM
				NOVO TAMANHO, SE AS DIMENSÕES DA FOTO ENVIADA FOREM
				MAIORES DO QUE O LIMITE DO DEFINIDO*/
				if(($tamanho_imagem[0]>=$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome])
					&&($tamanho_imagem[1]>=$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome]))
					{
						$porcentagem_largura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome] / $tamanho_imagem[0];
						$porcentagem_altura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome] / $tamanho_imagem[1];
						$nova_altura_largura = $tamanho_imagem[1] * $porcentagem_largura;
						$nova_largura_altura = $tamanho_imagem[0] * $porcentagem_altura;

						//echo "<br>largura: ".$tamanho_imagem[0];
						//echo "<br>altura: ".$tamanho_imagem[1];
						//echo "<br>largura SESS: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome];
						//echo "<br>altura SESS: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome];
						//echo "<br>porcentagem_largura: ".$porcentagem_largura;
						//echo "<br>porcentagem_altura: ".$porcentagem_altura;
						//echo "<br>nova_largura[largura]: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome];
						//echo "<br>nova_altura[largura]: ".$nova_altura_largura;
						//echo "<br>nova_largura[altura]: ".$nova_largura_altura;
						//echo "<br>nova_altura[altura]: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome];

						if(($miniatura_nome=="alta")||($miniatura_nome=="grande"))
							{
								if($nova_altura_largura > $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome])
									{
										$nova_largura = ceil($nova_largura_altura);
										$nova_altura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome];
									}
								else
									{
										$nova_largura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome];
										$nova_altura = ceil($nova_altura_largura);
									}
							}
						else
							{
								$nova_largura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome];
								$nova_altura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome];
							}

						//echo "<br>nova_largura: ".$nova_largura;
						//echo "<br>nova_altura: ".$nova_altura;


						if($tamanho_imagem["mime"]=="image/png")
							{
								//cria uma nova imagem grande com o tamanho reduzido de acordo com os limites do sistema
								$imagem_atual = imagecreatefrompng($foto_temporaria);
								$largura_imagem_atual = imagesx($imagem_atual);
								$altura_imagem_atual = imagesy($imagem_atual);
								imagealphablending($imagem_atual, true);
								$nova_imagem = imagecreatetruecolor($nova_largura,$nova_altura);
								imagealphablending($nova_imagem, false);
								imagesavealpha($nova_imagem, true);
								$transparente = imagecolorallocatealpha($nova_imagem, 255, 255, 255, 127);
								imagefilledrectangle($nova_imagem, 0, 0, $nova_largura, $nova_altura, $transparente);
								imagecopyresampled($nova_imagem, $imagem_atual, 0, 0, 0, 0, $nova_largura, $nova_altura, $tamanho_imagem[0], $tamanho_imagem[1]);
								imagepng($nova_imagem, $GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"][$miniatura_nome].$foto_id.'.'.$foto_extensao, 0);
								imagedestroy($imagem_atual);
								imagedestroy($nova_imagem);
								//echo "<br>imagem PNG criada";
							}
						else
							{
								//cria uma nova imagem grande com o tamanho reduzido de acordo com os limites do sistema
								$imagem_atual = imagecreatefromjpeg($foto_temporaria);
								$largura_imagem_atual = imagesx($imagem_atual);
								$altura_imagem_atual = imagesy($imagem_atual);
								$nova_imagem = imagecreatetruecolor($nova_largura,$nova_altura);
								imagecopyresampled($nova_imagem, $imagem_atual, 0, 0, 0, 0, $nova_largura, $nova_altura, $tamanho_imagem[0], $tamanho_imagem[1]);
								imagejpeg($nova_imagem, $GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"][$miniatura_nome].$foto_id.'.'.$foto_extensao, 100);
								imagedestroy($imagem_atual);
								imagedestroy($nova_imagem);
								//echo "<br>imagem JPG criada";
							}
					}
				else
					{
						//echo "<br>copiar";
						copy($foto_temporaria, $GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"][$miniatura_nome].$foto_id.'.'.$foto_extensao);
					}

		}

		//APAGAR A FOTO TEMPORÁRIA
		@unlink($foto_temporaria);

	}



//função que retorna o link ou caminho completo da foto baseado no ID
//foto_id = ID da foto no banco de dados
//tamanho_foto = tamanho da foto para retornar (baseado na config miniaturas_prefixo)
//tipo_url = tipo do retorno (url = em http | caminho = pasta absoluta)
function retorna_url_foto($foto_id, $tamanho_foto, $tipo_url)
	{
		$extensao_foto = (is_file($GLOBALS['pasta_fotos']."/".$tamanho_foto.$foto_id.".jpg")) ? "jpg" : "png";

		return ($tipo_url=="url") ? $GLOBALS['http_fotos']."/".$tamanho_foto.$foto_id.".".$extensao_foto : $GLOBALS['pasta_fotos']."/".$tamanho_foto.$foto_id.".".$extensao_foto;
	}


//função que retorna o link ou caminho completo da foto de uma galeria de fotos
//galeria_id = ID da foto no banco de dados
//nome_arquivo = nome do arquivo da foto da galeria
//tamanho_foto = tamanho da foto para retornar (baseado na config miniaturas_galerias_prefixo)
//tipo_url = tipo do retorno (url = em http | caminho = pasta absoluta)
function retorna_url_foto_galeria($galeria_id, $nome_arquivo, $tamanho_arquivo, $tipo_url)
	{
		global $_SESSION;

		$separa_nome = explode("-", $nome_arquivo);
		$id_foto = $separa_nome[1];

		return ($tipo_url=="url") ? $GLOBALS['http_fotos_galeria']."/".$galeria_id."/".$tamanho_arquivo.$id_foto : $GLOBALS['pasta_fotos_galeria']."/".$galeria_id."/".$tamanho_arquivo.$id_foto;
	}


//função que apaga um diretório inteiro, mesmo com arquivos dentro
//$dir = caminho completo do diretório
function apaga_diretorio($dir) {
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (filetype($dir."/".$object) == "dir") apaga_diretorio($dir."/".$object); else unlink($dir."/".$object);
       }
     }
     reset($objects);
     rmdir($dir);
   }
}




#######################FUNCAO QUE FAZ A CONSULTA SQL NA PAGINA GERENCIAR.PHP#######################
function sql_gerenciar($tipo_pesquisa,$pagina,$sql,$pagtotal,$limite_registros_pagina,$num,$db,$con)
	{
		global $numero_adicional_sugestoes;

		//echo "<br>tipo_pesquisa: ".$tipo_pesquisa;
		//echo "<br>pagina: ".$pagina;

		if($tipo_pesquisa=="ultimas") $limite = " LIMIT 0,$limite_registros_pagina";
		if(($tipo_pesquisa=="ind_acao_cidades")||($tipo_pesquisa=="ind_acao_excel")) $limite = " ";
		if(($pagina=="")&&($limite_registros_pagina>0)&&($limite_registros_pagina<>""))
			{
				$sql1 = $sql.$limite;
				//echo "<br>sql1: ".nl2br($sql1);
				$query=mysql_query($sql1,$con) or die("Erro do MySQL - SQL01: ".mysql_error());
				$num=mysql_num_rows($query);
				$pagtotal=ceil($num/$limite_registros_pagina);
				$pagina=1;
			}

		$pag_atual=($pagina*$limite_registros_pagina)-$limite_registros_pagina;
		//echo "<br>pag_atual: ".$pag_atual;
		//echo "<br>limite: ".$limite;

		if($limite=="") $sql .= " LIMIT ".$pag_atual.",".$limite_registros_pagina;
		else $sql .= $limite;
		//echo "<br>------------------<br>sql2: ".nl2br($sql);

		$query=mysql_query($sql,$con) or die("Erro do MySQL - SQL02: ".mysql_error());

		/*if($tipo_pesquisa<>"naoexibetotal")
			{
				if($num>0)
					{
						echo "<div class='erro' align='center'>";
						echo $num;
						echo " "."registros encontrados - ";
						echo "exibindo de ";
						echo $pag_atual+1;
						echo " a ";
						if($num >= $pag_atual+$limite_registros_pagina)
							{
								echo $pag_atual+$limite_registros_pagina;
							}
						else
							{
								echo $num;
							}
						echo "</div>";
					}
			}*/

		return array($query,$num,$pagtotal,$pag_atual);
	}
#######################FUNCAO QUE FAZ A CONSULTA SQL NA PAGINA GERENCIAR.PHP#######################







####################FUNCAO RESPONSAVEL PELA PAGINACAO DOS REGISTROS NO FRONT####################
//$pagtotal = numero_total de paginas da pesquisa
//$pagina = numero da pagina atual
//$arquivo = nome do arquivo onde aparecera o resultado da paginacao
//$num = numero total de registros da consulta
//$ordem = campo no banco de dados usado para classificar a pesquisa
//$classificacao = classificaco dos registros (ASC ou DESC)
//$tipo_pesquisa = tipo da pesquisa que esta sendo feita
//$css = classe CSS para os links dos textos da paginacao
//$adicional = itens adicionais em cada link da paginacao (ex: palavra, secao, etc)
//$sufixo_variavel = sufixo usado para cada variavel nos links
//$funcao_ajax = sintaxe completa da funcao ajax para carregamento da pagina - o LINK a ser carregado deve estar com valor %%LINK%%
function paginacao_resultados_front($pagtotal,$pagina,$arquivo,$num,$ordem,$classificacao,$tipo_pesquisa,$css,$adicional,$sufixo_variavel,$funcao_ajax)
	{
		global $_REQUEST;

		/*echo "<br>pagtotal: ".$pagtotal;
		echo "<br>pagina: ".$pagina;
		echo "<br>arquivo: ".$arquivo;
		echo "<br>num: ".$num;
		echo "<br>adicional: ".$adicional;
		echo "<br>sufixo_variavel: ".$sufixo_variavel;
		echo "<br>funcao_ajax: ".$funcao_ajax;*/

		//MOSTRA PRIMEIRA
		//echo "<br>pagina: ".$pagina." - ";
		if($pagina > 1)
			{
				$link = $arquivo."?pagina".$sufixo_variavel."=1&pagtotal".$sufixo_variavel."=".$pagtotal."&num".$sufixo_variavel."=".$num."&ordem".$sufixo_variavel."=".$ordem."&classificacao".$sufixo_variavel."=".$classificacao."&tipo_pesquisa".$sufixo_variavel."=".$tipo_pesquisa."&casa".$sufixo_variavel."=".$_REQUEST['casa']."&acao=ver".$adicional;
				//echo "a pagina e diferente de 1<br>";
				if($funcao_ajax=="") echo "<li class='next-post'><a href='".$link."'><i class='fa fa-angle-double-left'></i></a></li>";
				else
					{
						$monta_funcao_ajax = str_replace("%%LINK%%",$link,$funcao_ajax);
						$monta_funcao_ajax = str_replace("'","\"",$monta_funcao_ajax);
						echo "<a href='#1' onClick='".$monta_funcao_ajax."' class='".$css."'>Primeira</a> ";
					}
				unset($link);
			}


		//MOSTRA NUMEROS
		if(($pagtotal > 1) && $pagina > 5 )
			{
				for($t=$pagina-5;(($t<=$pagtotal) && ($t<$pagina+5));$t++)
					{
						if($t==$pagina)
							{
								echo "<li class='active'><a href='#'>";
								echo $t;
								echo "</a></li>";
							}
						else
							{
								$link = $arquivo."?pagina".$sufixo_variavel."=".$t."&pagtotal".$sufixo_variavel."=".$pagtotal."&num".$sufixo_variavel."=".$num."&ordem".$sufixo_variavel."=".$ordem."&classificacao".$sufixo_variavel."=".$classificacao."&tipo_pesquisa".$sufixo_variavel."=".$tipo_pesquisa."&casa".$sufixo_variavel."=".$_REQUEST['casa']."&acao=ver".$adicional;
								if($funcao_ajax=="") echo "<li><a href='".$link."'>".$t."</a></li>";
								else
									{
										$monta_funcao_ajax = str_replace("%%LINK%%",$link,$funcao_ajax);
										$monta_funcao_ajax = str_replace("'","\"",$monta_funcao_ajax);
										echo " <a href = '#1' onClick=".$monta_funcao_ajax." class='".$css."'>".$t."</a> ";
									}
								unset($link);
							}
					}
			}
		if(($pagtotal > 1) && $pagina <= 5 )
			{
				if($pagtotal > 10) $valor_pag = '10';
				else $valor_pag = $pagtotal;

				for($t=1;$t<=$valor_pag;$t++)
					{
						if($t==$pagina)
							{
								echo "<li class='active'><a href='#'>";
								echo $t;
								echo "</a></li>";
							}
						else
							{
								$link = $arquivo."?pagina".$sufixo_variavel."=".$t."&pagtotal".$sufixo_variavel."=".$pagtotal."&num".$sufixo_variavel."=".$num."&ordem".$sufixo_variavel."=".$ordem."&classificacao".$sufixo_variavel."=".$classificacao."&tipo_pesquisa".$sufixo_variavel."=".$tipo_pesquisa."&casa".$sufixo_variavel."=".$_REQUEST['casa']."&acao=ver".$adicional;
								if($funcao_ajax=="") echo "<li><a href='".$link."'>".$t."</a></li>";
								else
									{
										$monta_funcao_ajax = str_replace("%%LINK%%",$link,$funcao_ajax);
										$monta_funcao_ajax = str_replace("'","\"",$monta_funcao_ajax);
										echo " <a href='#1' onClick='".$monta_funcao_ajax."' class='".$css."'>".$t."</a> ";
									}
								unset($link);
							}
					}
			}


		//MOSTRA ULTIMA
		if(($pagina != $pagtotal)&&($pagtotal!=1))
			{
				$link = $arquivo."?pagina".$sufixo_variavel."=".$pagtotal."&pagtotal".$sufixo_variavel."=".$pagtotal."&num".$sufixo_variavel."=".$num."&ordem".$sufixo_variavel."=".$ordem."&classificacao".$sufixo_variavel."=".$classificacao."&tipo_pesquisa".$sufixo_variavel."=".$tipo_pesquisa."&casa".$sufixo_variavel."=".$_REQUEST['casa']."&acao=ver".$adicional;
				if($funcao_ajax=="") echo "<li class='next-post'><a href='".$link."'><i class='fa fa-angle-double-right'></i></a></li>";
				else
					{
						$monta_funcao_ajax = str_replace("%%LINK%%",$link,$funcao_ajax);
						$monta_funcao_ajax = str_replace("'","\"",$monta_funcao_ajax);
						echo " <a href = '#1' onClick='".$monta_funcao_ajax."' class='".$css."'>&Uacute;ltima</a>";
					}
				unset($link);
			}
	}
####################FUNCAO RESPONSAVEL PELA PAGINACAO DOS REGISTROS NO FRONT####################







//funcao que formata a data para aparecer Em 00/00/00 as 00:00h por XXXX com Atualizada em
//$data = data no formato US - YYYY-MM-DD HH:MM
//$atualizacao = data no formato US - YYYY-MM-DD HH:MM
//$autor = nome que aparecera depois do POR (XXXX)
function mostra_data_cadastro_autor_atualizacao($data,$atualizacao,$autor)
	{
		$hora_publicacao = substr($data, 11, 5);
		$mostra_autor = "Em: ".mostra_data_br($data)." &agrave;s ".$hora_publicacao."h por ".$autor;
		if($atualizacao<>"0000-00-00 00:00:00")
			{
				$hora_atualizacao = substr($atualizacao, 11, 5);
				$mostra_autor .= " - Atualizada em ".mostra_data_br($atualizacao)." &agrave;s ".$hora_atualizacao;
			}
		return $mostra_autor;
	}








######################## FUNÇÕES PARA LIMPEZA DO NOME DO ARQUIVO (baseado em  Germanix / Wordpress) ########################
/**
 * Limpar nome de arquivo no upload
 *
 * Sanitization test done with the filename:
 * ÄäÆæÀàÁáÂâÃãÅåªₐāĆćÇçÐđÈèÉéÊêËëₑƒğĞÌìÍíÎîÏïīıÑñⁿÒòÓóÔôÕõØøₒÖöŒœßŠšşŞ™ÙùÚúÛûÜüÝýÿŽž¢€‰№$℃°C℉°F⁰¹²³⁴⁵⁶⁷⁸⁹₀₁₂₃₄₅₆₇₈₉±×₊₌⁼⁻₋–—‑․‥…‧.png
 * @author toscho
 * @url    https://github.com/toscho/Germanix-WordPress-Plugin
 */
function t5f_sanitize_filename( $filename )
{

    $filename    = html_entity_decode( $filename, ENT_QUOTES, 'utf-8' );
    $filename    = t5f_translit( $filename );
    $filename    = t5f_lower_ascii( $filename );
    $filename    = t5f_remove_doubles( $filename );
    return $filename;
}

/**
 * Converte maiúsculas em minúsculas e remove o resto.
 * https://github.com/toscho/Germanix-WordPress-Plugin
 *
 * @uses   apply_filters( 'germanix_lower_ascii_regex' )
 * @param  string $str Input string
 * @return string
 */
function t5f_lower_ascii( $str )
{
    $str     = strtolower( $str );
    $regex   = array(
        'pattern'        => '~([^a-z\d_.-])~'
        , 'replacement'  => ''
    );
    // Leave underscores, otherwise the taxonomy tag cloud in the
    // backend won’t work anymore.
    return preg_replace( $regex['pattern'], $regex['replacement'], $str );
}


/**
 * Reduz meta caracteres (-=+.) repetidos para apenas um.
 * https://github.com/toscho/Germanix-WordPress-Plugin
 *
 * @param  string $str Input string
 * @return string
 */
function t5f_remove_doubles( $str )
{
    $regex = array(
        'pattern'        => '~([=+.-])\\1+~'
        , 'replacement'  => "\\1"
    );
    return preg_replace( $regex['pattern'], $regex['replacement'], $str );
}


/**
 * Substitui caracteres não-ASCII.
 * https://github.com/toscho/Germanix-WordPress-Plugin
 *
 * Modified version of Heiko Rabe’s code.
 *
 * @author Heiko Rabe http://code-styling.de
 * @link   http://www.code-styling.de/?p=574
 * @param  string $str
 * @return string
 */
function t5f_translit( $str )
{
    $utf8 = array(
        'Ä'  => 'Ae'
        , 'ä'    => 'ae'
        , 'Æ'    => 'Ae'
        , 'æ'    => 'ae'
        , 'À'    => 'A'
        , 'à'    => 'a'
        , 'Á'    => 'A'
        , 'á'    => 'a'
        , 'Â'    => 'A'
        , 'â'    => 'a'
        , 'Ã'    => 'A'
        , 'ã'    => 'a'
        , 'Å'    => 'A'
        , 'å'    => 'a'
        , 'ª'    => 'a'
        , 'ₐ'    => 'a'
        , 'ā'    => 'a'
        , 'Ć'    => 'C'
        , 'ć'    => 'c'
        , 'Ç'    => 'C'
        , 'ç'    => 'c'
        , 'Ð'    => 'D'
        , 'đ'    => 'd'
        , 'È'    => 'E'
        , 'è'    => 'e'
        , 'É'    => 'E'
        , 'é'    => 'e'
        , 'Ê'    => 'E'
        , 'ê'    => 'e'
        , 'Ë'    => 'E'
        , 'ë'    => 'e'
        , 'ₑ'    => 'e'
        , 'ƒ'    => 'f'
        , 'ğ'    => 'g'
        , 'Ğ'    => 'G'
        , 'Ì'    => 'I'
        , 'ì'    => 'i'
        , 'Í'    => 'I'
        , 'í'    => 'i'
        , 'Î'    => 'I'
        , 'î'    => 'i'
        , 'Ï'    => 'Ii'
        , 'ï'    => 'ii'
        , 'ī'    => 'i'
        , 'ı'    => 'i'
        , 'I'    => 'I' // turkish, correct?
        , 'Ñ'    => 'N'
        , 'ñ'    => 'n'
        , 'ⁿ'    => 'n'
        , 'Ò'    => 'O'
        , 'ò'    => 'o'
        , 'Ó'    => 'O'
        , 'ó'    => 'o'
        , 'Ô'    => 'O'
        , 'ô'    => 'o'
        , 'Õ'    => 'O'
        , 'õ'    => 'o'
        , 'Ø'    => 'O'
        , 'ø'    => 'o'
        , 'ₒ'    => 'o'
        , 'Ö'    => 'Oe'
        , 'ö'    => 'oe'
        , 'Œ'    => 'Oe'
        , 'œ'    => 'oe'
        , 'ß'    => 'ss'
        , 'Š'    => 'S'
        , 'š'    => 's'
        , 'ş'    => 's'
        , 'Ş'    => 'S'
        , '™'    => 'TM'
        , 'Ù'    => 'U'
        , 'ù'    => 'u'
        , 'Ú'    => 'U'
        , 'ú'    => 'u'
        , 'Û'    => 'U'
        , 'û'    => 'u'
        , 'Ü'    => 'Ue'
        , 'ü'    => 'ue'
        , 'Ý'    => 'Y'
        , 'ý'    => 'y'
        , 'ÿ'    => 'y'
        , 'Ž'    => 'Z'
        , 'ž'    => 'z'
        // misc
        , '¢'    => 'Cent'
        , '€'    => 'Euro'
        , '‰'    => 'promille'
        , '№'    => 'Nr'
        , '$'    => 'Dollar'
        , '℃'    => 'Grad Celsius'
        , '°C' => 'Grad Celsius'
        , '℉'    => 'Grad Fahrenheit'
        , '°F' => 'Grad Fahrenheit'
        // Superscripts
        , '⁰'    => '0'
        , '¹'    => '1'
        , '²'    => '2'
        , '³'    => '3'
        , '⁴'    => '4'
        , '⁵'    => '5'
        , '⁶'    => '6'
        , '⁷'    => '7'
        , '⁸'    => '8'
        , '⁹'    => '9'
        // Subscripts
        , '₀'    => '0'
        , '₁'    => '1'
        , '₂'    => '2'
        , '₃'    => '3'
        , '₄'    => '4'
        , '₅'    => '5'
        , '₆'    => '6'
        , '₇'    => '7'
        , '₈'    => '8'
        , '₉'    => '9'
        // Operators, punctuation
        , '±'    => 'plusminus'
        , '×'    => 'x'
        , '₊'    => 'plus'
        , '₌'    => '='
        , '⁼'    => '='
        , '⁻'    => '-' // sup minus
        , '₋'    => '-' // sub minus
        , '–'    => '-' // ndash
        , '—'    => '-' // mdash
        , '‑'    => '-' // non breaking hyphen
        , '․'    => '.' // one dot leader
        , '‥'    => '..'  // two dot leader
        , '…'    => '...'  // ellipsis
        , '‧'    => '.' // hyphenation point
        , ' '    => '-'   // nobreak space
        , ' '    => '-'   // normal space
    );

    $str = strtr( $str, $utf8 );
    return trim( $str, '-' );
}
######################## FUNÇÕES PARA LIMPEZA DO NOME DO ARQUIVO (baseado em  Germanix / Wordpress) ########################








//funcao que envia e-mails
//$FromName = nome do remetente
//$ToEmail = e-mail do destinatário
//$ToName = nome do destinatário
//$BccEmail = e-mail para enviar cópia oculta (em branco para não enviar)
//$Subject = assunto do e-mail
//$Content = conteúdo HTML da mensagem
//$ResultOKMsg = mensagem quando o envio for OK
//$ResultNOKMsg = mensagem quando o envio falhar (em branco para mensagem padrão)
//$LoadLibrary = define se deve ou não carregar a biblioteca (sim/nao)
function envia_email($FromName, $ToEmail, $ToName, $BccEmail, $Subject, $Content, $ResultOKMsg, $ResultNOKMsg, $LoadLibrary)
	{
		if($LoadLibrary<>"nao") require $GLOBALS['pasta_lib'].'/phpmailer/PHPMailerAutoload.php';

		//echo "\nconteudo_email: ".$conteudo_email;

		$mail = new PHPMailer;

		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';

		$mail->CharSet = 'UTF-8';

		$mail->isSMTP();
		$mail->Host = $GLOBALS['remetente_padrao_contato_host'];
		$mail->SMTPAuth = true;
		$mail->Username = $GLOBALS['remetente_padrao_contato_usuario'];
		$mail->Password = $GLOBALS['remetente_padrao_contato_senha'];
		$mail->SMTPSecure = 'tls';


		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);

		$mail->Port = $GLOBALS['remetente_padrao_contato_porta'];

		$mail->setFrom($GLOBALS['remetente_padrao_contato'], $FromName);
		$mail->addAddress(trim($ToEmail), trim($ToName));     // Add a recipient
		$mail->addReplyTo($GLOBALS['email_administrador'], $FromName);

		if($BccEmail<>"") $mail->addBCC(trim($BccEmail));

		$mail->isHTML(true);

		$mail->Subject = $Subject;
		$mail->Body    = $Content;


		if(!$mail->Send())
			{
				$result = ($ResultNOKMsg=="") ? "Erro no envio do e-mail. Contate o administrador do sistema no e-mail ".$GLOBALS['email_suporte']." e informe o seguinte erro: ".$Email->ErrorInfo : $ResultNOKMsg;
			}
		else
			{
				$result = $ResultOKMsg;
			}

		return $result;
	}




/*### FUNÇÃO QUE RETORNA O ÚLTIMO BRANCH, TAG E DATA ###*/
function GitBranchTagDate()
     {
         $rev = trim(@file_get_contents($GLOBALS['pasta_root']. "/.git/HEAD"));

         if (substr($rev, 0, 4) == 'ref:') {
             $branch =  end(explode('/', $rev));
             $hash = trim(@file_get_contents($GLOBALS['pasta_root'] . "/.git/refs/heads/{$branch}"));
         }

        // echo "<br>hash: ".$hash;
		$directoryIterator = new DirectoryIterator($GLOBALS['pasta_root']."/.git/refs/tags");
		foreach($directoryIterator as $arquivo_tag)
			{
				// echo "<br>arquivo_tag: ".$arquivo_tag;
				// echo "<br>   conteudo arquivo_tag: ".file_get_contents($GLOBALS['pasta_root']."/.git/refs/tags/".$arquivo_tag);
				if(trim($hash)==trim(@file_get_contents($GLOBALS['pasta_root']."/.git/refs/tags/".$arquivo_tag)))
					{
						$tag = $arquivo_tag->__toString();
						$data = $arquivo_tag->getMTime();
					}
			}

		if($tag=="")
			{
				$tag = substr($hash,0,7);
				$data = filemtime($GLOBALS['pasta_root'] . "/.git/refs/heads/{$branch}");
			}

        return array($branch,$hash,$tag,$data);
	 }
/*### FUNÇÃO QUE RETORNA O ÚLTIMO BRANCH, TAG E DATA ###*/






/*### FUNÇÃO QUE RETORNA O PID DO ARQUIVO QUE FOI ENVIADO ###*/
//pid = PID do arquivo
//directory = caminho absoluto do diretório on o arquivo está - sem barra no final
function getFileUploadedPid($pid, $directory) {

	$abre_diretorio = new DirectoryIterator($directory);
	foreach($abre_diretorio as $le_diretorio) {

		if(($le_diretorio->__toString() <> ".")&&
			($le_diretorio->__toString() <> "..")&&
			($le_diretorio->__toString() <> ".DS_Store")&&
			($le_diretorio->__toString() <> "index.php"))
			{
				if((substr_count($le_diretorio->__toString(),$pid)>0)) $arquivo = $le_diretorio->__toString();
				else @unlink($directory."/".$le_diretorio->__toString());
			}
	}

	return $arquivo;
}
/*### FUNÇÃO QUE RETORNA O PID DO ARQUIVO QUE FOI ENVIADO ###*/
?>