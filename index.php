<?php

    require_once("config/config.php");

    require_once("vendor/autoload.php");    

    // echo "<pre>SERVER: "; print_r($_SERVER); echo  "</pre>";

    $URL = new \Classes\Url;

    $BaseUrl = $URL->getBaseUrl();
    // echo "<pre>BaseUrl: "; print_r($BaseUrl); echo  "</pre>";

    $Page = ($_REQUEST["page"] <> "") ? $_REQUEST["page"] : "home";
    // echo "<pre>Page: "; print_r($Page); echo  "</pre>";

    $Parameters = $URL->getPage()["parameters"];
    // echo "<pre>Parameters: "; print_r($Parameters); echo  "</pre>";

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Qualycare</title>
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="Medic, Medical Center" />
        <meta name="description" content="Responsive Medical Health Template" />
        <meta http-equiv="Content-Language" content="pt-br">

        <!--style-->
		<link href='//fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Volkhov:400italic' rel='stylesheet' type='text/css'>
        <link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/superfish.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/fancybox/jquery.fancybox.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/jquery.qtip.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/jquery-ui-1.9.2.custom.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/style.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/css/qualycare.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/responsive.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/medical-clinic/style/animations.css" />
        <link rel="shortcut icon" href="<?php echo $BaseUrl; ?>images/favicon.ico" />
        <link href="<?php echo $BaseUrl; ?>lib/fontawesome-free-5.0.13/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo $BaseUrl; ?>lib/SliderRevolution/src/css/settings.css" media="screen" />

        <!--js-->
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery-migrate-1.4.1.min.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery-ui-1.9.2.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.carouFredSel-5.6.4-packed.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.sliderControl.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.timeago.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.hint.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.isotope.masonry.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/medical-clinic/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyCtdDH2CJOSsMLa6QMldd6l5VMrBORwuE8"></script>

        <script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/SliderRevolution/src/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/SliderRevolution/src/js/jquery.themepunch.revolution.min.js"></script>
    
        <script type="text/javascript" src="<?php echo $BaseUrl; ?>lib/js/qualycare.js"></script>
	</head>
	<body>
		<div class="site_container">
			<div class="header_container page-<?php echo $Page; ?>">
				<div class="header_line clearfix">
                    <div class="social-icons">
                        <a href="https://www.facebook.com/qualycare" target="_blank"><i class="fab fa-facebook-f"></i></a>&nbsp;
                        <!-- <a href="#" target="_blank"><i class="fab fa-youtube"></i></a>&nbsp; -->
                        <a href="https://www.instagram.com/qualycare/ " target="_blank"><i class="fab fa-instagram"></i></a>&nbsp;
                        <a href="https://www.linkedin.com/company/qualycarehomecare/" target="_blank"><i class="fab fa-linkedin"></i></a>
                    </div>
                    <!-- <div class="link-intranet"><a href="#">INTRANET</a></div> -->
                    <!-- <div class="search-box">
                        <form>
                            <input type="text" placeholder="Busca" />
                            <a class="search-icon" href="#"><i class="fas fa-search-plus"></i></a>
                        </form>
                    </div> -->
                </div>
                <div class="clearfix"></div>
                <hr class="header_line_hr">
				<div class="header clearfix">
					<div class="header_left">
						<a href="<?php echo $BaseUrl; ?>" title="Qualycare">
							<img src="<?php echo $BaseUrl; ?>images/logo-qualycare.png" alt="logo" />
						</a>
					</div>
					<ul class="sf-menu header_right">
						<li>
							<a href="<?php echo $BaseUrl; ?>?page=quem-somos" title="Quem Somos">
								Quem Somos
							</a>
						</li>
						<li>
							<a href="<?php echo $BaseUrl; ?>?page=servicos" title="Serviços">
								Serviços
							</a>
						</li>
						<li>
							<a href="<?php echo $BaseUrl; ?>?page=na-midia" title="Na mídia">
								Na Mídia
							</a>
						</li>
						<li>
							<a href="<?php echo $BaseUrl; ?>?page=duvidas-frequentes" title="Dúvidas Frequentes">
								Dúvidas Frequentes
							</a>
						</li>
						<li>
							<a href="<?php echo $BaseUrl; ?>?page=fale-conosco" title="Fale Conosco">
								Fale Conosco
							</a>
						</li>
					</ul>
					<div class="mobile_menu">
						<select>
							<option value="<?php echo $BaseUrl; ?>">Home</option>
							<option value="<?php echo $BaseUrl; ?>?page=quem-somos">Quem Somos</option>
							<option value="<?php echo $BaseUrl; ?>?page=servicos">Serviços</option>
							<option value="<?php echo $BaseUrl; ?>?page=na-midia">Na Mídia</option>
							<option value="<?php echo $BaseUrl; ?>?page=duvidas-frequentes">Dúvidas Frequentes</option>
							<option value="<?php echo $BaseUrl; ?>?page=fale-conosco">Fale Conosco</option>
						</select>
					</div>
				</div>
            </div>
            <!--### SLIDER - FOTOS ###-->
            <div class="tp-banner-container page-<?php echo $Page; ?>">

                <?php
                    if($Page<>"mostra") {
                ?>
                        <div class="tp-banner" >
                            <ul>
                                <?php
                                    if(count($Slider[$Page])>0) {

                                        foreach ($Slider[$Page] as $key => $SliderParameters) {

                                            // echo "<br><br>".$SliderParameters["img"];
                                            // echo "<br>".$SliderParameters["title"];
                                            // echo "<br>".$SliderParameters["description"];
                                ?>
                                            <li data-transition="fade">
                                                <?php
                                                    if($SliderParameters["img"]) {
                                                ?>
                                                        <img src="<?php echo $BaseUrl; ?>images/<?php echo $SliderParameters["img"]; ?>" />
                                                <?php
                                                    }

                                                    elseif($SliderParameters["html"]) {
                                                        
                                                        echo $SliderParameters["html"];
                                                    }
                                                ?>

                                                <div class="tp-caption large_bold_white customout"
                                                    data-x="550"
                                                    data-y="280"
                                                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                    data-speed="800"
                                                    data-start="100"
                                                    data-easing="Power4.easeOut"
                                                    data-endspeed="300"
                                                    data-endeasing="Power1.easeIn"
                                                    data-captionhidden="on"
                                                    style="z-index: 8"><?php echo $SliderParameters["title"]; ?>
                                                </div>
                                                <div class="tp-caption medium_light_white tp-resizeme tp-legend-wrap"
                                                    data-x="550"
                                                    data-y="340"
                                                    data-width="100"
                                                    data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                                    data-speed="600"
                                                    data-start="1200"
                                                    data-easing="Back.easeOut"
                                                    data-endspeed="300"
                                                    data-endeasing="Power1.easeIn"
                                                    style="z-index: 11"><div style="width:80%;"><?php echo $SliderParameters["description"]; ?></div>
                                                </div>
                                            </li>                        
                                <?php
                                        }
                                    }
                                ?>
                            </ul>
                            <div class="tp-bannertimer"></div>
                        </div>
                <?php
                    }
                ?>
            </div>
            <!--### SLIDER - FOTOS ###-->


            <?php
                if($Page=="home") {
            ?>
                <!--### CONTACT AREA ###-->
                <div class="page relative noborder home-contact-area">
                        <div class="columns columns_3 page_margin_top_section clearfix">
                            <ul class="column">
                                <li class="item_content clearfix">
                                    <a class="features_image" title="">
                                        <img src="<?php echo $BaseUrl; ?>images/ico-celular.png" alt="" class="" />
                                    </a>
                                    <div class="text">
                                        <h3>Ligue para nós</h3>
                                        0800 603 1012
                                    </div>
                                </li>
                            </ul>
                            <ul class="column">
                                <li class="item_content clearfix">
                                    <a class="features_image" title="">
                                        <img src="<?php echo $BaseUrl; ?>images/ico-carta.png" alt="" class="" />
                                    </a>
                                    <div class="text">
                                        <h3>Envie-nos uma mensagem</h3>
                                        <a href="<?php echo $BaseUrl; ?>?page=fale-conosco">Fale Conosco</a>
                                    </div>
                                </li>
                            </ul>
                            <ul class="column">
                                <li class="item_content clearfix">
                                    <a class="features_image" title="">
                                        <img src="<?php echo $BaseUrl; ?>images/ico-marker.png" alt="" class="" />
                                    </a>
                                    <div class="text">
                                        <h3>Visite nossa localização</h3>
                                        <span>Av. Miguel Sutil, 10000
                                        <br>Jardim Mariana - Cuiabá, MT</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--### CONTACT AREA ###-->
            <?php
                }
            ?>
        </div>
        
        <?php 
            if(file_exists("public/".$Routes[$Page]["file"])) {

                include_once("public/".$Routes[$Page]["file"]); 
            }
            else {

                include_once("public/home.php"); 
            }
        ?>

        <!--### FOOTER ###-->
		<div class="site-container-internal">
        
                <div class="footer_container">
                        <div class="footer">
                            <div class="footer_box_container clearfix">
                                <div class="footer_box">
                                    <h3 class="box_header">Contato e informações</h3>
							        <div class="columns columns_3 page_margin_top clearfix">
                                        <ul class="column">
                                            <li class="item_content clearfix">
                                                <a class="features_image" href="#" title="">
                                                    <img src="<?php echo $BaseUrl; ?>images/ico-marker-gray.png" alt="" />
                                                </a>
                                                <div class="text">
                                                    Av. Miguel Sutil, 10000
                                                    <br>Jardim Mariana - Cuiabá, MT
                                                </div>
                                            </li>
                                            <li class="item_content clearfix">
                                                <a class="features_image" href="#" title="">
                                                    <img src="<?php echo $BaseUrl; ?>images/ico-celular-gray.png" alt="" />
                                                </a>
                                                <div class="text">
                                                    0800 603 1012
                                                </div>
                                            </li>
                                            <li class="item_content clearfix">
                                                <a class="features_image" href="#" title="">
                                                    <img src="<?php echo $BaseUrl; ?>images/ico-carta-gray.png" alt="" />
                                                </a>
                                                <div class="text">
                                                    contato@qualycare.com.br
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="footer_box footer-service">
                                    <h3 class="box_header">Serviços</h3>
                                    <div class="columns columns_3 page_margin_top clearfix">
                                        <ul class="column">
                                            <li class="item_content clearfix">
                                                <div class="text"><a href="<?php echo $BaseUrl; ?>?page=servicos#tab-01">Home Care</a></div>
                                            </li>
                                            <li class="item_content clearfix">
                                                <div class="text"><a href="<?php echo $BaseUrl; ?>?page=servicos#tab-02">Atendimento Domiciliar</a></div>
                                            </li>
                                            <li class="item_content clearfix">
                                                <div class="text"><a href="<?php echo $BaseUrl; ?>?page=servicos#tab-03">Serviços de Remoção</a></div>
                                            </li>
                                            <li class="item_content clearfix">
                                                <div class="text"><a href="<?php echo $BaseUrl; ?>?page=servicos#tab-04">Área Protegida</a></div>
                                            </li>
                                            <li class="item_content clearfix">
                                                <div class="text"><a href="<?php echo $BaseUrl; ?>?page=servicos#tab-05">Cobertura de Eventos</a></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="footer_box">
                                    <div class="columns columns_3 page_margin_top clearfix">
                                        <div class="fb-page" data-href="https://www.facebook.com/qualycare" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/qualycare" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/qualycare">Qualycare Home Care e Resgate</a></blockquote></div>
                                    </div>
                                </div>
                            </div>
                            <div class="copyright_area clearfix">
                                <div class="copyright_left">
                                    © Copyright - <a href="http://www.qualycare.com.br" title="Qualycare" target="_blank">Qualycare</a> by <a href="http://www.datalinux.com.br" title="Datalinux Tecnologia" target="_blank">Datalinux Tecnologia</a>
                                </div>
                                <div class="copyright_right">
                                    <a class="scroll_top icon_small_arrow top_white" href="#top" title="Rolar para o topo">Topo</a>
                                </div>
                            </div>
                        </div>
                    </div>
        
        </div>
        <!--### FOOTER ###-->


        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0&appId=466573130068769';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b38d6e94b28eaea"></script>




    </body>
</html>