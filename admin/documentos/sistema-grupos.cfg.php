<?php
/*###################################################################
|																	|
|	MÓDULO: documento 												|
|	DESCRIÇÃO: Arquivo padrão de configuração para o grid de 		|
|	de usuários														|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 20/10/2016												|
|																	|
###################################################################*/


	/*###################################################################
	|																	|
	|	Variáveis de informações sobre o sistema, títulos e descrição 	|
	|	e informações das tabelas e campos.								|
	|	 																|
	###################################################################*/

	//NOME DO SISTEMA UTILIZADO COMO REFERÊNCIA PARA PERMISÕES DE ACESSO
	$sistema = "documentos";

	//NOME DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ EXIBIR NO GRID
	$sistema_nome_da_tabela_grid = "visao_grupos";

	//NOME DO CAMPO NA TABELA DO BANCO DE DADOS QUE É CHAVE-PRIMÁRIA E ID PARA O DT_RowID
	$sistema_chave_primaria_grid = "vis_grupo_id";

	//PREFIXO DOS CAMPOS DA TABELA/VIEW DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ EXIBIR NO GRID
	$sistema_prefixo_campos_grid = "vis_grupo_";




	/*###################################################################
	|																	|
	|	Variável $sistema_sql_adicional armazena condições WHERE 		|
	|	adicionais que poderão ser utilizadas para limitar as 			|
	|	pesquisas. Esta condição é aplicada em todas as consultas do 	|
	|	GRID. 															|
	|																	|
	|	Formato: usar sintaxe padrão SQL condicional, após o WHERE, 	|
	|	iniciando com "( " e terminando com " ) AND ".					|
	|	Ex: campo = 'valor' AND campo <> 'valor', etc					|
	|	 																|
	|	Se não houver necessidade de SQL adicional, deixar em branco.	|
	|	 																|
	###################################################################*/

	$sistema_sql_adicional = "";




	/*###################################################################
	|																	|
	|	Variável $array_campos_filtragem_permissoes é um array 			|
	|	multidimensional que contém as informações dos campos de 		|
	|	filtragem com base nas permissões, para o sistema não exibir 	|
	|	as informações que o usuário não tem acesso.					|
	|	 																|
	|	Fomato: "nome_do_campo_grid" => "array de permissões"			|
	|			array de permissões do $auth_filial						|
	|	 																|
	###################################################################*/

	$array_campos_filtragem_permissoes = "";





	/*###################################################################
	|																	|
	|	Variável $array_colunas_grid é array multidimensional que 		|
	|	contém todas as configucações das colunas que constam no GRID	|
	| 	da tabela.														|
	|																	|
	|	Formato: NOME_DO_CAMPO_BD => (title,							|
	|								width,								|
	|								visible,							|
	|								searchable,							|
	|								orderable,							|
	|								array(formatacao)					|
	|	 																|
	|	NOME_DO_CAMPO_BD: deve ser escritos SEM O PREFIXO 				|
	|																	|
	|	title: título da coluna pode ser escrito em UTF8 acentudo		|
	|																	|
	|	width: tamanho da coluna no GRID. O somatório total das 		|
	|			colunas não pode ser maior do que 90%					|
	|																	|
	|	visible: "true" se deseja mostrar a coluna no GRID, "false" 	|
	|			para não mostrar	 									|
	|																	|
	|	searchable: "true" se deseja permitir busca no valor dessa 		|
	|				coluna, "false" para não mostrar	 				|
	|																	|
	|	className: "nome_da_class" indicar o nome da classe CSS para	|
	|				uso nas colunas quando desejar formatação diferente	|
	|				da padrão, uso opcional								|
	|																	|
	|	orderable: "true" se deseja permitir busca no valor dessa 		|
	|				coluna, "false" para não mostrar	 				|
	|																	|
	|	hidden: indicar o nome da classe hidden da coluna conforme		|
	|			a resolução do dispositivo. Opcional	 				|
	|																	|
	|	array(formatacao): array que contém a informações de formatação	|
	|						do valor a ser exibido no GRID,incluindo a 	|
	|						aplicação de funções de formatação de data,	|
	|						moeda, ou outros tratamentos que precisam 	|
	|						ser feitos com o valor. Só utilizar quendo	|
	|						exigir formatação. A formatação deverá 		|
	|						ser colocada após o return do function		|
	|	 																|
	###################################################################*/

	$array_colunas_grid = array("id" => array("title" => "ID",
											"width" => "5%",
											"visible" => "true",
											"searchable" => "true",
											"orderable" => "false"),

								"titulo" => array("title" => "Grupo",
												"width" => "85%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true"),

								"selecionado" => array("title" => "Associado",
												"width" => "10%",
												"visible" => "true",
												"searchable" => "false",
												"orderable" => "false",
												"formatacao" => array("formatacao" => function($formatacao) {
													global $con;
													if($_REQUEST['id_doc']>0)
														{
															$sql_pertence_grupo = "SELECT
																						doc_grupo_grupo
																					FROM
																						documentos_grupos
																					WHERE
																						doc_grupo_grupo = ".$formatacao."
																						AND doc_grupo_doc = ".$_REQUEST['id_doc'];
															// echo "<br>sql_pertence_grupo: ".$sql_pertence_grupo;
															$exe_pertence_grupo = mysql_query($sql_pertence_grupo, $con) or die("Erro do MySQL[exe_pertence_grupo]: ".mysql_error());
															$ver_pertence_grupo = mysql_fetch_array($exe_pertence_grupo);
														}
													$checkbox = "<label><input type='checkbox' name='associar_grupo[".$formatacao."]' id='associar_grupo_".$formatacao."' value='".$formatacao."' class='ace associar_grupo' title='Marque para associar este grupo' ";
													if(($formatacao==$ver_pertence_grupo["doc_grupo_grupo"])&&($formatacao>0)) $checkbox .= "checked";
													$checkbox .= " /><span class='lbl'></span></label>";
													return $checkbox;
												})),
												);


	/*###################################################################
	|																	|
	|	Variável $array_colunas_grid é array multidimensional que 		|
	|	contém as informações de como será a ordenação padrão do GRID. 	|
	|	Colocar a array na sequencia que deseja que seja feita a 		|
	|	ordenação. A primeira key é o nome do campo igual ao utilizado 	|
	|	no $array_colunas_grid e o valor indica se será ASC ou DESC		|
	|																	|
	###################################################################*/

	$array_ordenacao_grid = array("titulo" => "asc");


	//INCLUI O ARQUIVO DE FUNÇÕES ADICIONAIS PARA TODOS OS SISTEMAS
	include($pasta_includes."/functions.sistema.inc.php");

?>