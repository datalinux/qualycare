<?php
/*###################################################################
|																	|
|	MÓDULO: documentos												|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 20/10/2016												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if(($_REQUEST['operacao']=="selecionar")||($_REQUEST['operacao']=="leitura"))
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			// echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//MONTA O ARRAY ORDENADO COM AS SEÇÕES DO REGISTRO
					$separa_secoes = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."secao"]);
					foreach($separa_secoes as $secao_nome) {
						if($secao_nome<>"") {
							$num_secao++;
							$rows[$num][$sistema_prefixo_campos."secao[]"][$num_secao] = $secao_nome;
						}
					}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];


					/*### CONSULTA E ARMAZENA TODOS OS NÍVEIS QUE O USUÁRIO FAZ PARTE ###*/
					$sql_niveis_usuario = "SELECT
												NVG.vis_usr_nv_grp_nivel_id AS USR_NVL_NIVEL,
												HIE.vis_nvl_hie_hierarquia AS USR_NVL_HIERARQUIA
											FROM
												visao_usuarios_niveis_grupos NVG
											INNER JOIN
												visao_niveis_hierarquia HIE ON HIE.vis_nvl_hie_id = NVG.vis_usr_nv_grp_nivel_id
											WHERE
												NVG.vis_usr_nv_grp_usuario_id = ".$_SESSION["login"]["id"];
					// echo "\nsql_niveis_usuario: ".$sql_niveis_usuario;
					$exe_niveis_usuario = mysql_query($sql_niveis_usuario, $con) or die("Erro do MySQL[exe_niveis_usuario]: ".mysql_error());
					if(mysql_num_rows($exe_niveis_usuario)>0)
						{
							while($ver_niveis_usuario = mysql_fetch_array($exe_niveis_usuario))
								{
									$hierarquia_niveis_usuario[$ver_niveis_usuario["USR_NVL_NIVEL"]] = $ver_niveis_usuario["USR_NVL_HIERARQUIA"];
								}
						}
					/*### CONSULTA E ARMAZENA TODOS OS NÍVEIS QUE O USUÁRIO FAZ PARTE ###*/




					//CONSULTA TODOS OS NÍVEIS DO DOCUMENTO E VERIFICA SE O USUÁRIO É DE UM NÍVEL SUPERIOR,
					//SE SIM, AUTORIZA A PUBLICAR DOCUMENTOS EM QUALQUER STATUS
					$sql_hierarquia = "SELECT
												vis_doc_nvl_hie_nvl,
												vis_doc_nvl_hie_grp,
												vis_doc_nvl_hie_nvl_sup,
												vis_doc_nvl_hie_grp_sup
											FROM
												visao_documentos_niveis
											WHERE
												vis_doc_nvl_doc_id = ".$ver_selecionar[$sistema_prefixo_campos."id"];
					// echo "\sql_hierarquia: ".$sql_hierarquia;
					$exe_hierarquia = mysql_query($sql_hierarquia, $con) or die("Erro do MySQL[exe_hierarquia]: ".mysql_error());
					if(mysql_num_rows($exe_hierarquia)>0)
						{
							//CRIA ARRAY DOS SUPERVISORES DOS NÍVEIS
							$rows["supervisores_niveis"][] = "";

							while($ver_hierarquia = mysql_fetch_array($exe_hierarquia))
								{
									/*### VERIFICA SE O USUÁRIO PERTENCE A ALGUM NÍVEL SUPERIOR ###*/
									$separa_hie_nvl = explode($separador_string,$ver_hierarquia["vis_doc_nvl_hie_nvl"]);
									foreach ($separa_hie_nvl as $key => $hie_nvl) {
										$hierarquia_hie_nvl++;
										if(($hierarquia_niveis_usuario[$hie_nvl]>=1)&&($hierarquia_hie_nvl>2)) $rows["pertence_nivel_superior"] = "sim";
										if(($hierarquia_hie_nvl>2)&&($hie_nvl>0)) $rows["niveis_superiores"][] = $hie_nvl;
									}
									unset($hierarquia_hie_nvl);
									/*### VERIFICA SE O USUÁRIO PERTENCE A ALGUM NÍVEL SUPERIOR ###*/


									/*### VERIFICA SE O USUÁRIO PERTENCE A ALGUM NÍVEL SUPERIOR DENTRO DE ALGUM GRUPO ###*/
									$separa_hie_grp = explode($separador_string,$ver_hierarquia["vis_doc_nvl_hie_grp"]);
									foreach ($separa_hie_grp as $key => $hie_grp) {
										$hierarquia_hie_grp++;
										if(($hierarquia_niveis_usuario[$hie_grp]>=1)&&($hierarquia_hie_grp>2)) $rows["pertence_nivel_superior"] = "sim";
										if(($hierarquia_hie_grp>2)&&($hie_grp>0)) $rows["niveis_superiores"][] = $hie_grp;
									}
									unset($hierarquia_hie_grp);
									/*### VERIFICA SE O USUÁRIO PERTENCE A ALGUM NÍVEL SUPERIOR DENTRO DE ALGUM GRUPO ###*/

									if($ver_hierarquia["vis_doc_nvl_hie_nvl_sup"]>0) $rows["supervisores_niveis"][] = $ver_hierarquia["vis_doc_nvl_hie_nvl_sup"];
									if($ver_hierarquia["vis_doc_nvl_hie_grp_sup"]>0) $rows["supervisores_niveis"][] = $ver_hierarquia["vis_doc_nvl_hie_grp_sup"];
								}

							//ELIMINA NÍVEIS DUPLICADOS
							$rows["niveis_superiores"] = @array_unique($rows["niveis_superiores"]);

							//ELIMINA SUPERVISORES DUPLICADOS
							$rows["supervisores_niveis"] = @array_unique($rows["supervisores_niveis"]);

							//SE O USUÁRIO FOR SUPERVISOR DE ALGUM NÍVEL, AUTORIZA A PUBLICAR DOCUMENTOS EM QUALQUER STATUS
							if(@in_array($_SESSION["login"]["id"], $rows["supervisores_niveis"])) $rows["pertence_nivel_superior"] = "sim";
						}

					if(count($rows["niveis_superiores"])==0)
						{
							$rows["doc_sem_nivel_grupo"] = true;
						}


					//CRIA O OBJETO COM OS TIPOS DE STATUS PERMITIDOS
					foreach($_SESSION["guarda_config"]["titulo"]["todas"]["status_versoes"] as $config_value => $config_title) {

						$rows["status_versoes"][$config_value] = $config_title;
					}

					$sql_versoes = "SELECT
											DV.doc_ver_id,
											DV.doc_ver_versao,
											DV.doc_ver_status,
											CF.configuracao_titulo AS doc_ver_status_nome,
											DV.doc_ver_observacoes,
											DV.doc_ver_obs_aprovador,
											DV.doc_ver_criacao_data,
											US_CR.usuario_id AS doc_ver_criacao_usuario,
											US_CR.usuario_nome AS doc_ver_criacao_usuario_nome,
											DV.doc_ver_atualizacao_data,
											US_AT.usuario_id AS doc_ver_atualizacao_usuario,
											US_AT.usuario_nome AS doc_ver_atualizacao_usuario_nome,
											DV.doc_ver_aprovado_data,
											US_AP.usuario_id AS doc_ver_aprovado_usuario,
											US_AP.usuario_nome AS doc_ver_aprovado_usuario_nome,
											DV.doc_ver_publicado_data,
											US_PU.usuario_id AS doc_ver_publicado_usuario,
											US_PU.usuario_nome AS doc_ver_publicado_usuario_nome,
											DV.doc_ver_revogado_data,
											US_RV.usuario_id AS doc_ver_revogado_usuario,
											US_RV.usuario_nome AS doc_ver_revogado_usuario_nome
										FROM
											documentos_versoes AS DV
										INNER JOIN
											configuracoes AS CF ON CF.configuracao_categoria = 'status_versoes' AND CF.configuracao_valor = DV.doc_ver_status AND CF.configuracao_excluido <> 'sim'
										INNER JOIN
											usuarios AS US_CR ON US_CR.usuario_id = DV.doc_ver_criacao_usuario
										LEFT JOIN
											usuarios AS US_AT ON US_AT.usuario_id = DV.doc_ver_atualizacao_usuario
										LEFT JOIN
											usuarios AS US_AP ON US_AP.usuario_id = DV.doc_ver_aprovado_usuario
										LEFT JOIN
											usuarios AS US_PU ON US_PU.usuario_id = DV.doc_ver_publicado_usuario
										LEFT JOIN
											usuarios AS US_RV ON US_RV.usuario_id = DV.doc_ver_revogado_usuario
										WHERE
											DV.doc_ver_doc = ".$ver_selecionar[$sistema_prefixo_campos."id"]."
											AND DV.doc_ver_excluido <> 'sim'";
					$exe_versoes = mysql_query($sql_versoes, $con) or die("Erro do MySQL[exe_versoes]: ".mysql_error());
					if(mysql_num_rows($exe_versoes)>0)
						{
							while($ver_versoes = mysql_fetch_array($exe_versoes))
								{
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["versao"] = $ver_versoes["doc_ver_versao"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["status"] = $ver_versoes["doc_ver_status"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["status_nome"] = $ver_versoes["doc_ver_status_nome"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["observacoes"] = $ver_versoes["doc_ver_observacoes"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["obs_aprovador"] = $ver_versoes["doc_ver_obs_aprovador"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["criacao_usuario"] = $ver_versoes["doc_ver_criacao_usuario"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["criacao_usuario_nome"] = $ver_versoes["doc_ver_criacao_usuario_nome"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["criacao"] = "Criado em ".mostra_data_completa_br($ver_versoes["doc_ver_criacao_data"])." por ".$ver_versoes["doc_ver_criacao_usuario_nome"]."<br><br>";
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["atualizacao_usuario"] = $ver_versoes["doc_ver_atualizacao_usuario"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["atualizacao_usuario_nome"] = $ver_versoes["doc_ver_atualizacao_usuario_nome"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["atualizacao"] = ($ver_versoes["doc_ver_atualizacao_data"]<>"0000-00-00 00:00:00") ? "Atualizado em ".mostra_data_completa_br($ver_versoes["doc_ver_atualizacao_data"])." por ".$ver_versoes["doc_ver_atualizacao_usuario_nome"]."<br><br>" : "";
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["aprovacao_usuario"] = $ver_versoes["doc_ver_aprovacao_usuario"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["aprovacao_usuario_nome"] = $ver_versoes["doc_ver_aprovacao_usuario_nome"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["aprovacao"] = ($ver_versoes["doc_ver_aprovado_data"]<>"0000-00-00 00:00:00") ? "Revisado/Aprovado em ".mostra_data_completa_br($ver_versoes["doc_ver_aprovado_data"])." por ".$ver_versoes["doc_ver_aprovado_usuario_nome"]."<br><br>" : "";
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["publicacao_usuario"] = $ver_versoes["doc_ver_publicacao_usuario"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["publicacao_usuario_nome"] = $ver_versoes["doc_ver_publicacao_usuario_nome"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["publicacao"] = ($ver_versoes["doc_ver_publicado_data"]<>"0000-00-00 00:00:00") ? "Publicado em ".mostra_data_completa_br($ver_versoes["doc_ver_publicado_data"])." por ".$ver_versoes["doc_ver_publicado_usuario_nome"]."<br><br>" : "";
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["revogacao_usuario"] = $ver_versoes["doc_ver_revogacao_usuario"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["revogacao_usuario_nome"] = $ver_versoes["doc_ver_revogacao_usuario_nome"];
									$rows["documentos_versoes"][$ver_versoes["doc_ver_id"]]["revogacao"] = ($ver_versoes["doc_ver_revogado_data"]<>"0000-00-00 00:00:00") ? "Revogado em ".mostra_data_completa_br($ver_versoes["doc_ver_revogado_data"])." por ".$ver_versoes["doc_ver_revogado_usuario_nome"]."<br><br>" : "";
								}
						}


					/*### HISTÓRICO DE LEITURA DO REGISTRO ###*/
					if($_REQUEST['operacao']=="leitura")
						{
							$sql_historico = "SELECT DISTINCT
														documentos_leituras.doc_ler_primeira_leitura,
														documentos_leituras.doc_ler_ultima_leitura,
														documentos_versoes.doc_ver_versao,
														usuarios.usuario_nome,
														usuarios.usuario_email
													FROM
														documentos_leituras
													INNER JOIN
														documentos_versoes ON documentos_versoes.doc_ver_id = documentos_leituras.doc_ler_doc_ver_id
																			AND documentos_versoes.doc_ver_excluido <> 'sim'
													INNER JOIN
														documentos ON documentos.doc_id = documentos_versoes.doc_ver_doc
																	AND documentos.doc_excluido <> 'sim'
													INNER JOIN
														usuarios ON usuarios.usuario_id = documentos_leituras.doc_ler_usuario_id
																AND usuarios.usuario_excluido <> 'sim'
													WHERE
														documentos.doc_id = ".$_REQUEST['chave_primaria']."
													ORDER BY
														documentos_leituras.doc_ler_ultima_leitura ASC";
							$exe_historico = mysql_query($sql_historico, $con) or die("Erro do MySQL[exe_historico]: ".mysql_error());
							if(mysql_num_rows($exe_historico)>0)
								{
									$rows[$num]["historico"] = '<table id="simple-table" class="table  table-bordered table-hover">
																	<thead>
																		<tr>
																			<th width="10%">Versão</th>
																			<th width="25%">Usuário</th>
																			<th width="25%">E-mail</th>
																			<th width="20%">Primeira leitura</th>
																			<th width="20%">Última leitura</th>
																		</tr>
																	</thead>

																	<tbody>';

									while($ver_historico = mysql_fetch_array($exe_historico))
										{
											$rows[$num]["historico"] .= "<tr>";

											$rows[$num]["historico"] .= "<td>";
											$rows[$num]["historico"] .= $ver_historico["doc_ver_versao"];
											$rows[$num]["historico"] .= "</td>";

											$rows[$num]["historico"] .= "<td>";
											$rows[$num]["historico"] .= $ver_historico["usuario_nome"];
											$rows[$num]["historico"] .= "</td>";

											$rows[$num]["historico"] .= "<td>";
											$rows[$num]["historico"] .= $ver_historico["usuario_email"];
											$rows[$num]["historico"] .= "</td>";

											$rows[$num]["historico"] .= "<td>";
											$rows[$num]["historico"] .= mostra_data_completa_br($ver_historico["doc_ler_primeira_leitura"]);
											$rows[$num]["historico"] .= "</td>";

											$rows[$num]["historico"] .= "<td>";
											$rows[$num]["historico"] .= mostra_data_completa_br($ver_historico["doc_ler_ultima_leitura"]);
											$rows[$num]["historico"] .= "</td>";

											$rows[$num]["historico"] .= "</tr>";
										}

									$rows[$num]["historico"] .= '</tbody></table>';
								}
							else
								{
									$rows[$num]["historico"] = "<h3>Não há registros de leitura deste documento</h3>";
								}

								$rows[$num]["sql_historico"] = $sql_historico;
								$rows[$num]["sql_historico"] = $sql_historico;
						}
					/*### HISTÓRICO DE LEITURA DO REGISTRO ###*/


					include($pasta_includes."/auth.inc.php");

					//RETORNA SE O USUÁRIO TER PERMISSÃO DE APROVADOR
					$rows["aprovador"] = $auth["documentos"]["aprovar"];

					//RETORNA SE O USUÁRIO TER PERMISSÃO DE PUBLICADOR
					$rows["publicador"] = $auth["documentos"]["publicar"];

				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/














	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{
			//print_r($_REQUEST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['doc_secao'] as $secao_key => $secao_marcada)
				{
					$doc_secao .= $separador_string.$secao_marcada;
				}
			$doc_secao .= $separador_string;
			$_POST['doc_secao'] = $doc_secao;
			$_POST["doc_status"] = $_POST["doc_status_hidden"];
			unset($_POST['doc_status_hidden'], $_POST['doc_nivel'], $_POST['usuario_nivel']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/






			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			//echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_registro = mysql_insert_id($con);

			/*### SE INSERIU REGISTRA A PERMISSÃO PADRÃO INICIAL APENAS PARA O USUÁRIO CRIADOR ###*/
			if($exe_inserir)
				{
					$sql_insere_permissao = "INSERT INTO documentos_usuarios (doc_usuario_usuario,doc_usuario_doc) VALUES (".$_SESSION["login"]["id"].", ".$id_registro.")";
					$exe_insere_permissao = mysql_query($sql_insere_permissao, $con) or die("Erro do MySQL[exe_insere_permissao]: ".mysql_error());

					//RETORNA TEXTO OK
					$result = "OK";
				}
			/*### SE INSERIU REGISTRA A PERMISSÃO PADRÃO INICIAL APENAS PARA O USUÁRIO CRIADOR ###*/
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['doc_secao'] as $secao_key => $secao_marcada)
				{
					$doc_secao .= $separador_string.$secao_marcada;
				}
			$doc_secao .= $separador_string;
			$_POST['doc_secao'] = $doc_secao;
			$_POST["doc_status"] = $_POST["doc_status_hidden"];
			unset($_POST['doc_status_hidden'], $_POST['doc_nivel'], $_POST['usuario_nivel']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/









	/*#### AÇÕES QUANDO A OPERAÇÃO FOR AUMENTAR/REDUZIR A ORDEM DO TEXTO ####*/
	if(($_REQUEST['operacao']=="txt-up")||($_REQUEST['operacao']=="txt-down"))
		{
			//print_r($_POST);

			$incremento = ($_REQUEST['operacao']=="txt-up") ? "+1" : "-1";

			/*#### CONSTRÓI A QUERY ####*/
			$sql_incremento = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."ordem = ".$sistema_prefixo_campos."ordem ".$incremento.", ".
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_incremento: ".$sql_incremento;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_incremento = mysql_query($sql_incremento, $con) or die("Erro do MySQL[exe_incremento]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_incremento) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR AUMENTAR/REDUZIR A ORDEM DO TEXTO ####*/












	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
























	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM USUÁRIO A UM DOCUMENTO ####*/
	if($_REQUEST['operacao']=="associar_usuario")
		{
			// print_r($_REQUEST);

			if($_REQUEST['usuario']=="todos") $sql_remover = "DELETE FROM documentos_usuarios WHERE doc_usuario_doc = ".$_REQUEST['doc'];
			else $sql_remover = "DELETE FROM documentos_usuarios WHERE doc_usuario_usuario = ".$_REQUEST['usuario']." AND doc_usuario_doc = ".$_REQUEST['doc'];
			// echo "<br>sql_remover: ".$sql_remover;
			$exe_remover = mysql_query($sql_remover, $con) or die("Erro do MySQL[exe_remover]: ".mysql_error());


			if($_REQUEST['selecionado']=="true")
				{
					if($_REQUEST['usuario']=="todos") $sql_associar = "INSERT INTO documentos_usuarios (doc_usuario_doc, doc_usuario_usuario) SELECT ".$_REQUEST['doc']." AS doc, vis_usuario_id FROM visao_usuarios";
					else $sql_associar = "INSERT INTO documentos_usuarios (doc_usuario_doc, doc_usuario_usuario) VALUES (".$_REQUEST['doc'].", ".$_REQUEST['usuario'].")";
					$exe_associar = mysql_query($sql_associar, $con) or die("Erro do MySQL[exe_associar]: ".mysql_error());
				}


			$rows["resultado"] = "OK";
			$rows["usuario"] = $_REQUEST['usuario'];
			$rows["doc"] = $_REQUEST['doc'];
			$rows["selecionado"] = $_REQUEST['selecionado'];

			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM USUÁRIO A UM DOCUMENTO ####*/
























	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM NÍVEL A UM DOCUMENTO ####*/
	if($_REQUEST['operacao']=="associar_nivel")
		{
			// print_r($_REQUEST);

			if($_REQUEST['nivel']=="todos") $sql_remover = "DELETE FROM documentos_niveis WHERE doc_nivel_doc = ".$_REQUEST['doc'];
			else $sql_remover = "DELETE FROM documentos_niveis WHERE doc_nivel_nivel = ".$_REQUEST['nivel']." AND doc_nivel_doc = ".$_REQUEST['doc'];
			// echo "<br>sql_remover: ".$sql_remover;
			$exe_remover = mysql_query($sql_remover, $con) or die("Erro do MySQL[exe_remover]: ".mysql_error());


			if($_REQUEST['selecionado']=="true")
				{
					if($_REQUEST['nivel']=="todos") $sql_associar = "INSERT INTO documentos_niveis (doc_nivel_doc, doc_nivel_nivel) SELECT ".$_REQUEST['doc']." AS doc, vis_nivel_id FROM visao_niveis";
					else $sql_associar = "INSERT INTO documentos_niveis (doc_nivel_doc, doc_nivel_nivel) VALUES (".$_REQUEST['doc'].", ".$_REQUEST['nivel'].")";
					$exe_associar = mysql_query($sql_associar, $con) or die("Erro do MySQL[exe_associar]: ".mysql_error());
				}


			$rows["resultado"] = "OK";
			$rows["nivel"] = $_REQUEST['nivel'];
			$rows["doc"] = $_REQUEST['doc'];
			$rows["selecionado"] = $_REQUEST['selecionado'];

			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM NÍVEL A UM DOCUMENTO ####*/
























	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM GRUPO A UM DOCUMENTO ####*/
	if($_REQUEST['operacao']=="associar_grupo")
		{
			// print_r($_REQUEST);

			if($_REQUEST['grupo']=="todos") $sql_remover = "DELETE FROM documentos_grupos WHERE doc_grupo_doc = ".$_REQUEST['doc'];
			else $sql_remover = "DELETE FROM documentos_grupos WHERE doc_grupo_grupo = ".$_REQUEST['grupo']." AND doc_grupo_doc = ".$_REQUEST['doc'];
			// echo "<br>sql_remover: ".$sql_remover;
			$exe_remover = mysql_query($sql_remover, $con) or die("Erro do MySQL[exe_remover]: ".mysql_error());


			if($_REQUEST['selecionado']=="true")
				{
					if($_REQUEST['grupo']=="todos") $sql_associar = "INSERT INTO documentos_grupos (doc_grupo_doc, doc_grupo_grupo) SELECT ".$_REQUEST['doc']." AS doc, vis_grupo_id FROM visao_grupos";
					else $sql_associar = "INSERT INTO documentos_grupos (doc_grupo_doc, doc_grupo_grupo) VALUES (".$_REQUEST['doc'].", ".$_REQUEST['grupo'].")";
					$exe_associar = mysql_query($sql_associar, $con) or die("Erro do MySQL[exe_associar]: ".mysql_error());
				}


			$rows["resultado"] = "OK";
			$rows["grupo"] = $_REQUEST['grupo'];
			$rows["doc"] = $_REQUEST['doc'];
			$rows["selecionado"] = $_REQUEST['selecionado'];

			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM GRUPO A UM DOCUMENTO ####*/

















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SALVAR UMA VERSÃO DE DOCUMENTO ####*/
	if(($_REQUEST['operacao']=="salvar-versao")||($_REQUEST['operacao']=="salvar-encaminhar")||($_REQUEST['operacao']=="publicar-versao")||($_REQUEST['operacao']=="revogar-versao"))
		{
			// print_r($_REQUEST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			$_POST["doc_ver_vigencia"] = mostra_data_us($_POST["doc_ver_vigencia"]);
			$_POST["doc_ver_leitura"] = $_POST["doc_ver_leitura_hidden"];
			$status_original = $_POST['doc_ver_status'];
			unset($_POST['doc_nivel'], $_POST['usuario_nivel'], $_POST["doc_ver_leitura_hidden"]);


			// VERIFICA SE ESTÁ ENCAMINHANDO PARA APROVAÇÃO E TROCA O STATUS PARA AGUARDANDO APROVAÇÃO
			if(($_POST['doc_ver_status']==0)&&($_REQUEST['operacao']=="salvar-encaminhar")) $_POST['doc_ver_status'] = 10;

			// VERIFICA SE ESTÁ ENCAMINHANDO PARA REVISÃO/CORREÇÃO E TROCA O STATUS PARA REVISAR/CORRIGIR
			if(($_POST['doc_ver_status']==20)&&($_REQUEST['operacao']=="salvar-encaminhar")) $_POST['doc_ver_status'] = 30;

			//SE O STATUS QUE FOR SALVAR É EM ELABORAÇÃO, REMOVE A OBS APROVADOR
			if($_POST['doc_ver_status']==0) unset($_POST['doc_ver_obs_aprovador']);

			//SE A OPERAÇÃO FOR PUBLICAR-VERSAO TROCA O STATUS PARA VIGENTE
			if($_REQUEST['operacao']=="publicar-versao") $_POST['doc_ver_status'] = 50;

			//SE A OPERAÇÃO FOR REVOGAR-VERSAO TROCA O STATUS PARA REVOGADO
			if($_REQUEST['operacao']=="revogar-versao") $_POST['doc_ver_status'] = 60;

			//SE O STATUS FOR EM APROVAÇÃO, REVISAR/CORRIGIR OU APROVADO, DETERMINA A DATA E USUÁRIO
			if(($_POST['doc_ver_status']==20)||($_POST['doc_ver_status']==30)||($_POST['doc_ver_status']==40))
				{
					$aprovacao_data = $hoje_data_us;
					$aprovacao_usuario = $_SESSION['login']['id'];
				}

			/*#### VERIFICA SE TEM OUTRA VERSÃO VIGENTE JÁ INSERIDA ####*/
			if($_POST['doc_ver_status']==50)
				{
					$sql_versao_vigente = "SELECT doc_ver_id FROM documentos_versoes WHERE doc_ver_doc = '".$_POST['doc_id']."' AND doc_ver_status = '50'";
					// echo "\nsql_versao_vigente: ".$sql_versao_vigente;
					$exe_versao_vigente = mysql_query($sql_versao_vigente, $con) or die("Erro do MySQL[exe_versao_vigente]: ".mysql_error());
					if(mysql_num_rows($exe_versao_vigente)>0)
						{
							$permitir_insercao = "nao";
							$msg_erro = "Já existe uma versão vigente para este documento, revogue a versão atual e insira novamente.";
						}
				}
			/*#### VERIFICA SE TEM OUTRA VERSÃO VIGENTE JÁ INSERIDA ####*/

			if($permitir_insercao<>"nao")
				{
					if($_POST['doc_ver_id']>0)
						{
							/*### APAGA O ANTIGO ARQUIVO E TRATA O NOME DO NOVO ARQUIVO ENVIADO ###*/
							if(($_POST['arquivo_enviado']>1)||(($_POST['arquivo_enviado']==1)&&($_POST['arquivo_filename']<>"")))
								{
									@unlink($GLOBALS['pasta_documentos']."/".$_POST['doc_ver_arquivo']);
									if($status_original==0) @unlink($GLOBALS['pasta_documentos']."/".$_POST['doc_ver_arquivo_original']);
									$_POST['doc_ver_arquivo'] = t5f_sanitize_filename($_POST['arquivo_filename']);
									if($status_original==0) $_POST['doc_ver_arquivo_original'] = $_POST['doc_ver_arquivo'];
									unset($_POST['doc_ver_texto'], $_POST['arquivo_filename']);
									$atualizar_arquivo = "sim";
								}
							else
								{
									unset($_POST['arquivo_filename']);
								}
							/*### APAGA O ANTIGO ARQUIVO E TRATA O NOME DO NOVO ARQUIVO ENVIADO ###*/



							//ARMAZENA O TEXTO ORIGINAL DO DOCUMENTO
							$linha_doc_ver_texto_original = ($status_original==0) ? "doc_ver_texto_original = '".$_POST['doc_ver_texto']."', " : "";

							//ARMAZENA O ARQUIVO ORIGINAL DO DOCUMENTO
							$linha_doc_ver_arquivo_original = ($status_original==0) ? "doc_ver_arquivo_original = '".$_POST['doc_ver_arquivo_original']."', " : "";

							//SQL DE ATUALIZAÇÃO DO DOCUMENTO SE FOR COM STATUS DE VIGENTE OU REVOGADO
							if(($_POST['doc_ver_status']==50)||($_POST['doc_ver_status']==60))
								{

									//SE FOR STATUS DE VIGENTE, ATUALIZA APENAS A DATA E USUÁRIO DE PUBLICAÇÃO E A DATA DE VIGÊNCIA
									if($_POST['doc_ver_status']==50)
										{
											//ATUALIZA A DATA DE PUBLICAÇÃO
											$linha_sql_publicacao_revogacao = ",doc_ver_publicado_data = '".$hoje_data_us."', doc_ver_publicado_usuario = ".$_SESSION['login']['id'];

											////SE A DATA DE VIGÊNCIA NÃO FOI INFORMADA, MARCA COM A DATA ATUAL
											if($_POST['doc_ver_vigencia']=="") $linha_sql_publicacao_revogacao .= ",doc_ver_vigencia = '".$hoje_data_us."' ";
										}
									//SE FOR STATUS DE REVOGAR, ATUALIZA APENAS A DATA E USUÁRIO DE REVOGAÇÃO
									elseif($_POST['doc_ver_status']==60)
										{
											$linha_sql_publicacao_revogacao = ",doc_ver_revogado_data = '".$hoje_data_us."', doc_ver_revogado_usuario = ".$_SESSION['login']['id'];
										}


									$sql_dados_versao = "UPDATE documentos_versoes SET doc_ver_vigencia = '".$_POST['doc_ver_vigencia']."',
																						doc_ver_status = '".$_POST['doc_ver_status']."'"
																						.$linha_sql_publicacao_revogacao."
																					WHERE
																						doc_ver_id = '".$_POST["doc_ver_id"]."'";
								}

							//SQL DE ATUALIZAÇÃO DO DOCUMENTO
							else
								{
									$sql_dados_versao = "UPDATE documentos_versoes SET doc_ver_tipo = '".$_POST['doc_ver_tipo']."',
																						doc_ver_doc = '".$_POST['doc_id']."',
																						doc_ver_versao = '".$_POST['doc_ver_versao']."',
																						doc_ver_vigencia = '".$_POST['doc_ver_vigencia']."',
																						doc_ver_obs_aprovador = '".$_POST['doc_ver_obs_aprovador']."',
																						doc_ver_observacoes = '".$_POST['doc_ver_observacoes']."',
																						doc_ver_texto = '".$_POST['doc_ver_texto']."', "
																						.$linha_doc_ver_texto_original.
																						"doc_ver_arquivo = '".$_POST['doc_ver_arquivo']."', "
																						.$linha_doc_ver_arquivo_original.
																						"doc_ver_status = '".$_POST['doc_ver_status']."',
																						doc_ver_leitura = '".$_POST['doc_ver_leitura']."',
																						doc_ver_aprovado_data = '".$aprovacao_data."',
																						doc_ver_aprovado_usuario = '".$aprovacao_usuario."',
																						doc_ver_publicado_data = '".$publicacao_data."',
																						doc_ver_publicado_usuario = '".$publicacao_usuario."',
																						doc_ver_revogado_data = '".$revogacao_data."',
																						doc_ver_revogado_usuario = '".$revogacao_usuario."',
																						doc_ver_atualizacao_data = '".$hoje_data_us."',
																						doc_ver_atualizacao_usuario ='".$_SESSION["login"]["id"]."'
																					WHERE
																						doc_ver_id = '".$_POST["doc_ver_id"]."'";
								}


						}
					else
						{
							/*### TRATA O NOME DO ARQUIVO ENVIADO ###*/
							if($_POST['doc_ver_tipo']=="arquivo")
								{
									$_POST['doc_ver_arquivo'] = t5f_sanitize_filename($_POST['arquivo_filename']);
									unset($_POST['doc_ver_texto'], $_POST['arquivo_filename']);
									$atualizar_arquivo = "sim";
								}
							elseif($_POST['doc_ver_tipo']=="texto")
								{
									unset($_POST['arquivo_filename'], $_POST['doc_ver_arquivo']);
								}
							/*### TRATA O NOME DO ARQUIVO ENVIADO ###*/


							//SE FOR STATUS DE VIGENTE, INFORMA A DATA E USUÁRIO DE PUBLICAÇÃO
							if($_POST['doc_ver_status']==50)
								{
									$publicacao_data = $hoje_data_us;
									$publicacao_usuario = $_SESSION['login']['id'];
								}
							//SE FOR STATUS DE REVOGAR, INFORMA A DATA E USUÁRIO DE REVOGAÇÃO
							elseif($_POST['doc_ver_status']==60)
								{
									$revogacao_data = $hoje_data_us;
									$revogacao_usuario = $_SESSION['login']['id'];
								}


							/*#### QUERY DE INSERIR NOVA VERSÃO DE DOCUMENTO ####*/
							$sql_dados_versao = "INSERT INTO documentos_versoes (doc_ver_pid,
																					doc_ver_tipo,
																					doc_ver_doc,
																					doc_ver_versao,
																					doc_ver_vigencia,
																					doc_ver_obs_aprovador,
																					doc_ver_observacoes,
																					doc_ver_texto,
																					doc_ver_texto_original,
																					doc_ver_arquivo,
																					doc_ver_arquivo_original,
																					doc_ver_status,
																					doc_ver_leitura,
																					doc_ver_aprovado_data,
																					doc_ver_aprovado_usuario,
																					doc_ver_publicado_data,
																					doc_ver_publicado_usuario,
																					doc_ver_revogado_data,
																					doc_ver_revogado_usuario,
																					doc_ver_criacao_data,
																					doc_ver_criacao_usuario)
																				VALUES
																					('".$_POST['doc_ver_pid']."', ".
																					"'".$_POST['doc_ver_tipo']."', ".
																					"'".$_POST['doc_id']."', ".
																					"'".$_POST['doc_ver_versao']."', ".
																					"'".$_POST['doc_ver_vigencia']."', ".
																					"'".$_POST['doc_ver_obs_aprovador']."', ".
																					"'".$_POST['doc_ver_observacoes']."', ".
																					"'".$_POST['doc_ver_texto']."', ".
																					"'".$_POST['doc_ver_texto']."', ".
																					"'".$_POST['doc_ver_arquivo']."', ".
																					"'".$_POST['doc_ver_arquivo']."', ".
																					"'".$_POST['doc_ver_status']."', ".
																					"'".$_POST['doc_ver_leitura']."', ".
																					"'".$aprovacao_data."', ".
																					"'".$aprovacao_usuario."', ".
																					"'".$publicacao_data."', ".
																					"'".$publicacao_usuario."', ".
																					"'".$revogacao_data."', ".
																					"'".$revogacao_usuario."', ".
																					"'".$hoje_data_us."', ".
																					"'".$_SESSION["login"]["id"]."')";
							/*#### QUERY DE INSERIR NOVA VERSÃO DE DOCUMENTO ####*/
						}

					// print_r($_POST);

					// echo "<br>sql_dados_versao: ".$sql_dados_versao;

					//SE FOR UMA VERSÃO PARA APROVAÇÃO, MARCA PARA ENVIO DE E-MAIL
					if(($_POST['doc_ver_status']==10)&&($_REQUEST['operacao']=="salvar-encaminhar")) $envia_email = "sim";

					//SE FOR UMA VERSÃO PARA REVISAR/CORRIGIR, APROVADO, VIGENTE OU REVOGADO, MARCA PARA ENVIO DE E-MAIL
					if((($_POST['doc_ver_status']==30)||($_POST['doc_ver_status']==40)||($_POST['doc_ver_status']==50)||($_POST['doc_ver_status']==60))&&($_REQUEST['operacao']=="salvar-encaminhar")) $envia_email = "sim";

					//EXECUTA A QUERY
					$exe_salvar_versao = mysql_query($sql_dados_versao, $con) or die("Erro do MySQL[exe_salvar_versao]: ".mysql_error());
					$id_registro = ($_POST['doc_ver_id']>0) ? $_POST['doc_ver_id'] : mysql_insert_id($con);
				}

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_salvar_versao)
				{
					$rows["salvo"] = "OK";

					//ATUALIZA O CAMPO DO NOME DE ARQUIVO, SE FOR TIPO ARQUIVO
					if(($_POST['doc_ver_tipo']=="arquivo")&&($atualizar_arquivo=="sim"))
						{
							$linha_doc_ver_arquivo_original = ($status_original==0) ? " ,doc_ver_arquivo_original = CONCAT('".str_pad($_POST['doc_id'],4,0,STR_PAD_LEFT)."_".str_pad($id_registro,4,0,STR_PAD_LEFT)."','_ORIG_',doc_ver_arquivo_original) " : "";

							$sql_atualiza_arquivo = "UPDATE
															documentos_versoes
														SET
															doc_ver_arquivo = CONCAT('".str_pad($_POST['doc_id'],4,0,STR_PAD_LEFT)."_".str_pad($id_registro,4,0,STR_PAD_LEFT)."','_',doc_ver_arquivo) "
															.$linha_doc_ver_arquivo_original.
														"WHERE
															doc_ver_id = ".$id_registro;
							// echo "<br>sql_atualiza_arquivo: ".$sql_atualiza_arquivo;
							$exe_atualiza_arquivo = mysql_query($sql_atualiza_arquivo, $con) or die("Erro do MySQL[exe_atualiza_arquivo]: ".mysql_error());

							if($exe_atualiza_arquivo)
								{
									//MOVE O ARQUIVO PARA A PASTA FINAL, JÁ RENOMEANDO
									rename($GLOBALS['pasta_documentos']."/tmp/".$_POST['doc_ver_pid'],$GLOBALS['pasta_documentos']."/".str_pad($_POST['doc_id'],4,0,STR_PAD_LEFT)."_".str_pad($id_registro,4,0,STR_PAD_LEFT)."_".$_POST['doc_ver_arquivo']);

									//SE O STATUS ORIGINAL FOR EM ELABORAÇÃO, E O ARQUIVO FOI SUBSTITUÍDO
									//ATUALIZA O ARQUIVO ORIGINAL
									if($status_original==0)
										{
											copy($GLOBALS['pasta_documentos']."/".str_pad($_POST['doc_id'],4,0,STR_PAD_LEFT)."_".str_pad($id_registro,4,0,STR_PAD_LEFT)."_".$_POST['doc_ver_arquivo'], $GLOBALS['pasta_documentos']."/".str_pad($_POST['doc_id'],4,0,STR_PAD_LEFT)."_".str_pad($id_registro,4,0,STR_PAD_LEFT)."_ORIG_".$_POST['doc_ver_arquivo']);
										}
								}
						}

					//SETA A VARIÁVEL DE ENVIO DE E-MAIL COMO VAZIA
					$rows["dados_email"]["envia_email"] = "";

					//ENVIA O E-MAIL PARA INFORMAR DA APROVAÇÃO DO DOCUMENTO
					if($envia_email=="sim")
						{
							$rows["dados_email"]["envia_email"] = "sim";

							$sql_dados_documento = "SELECT
															DOC.doc_id,
															DOC.doc_titulo,
															DOC_V.doc_ver_versao,
															DOC_V.doc_ver_criacao_data,
															DOC_V.doc_ver_vigencia,
															DOC_V.doc_ver_status,
															DOC_V.doc_ver_criacao_usuario,
															USR_C.usuario_nome,
															DOC_V.doc_ver_observacoes,
															DOC_V.doc_ver_obs_aprovador,
															DOC_V.doc_ver_id,
															DOC_V.doc_ver_aprovado_data,
															DOC_V.doc_ver_publicado_data,
															DOC_V.doc_ver_revogado_data,
															USR_AP.usuario_nome AS usuario_aprovado_nome,
															USR_PU.usuario_nome AS usuario_publicado_nome,
															USR_RV.usuario_nome AS usuario_revogado_nome
														FROM
															documentos_versoes DOC_V
														INNER JOIN
															documentos DOC ON DOC.doc_id = DOC_V.doc_ver_doc
														INNER JOIN
															usuarios USR_C ON USR_C.usuario_id = DOC_V.doc_ver_criacao_usuario
														LEFT JOIN
															usuarios USR_AP on USR_AP.usuario_id = DOC_V.doc_ver_aprovado_usuario
														LEFT JOIN
															usuarios USR_PU on USR_PU.usuario_id = DOC_V.doc_ver_publicado_usuario
														LEFT JOIN
															usuarios USR_RV on USR_RV.usuario_id = DOC_V.doc_ver_revogado_usuario
														WHERE
															DOC_V.doc_ver_id = ".$id_registro;
							// echo "\nsql_dados_documento: ".$sql_dados_documento;
							$exe_dados_documento = mysql_query($sql_dados_documento, $con) or die("Erro do MySQL[exe_dados_documento]: ".mysql_error());
							$ver_dados_documento = mysql_fetch_array($exe_dados_documento);

							$array_niveis = json_decode(str_replace("\\","", $_POST['niveis_superiores']));
							$array_supervisores = json_decode(str_replace("\\","", $_POST['supervisores_niveis']));

							// echo "\narray_niveis: ".$array_niveis;
							// echo "\narray_supervisores: ".$array_supervisores;

							if(count($array_niveis)>0)
								{
									foreach ($array_niveis as $key => $nivel) {
										$sql_in_niveis .= $nivel.", ";
									}
								}

							if((count($array_supervisores)>0)&&(count($array_niveis)>0))
								{
									foreach ($array_niveis as $key => $supervisor) {
										$sql_in_supervisores .= $supervisor.", ";
									}
								}

							// echo "\nsql_in_supervisores: ".$sql_in_supervisores;


							//SE A AÇÃO FOR ENVIAR AVISO PARA OS APROVADORES
							if(($_POST['doc_ver_status']==10)&&($sql_in_supervisores<>""))
								{
									$remetente_email = "J.DREL - Intranet";
									$assunto_email = "INTRANET - Novo documento para aprovação";

									$conteudo_email = "Olá, %%NOME%%,";
									$conteudo_email .= "<br><br>Um documento foi inserido e aguarda aprovação, abaixo as informações:";
									$conteudo_email .= "<br><br><strong>ID Documento:</strong> ".$ver_dados_documento["doc_id"];
									$conteudo_email .= "<br><strong>Documento:</strong> ".$ver_dados_documento["doc_titulo"];
									$conteudo_email .= "<br><strong>Versão:</strong> ".$ver_dados_documento["doc_ver_versao"];
									$conteudo_email .= "<br><strong>Data:</strong> ".mostra_data_completa_br($ver_dados_documento["doc_ver_criacao_data"]);
									$conteudo_email .= "<br><strong>Vigência:</strong> ".mostra_data_br($ver_dados_documento["doc_ver_vigencia"]);
									$conteudo_email .= "<br><strong>Autor:</strong> ".$ver_dados_documento["usuario_nome"];
									$conteudo_email .= "<br><strong>Observações:</strong> ".$ver_dados_documento["doc_ver_observacoes"];
									$conteudo_email .= "<br><br>Para acessar o documento <a href='".$http_admin."/home.php#documentos/gerenciar|jdrel|doc_id=".$ver_dados_documento["doc_id"]."|doc_ver_id=".$ver_dados_documento["doc_ver_id"]."' target='_blank'>clique aqui</a>.";

									$msg_retorno_email = "O documento foi enviado para aprovação.";

									$sql_emails = "SELECT DISTINCT EMAILS.EMAIL, EMAILS.NOME FROM (SELECT DISTINCT
															user_perm_email AS EMAIL,
															user_perm_nome AS NOME
														FROM
															visao_usuarios_permissoes USR
														INNER JOIN
															visao_usuarios_niveis_grupos NVG ON NVG.vis_usr_nv_grp_usuario_id = USR.user_perm_usuario_id
														WHERE
															NVG.vis_usr_nv_grp_nivel_id IN (".substr($sql_in_niveis, 0, -2).")
															AND USR.user_perm_email <> ''
														GROUP BY
															user_perm_email,
															user_perm_nome
														HAVING
															GROUP_CONCAT(CONCAT(user_perm_secao,'-',user_perm_operacao) ORDER BY user_perm_secao) LIKE '%documentos-alterar%'
															AND GROUP_CONCAT(CONCAT(user_perm_secao,'-',user_perm_operacao) ORDER BY user_perm_secao) LIKE '%documentos-gerenciar%'
															AND GROUP_CONCAT(CONCAT(user_perm_secao,'-',user_perm_operacao) ORDER BY user_perm_secao) LIKE '%documentos-aprovar%'
													UNION ALL
														SELECT DISTINCT
															user_perm_email AS EMAIL,
															user_perm_nome AS NOME
														FROM
															visao_usuarios_permissoes USR
														WHERE
															USR.user_perm_usuario_id IN (".substr($sql_in_supervisores, 0, -2).")
															AND USR.user_perm_email <> ''
														GROUP BY
															user_perm_email,
															user_perm_nome
														HAVING
															GROUP_CONCAT(CONCAT(user_perm_secao,'-',user_perm_operacao) ORDER BY user_perm_secao) LIKE '%documentos-alterar%'
															AND GROUP_CONCAT(CONCAT(user_perm_secao,'-',user_perm_operacao) ORDER BY user_perm_secao) LIKE '%documentos-gerenciar%'
															AND GROUP_CONCAT(CONCAT(user_perm_secao,'-',user_perm_operacao) ORDER BY user_perm_secao) LIKE '%documentos-aprovar%') EMAILS";
								}





							//SE A AÇÃO FOR ENVIAR AVISO PARA O CRIADOR
							elseif(($_POST['doc_ver_status']==30)||($_POST['doc_ver_status']==40)||($_POST['doc_ver_status']==50)||($_POST['doc_ver_status']==60))
								{
									$remetente_email = "J.DREL - Intranet";
									$assunto_email = "INTRANET - O status do seu documento foi modificado";

									$conteudo_email = "Olá, %%NOME%%,";
									$conteudo_email .= "<br><br>Um documento que você criou, sofreu alteração de status:";
									$conteudo_email .= "<br><br><strong>ID Documento:</strong> ".$ver_dados_documento["doc_id"];
									$conteudo_email .= "<br><strong>Documento:</strong> ".$ver_dados_documento["doc_titulo"];
									$conteudo_email .= "<br><strong>Versão:</strong> ".$ver_dados_documento["doc_ver_versao"];
									$conteudo_email .= "<br><strong>Data:</strong> ".mostra_data_completa_br($ver_dados_documento["doc_ver_criacao_data"]);
									$conteudo_email .= "<br><strong>Vigência:</strong> ".mostra_data_br($ver_dados_documento["doc_ver_vigencia"]);
									$conteudo_email .= "<br><br><strong>Data da modificação:</strong> ".mostra_data_completa_br($ver_dados_documento["doc_ver_aprovado_data"]);
									$conteudo_email .= "<br><strong>Aprovador:</strong> ".$ver_dados_documento["usuario_aprovado_nome"];
									$conteudo_email .= "<br><br><strong>Novo status:</strong> ".$_SESSION["guarda_config"]["titulo"]["todas"]["status_versoes"][$ver_dados_documento["doc_ver_status"]];
									$conteudo_email .= "<br><br><strong>Observações do aprovador:</strong> ".$ver_dados_documento["doc_ver_obs_aprovador"];
									$conteudo_email .= "<br><br>Para acessar o documento <a href='".$http_admin."/home.php#documentos/gerenciar|jdrel|doc_id=".$ver_dados_documento["doc_id"]."|doc_ver_id=".$ver_dados_documento["doc_ver_id"]."' target='_blank'>clique aqui</a>.";

									$msg_retorno_email = "Alteração efetuada. Um e-mail foi encaminhado ao autor avisando a modificação.";

									$sql_emails = "SELECT DISTINCT
															vis_usuario_email AS EMAIL,
															vis_usuario_nome AS NOME
														FROM
															visao_usuarios
														WHERE
															vis_usuario_id = ".$ver_dados_documento["doc_ver_criacao_usuario"];
								}


							if($sql_emails<>"")
								{
									$exe_emails = mysql_query($sql_emails, $con) or die("Erro do MySQL[sql_emails]: ".mysql_error());

									if(mysql_num_rows($exe_emails)>0)
										{
											require $GLOBALS['pasta_lib'].'/phpmailer/PHPMailerAutoload.php';

											while($ver_emails = mysql_fetch_array($exe_emails))
												{
													$num_msg++;
													$conteudo_mensagem = str_replace("%%NOME%%", $ver_emails["NOME"], $conteudo_email);
													// $rows["msg_envio_email"] = envia_email("J.DREL - Intranet", $ver_emails["EMAIL"], $ver_emails["NOME"], "", $assunto_email, $conteudo_mensagem, $msg_retorno_email, "", "nao");

													$rows["destino_email"][$num_msg]["email"] = $ver_emails["EMAIL"];
													$rows["destino_email"][$num_msg]["nome"] = $ver_emails["NOME"];
													$rows["destino_email"][$num_msg]["conteudo_mensagem"] = $conteudo_mensagem;
													$rows["dados_email"]["remetente"] = $remetente_email;
													$rows["dados_email"]["assunto"] = $assunto_email;
													$rows["dados_email"]["msg_retorno_email"] = $msg_retorno_email;
												}
										}
									// $rows["sql_emails"] = $sql_emails;
								}
						}

						// $rows["sql_dados_documento"] = $sql_dados_documento;
						// $rows["sql_emails"] = $sql_emails;
				}
			else
				{
					$rows["salvo"] = "NOK";
				}

			// $rows["sql_dados_versao"] = $sql_dados_versao;
			// $rows["POST"] = print_r($_POST, true);
			$rows["msg_erro"] = $msg_erro;

			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SALVAR UMA NOVA VERSÃO DE DOCUMENTO ####*/





























	/*#### AÇÕES QUANDO A OPERAÇÃO FOR PARA CRIAR UMA NOVA VERSÃO BASEADA OU VISUALIZAR A VERSÃO DE UM DOCUMENTO ####*/
	if(($_REQUEST['operacao']=="ver-versao")||($_REQUEST['operacao']=="nova-versao-baseada"))
		{
			// print_r($_REQUEST);

			if($_REQUEST['operacao']=="nova-versao-baseada")
				{
					$sql_nova_versao = "INSERT INTO documentos_versoes (doc_ver_pid,
																		doc_ver_tipo,
																		doc_ver_doc,
																		doc_ver_versao,
																		doc_ver_vigencia,
																		doc_ver_texto,
																		doc_ver_texto_original,
																		doc_ver_arquivo,
																		doc_ver_arquivo_original,
																		doc_ver_leitura,
																		doc_ver_criacao_data,
																		doc_ver_criacao_usuario)
																		SELECT
																			'".md5(uniqid(microtime(),1)).getmypid()."' as doc_ver_pid,
																			doc_ver_tipo,
																			doc_ver_doc,
																			doc_ver_versao,
																			doc_ver_vigencia,
																			doc_ver_texto,
																			doc_ver_texto,
																			doc_ver_arquivo,
																			doc_ver_arquivo,
																			doc_ver_leitura,
																			'".$hoje_data_us."' as doc_ver_criacao_data,
																			'".$_SESSION['login']['id']."' as doc_ver_criacao_usuario
																		FROM
																			documentos_versoes WHERE doc_ver_id = ".$_REQUEST['chave_primaria'];
					// echo "\nsql_nova_versao: ".$sql_nova_versao;
					$exe_nova_versao = mysql_query($sql_nova_versao, $con) or die("Erro do MySQL[sql_nova_versao]: ".mysql_error());
					$id_nova_versao = mysql_insert_id($con);

					$_POST['chave_primaria'] = $id_nova_versao;
					$rows["doc_ver_novo_id"] = $id_nova_versao;
				}

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE documentos_versoes";
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/



			/*#### CONSTRÓI A QUERY ####*/
			$sql_versao = "SELECT
									*,
									USR_AP.usuario_nome AS usuario_aprovado_nome,
									USR_PU.usuario_nome AS usuario_publicado_nome,
									USR_RV.usuario_nome AS usuario_revogado_nome
								FROM
									documentos_versoes DOC_V
								INNER JOIN
									documentos DOC ON DOC.doc_id = DOC_V.doc_ver_doc
								INNER JOIN
									configuracoes CFG ON CFG.configuracao_valor = DOC_V.doc_ver_status
														AND CFG.configuracao_categoria = 'status_versoes'
								LEFT JOIN
									usuarios USR_AP on USR_AP.usuario_id = DOC_V.doc_ver_aprovado_usuario
								LEFT JOIN
									usuarios USR_PU on USR_PU.usuario_id = DOC_V.doc_ver_publicado_usuario
								LEFT JOIN
									usuarios USR_RV on USR_RV.usuario_id = DOC_V.doc_ver_revogado_usuario
								WHERE
									DOC_V.doc_ver_id = ".$_POST['chave_primaria'];
			// echo "<br>sql_versao: ".$sql_versao;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_versao = mysql_query($sql_versao, $con) or die("Erro do MySQL[exe_versao]: ".mysql_error());
			while($ver_versao = mysql_fetch_array($exe_versao))
				{
					foreach ($campos_tabela as $campo => $valor) {
						$rows[$campo] = $ver_versao[$campo];
					}

					//MONTA O HTML COM O LINK DO ARQUIVO, SE EXISTIR
					if($ver_versao["doc_ver_arquivo"]<>"")
						{
							$rows["arquivo_existente"] .= "<div style='margin-bottom:10px;'>";
							if($ver_versao["doc_ver_status"]>=20) $rows["arquivo_existente"] .= "<i class='fa fa-file-o'></i> Arquivo original: <a target='_blank' href='".$GLOBALS['http_documentos']."/".$ver_versao["doc_ver_arquivo_original"]."'>".$ver_versao["doc_ver_arquivo_original"]."</a><br><br>";
							$rows["arquivo_existente"] .= "<i class='fa fa-file'></i> Arquivo atual: <a target='_blank' href='".$GLOBALS['http_documentos']."/".$ver_versao["doc_ver_arquivo"]."'>".$ver_versao["doc_ver_arquivo"]."</a>";
							$rows["arquivo_existente"] .= "</div><br>";
						}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows["doc_ver_vigencia"] = ($rows["doc_ver_vigencia"]<>"0000-00-00") ? mostra_data_br($ver_versao["doc_ver_vigencia"]) : "";
					$rows["doc_ver_status_nome"] = $ver_versao["configuracao_titulo"];
					$rows["doc_ver_aprovado_data"] = mostra_data_completa_br($ver_versao["doc_ver_aprovado_data"]);
					$rows["doc_ver_aprovado_usuario_nome"] = $ver_versao["usuario_aprovado_nome"];
					$rows["doc_ver_publicado_data"] = mostra_data_completa_br($ver_versao["doc_ver_publicado_data"]);
					$rows["doc_ver_publicado_usuario_nome"] = $ver_versao["usuario_publicado_nome"];
					$rows["doc_ver_revogado_data"] = mostra_data_completa_br($ver_versao["doc_ver_revogado_data"]);
					$rows["doc_ver_revogado_usuario_nome"] = $ver_versao["usuario_revogado_nome"];
					$rows["doc_titulo"] = $ver_versao["doc_titulo"];
				}

			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR PARA CRIAR UMA NOVA VERSÃO BASEADA OU VISUALIZAR A VERSÃO DE UM DOCUMENTO ####*/




























	/*#### REGISTRA A LEITURA DO DOCUMENTO ####*/
	if($_REQUEST['operacao']=="registra-leitura")
		{
			// print_r($_REQUEST);

			/*#### SQL PARA SELECIONAR OS DADOS DA LEITURA ####*/
			$sql_dados_leitura = "SELECT * FROM
												documentos_leituras
											WHERE
												doc_ler_usuario_id = ".$_SESSION["login"]["id"]."
												AND doc_ler_doc_ver_id = '".$_REQUEST['doc_ver_id']."'";
			//echo "<br>sql_dados_leitura: ".$sql_dados_leitura;
			/*#### SQL PARA SELECIONAR OS DADOS DA LEITURA ####*/

			$exe_dados_leitura = mysql_query($sql_dados_leitura, $con) or die("Erro do MySQL[exe_dados_leitura]: ".mysql_error());

			//SE JÁ HOUVE LEITURA, GRAVA A DATA DA ÚLTIMA LEITURA
			if(mysql_num_rows($exe_dados_leitura)>0)
				{
					$ver_dados_leitura = mysql_fetch_array($exe_dados_leitura);
					$sql_atualiza_ultima_leitura = "UPDATE documentos_leituras SET doc_ler_ultima_leitura = '".$hoje_data_us."' WHERE doc_ler_id = ".$ver_dados_leitura["doc_ler_id"];
					$exe_atualiza_ultima_leitura = mysql_query($sql_atualiza_ultima_leitura, $con) or die("Erro do MySQL[exe_atualiza_ultima_leitura]: ".mysql_error());
				}
			//SE NÃO HOUVE LEITURA, GRAVA A DATA DA PRIMEIRA LEITURA
			else
				{
					$sql_insere_leitura = "INSERT INTO documentos_leituras (doc_ler_doc_ver_id,
																			doc_ler_usuario_id,
																			doc_ler_primeira_leitura)
																		VALUES
																			(".$_REQUEST['doc_ver_id'].",
																			".$_SESSION["login"]["id"].",
																			'".$hoje_data_us."')";
					$exe_insere_leitura = mysql_query($sql_insere_leitura, $con) or die("Erro do MySQL[exe_insere_leitura]: ".mysql_error());
				}

			$result = json_encode(array("LEITURA" => "OK"));
		}
	/*#### REGISTRA A LEITURA DO DOCUMENTO ####*/




























	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UMA VERSÃO DE DOCUMENTO ####*/
	if($_REQUEST['operacao']=="excluir-versao")
		{
			//print_r($_REQUEST);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE documentos_versoes SET doc_ver_excluido = 'sim',
																	doc_ver_excluido_data = '".$hoje_data_us."',
																	doc_ver_excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	doc_ver_id = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UMA VERSÃO DE DOCUMENTO ####*/






























	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
