<?php
/*###################################################################
|                                                                   |
|	MÓDULO: documentos												|
|   DESCRIÇÃO: Arquivo que realiza as ações de envio de arquivos 	|
|	do plugin FileInput 											|
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 24/10/2016                                                |
|                                                                   |
###################################################################*/

	include("../../includes/configure.inc.php");

	//print_r($_FILES);
	//print_r($_REQUEST);

	if($_REQUEST['acao']=="DELETE")
		{
			//REMOVE O ARQUIVO TEMPORÁRIO
			@unlink($GLOBALS['pasta_documentos']."/".$_REQUEST['pid']);
			@unlink($GLOBALS['pasta_documentos']."/tmp/".$_REQUEST['pid']);
		}
	else
		{
			//REMOVE, SE EXISTIR, UM ARQUIVO TEMPORÁRIO ANTIGO COM MESMO PID
			@unlink($GLOBALS['pasta_documentos']."/".$_REQUEST['pid']);
			@unlink($GLOBALS['pasta_documentos']."/tmp/".$_REQUEST['pid']);

			//NOMEIA O NOVO ARQUIVO, MOVE PARA DIETÓRIO TEMPORÁRIO E ARMAZENA VARIÁVEL COM O NOME REAL DO ARQUIVO
			$nome_arquivo = $_REQUEST['doc_ver_pid'];
			move_uploaded_file($_FILES["files"]["tmp_name"][0],$GLOBALS['pasta_documentos']."/tmp/".$nome_arquivo);
			$id_arquivo = $_REQUEST['doc_ver_pid'];
			$filename = $_FILES["files"]["name"][0];
		}

	echo json_encode([
		'error' => $msg_erro,
		'apagado' => $msg_apaga,
		'initialPreviewConfig' => [
			['caption' => $filename, 'width' => '100px', 'url' => "documentos/upload.php?acao=DELETE&pid=".$_REQUEST["doc_ver_pid"], 'key' => $id_arquivo],
		],
		'append' => true,
		'extra' => ['filename' => $filename]]);

?>