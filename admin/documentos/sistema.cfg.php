<?php
/*###################################################################
|																	|
|	MÓDULO: documentos													|
|	DESCRIÇÃO: Arquivo padrão de configurações do módulo do 		|
|	sistema															|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 14/09/2016												|
|																	|
###################################################################*/


	/*###################################################################
	|																	|
	|	Variáveis de informações sobre o sistema, títulos e descrição 	|
	|	e informações das tabelas e campos.								|
	|	 																|
	###################################################################*/

	//NOME DO SISTEMA UTILIZADO COMO REFERÊNCIA PARA PERMISÕES DE ACESSO
	$sistema = "documentos";

	//INFORMAÇÕES DE IDENTIFICAÇÃO DO SISTEMA
	$sistema_titulo = "Documentos";
	$sistema_titulo_item = "Documento";
	$sistema_descricao = "Gerencie os documentos que são disponibilizados na Intranet";

	//NOME DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ GERENCIAR
	$sistema_nome_da_tabela = "documentos";

	//NOME DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ EXIBIR NO GRID
	$sistema_nome_da_tabela_grid = "visao_documentos";

	//NOME DO CAMPO NA TABELA DO BANCO DE DADOS QUE É CHAVE-PRIMÁRIA PARA ALTERAÇÃO/EXCLUSÃO DOS REGISTROS
	$sistema_chave_primaria = "doc_id";

	//NOME DO CAMPO NA TABELA DO BANCO DE DADOS QUE É CHAVE-PRIMÁRIA E ID PARA O DT_RowID
	$sistema_chave_primaria_grid = "vis_doc_id";

	//PREFIXO DOS CAMPOS DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ GERENCIAR
	$sistema_prefixo_campos = "doc_";

	//PREFIXO DOS CAMPOS DA TABELA/VIEW DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ EXIBIR NO GRID
	$sistema_prefixo_campos_grid = "vis_doc_";

	//NOME COMPLETO DO CAMPO NO BD QUE SERÁ O CONTROLE PARA NÃO EXIBIR OS EXCLUÍDOS
	$sistema_campo_excluido = "";






	/*###################################################################
	|																	|
	|	Variável $sistema_sql_adicional armazena condições WHERE 		|
	|	adicionais que poderão ser utilizadas para limitar as 			|
	|	pesquisas. Esta condição é aplicada em todas as consultas do 	|
	|	GRID. 															|
	|																	|
	|	Formato: usar sintaxe padrão SQL condicional, após o WHERE, 	|
	|	iniciando com "( " e terminando com " ) AND ".					|
	|	Ex: campo = 'valor' AND campo <> 'valor', etc					|
	|	 																|
	|	Se não houver necessidade de SQL adicional, deixar em branco.	|
	|	 																|
	###################################################################*/
	// echo "<br>auth[especial][ignorar_perm_doc]: ".$auth["especial"]["ignorar_perm_doc"];
	if($auth["especial"]["ignorar_perm_doc"]<>"sim")
		{
			$sistema_sql_adicional_usuario = " (SELECT COUNT(*) FROM documentos_usuarios DOC_USR WHERE DOC_USR.doc_usuario_doc = vis_doc_id AND DOC_USR.doc_usuario_usuario = ".$_SESSION["login"]["id"]." > 0 )";
			$sistema_sql_adicional_grupo = (count($_SESSION["login"]["todos_grupos"])>0) ? " OR (SELECT COUNT(*) FROM documentos_grupos DOC_GRP WHERE DOC_GRP.doc_grupo_doc = vis_doc_id AND DOC_GRP.doc_grupo_grupo IN (".substr($_SESSION["login"]["todos_grupos_in"],0,-1).") > 0) " : "";
			$sistema_sql_adicional = (count($_SESSION["login"]["todos_niveis"])>0) ? " ( (SELECT COUNT(*) FROM visao_documentos_niveis DOC_NV WHERE DOC_NV.vis_doc_nvl_doc_id = vis_doc_id AND (DOC_NV.vis_doc_nvl_nivel_id IN (".substr($_SESSION["login"]["todos_niveis_in"],0,-1).") OR DOC_NV.vis_doc_nvl_nivel_grupo IN (".substr($_SESSION["login"]["todos_niveis_in"],0,-1)."))) > 0 OR ".$sistema_sql_adicional_usuario." ".$sistema_sql_adicional_grupo." ) AND " : "";
			if($sistema_sql_adicional=="")
				{
					$sistema_sql_adicional .= "( ".$sistema_sql_adicional_usuario;
					$sistema_sql_adicional .= ($sistema_sql_adicional_grupo<>"") ? $sistema_sql_adicional_grupo.") AND " : " ) AND ";
				}
		}

		// echo "<br>sistema_sql_adicional: ".$sistema_sql_adicional;



	/*###################################################################
	|																	|
	|	Variável $array_campos_filtragem_permissoes é um array 			|
	|	multidimensional que contém as informações dos campos de 		|
	|	filtragem com base nas permissões, para o sistema não exibir 	|
	|	as informações que o usuário não tem acesso.					|
	|	 																|
	|	Fomato: "nome_do_campo_grid" => "array de permissões"			|
	|			array de permissões do $auth_filial						|
	|	 																|
	###################################################################*/

	$array_campos_filtragem_permissoes = array("vis_doc_secao" => $auth_filial[$_REQUEST['filial']][$sistema."-secao"]);





	/*###################################################################
	|																	|
	|	Variável $array_colunas_grid é array multidimensional que 		|
	|	contém todas as configucações das colunas que constam no GRID	|
	| 	da tabela.														|
	|																	|
	|	Formato: NOME_DO_CAMPO_BD => (title,							|
	|								width,								|
	|								visible,							|
	|								searchable,							|
	|								orderable,							|
	|								array(formatacao)					|
	|	 																|
	|	NOME_DO_CAMPO_BD: deve ser escritos SEM O PREFIXO 				|
	|																	|
	|	title: título da coluna pode ser escrito em UTF8 acentudo		|
	|																	|
	|	width: tamanho da coluna no GRID. O somatório total das 		|
	|			colunas não pode ser maior do que 90%					|
	|																	|
	|	visible: "true" se deseja mostrar a coluna no GRID, "false" 	|
	|			para não mostrar	 									|
	|																	|
	|	searchable: "true" se deseja permitir busca no valor dessa 		|
	|				coluna, "false" para não mostrar	 				|
	|																	|
	|	className: "nome_da_class" indicar o nome da classe CSS para	|
	|				uso nas colunas quando desejar formatação diferente	|
	|				da padrão, uso opcional								|
	|																	|
	|	orderable: "true" se deseja permitir busca no valor dessa 		|
	|				coluna, "false" para não mostrar	 				|
	|																	|
	|	hidden: indicar o nome da classe hidden da coluna conforme		|
	|			a resolução do dispositivo. Opcional	 				|
	|																	|
	|	array(formatacao): array que contém a informações de formatação	|
	|						do valor a ser exibido no GRID,incluindo a 	|
	|						aplicação de funções de formatação de data,	|
	|						moeda, ou outros tratamentos que precisam 	|
	|						ser feitos com o valor. Só utilizar quendo	|
	|						exigir formatação. A formatação deverá 		|
	|						ser colocada após o return do function		|
	|	 																|
	###################################################################*/

	$array_colunas_grid = array("id" => array("title" => "ID",
											"width" => "3%",
											"visible" => "true",
											"searchable" => "true",
											"orderable" => "false"),

								"titulo" => array("title" => "Título",
												"width" => "23%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true"),

								"secao" => array("title" => "Seção",
												"width" => "15%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true",
												"formatacao" => array("formatacao" => function($formatacao) {
															$separa_secoes = explode($GLOBALS['separador_string'],$formatacao);
															foreach($separa_secoes as $nome_secao) {
																if($nome_secao<>"") {
																	$titulo_secoes .= $_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$GLOBALS['sistema']."-secao"][$nome_secao]."<br>";
																}
															}
															return $titulo_secoes;
														})),

								"titulo_nivel_cat" => array("title" => "Categoria",
												"width" => "20%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true"),

								"status" => array("title" => "Status",
												"width" => "10%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true",
												"formatacao" => array("formatacao" => function($formatacao) { return $_SESSION["guarda_config"]["titulo"]["todas"]["status"][$formatacao]; })),

								// "ordem" => array("title" => "Ordem",
								// 				"width" => "10%",
								// 				"visible" => "true",
								// 				"searchable" => "true",
								// 				"orderable" => "true"),

								// "ordem_max" => array("title" => "",
								// 				"width" => "5%",
								// 				"visible" => "true",
								// 				"searchable" => "true",
								// 				"orderable" => "false",
								// 				"formatacao" => array("formatacao" => function($formatacao) {
								// 					$separa_dados = explode('$', $formatacao);
								// 					return (($separa_dados[3]<$separa_dados[0])||($separa_dados[3]>=0)) ? "<a id='txt-up' data-id='".$separa_dados[4]."' title='Aumentar ordem'><i class='ace-icon fa fa-arrow-circle-down bigger-130'></i></a>" : "";
								// 				})),

								// "ordem_min" => array("title" => "",
								// 				"width" => "5%",
								// 				"visible" => "true",
								// 				"searchable" => "true",
								// 				"orderable" => "false",
								// 				"formatacao" => array("formatacao" => function($formatacao) {
								// 					$separa_dados = explode('$', $formatacao);
								// 					return (($separa_dados[3]>$separa_dados[0])||($separa_dados[3]>0)) ? "<a id='txt-down' data-id='".$separa_dados[4]."' title='Reduzir ordem'><i class='ace-icon fa fa-arrow-circle-up bigger-130'></i></a>" : "";
								// 				})),

								"ultima_versao" => array("title" => "Última versão",
												"width" => "17%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true",
												"formatacao" => array("formatacao" => function($formatacao) {
														$separa_ultima_versao = explode("#", $formatacao);
														if($separa_ultima_versao[0]<>"") {
															return $_SESSION["guarda_config"]["titulo"]["todas"]["status_versoes"][$separa_ultima_versao[0]]." (".$separa_ultima_versao[1].")";
														}
													})),

								"ultima_versao_status" => array("title" => "STATUS_ULTIMA_VERSAO",
												"width" => "",
												"visible" => "false",
												"searchable" => "true",
												"orderable" => "true"),

								"resumo" => array("title" => "RESUMO",
												"width" => "",
												"visible" => "false",
												"searchable" => "true",
												"orderable" => "true"),

								"secao_buscar" => array("title" => "SECAO_BUSCAR",
												"width" => "",
												"visible" => "false",
												"searchable" => "true",
												"orderable" => "true"),

								"categoria_buscar" => array("title" => "CATEGORIA_BUSCAR",
												"width" => "",
												"visible" => "false",
												"searchable" => "true",
												"orderable" => "true")
												);


	/*###################################################################
	|																	|
	|	Variável $array_colunas_grid é array multidimensional que 		|
	|	contém as informações de como será a ordenação padrão do GRID. 	|
	|	Colocar a array na sequencia que deseja que seja feita a 		|
	|	ordenação. A primeira key é o nome do campo igual ao utilizado 	|
	|	no $array_colunas_grid e o valor indica se será ASC ou DESC		|
	|																	|
	###################################################################*/

	$array_ordenacao_grid = array("secao" => "asc",
									// "ordem" => "asc",
									"titulo" => "asc");


	//INCLUI O ARQUIVO DE FUNÇÕES ADICIONAIS PARA TODOS OS SISTEMAS
	include($pasta_includes."/functions.sistema.inc.php");

?>