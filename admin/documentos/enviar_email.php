<?php
/*###################################################################
|                                                                   |
|	MÓDULO: documentos												|
|   DESCRIÇÃO: Arquivo que realiza o envio dos e-mails do sistema 	|
|	de documentos 		 											|
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 29/01/2017                                                |
|                                                                   |
###################################################################*/

	include("../../includes/configure.inc.php");
	include($pasta_includes."/auth.inc.php");

	//print_r($_FILES);
	//print_r($_REQUEST);

	//require $GLOBALS['pasta_lib'].'/phpmailer/PHPMailerAutoload.php';
	$rows["msg_envio_email"] = envia_email($_REQUEST['remetente_nome'], $_POST['destinatario_email'], $_POST['destinatario_nome'], "", $_POST['assunto_email'], $_POST['mensagem'], $_POST['msg_retorno_email'], "", "");

	echo $rows["msg_envio_email"];
?>