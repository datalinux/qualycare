<?php
/*###################################################################
|                                                                   |
|   MÓDULO: documentos                                              |
|   DESCRIÇÃO: Arquivo principal de telas e gerenciamento do        |
|   módulo                                                          |
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 20/10/2016                                                |
|                                                                   |
###################################################################*/


	//INCLUSÃO DOS ARQUIVOS DE CONFIGURAÇÃO SEM FUNÇÕES JS
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//DEFINIÇÃO DO NOME DA OPERAÇÃO PRINCIPAL DE VISUALIZAÇÃO DE REGISTROS
	$operacao = "gerenciar";

	//INCLUSÃO DO ARQUIVO QUE CARREGA AS PERMISSÕES DO USUÁRIO
	include($pasta_includes."/auth.inc.php");
	//echo "<br>autorizado: ".$autorizado;
?>


<link rel="stylesheet" href="../lib/ace/assets/css/chosen.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/jquery.gritter.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/datepicker.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/bootstrap-datetimepicker.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/bootstrap-fileinput/fileinput.css" />
<link rel="stylesheet" href="../lib/cropit/dist/cropit.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/daterangepicker.css" />
<link rel="stylesheet" href="../includes/estilos.admin.sistemas.css" />


<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		<? echo $sistema_titulo; ?>
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			<? echo $sistema_descricao; ?>
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">


	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS --><!-- /.row -->

















        <!--### FILTROS DO GRID PRINCIPAL ###-->
        <div class="row">
            <div class="col-xs-12">
                <h3 class="header smaller lighter blue">Filtros</h3>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <label for="filtro_secao">Filtrar por Seção</label><br />
                    <select class="chosen-select" name="filtro_secao" id="filtro_secao" data-id="8" data-placeholder="Escolha a seção...">
                        <option value="">Todas as seções</option>
                        <?
                            //ORDENA O ARRAY DE SEÇÕES POR ORDEM ALFABETICA DE TÍTULO
                            @ksort($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"]);

                            foreach($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"] as $secao_value => $secao_title)
                                {
                                    if(@in_array($secao_value, $auth_filial[$_REQUEST['filial']][$sistema."-secao"]))
                                        {
                        ?>
                                            <option value="<?php echo $separador_string.$secao_value.$separador_string; ?>"><?php echo $secao_title; ?></option>
                        <?
                                        }
                                } 
                        ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <label for="filtro_categoria">Filtrar por Categoria</label><br />
                    <select class="chosen-select" name="filtro_categoria" id="filtro_categoria" data-id="9" data-placeholder="Escolha a categoria...">
                        <option value="">Todas as categorias</option>
                        <?
                            $sql_categorias = "SELECT DISTINCT vis_doc_cat_id, vis_doc_cat_titulo_nivel_cat FROM visao_documentos_categorias ORDER BY vis_doc_cat_titulo_nivel_cat";
                            $exe_categorias = mysql_query($sql_categorias, $con) or die("Erro do MySQL[exe_categorias]: ".mysql_error());
                            while($ver_categorias = mysql_fetch_array($exe_categorias))
                                {
                        ?>
                                    <option value="<?php echo $separador_string.$ver_categorias["vis_doc_cat_id"].$separador_string; ?>"><?php echo $ver_categorias["vis_doc_cat_titulo_nivel_cat"]; ?></option>
                        <?
                                } 
                        ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <label for="filtro_status">Filtrar por Status</label><br />
                    <select class="chosen-select" name="filtro_status" id="filtro_status" data-id="4" data-placeholder="Escolha o status...">
                        <option value="">Todos os status</option>
                        <?
                            foreach($_SESSION["guarda_config"]["titulo"]["todas"]["status"] as $valor_bd => $valor_mostrar)
                                {
                        ?>
                                    <option value="<?php echo $valor_bd; ?>"><?php echo $valor_mostrar; ?></option>
                        <?
                                } 
                        ?>
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-4">
                    <label for="filtro_versao">Filtrar por Status da Última Versão</label><br />
                    <select class="chosen-select" name="filtro_versao" id="filtro_versao" data-id="6" data-placeholder="Escolha o status...">
                        <option value="">Todos os status</option>
                        <?
                            //ORDENA O ARRAY DE SEÇÕES POR ORDEM ALFABETICA DE TÍTULO
                            @ksort($_SESSION["guarda_config"]["titulo"]["todas"]["status_versoes"]);

                            foreach($_SESSION["guarda_config"]["titulo"]["todas"]["status_versoes"] as $config_value => $config_title)
                                {
                        ?>
                                    <option value="<?php echo $separador_string.$config_value.$separador_string; ?>"><?php echo $config_title; ?></option>
                        <?
                                } 
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <!--### FILTROS DO GRID PRINCIPAL ###-->












        <!--### GRID PRINCIPAL ###-->
		<div class="row">
			<div class="col-xs-12">
                <h3 class="header smaller lighter blue">
                    <button class="btn btn-white btn-default btn-round" title="Inserir <? echo $sistema_titulo_item; ?>" data-id="btn-form-inserir">
                        <i class="ace-icon fa fa-plus-square bigger-120 green"></i>
                        Inserir <? echo $sistema_titulo_item; ?>
                    </button>
                </h3>


				<div class="table-header">
					Lista de <? echo strtolower($sistema_titulo); ?> existentes
				</div>

				<!-- <div class="table-responsive"> -->

				<!-- <div class="dataTables_borderWrap"> -->
				<div>
					<table id="sample-table-2" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
                            	<?
									//FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
									foreach($array_colunas_grid as $key_coluna => $config_coluna)
										{
											echo "<th";
											if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
											echo ">";
											echo $config_coluna["title"];
											echo "</th>";
										}
								?>
                                <th></th>
							</tr>
						</thead>


					</table>
				</div>
			</div>
		</div>
        <!--### GRID PRINCIPAL ###-->















		<div class="row">
			<div class="col-xs-12">




                <!--### MODAL DO FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                <div id="modal-form" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="form-titulo-acao" class="blue bigger"></h4>
                            </div>

                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                            <form class="form-horizontal" role="form" id="form-cadastros" name="form-cadastros">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">
                                                <div id="info_reg_user" class="form-group">
                                                    <div class="col-sm-3 no-padding-right" style="height: 22px; line-height:22px; text-align:right;">ID</div>

                                                    <div class="col-sm-9">
                                                        <span id="txt_id"></span> &nbsp;&nbsp;<i id="txt_id_reg" class="ace-icon fa fa-history bigger-120" data-rel="reg_user_popover" data-trigger="hover" data-placement="right" data-content="" title="Histórico do registro"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_secao">Seções</label>

													<div class="col-sm-9">
 														<?php

                                                            //ORDENA O ARRAY DE SEÇÕES POR ORDEM ALFABETICA DE TÍTULO
                                                            @ksort($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"]);

                                                            foreach($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"] as $secao_value => $secao_title)
                                                                {
                                                                	$num_secao++;
                                                        ?>
                                                                    <label class="col-md-3 col-sm-6 col-xs-12" <?php if(!@in_array($secao_value, $auth_filial[$_REQUEST['filial']][$sistema."-secao"])) echo "style='display: none;'"; ?>>
                                                                        <input name="doc_secao[]" type="checkbox" id="doc_secao_<?php echo $num_secao; ?>" value="<?php echo $secao_value; ?>" class="ace" />
                                                                        <span class="lbl"> <?php echo $secao_title; ?></span>
                                                                    </label>
                                                        <?php
                                                                }
                                                        ?>
													</div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_titulo">Título</label>

                                                  <div class="col-sm-9">
                                                        <input type="text" name="doc_titulo" id="doc_titulo" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_categoria">Categoria</label>

                                                    <div class="col-sm-9">
                                                        <select class="chosen-select" name="doc_categoria" id="doc_categoria" data-placeholder="Selecione o nível superior" style="width:270px;">
                                                            <option value="">Selecione a categoria</option>
                                                            <?
                                                                $exe_categorias = mysql_query($sql_categorias, $con) or die("Erro do MySQL[exe_categorias]: ".mysql_error());
                                                                while($ver_categorias = mysql_fetch_array($exe_categorias))
                                                                    { 
                                                            ?>
                                                                        <option value="<? echo $ver_categorias["vis_doc_cat_id"]; ?>"><? echo $ver_categorias["vis_doc_cat_titulo_nivel_cat"]; ?></option>
                                                            <?
                                                                    } 
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_ordem">Ordem</label>

                                                  <div class="col-sm-9">
                                                        <input type="text" name="doc_ordem" id="doc_ordem" class="typeahead col-xs-1 col-sm-1" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                	<label class="col-sm-3 control-label no-padding-right" for="doc_resumo">Resumo</label>

                                                    <div class="col-sm-9">
                                                        <textarea class="autosize-transition form-control" name="doc_resumo" id="doc_resumo" rows="4" placeholder=""></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_status">Status</label>

                                                    <div class="col-sm-9">
                                                        <?
                                                            foreach($_SESSION["guarda_config"]["titulo"]["todas"]["status"] as $config_value => $config_title)
                                                                {
                                                                    $num_radio++;
                                                        ?>
                                                                    <label style="margin-right:20px;">
                                                                        <input name="doc_status" type="radio" id="doc_status_<? echo $num_radio; ?>" value="<? echo $config_value; ?>" class="ace" onClick="eval('this.form.'+this.name+'_hidden.value = \''+this.value+'\'');" />
                                                                        <span class="lbl"> <? echo $config_title; ?></span>
                                                                    </label>
                                                        <?
                                                                }
                                                        ?>
                                                    </div>
                                                </div>



                                                <input type="hidden" name="doc_id" id="doc_id" />
                                                <input type="hidden" name="doc_status_hidden" id="doc_status_hidden" lang="NaoAtribuir" />
                                                <input type="hidden" name="operacao" id="operacao" />
                                                <input type="hidden" name="filial" id="filial" />

                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Cancelar
                                    </button>

                                    <button type="submit" id="btn-inserir" class="btn btn-sm btn-primary" data-id="inserir">
                                        <i class="ace-icon fa fa-check"></i>
                                        Inserir
                                    </button>

                                    <button type="submit" id="btn-alterar" class="btn btn-sm btn-success" data-id="alterar">
                                        <i class="ace-icon fa fa-check"></i>
                                        Alterar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->

                        </div>
                    </div>
                </div>
                <!--### MODAL DO FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->






































                <!--### MODAL DO FORMULÁRIO DE VERSÕES ###-->
                <div id="modal-form-versoes" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="form-titulo-acao" class="blue bigger"></h4>
                            </div>

                            <!--### FORMULÁRIO DE CADASTRO DE VERSÕES ###-->
                            <form class="form-horizontal" role="form" id="form-versoes" name="form-versoes">
                                <div class="modal-body">
                                    <div class="row">

                                        <div id="msg-sem-nivel-grupo" class="col-xs-12">
                                            <div class="bg-danger"><p>Este documento não possui nível ou grupo atribuído. Não será possível utilizar o fluxo de revisões e aprovações.</p></div>
                                        </div>
                                        <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_titulo">Documento</label>

                                                    <div class="col-sm-9">
                                                        <span id="doc_titulo"></span>
                                                    </div>
                                                </div>




                                                <div class="table-header">Versões do documento</div>
                                                <table id="tabela-versoes" class="table  table-bordered table-hover">
                                                    <thead>
                                                        <th>Versão</th>
                                                        <th>Status</th>
                                                        <th>Observações</th>
                                                        <th>Obs. Aprovador</th>
                                                        <th></th>
                                                        <th></th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>



                                                <div id="widget-detalhe-versao" class="widget-box collapsed">
                                                    <div class="widget-header">
                                                        <h4 class="widget-title" id="titulo-form-versao" data-toggle="nova-versao" style="cursor:pointer;">Nova versão</h4>
                                                    </div>

                                                    <div class="widget-body">
                                                        <div class="widget-main">

                                                            <div class="form-group col-sm-3">
                                                                <label class="control-label no-padding-right" for="doc_ver_tipo">Tipo</label><br/>

                                                                <select class="chosen-select" name="doc_ver_tipo" id="doc_ver_tipo" data-placeholder="Escolha o tipo de documento">
                                                                    <option value=""></option>
                                                                    <?
                                                                        foreach($_SESSION["guarda_config"]["titulo"]["todas"]["tipos_versoes"] as $config_value => $config_title)
                                                                            {
                                                                    ?>
                                                                                <option value="<? echo $config_value; ?>"><? echo $config_title; ?></option>
                                                                    <?
                                                                            } 
                                                                    ?>
                                                                </select>
                                                            </div>


                                                            <div id="campo_texto" class="form-group" style="clear:both; display: none;">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label no-padding-right" for="doc_ver_texto">Texto</label><br />

                                                                    <?php include("../../includes/tinymce.inc.php"); ?>

                                                                    <textarea class="autosize-transition form-control mceEditor" name="doc_ver_texto" id="doc_ver_texto" rows="4" placeholder=""></textarea>
                                                                </div>
                                                            </div>

                                                            <div id="campo_file" class="form-group" style="clear:both; display: none;">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label no-padding-right" for="files">Enviar arquivo</label><br/>

                                                                    <div id="arquivo_existente"></div>
                                                                    <input id="files" name="files[]" type="file" multiple class="file-loading" lang="NaoAtribuir" />
                                                                    <input type="hidden" name="arquivo_enviado" id="arquivo_enviado" value="0" lang="NaoAtribuir" />
                                                                    <input type="hidden" name="doc_ver_arquivo" id="doc_ver_arquivo" />
                                                                    <input type="hidden" name="doc_ver_arquivo_original" id="doc_ver_arquivo_original" />
                                                                </div>
                                                            </div>


                                                            <div style="clear:both">
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label no-padding-right" for="doc_ver_versao">Versão</label><br/>

                                                                        <input type="text" name="doc_ver_versao" id="doc_ver_versao" class="typeahead" placeholder="ex: 1.0.0" />
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-3">
                                                                    <div class="col-sm-8">
                                                                        <div class="form-group">
                                                                            <label class="control-label no-padding-right" for="doc_ver_vigencia">Vigência</label><br/>

                                                                            <div class="input-group">
                                                                                <input class="form-control date-picker" id="doc_ver_vigencia" name="doc_ver_vigencia" type="text" data-date-format="dd/mm/yyyy" />
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-calendar bigger-110"></i>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label no-padding-right" for="doc_ver_status">Status</label><br/>

                                                                        <select class="chosen-select" name="doc_ver_status" id="doc_ver_status" data-placeholder="Selecione o status">
                                                                            <option value=""></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div style="clear:both;">
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label no-padding-right" for="doc_ver_leitura">Requer leitura?</label>

                                                                    <div class="col-sm-10">
                                                                        <?
                                                                            foreach($_SESSION["guarda_config"]["titulo"]["todas"]["sim_nao"] as $config_value => $config_title)
                                                                                {
                                                                                    $num_radio++;
                                                                        ?>
                                                                                    <label style="margin-right:20px;">
                                                                                        <input name="doc_ver_leitura" type="radio" id="doc_ver_leitura_<? echo $num_radio; ?>" value="<? echo $config_value; ?>" class="ace" onClick="eval('this.form.'+this.name+'_hidden.value = \''+this.value+'\'');" />
                                                                                        <span class="lbl"> <? echo $config_title; ?></span>
                                                                                    </label>
                                                                        <?
                                                                                }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div id="campo_obs" class="form-group" style="clear:both;">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label no-padding-right" for="doc_ver_observacoes">Observações (para uso no processo de aprovação)</label><br />
                                                                    <textarea class="autosize-transition form-control" name="doc_ver_observacoes" id="doc_ver_observacoes" rows="4" placeholder=""></textarea>
                                                                </div>
                                                            </div>

                                                            <div id="campo_obs_aprovador" class="form-group" style="clear:both; display: none;">
                                                                <div class="col-sm-12">
                                                                    <label class="control-label no-padding-right" for="doc_ver_obs_aprovador">Recomendações do aprovador (para uso no processo de aprovação)</label><br />
                                                                    <textarea class="autosize-transition form-control" name="doc_ver_obs_aprovador" id="doc_ver_obs_aprovador" rows="4" placeholder=""></textarea>
                                                                </div>
                                                                <br>
                                                                <div id="info_aprovador" class="col-sm-12"></div>
                                                                <div id="info_publicador" class="col-sm-12"></div>
                                                                <div id="info_revogador" class="col-sm-12"></div>
                                                            </div>

                                                            <div style="clear:both;">
                                                            </div>
                                                            <div class="row">&nbsp;</div>


                                                        </div>
                                                    </div>
                                                </div>



                                                <input type="hidden" name="doc_id" id="doc_id" />
                                                <input type="hidden" name="doc_ver_id" id="doc_ver_id" />
                                                <input type="hidden" name="doc_ver_pid" id="doc_ver_pid" />
                                                <input type="hidden" name="doc_nivel" id="doc_nivel" />
                                                <input type="hidden" name="usuario_nivel" id="usuario_nivel" />
                                                <input type="hidden" name="doc_ver_leitura_hidden" id="doc_ver_leitura_hidden" />
                                                <input type="hidden" name="niveis_superiores" id="niveis_superiores" />
                                                <input type="hidden" name="supervisores_niveis" id="supervisores_niveis" />
                                                <input type="hidden" name="arquivo_filename" id="arquivo_filename" lang="NaoAtribuir" />
                                                <input type="hidden" name="operacao_upload" id="operacao_upload" />
                                                <input type="hidden" name="operacao" id="operacao" />
                                                <input type="hidden" name="filial" id="filial" />

                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Fechar
                                    </button>

                                    <button type="submit" id="btn-salvar" class="btn btn-sm btn-primary" data-id="salvar-versao">
                                        <i class="ace-icon fa fa-check"></i>
                                        Salvar e alterar depois
                                    </button>

                                    <button type="submit" id="btn-encaminhar" class="btn btn-sm btn-success" data-id="salvar-encaminhar">
                                        <i class="ace-icon fa fa-forward"></i>
                                        Salvar e Encaminhar
                                    </button>

                                    <button type="submit" id="btn-publicar" class="btn btn-sm btn-info" data-id="publicar-versao">
                                        <i class="ace-icon fa fa-share-square-o"></i>
                                        Publicar
                                    </button>

                                    <button type="submit" id="btn-revogar" class="btn btn-sm btn-danger" data-id="revogar-versao">
                                        <i class="ace-icon fa fa-ban"></i>
                                        Revogar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE CADASTRO DE VERSÕES ###-->

                        </div>
                    </div>
                </div>
                <!--### MODAL DO FORMULÁRIO DE VERSÕES ###-->







































                <!--### MODAL DO FORMULÁRIO DE PERMISSÕES ###-->
                <div id="modal-form-permissoes" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="blue bigger">Permissões do Documento</h4>
                            </div>







                            <!--### FORMULÁRIO DE NÍVEIS, GRUPOS OU USUÁRIOS ###-->
                            <form class="form-horizontal" role="form" id="form-permissoes" name="form-permissoes">
                                <div class="modal-body">
                                    <div class="row">

                                            <div class="col-xs-12">

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_titulo">Documento</label>

                                                    <div class="col-sm-9">
                                                        <span id="doc_titulo"></span>
                                                    </div>
                                                </div>

                                                <? include("sistema-usuarios.cfg.php"); ?>

                                                <div class="table-header">
                                                    Lista de usuários do documento
                                                </div>

                                                <!-- <div class="table-responsive"> -->

                                                <!-- <div class="dataTables_borderWrap"> -->
                                                <div>
                                                    <div id="div_associar_todos_usuarios" class="text-right" style="background-color: #EFF3F8; padding: 5px;"><label><input type="checkbox" name="associar_todos_usuarios" id="associar_todos_usuarios" value='todos' class='ace associar_usuario' title="Clique para associar todos os usuários a este link" /><span class='lbl'> Associar todos os usuários</span></label></div>
                                                    <table id="tabela-usuarios" class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <?php
                                                                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                                                                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                                                                        {
                                                                            echo "<th";
                                                                            if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
                                                                            echo ">";
                                                                            echo $config_coluna["title"];
                                                                            echo "</th>";
                                                                        }
                                                                ?>
                                                            </tr>
                                                        </thead>


                                                    </table>
                                                </div>


                                                <p>&nbsp;</p>

                                                <? include("sistema-niveis.cfg.php"); ?>

                                                <div class="table-header">
                                                    Lista de níveis do documento
                                                </div>

                                                <!-- <div class="table-responsive"> -->

                                                <!-- <div class="dataTables_borderWrap"> -->
                                                <div>
                                                    <div id="div_associar_todos_niveis" class="text-right" style="background-color: #EFF3F8; padding: 5px;"><label><input type="checkbox" name="associar_todos_niveis" id="associar_todos_niveis" value='todos' class='ace associar_nivel' title="Clique para associar todos os níveis a este link" /><span class='lbl'> Associar todos os níveis</span></label></div>
                                                    <table id="tabela-niveis" class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <?php
                                                                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                                                                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                                                                        {
                                                                            echo "<th";
                                                                            if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
                                                                            echo ">";
                                                                            echo $config_coluna["title"];
                                                                            echo "</th>";
                                                                        }
                                                                ?>
                                                            </tr>
                                                        </thead>


                                                    </table>
                                                </div>


                                                <p>&nbsp;</p>

                                                <? include("sistema-grupos.cfg.php"); ?>

                                                <div class="table-header">
                                                    Lista de grupos do documento
                                                </div>

                                                <!-- <div class="table-responsive"> -->

                                                <!-- <div class="dataTables_borderWrap"> -->
                                                <div>
                                                    <div id="div_associar_todos_grupos" class="text-right" style="background-color: #EFF3F8; padding: 5px;"><label><input type="checkbox" name="associar_todos_grupos" id="associar_todos_grupos" value='todos' class='ace associar_grupo' title="Clique para associar todos os grupos a este link" /><span class='lbl'> Associar todos os grupos</span></label></div>
                                                    <table id="tabela-grupos" class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <?php
                                                                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                                                                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                                                                        {
                                                                            echo "<th";
                                                                            if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
                                                                            echo ">";
                                                                            echo $config_coluna["title"];
                                                                            echo "</th>";
                                                                        }
                                                                ?>
                                                            </tr>
                                                        </thead>


                                                    </table>
                                                </div>

                                                <? include("sistema.cfg.php"); ?>


                                                <input type="hidden" name="doc_id" id="doc_id" />
                                                <input type="hidden" name="operacao" id="operacao" />
                                                <input type="hidden" name="filial" id="filial" />


                                            </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Fechar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE NÍVEIS, GRUPOS OU USUÁRIOS ###-->








                        </div>
                    </div>
                </div>
                <!--### MODAL DO FORMULÁRIO DE PERMISSÕES ###-->




















                <!--### MODAL DA ÁREA DE HISTÓRICO DE LEITURA DO DOCUMENTO ###-->
                <div id="modal-form-leitura" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="blue bigger">Histórico de leitura do arquivo</h4>
                            </div>

                            <!--### FORMULÁRIO DE HISTÓRICO DE LEITURA DO DOCUMENTO ###-->
                            <form class="form-horizontal" role="form" id="form-leitura" name="form-leitura">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">
                                                <div id="info_reg_user" class="form-group">
                                                    <div class="col-sm-3 no-padding-right" style="height: 22px; line-height:22px; text-align:right;">ID</div>

                                                    <div class="col-sm-9">
                                                        <span id="txt_id"></span> &nbsp;&nbsp;<i id="txt_id_reg" class="ace-icon fa fa-history bigger-120" data-rel="reg_user_popover" data-trigger="hover" data-placement="right" data-content="" title="Histórico do registro"></i>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="doc_titulo_hist">Título</label>

                                                  <div class="col-sm-9">
                                                        <input type="text" name="doc_titulo_hist" id="doc_titulo_hist" class="typeahead col-xs-10 col-sm-10" readonly="" />
                                                    </div>
                                                </div>

                                                <div>
                                                    <label for="historico_leitura">Histórico de leitura</label>
                                                    <div id="historico"></div>
                                                </div>



                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Fechar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE HISTÓRICO DE LEITURA DO DOCUMENTO ###-->

                        </div>
                    </div>
                </div>
                <!--### MODAL DA ÁREA DE HISTÓRICO DE LEITURA DO DOCUMENTO ###-->






            </div>
		</div>








		<!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->








<!-- page specific plugin scripts -->
<script type="text/javascript">



    var scripts = [null]
    $('[data-ajax-content=true]').ace_ajax('loadScripts', scripts, function() {






	  //inline scripts related to this page
		 jQuery(function($) {


		  moment.locale('pt_BR');

	<?
		//SE NÃO FOR AUTORIZADO O ACESSO AO ARQUIVO INFORMA MENSAGEM DE ERRO USANDO GRITTER
		if($autorizado=="nao")
			{
	?>
				$(".col-xs-12").hide();
				$.gritter.add({
					title: '<i class="ace-icon fa fa-times"></i> Erro!',
					text: '<? echo $sistema_nao_autorizado; ?>',
					class_name: 'gritter-error gritter-center',
					sticky: false,
					fade_out_speed:500
				});
	<?
			}
	?>


		/*###################################################################
		|																			|
		|	A variável (objeto) oTable1 é a que recebe o GRID e todas as 	|
		|	configurações. Sempre que necessário referenciar o GRID 			|
		|	utilizar este nome deste objeto.										|
		|																			|
		###################################################################*/
		var oTable1 =
		$('#sample-table-2')
		//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
		.DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": {
				//URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
				"url": "<? echo $http_includes; ?>/jquery.datatables.serverSide.php",
				"data": function ( d ) {
					d.sistema = "<? echo $sistema; ?>";
					d.filial = "<? echo $_REQUEST['filial']; ?>";
				}
			},
			//"ajax": "configuracoes/serverSide.php",
			//stateSave: true,
			bAutoWidth: false,
			"paging": true,
			"columns": [
				<?
					//FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
					foreach($array_colunas_grid as $key_coluna => $config_coluna)
						{
							$num_coluna_grid++;
							echo '
							{ ';
							echo '"name": "'.$key_coluna.'", ';
							echo '"data": "'.$key_coluna.'", ';
							echo '"title": "'.$config_coluna["title"].'", ';
							echo '"width": "'.$config_coluna["width"].'", ';
							echo '"visible": '.$config_coluna["visible"].', ';
							echo '"searchable": '.$config_coluna["searchable"].', ';
							if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
							echo '"orderable": '.$config_coluna["orderable"];
							echo ' }';
							if($num_coluna_grid<$total_colunas_grid) echo ',';
						}
				?>,
				{
                	"name": "acoes",
                	"width": "12%",
                	"visible": true,
                	"searchable": false,
                	"orderable": false,
					"data": null,

					/*###################################################################
					|																			|
					|	defaultContent é a variável que contém o conteúdo da última 		|
					|	coluna do GRID onde existem os botões de controle das ações 		|
					|	para cada registro. Há dois grupos de botões, um para mobile e 	|
					|	outro para telas normais. Observar os elementns 					|
					|	<i class='ace-icon fa fa-pencil que contém os botões. 			|
					|																			|
					|	Propriedades															|
					|	data-id: é a operação do botão (alterar, excluir, etc) 			|
					|	title: texto da descrição do botão									|
					|	data-toggle: padrão modal para chamada do form						|
					|	data-target: ID da janela modal que será aberta					|
					|																			|
					###################################################################*/
					"defaultContent": "<div class='hidden-sm hidden-xs action-buttons'><a class='orange'><i class='ace-icon fa fa-book bigger-130' title='Histórico de leitura' data-id='leitura'></i></a><a class='blue'><i class='ace-icon fa fa-history bigger-130' title='Versões do documento' data-id='versoes'></i></a><a class='dark'><i class='ace-icon fa fa-lock bigger-130' title='Alterar permissões' data-id='permissoes'></i></a><a class='green'><i class='ace-icon fa fa-edit bigger-130' title='Alterar <? echo $sistema_titulo_item; ?>' data-id='alterar'></i></a><a class='red'><i class='ace-icon fa fa-trash-o bigger-130' title='Excluir <? echo $sistema_titulo_item; ?>' data-id='excluir'></i></a></div><div class='hidden-md hidden-lg'><div class='inline position-relative'><button class='btn btn-minier btn-yellow dropdown-toggle' data-toggle='dropdown' data-position='auto'><i class='ace-icon fa fa-caret-down icon-only bigger-120'></i></button><ul class='dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'><li><a class='tooltip-success' data-rel='tooltip'><span class='orange'><i class='ace-icon fa fa-book bigger-120' title='Histórico de leitura' data-id='leitura'></i></span></a></li><li><a class='tooltip-success' data-rel='tooltip'><span class='blue'><i class='ace-icon fa fa-history bigger-120' title='Versões do documento' data-id='versoes'></i></span></a></li><li><a class='tooltip-success' data-rel='tooltip'><span class='dark'><i class='ace-icon fa fa-lock bigger-120' title='Alterar permissões' data-id='permissoes'></i></span></a></li><li><a class='tooltip-success' data-rel='tooltip'><span class='green'><i class='ace-icon fa fa-edit bigger-120' title='Alterar <? echo $sistema_titulo_item; ?>' data-id='alterar'></i></span></a></li><li><a class='tooltip-error' data-rel='tooltip'><span class='red'><i class='ace-icon fa fa-trash-o bigger-120' title='Excluir <? echo $sistema_titulo_item; ?>' data-id='excluir'></i></span></a></li></ul></div></div>"
				}
			],
			"order": [
				<?
					//FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
					foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
						{
							$num_ordenacao_grid++;
							echo '
							[ ';
							echo $array_colunas_key_grid[$coluna_nome].', ';
							echo '"'.$coluna_ordenacao.'" ';
							echo ' ]';
							if($num_ordenacao_grid<$total_ordenacao_grid) echo ',';
						}
				?>
			],
			//TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"zeroRecords": "A busca não retornou resultados",
				"info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
				"infoEmpty": "0 registros",
				"infoFiltered": "de um total de _MAX_ cadastrados",
				"loadingRecords": "Carregando...",
				"sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
				"search":         "Buscar:",
				"paginate": {
						"first":      "Primeira",
						"last":       "Última",
						"next":       "Próxima",
						"previous":   "Anterior"
				}
			},
	    } );



		<!--### CONTROLES DO FILTROS EXTERNOS NO GRID ###-->
        $('#filtro_secao, #filtro_categoria, #filtro_status, #filtro_versao').on( 'change', function () {
			oTable1
				.columns( $(this).attr('data-id') )
				.search( this.value )
				.draw();
		});
		<!--### CONTROLES DO FILTROS EXTERNOS NO GRID ###-->



		<!--### CONTROLES DO INPUT CHOSEN ###-->
		$('.chosen-select').chosen({allow_single_deselect:true, search_contains: true});
		//resize the chosen on window resize

		//FUNÇÃO QUE CORRIGE BUG NO VALIDATE() PARA VALIDAR CAMPOS HIDDEN DO CHOSEN
		jQuery.validator.setDefaults({
		  ignore: []
		});

		$(window)
		.off('resize.chosen')
		.on('resize.chosen', function() {
			$('.chosen-select').each(function() {
				 var $this = $(this);
				 $this.next().css({'width': $this.parent().width()});
			})
		}).trigger('resize.chosen');
		<!--### CONTROLES DO INPUT CHOSEN ###-->



		//HACK PARA QUANDO PASSAR O MOUSE SOBRE OS BOTÕES O CURSOR FICAR TIPO pointer
		$('#sample-table-2 tbody').on( 'mouseover', 'tr td:last-child i', function () { $(this).css("cursor","pointer") } );



		/*###################################################################
		|																	|
		|	A função abaixo é aque controla todas as ações dos botões de 	|
		|	ação de cada registro da tabela. Sempre que for necessário 		|
		|	adicionar novos botões será nesta função que as as operações 	|
		|	serão definidas, incluindo operações de selecionar registro, 	|
		|	comandos de alterar, comandos de excluir e demais ações do 		|
		|	módulo.															|
		|																	|
		###################################################################*/
		$('#sample-table-2 tbody').on( 'click', 'tr td:last-child i', function () {

				//REFERÊNCIA PARA INDICAR AS POSIÇÕES PADRÃO DO ID DO REGISTRO NO GRID
				//eq(5) class: hidden-md hidden-lg => eq(7) id: 37 = REDUZIDO => (hidden-md eq(5): 0 / hidden-sm eq(1): -1)
				//eq(1) class: hidden-sm hidden-xs action-buttons => eq(3) id: 27 = GRANDE (hidden-md eq(5): -1 / hidden-sm eq(1): 0)


				//$('.senhamask').on( 'click', function () { alert("over"); } );
				//$('.senhamask').on('click', function () { alert("over"); } );




				//CONDIÇÃO PARA DEFINIR A VARIÁVEL id_reg QUE ARMAZENA O ID DO REGISTRO NO GRID
				if($(this).parents().eq(5).attr("class").indexOf('hidden-md')==0) var id_reg = $(this).parents().eq(7).attr("id");
				else if($(this).parents().eq(1).attr("class").indexOf('hidden-sm')==0) var id_reg = $(this).parents().eq(3).attr("id");





				<!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ALTERAR NO GRID ###-->
				if(($(this).attr('data-id')=="alterar")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
					{
						//alert("alterar "+id_reg);

						//TROCA O TÍTULO DO FORMULÁRIO PARA A VARIÁVEL title QUE ESTÁ NO BOTÃO
						$('#form-titulo-acao').html( $(this).attr('title') );

						//ESCONDE O BOTÃO DE INSERIR E MOSTRA O DE ALTERAR
						$('#btn-inserir').hide();
						$('#btn-alterar').show();

						<!--### MOSTRA A DIV DE CARREGANDO ###-->
						var contentArea = $('body');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						<!--### MOSTRA A DIV DE CARREGANDO ###-->


						<!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
						$.ajax({
							dataType: "json",
							//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
							url: '<? echo $sistema; ?>/acoes_registros.php',
							data: {
								//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
								chave_primaria: id_reg,
								operacao: "selecionar",
								filial: "<? echo $_REQUEST['filial']; ?>"
							},
							success: function(data, textStatus, jqXHR)
							{
								//console.log("data: "+data+"\ntextStatus: "+textStatus);

								<!--### EACH PARA PERCORRER TODO O FORMULÁRIO DE CADASTRO E PREENCHER COM OS DADOS DO BANCO ###-->
								$.each($('#form-cadastros :input'), function(index, element) {
									/*console.log("type: "+index+" => "+element.type);
									console.log("name: "+index+" => "+element.name);
									console.log("id: "+index+" => "+element.id);
									console.log("value: "+index+" => "+element.value);
									console.log("lang: "+index+" => "+element.lang);
									console.log("data[id]: "+data[1][element.id]);
									console.log("data[name]: "+data[1][element.name]);
									console.log("this.val(): "+$(this).val());
									console.log("--------\n");*/

									if(element.type=="radio")
										{
											if($(this).val() == data[1][element.name])
												{
													$(this).prop('checked',true);
													$(':input[name='+element.name+'_hidden]').val(data[1][element.name]);
												}
											else $(this).prop('checked',false);
										}
									/*###################################################################
									|																	|
									|	O padrão de nomenclatura dos campos checkbox sempre será o 		|
									|	name='campo[]' e o id='campo_X' onde X é um numeral incremental	|
									|	e o JSON deve retornar um array com a chave sendo o nome, em  	|
									| 	formato vetor, do campo do checkbox (ex: foto_secao[])			|
									|																	|
									###################################################################*/
									if(element.type=="checkbox")
                                        {
                                            /*console.log("\n-------");
                                            console.log("element.name: "+element.name);
                                            console.log("element.id: "+element.id);
                                            console.log("this.val: "+$(this).val());
                                            console.log("this.id: "+$(this).attr('id'));*/
                                            $(this).prop('checked', false);
                                            if(data[1][element.name] != null)
                                                {
                                                    $.each(data[1][element.name], function(key, value) {
                                                        //console.log("value do bd: "+value);
                                                        if($("#"+element.id).val() == value)
                                                            {
                                                                //console.log("#"+element.id+" é true");
                                                                $("#"+element.id).prop('checked', true);
                                                            }
                                                    });
                                                }
                                        }
									/*###################################################################
									|																			|
									|	Para contornar o bug de serialize() do jquery com campo tipo 		|
									|	radio é necessário incluir no form um campo tipo hidden com o 	|
									|	memso nome do radio acrescido de _hidden e colocar como 			|
									|	lang="NaoAtribuir" para não processar no $.each					|
									|																			|
									###################################################################*/
									else if((element.lang!="NaoAtribuir")&&(element.type!="radio")&&(element.type!="checkbox"))
										{
											$(this).val(data[1][element.id]).trigger('chosen:updated');
										}
								});
								<!--### EACH PARA PERCORRER TODO O FORMULÁRIO DE CADASTRO E PREENCHER COM OS DADOS DO BANCO ###-->



								<!--### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###-->
								$('#txt_id').html( data[1]["<?php echo $sistema_prefixo_campos; ?>id_reg_user"] );
								var texto_id_reg = "<strong>Criação:</strong> <br> "+data[1]["<?php echo $sistema_prefixo_campos; ?>criacao_data"]+" - "+data[1]["reg_criacao_usuario"];
								if(data[1]["reg_atualizacao_usuario"] != null) texto_id_reg += "<br><br><strong>Última atualização:</strong> <br> "+data[1]["<? echo $sistema_prefixo_campos; ?>atualizacao_data"]+" - "+data[1]["reg_atualizacao_usuario"];
								$('#txt_id_reg').attr("data-content", texto_id_reg);
								$('#info_reg_user').show();
								<!--### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###-->

								<!--### ESCONDE A DIV DE CARREGANDO ###-->
								contentArea.css('opacity', 1);
								contentArea.prevAll('.ajax-loading-overlay').remove();
								<!--### ESCONDE A DIV DE CARREGANDO ###-->

								//CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
								$('#modal-form').modal("show");

							},
							error: function(jqXHR, textStatus, errorThrown) {
								//MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
								//console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
								$.gritter.add({
									title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
									text: errorThrown,
									class_name: 'gritter-error gritter-center',
									sticky: false,
									fade_out_speed:500,
									after_close: function(e) {
										contentArea.css('opacity', 1)
										contentArea.prevAll('.ajax-loading-overlay').remove();
									}
								});
							}
						});
						<!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
					}
				<!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ALTERAR NO GRID ###-->






























                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE HISTÓRICO DE LEITURA NO GRID ###-->
                else if(($(this).attr('data-id')=="leitura")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
                    {
                        //alert("alterar "+id_reg);

                        //ESCONDE OS BOTÕES DE INSERIR E ALTERAR
                        $('#btn-inserir').hide();
                        $('#btn-alterar').hide();

                        <!--### MOSTRA A DIV DE CARREGANDO ###-->
                        var contentArea = $('body');
                        contentArea
                        .css('opacity', 0.25)

                        var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                        var offset = contentArea.offset();
                        loader.css({top: offset.top, left: offset.left})
                        <!--### MOSTRA A DIV DE CARREGANDO ###-->




                        <!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
                        $.ajax({
                            dataType: "json",
                            //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                            url: '<? echo $sistema; ?>/acoes_registros.php',
                            data: {
                                //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                                chave_primaria: id_reg,
                                operacao: "leitura",
                                filial: "<? echo $_REQUEST['filial']; ?>"
                            },
                            success: function(data, textStatus, jqXHR)
                            {
                                //console.log("data: "+data+"\ntextStatus: "+textStatus);


                                <!--### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###-->
                                $('#form-leitura #txt_id').html( data[1]["<?php echo $sistema_prefixo_campos; ?>id_reg_user"] );
                                var texto_id_reg = "<strong>Criação:</strong> <br> "+data[1]["<?php echo $sistema_prefixo_campos; ?>criacao_data"]+" - "+data[1]["reg_criacao_usuario"];
                                if(data[1]["reg_atualizacao_usuario"] != null) texto_id_reg += "<br><br><strong>Última atualização:</strong> <br> "+data[1]["<? echo $sistema_prefixo_campos; ?>atualizacao_data"]+" - "+data[1]["reg_atualizacao_usuario"];
                                $('#form-leitura #txt_id_reg').attr("data-content", texto_id_reg);
                                $('#form-leitura #info_reg_user').show();
                                <!--### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###-->

                                //ADICIONA O TÍTULO DO ARQUIVO
                                $("#form-leitura #doc_titulo_hist").val(data[1].doc_titulo);

                                //ADICIONA O TÍTULO DO ARQUIVO
                                $("#form-leitura #historico").html(data[1].historico);

                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                contentArea.css('opacity', 1);
                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                <!--### ESCONDE A DIV DE CARREGANDO ###-->

                                //CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
                                $('#modal-form-leitura').modal("show");

                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                                //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                                $.gritter.add({
                                    title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                    text: errorThrown,
                                    class_name: 'gritter-error gritter-center',
                                    sticky: false,
                                    fade_out_speed:500,
                                    after_close: function(e) {
                                        contentArea.css('opacity', 1)
                                        contentArea.prevAll('.ajax-loading-overlay').remove();
                                    }
                                });
                            }
                        });
                        <!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
                    }
                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE HISTÓRICO DE LEITURA NO GRID ###-->




























                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE VERSÕES NO GRID ###-->
                else if(($(this).attr('data-id')=="versoes")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
                    {

                        <!--### MOSTRA A DIV DE CARREGANDO ###-->
                        var contentArea = $('body');
                        contentArea
                        .css('opacity', 0.25)

                        var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                        var offset = contentArea.offset();
                        loader.css({top: offset.top, left: offset.left})
                        <!--### MOSTRA A DIV DE CARREGANDO ###-->

                        //CHAMA A FUNÇÃO QUE ABRE OS DADOS DAS VERSÃO DO DOCUMENTO
                        SelecionaVersaoDocumento(id_reg, '', contentArea, 'interna', tinymce);

                    }
                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE VERSÕES NO GRID ###-->






































                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE PERMISSÕES NO GRID ###-->
                else if(($(this).attr('data-id')=="permissoes")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
                    {

                        <!--### MOSTRA A DIV DE CARREGANDO ###-->
                        var contentArea = $('body');
                        contentArea
                        .css('opacity', 0.25)

                        var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                        var offset = contentArea.offset();
                        loader.css({top: offset.top, left: offset.left})
                        <!--### MOSTRA A DIV DE CARREGANDO ###-->



                        <!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
                        $.ajax({
                            dataType: "json",
                            //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                            url: '<? echo $sistema; ?>/acoes_registros.php',
                            data: {
                                //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                                chave_primaria: id_reg,
                                operacao: "selecionar",
                                filial: "<? echo $_REQUEST['filial']; ?>"
                            },
                            success: function(data, textStatus, jqXHR)
                            {
                                //console.log("data: "+data+"\ntextStatus: "+textStatus);

                                //ATRIBIU O ID DO DOCUMENTO NA VARIAVEL id_doc
                                var id_doc = data[1]["doc_id"];

                                //ARMAZENA NO FORM DE USUÁRIO O ID DO DOCUMENTO
                                $("#form-permissoes #doc_id").val(id_doc);

                                //ESCREVE O NOME DO DOCUMENTO
                                $("#form-permissoes #doc_titulo").html(data[1]["doc_titulo"]);

                                //REDESENHA A TABELA DE USUÁRIOS
                                oTable1_usuarios.draw();

                                //REDESENHA A TABELA DE NÍVEIS
                                oTable1_niveis.draw();

                                //REDESENHA A TABELA DE NÍVEIS
                                oTable1_grupos.draw();


                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                contentArea.css('opacity', 1);
                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                <!--### ESCONDE A DIV DE CARREGANDO ###-->

                                //CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
                                $('#modal-form-permissoes').modal("show");

                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                                //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                                $.gritter.add({
                                    title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                    text: errorThrown,
                                    class_name: 'gritter-error gritter-center',
                                    sticky: false,
                                    fade_out_speed:500,
                                    after_close: function(e) {
                                        contentArea.css('opacity', 1)
                                        contentArea.prevAll('.ajax-loading-overlay').remove();
                                    }
                                });
                            }
                        });
                        <!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
                    }
                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE PERMISSÕES NO GRID ###-->



































				<!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE EXCLUIR NO GRID ###-->
				else if(($(this).attr('data-id')=="excluir")&&("<? echo $auth[$sistema]["excluir"]; ?>"=="sim"))
					{
						//alert("excluir "+id_reg);

						<!--### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A EXCLUSÃO ###-->
						bootbox.dialog({
							message: "<span class='bigger-110'>O registro será excluído. Confirma?</span>",
							buttons:
							{
								"danger" :
								{
									"label" : "Excluir!",
									"className" : "btn-sm btn-danger",
									"callback": function() {

										<!--### MOSTRA A DIV DE CARREGANDO ###-->
										var contentArea = $('body');
										contentArea
										.css('opacity', 0.25)

										var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
										var offset = contentArea.offset();
										loader.css({top: offset.top, left: offset.left})
										<!--### MOSTRA A DIV DE CARREGANDO ###-->




										<!--### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###-->
										$.ajax({
											type: "POST",
											//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
											url: '<? echo $sistema; ?>/acoes_registros.php',
											data: {
												//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
												chave_primaria: id_reg,
												operacao: "excluir",
												filial: "<? echo $_REQUEST['filial']; ?>"
											},
											success: function(data)
											{
												<!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
												if (data === 'OK') {
													//ABRE MENSAGEM DE OK
													$.gritter.add({
														title: '<i class="ace-icon fa fa-check"></i> OK!',
														text: '<? echo $registro_excluido; ?>',
														class_name: 'gritter-success gritter-center',
														fade_out_speed:500,
														time:2000,
														before_open: function(e){
															<!--### ESCONDE A DIV DE CARREGANDO ###-->
															contentArea.css('opacity', 1)
															contentArea.prevAll('.ajax-loading-overlay').remove();
															<!--### ESCONDE A DIV DE CARREGANDO ###-->

															//FADEOUT NA LINHA DO GRID QUE FOI EXCLUÍDO
															$('#sample-table-2 #'+id_reg).fadeOut();

															//ATUALIZA A TABELA DO GRID
															oTable1.ajax.reload(null,false);
														}
													});
												}
												<!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






												<!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
												else {
													$.gritter.add({
														title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
														//O TEXTO DA MENSAGEM DE ERRO SERÁ TUDO O QUE TIVER RETORNADO DE MENSAGEM NO ARQUIVO DE EXECUÇÃO
														text: data,
														class_name: 'gritter-error gritter-center',
														sticky: false,
														fade_out_speed:500,
														after_close: function(e) {
															<!--### ESCONDE A DIV DE CARREGANDO ###-->
															contentArea.css('opacity', 1)
															contentArea.prevAll('.ajax-loading-overlay').remove();
															<!--### ESCONDE A DIV DE CARREGANDO ###-->
														}
													});
												}
											}
										});
										<!--### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###-->
									}
								},
								"button" :
								{
									"label" : "Cancelar",
									"className" : "btn-sm"
								}
							}
						});
                        <!--### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A EXCLUSÃO ###-->

					}
				<!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE EXCLUIR NO GRID ###-->





















				<!--### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM AUTORIZAÇÃO PARA EXECUTAR A OPERAÇÃO ###-->
				else
					{
						$.gritter.add({
							title: '<i class="ace-icon fa fa-times"></i> Erro!',
							text: '<? echo $sistema_nao_autorizado; ?>',
							class_name: 'gritter-error gritter-center',
							sticky: false,
							fade_out_speed:500,
							before_open: function(e){
								$('#modal-form').modal("hide");
							}
						});
					}
				<!--### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM AUTORIZAÇÃO PARA EXECUTAR A OPERAÇÃO ###-->
			} );



























            <!--### FUNÇÃO QUE CARREGA OS DADOS DE VERSÕES DO DOCUMENTO ###-->
            function SelecionaVersaoDocumento(id_documento, id_versao, contentArea, origem, tinymce)
                {
                    <!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->
                    var DocResult = $.ajax({
                        data: "POST",
                        dataType: "json",
                        //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                        url: '<? echo $sistema; ?>/acoes_registros.php',
                        data: {
                            //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                            chave_primaria: id_documento,
                            operacao: "selecionar",
                            filial: "<? echo $_REQUEST['filial']; ?>"
                        },
                        success: function(data, textStatus, jqXHR)
                        {
                            //console.log("data: "+data+"\ntextStatus: "+textStatus);

                            //ATRIBIU O ID DO DOCUMENTO NA VARIAVEL id_doc
                            var id_doc = data[1]["doc_id"];

                            //ARMAZENA NO FORM DE USUÁRIO O ID DO DOCUMENTO
                            $("#form-versoes #doc_id").val(id_doc);

                            //ESCREVE O NOME DO DOCUMENTO
                            $("#form-versoes #doc_titulo").html(data[1]["doc_titulo"]);

                            //ESCONDE OS BOTÕES DE AÇÃO DO FORMULÁRIO DE VERSÕES
                            $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar, #form-versoes #btn-publicar, #form-versoes #btn-revogar").hide();

                            //REMOVE OS VALORES ATUAIS DO SELECT DE STATUS DE VERSÃO
                            $('#form-versoes #doc_ver_status').find('option').remove().end();

                            //INSERE O CAMPO DE VALOR VAZIO NO SELECT DE STATUS DE VERSÃO
                            $('#form-versoes #doc_ver_status').append($('<option>', { value: '', text: 'Escolha o status' }));

                            //ESCONDE A MENSAGEM DE SEM NÍVEL/GRUPO
                            $("#msg-sem-nivel-grupo").hide();

                            <!--### ZERAR O FORMULÁRIO DE VERSÕES ###-->
                            $('#form-versoes #arquivo_enviado').val(0);
                            if(origem=='interna') {
                                $('#form-versoes').trigger("reset");
                            }
                            $('#form-versoes .chosen-select').trigger("chosen:updated");
                            $("#form-versoes #arquivo_existente").html('');
                            $('.ace-file-container, .ace-file-input').remove();
                            $("#form-versoes #arquivo_filename").val('');
                            $("#form-versoes #doc_ver_arquivo").val('');
                            $("#form-versoes #doc_ver_arquivo_original").val('');
                            <!--### ZERAR O FORMULÁRIO DE VERSÕES ###-->


                            <!--### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###-->
                            $('#form-versoes #operacao_upload').val('inserir');
                            $('#form-versoes #files').fileinput('refresh');
                            $('#form-versoes label[for=files]').text('Enviar arquivo');
                            <!--### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###-->


                            //LIMPA A TABELA DE VERSÕES
                            $('#tabela-versoes tbody').html('');

                            <!--### POPULA A TABELA DE VERSÕES ###-->
                            if(data["documentos_versoes"]!=null)
                                {
                                    $.each(data["documentos_versoes"], function(id_doc_ver, dados_doc_ver) {
                                        //console.log(id_doc_ver);
                                        // console.log(dados_doc_ver);
                                        $('#tabela-versoes tbody').append('\
                                        <tr id="linha_doc_ver_'+id_doc_ver+'">\
                                            <td>'+dados_doc_ver.versao+'</td>\
                                            <td>'+dados_doc_ver.status_nome+'</td>\
                                            <td>'+dados_doc_ver.observacoes+'</td>\
                                            <td>'+dados_doc_ver.obs_aprovador+'</td>\
                                            <td><a class="dark" style="cursor: pointer;" data-rel="popover_versao" data-trigger="hover" data-placement="top" data-content="'+dados_doc_ver.criacao+dados_doc_ver.atualizacao+dados_doc_ver.aprovacao+dados_doc_ver.publicacao+dados_doc_ver.revogacao+'" title="Histórico da versão"><i class="ace-icon fa fa-history bigger-120"></i></a></td>\
                                            <td>\
                                                <div id="botoes_doc_ver_'+id_doc_ver+'"" class="action-buttons">\
                                                    <a data-toggle="nova-versao-baseada" data-id="'+id_doc_ver+'" title="Nova versão baseada" class="blue botoes-versoes" style="display:none;"><i class="ace-icon fa fa-plus-square bigger-120"></i></a>\
                                                    <a data-toggle="ver-versao" data-id="'+id_doc_ver+'" title="Visualizar versão" class="dark botoes-versoes" style="display:none;"><i class="ace-icon fa fa-search-plus bigger-120"></i></a>\
                                                    <a data-toggle="publicar" data-id="'+id_doc_ver+'" title="Publicar versão" class="green botoes-versoes" style="display:none;"><i class="ace-icon fa fa-share-square-o bigger-120"></i></a>\
                                                    <a data-toggle="revogar" data-id="'+id_doc_ver+'" title="Revogar versão" class="grey botoes-versoes" style="display:none;"><i class="ace-icon fa fa-ban bigger-120"></i></a>\
                                                    <a data-toggle="excluir" data-id="'+id_doc_ver+'" title="Excluir versão" class="red botoes-versoes" style="display:none;"><i class="ace-icon fa fa-trash bigger-120"></i></a>\
                                                </div>\
                                            </td>\
                                        </tr>');

                                        //MOSTRA O BOTÃO DE NOVA VERSÃO SE O DOCUMENTO JÁ PASSOU PELA APROVAÇÃO
                                        if((dados_doc_ver.status>20)&&((data["pertence_nivel_superior"]=="sim")||(dados_doc_ver.criacao_usuario=="<?php echo $_SESSION['login']['id']; ?>"))) $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='nova-versao-baseada']").show();

                                        //SE O STATUS FOR APROVADO E SE FOR O USUÁRIO CRIADOR OU PUBLICADOR,
                                        //HABILITA O BOTÃO DE PUBLICAR
                                        if((dados_doc_ver.status=='40')&&((data["publicador"]=="sim")||(dados_doc_ver.criacao_usuario=="<?php echo $_SESSION['login']['id']; ?>"))) $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='publicar']").show();

                                        //SE O STATUS FOR VIGENTE E SE FOR O USUÁRIO APROVADOR,
                                        //HABILITA O BOTÃO DE PUBLICAR
                                        if((dados_doc_ver.status=='50')&&(data["aprovador"]=="sim")) $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='revogar']").show();

                                        //SE O STATUS FOR DIFERENTE DE VIGENTE OU REVOGADO E O USUÁRIO FOR APROVADOR E
                                        //DE NÍVEL SUPERIOR, MOSTRA O BOTÃO DE EXCLUIR
                                        if((dados_doc_ver.status!='50')&&(dados_doc_ver.status!='60')&&(data["aprovador"]=="sim")&&(data["pertence_nivel_superior"]=="sim")) $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='excluir']").show();

                                        //SE O STATUS FOR EM ELABORAÇÃO E FOR O USUÁRIO CRIADOR
                                        //MOSTRA O BOTÃO DE EXCLUIR
                                        if((dados_doc_ver.status=='0')&&(dados_doc_ver.criacao_usuario=="<?php echo $_SESSION['login']['id']; ?>")) $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='excluir']").show();

                                        //SE É O USUÁRIO CRIADOR, MOSTRA O BOTÃO DE VISUALIZAR VERSÃO
                                        if(dados_doc_ver.criacao_usuario=="<?php echo $_SESSION['login']['id']; ?>") $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='ver-versao']").show();

                                        //SE É APROVADOR E PERTENCE A NÍVEL SUPERIOR, MOSTRA BOTÃO DE VISUALIZAR
                                        if((data["aprovador"]=="sim")&&(data["pertence_nivel_superior"]=="sim")) $("#botoes_doc_ver_"+id_doc_ver+" a[data-toggle='ver-versao']").show();
                                    });
                                    <!--### POPULA A TABELA DE VERSÕES ###-->
                                }

                            //FECHA A CAIXA DO DETALHE DA VERSÃO
                            $('#widget-detalhe-versao').widget_box('hide');

                            //FUNÇÃO QUE ATIVA O PROPOVER DO HISTÓRICO DO REGISTRO DE VERSÃO
                            $('[data-rel=popover_versao]').popover({container:'body', html:true});

                            //POPULA O OBJETO DOS NÍVEIS SUPERIORES DO DOCUMENTO
                            $("#form-versoes #niveis_superiores").val(JSON.stringify(data["niveis_superiores"]));

                            //POPULA O OBJETO DOS SUPERVISORES DOS NÍVEIS
                            $("#form-versoes #supervisores_niveis").val(JSON.stringify(data["supervisores_niveis"]));


                            /*### ESCONDE A DIV DE CARREGANDO ###*/
                            contentArea.css('opacity', 1);
                            contentArea.prevAll('.ajax-loading-overlay').remove();
                            /*### ESCONDE A DIV DE CARREGANDO ###*/


                            /*### HABILITA AVISO SE NÃO EXISTIR NÍVEIS NO DOCUMENTO ###*/
                            if(data["doc_sem_nivel_grupo"]==true) {

                                $("#msg-sem-nivel-grupo").show();
                            }

                            /*### HABILITA AVISO SE NÃO EXISTIR NÍVEIS NO DOCUMENTO ###*/

                            //CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
                            $('#modal-form-versoes').modal("show");


                            <!--### FUNÇÕES QUANDO CLICA NOS BOTÕES DE AÇÃO DE CADA VERSÃO DO DOCUMENTO ###-->
                            $('.botoes-versoes, #titulo-form-versao').on('click', function(event) {

                                event.preventDefault();
                                // console.log("toggle");
                                // console.log($(this).data("toggle"));
                                // console.log("id");
                                // console.log($(this).data("id"));

                                //GUARDA A AÇÃO DO BOTÃO
                                var acao = $(this).data("toggle");

                                //GUARDA O ID DA VERSÃO
                                var id_reg = $(this).data("id");

                                $('#tabela-versoes tbody tr').removeClass('bg-success');

                                AcoesVersao(acao, id_reg, data, tinymce, contentArea, 'interna');

                            })
                            <!--### FUNÇÕES QUANDO CLICA NOS BOTÕES DE AÇÃO DE CADA VERSÃO DO DOCUMENTO ###-->




                            <!--### FUNÇÕES QUANDO CLICA NOS BOTÕES DE AÇÃO DE CADA VERSÃO DO DOCUMENTO ###-->
                            if(id_versao>0)
                                {
                                    AcoesVersao('ver-versao', id_versao, data, tinymce, contentArea, 'externa');
                                }
                            <!--### FUNÇÕES QUANDO CLICA NOS BOTÕES DE AÇÃO DE CADA VERSÃO DO DOCUMENTO ###-->


                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                            //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                            $.gritter.add({
                                title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                text: errorThrown,
                                class_name: 'gritter-error gritter-center',
                                sticky: false,
                                fade_out_speed:500,
                                after_close: function(e) {
                                    contentArea.css('opacity', 1)
                                    contentArea.prevAll('.ajax-loading-overlay').remove();
                                }
                            });
                        }

                    });
                    <!--### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###-->

                    //RESULTADO DA FUNCAO
                    return DocResult;
                }
            <!--### FUNÇÃO QUE CARREGA OS DADOS DE VERSÕES DO DOCUMENTO ###-->



































            <!--### FUNÇÃO QUE EXECUTA AS AÇÕES DOS BOTÕES DE DE CADA VERSÃO ###-->
            function AcoesVersao(acao, id_versao, DocResult, tinymce, contentArea, origem)
                {
                    //ESCONDE O BOX DE DETALHE DA VERSÃO
                    $('#widget-detalhe-versao').widget_box('hide');
                    // console.log("escondeu");

                    //ADICIONA A CLASS DE MODAL-OPEN NO BODY
                    contentArea.addClass('modal-open');

                    <!--### ZERAR O FORMULÁRIO DE VERSÕES ###-->
                    $('#form-versoes #arquivo_enviado').val(0);
                    if(origem=='interna') {
                        $('#form-versoes').trigger("reset");
                    }
                    $('#form-versoes #doc_ver_id').val('');
                    $('#form-versoes #doc_ver_pid').val('');
                    $('#form-versoes .chosen-select').trigger("chosen:updated");
                    $("#form-versoes #arquivo_existente").html('');
                    $('.ace-file-container, .ace-file-input').remove();
                    $("#form-versoes #arquivo_filename").val('');
                    <!--### ZERAR O FORMULÁRIO DE VERSÕES ###-->

                    <!--### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###-->
                    $('#form-versoes #operacao_upload').val('inserir');
                    $('#form-versoes #files').fileinput('refresh');
                    $('#form-versoes label[for=files]').text('Enviar arquivo');
                    <!--### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###-->

                    <!--### ESCONDE OS CAMPOS DE ARQUIVO E TEXTO ###-->
                    $('#campo_texto').hide();
                    $('#campo_file').hide();
                    <!--### ESCONDE OS CAMPOS DE ARQUIVO E TEXTO ###-->

                    //LIMPA O CAMPO DE OBSERVAÇÕES
                    $('#form-versoes #doc_ver_observacoes').val('');

                    <!--### ESCONDE E LIMPA O CAMPO DE OBS APROVADOR ###-->
                    $('#campo_obs_aprovador').hide();
                    $('#form-versoes #doc_ver_obs_aprovador').val('');
                    $('#info_aprovador').html('');
                    $('#info_publicador').html('');
                    $('#info_revogador').html('');
                    <!--### ESCONDE E LIMPA O CAMPO DE OBS APROVADOR ###-->

                    //ESCONDE OS BOTÕES DE AÇÃO DO FORMULÁRIO DE VERSÕES
                    $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar, #form-versoes #btn-publicar, #form-versoes #btn-revogar").hide();

                    //REMOVE OS VALORES ATUAIS DO SELECT DE STATUS DE VERSÃO
                    $('#form-versoes #doc_ver_status').find('option').remove().end();

                    //INSERE O CAMPO DE VALOR VAZIO NO SELECT DE STATUS DE VERSÃO
                    $('#form-versoes #doc_ver_status').append($('<option>', { value: '', text: 'Escolha o status' }));


                    if((acao=="nova-versao-baseada")||(acao=="ver-versao")) {

                        <!--### MOSTRA A DIV DE CARREGANDO ###-->
                        contentArea.css('opacity', 0.25)

                        var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                        var offset = contentArea.offset();
                        loader.css({top: offset.top, left: offset.left})
                        <!--### MOSTRA A DIV DE CARREGANDO ###-->

                        <!--### AJAX QUE BUSCA AS INFORMAÇÕES DA VERSÃO ESCOLHIDA ###-->
                        $.ajax({
                            method: "POST",
                            dataType: "json",
                            //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                            url: '<? echo $sistema; ?>/acoes_registros.php',
                            data: {
                                //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                                chave_primaria: id_versao,
                                operacao: acao,
                                filial: "<? echo $_REQUEST['filial']; ?>"
                            },
                            success: function(data, textStatus, jqXHR)
                            {
                                // console.log("tinymce");
                                // console.log(tinymce);
                                // console.log("tinymce get");
                                // console.log(tinymce.get('doc_ver_texto'));
                                // console.log("tinymce setContent");
                                // console.log(tinymce.get('doc_ver_texto').setContent(data.doc_ver_texto));
                                // console.log("tinymce setMode");
                                // console.log(tinymce.get('doc_ver_texto').setMode('design'));

                                <!--### PREENCHE OS CAMPOS DA VERSÃO ATUAL ###-->
                                $("#form-versoes #doc_ver_id").val(data.doc_ver_id);
                                $("#form-versoes #doc_ver_pid").val(data.doc_ver_pid);
                                $("#form-versoes #doc_ver_tipo").val(data.doc_ver_tipo).trigger('chosen:updated');
                                tinymce.get('doc_ver_texto').setMode('design');
                                tinymce.get('doc_ver_texto').setContent(data.doc_ver_texto);
                                $("#form-versoes #doc_ver_versao").val(data.doc_ver_versao);
                                $("#form-versoes #doc_ver_vigencia").val(data.doc_ver_vigencia);
                                $("#form-versoes #doc_ver_status").val(data.doc_ver_status).trigger('chosen:updated');
                                $("#form-versoes #doc_ver_observacoes").val(data.doc_ver_observacoes).trigger('chosen:updated');
                                <!--### PREENCHE OS CAMPOS DA VERSÃO ATUAL ###-->


                                <!--### MOSTRA/ESCONDE AS ÁREAS DE UPLOAD/LINK CONFORME TIPO E PREENCHE COM O ARQUIVO EXISTENTE ###-->
                                if(data.doc_ver_tipo=="arquivo") {

                                    $('#campo_file').slideDown();
                                    $("#form-versoes #arquivo_existente").html(data.arquivo_existente);
                                    $("#form-versoes #arquivo_enviado").val(1);
                                    $("#form-versoes #doc_ver_arquivo").val(data.doc_ver_arquivo);
                                    $("#form-versoes #doc_ver_arquivo_original").val(data.doc_ver_arquivo_original);
                                }
                                else if(data.doc_ver_tipo=="texto") {

                                    $('#campo_texto').slideDown();
                                    $('#form-versoes #arquivo_enviado').val(0);
                                    $("#form-versoes #arquivo_existente").html('');
                                    $('.ace-file-container, .ace-file-input').remove();
                               }
                                <!--### MOSTRA/ESCONDE AS ÁREAS DE UPLOAD/LINK CONFORME TIPO E PREENCHE COM O ARQUIVO EXISTENTE ###-->


                                <!--### AÇÕES QUANDO FOR PARA VER UMA VERSÃO ATUAL ###-->
                                if(acao=="ver-versao")
                                    {
                                        //MOSTRA OS BOTÕES DE AÇÃO DO FORMULÁRIO DE VERSÕES
                                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").show();
                                        $("#form-versoes #btn-salvar").show();

                                        //CARREGA O CONTEÚDO DAS OBSERVAÇÕES
                                        $("#form-versoes #doc_ver_observacoes").val(data.doc_ver_observacoes);

                                        //CARREGA O CONTEÚDO DAS OBS DO APROVADOR
                                        $("#form-versoes #doc_ver_obs_aprovador").val(data.doc_ver_obs_aprovador);

                                        //POPULA O CAMPO DE SELECT DE STATUS DE VERSÃO
                                        $.each(DocResult["status_versoes"], function(valor, texto) {
                                            $('#form-versoes #doc_ver_status').append($('<option>', { value: valor, text: texto })).trigger('chosen:updated');
                                        });

                                        <!--### MARCA SE REQUER OU NÃO LEITURA DO DOCUMENTO ###-->
                                        $.each($("#form-versoes input[name=doc_ver_leitura]"), function(index, element) {
                                            if($(element).val()==data.doc_ver_leitura) $(element).prop('checked', true);
                                            else $(element).prop('checked', false);
                                        });
                                        $("#form-versoes #doc_ver_leitura_hidden").val(data.doc_ver_leitura);
                                        <!--### MARCA SE REQUER OU NÃO LEITURA DO DOCUMENTO ###-->


                                        //MARCA A LINHA DA VERSÃO QUE ESTÁ ABERTA
                                        $("#linha_doc_ver_"+id_versao).addClass('bg-success');

                                        //MARCA A VERSÃO ATUAL DO DOCUMENTO
                                        $('#form-versoes #doc_ver_status').val(data.doc_ver_status).trigger('chosen:updated');

                                        //ABRE A CAIXA DO DETALHE DA VERSÃO
                                        $('#widget-detalhe-versao').widget_box('show');
                                        // console.log("mostrou ver-versao");

                                        //PREENCHE COM AS INFORMAÇÕES DO USUÁRIO APROVADOR
                                        if(data.doc_ver_aprovado_usuario>0) $("#info_aprovador").html('<br>Revisado/Aprovado em <strong>'+data.doc_ver_aprovado_data+'</strong> por <strong>'+data.doc_ver_aprovado_usuario_nome+'</strong>');

                                        //PREENCHE COM AS INFORMAÇÕES DO USUÁRIO PUBLICADOR
                                        if(data.doc_ver_publicado_usuario>0) $("#info_publicador").html('<br>Publicado em <strong>'+data.doc_ver_publicado_data+'</strong> por <strong>'+data.doc_ver_publicado_usuario_nome+'</strong>');

                                        //PREENCHE COM AS INFORMAÇÕES DO USUÁRIO REVOGADOR
                                        if(data.doc_ver_revogado_usuario>0) $("#info_revogador").html('<br>Revogado em <strong>'+data.doc_ver_revogado_data+'</strong> por <strong>'+data.doc_ver_revogado_usuario_nome+'</strong>');

                                        //EXECUTA AS AÇÕES DE ACORDO COM O STATUS
                                        AcoesStatus(data.doc_ver_status, data, DocResult, 'inicial', tinymce)
                                    }
                                <!--### AÇÕES QUANDO FOR PARA VER UMA VERSÃO ATUAL ###-->


                                <!--### AÇÕES QUANDO FOR UMA NOVA VERSÃO BASEADA ###-->
                                if(acao=="nova-versao-baseada")
                                    {
                                        window.location.replace('<?php echo $http_admin; ?>/home.php#documentos/gerenciar|jdrel|doc_id='+data.doc_ver_doc+'|doc_ver_id='+data.doc_ver_novo_id)
                                    }
                                <!--### AÇÕES QUANDO FOR UMA NOVA VERSÃO BASEADA ###-->



                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                contentArea.css('opacity', 1);
                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                <!--### ESCONDE A DIV DE CARREGANDO ###-->

                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                                //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                                $.gritter.add({
                                    title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                    text: errorThrown,
                                    class_name: 'gritter-error gritter-center',
                                    sticky: false,
                                    fade_out_speed:500,
                                    after_close: function(e) {
                                        contentArea.css('opacity', 1)
                                        contentArea.prevAll('.ajax-loading-overlay').remove();
                                    }
                                });
                            }
                        });
                        <!--### AJAX QUE BUSCA AS INFORMAÇÕES DA VERSÃO ESCOLHIDA ###-->
                    }


                    <!--### AÇÕES QUANDO FOR UMA NOVA VERSÃO ###-->
                    if(acao=="nova-versao")
                        {
                            //CRIA O PID DO DOCUMENTO
                            $('#form-versoes #doc_ver_pid').val(((new Date().getTime()).toString(16)) + (Math.random().toString(36).substr(2)));

                            //POPULA O CAMPO DE SELECT DE STATUS DE VERSÃO, BASEADO NA PERMISSÃO DO USUÁRIO
                            $.each(DocResult["status_versoes"], function(valor, texto) {

                                if((DocResult["pertence_nivel_superior"]!="sim")&&(valor=='0'))
                                    {
                                        $('#form-versoes #doc_ver_status').append($('<option>', { value: valor, text: texto })).trigger('chosen:updated');
                                    }
                                else if(DocResult["pertence_nivel_superior"]=="sim")
                                    {
                                        $('#form-versoes #doc_ver_status').append($('<option>', { value: valor, text: texto })).trigger('chosen:updated');
                                    }
                            });

                            //MOSTRA OS BOTÕES DE AÇÃO DO FORMULÁRIO DE VERSÕES
                            $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").show();
                            $("#form-versoes #btn-salvar").show();

                            //ABRE A CAIXA DO DETALHE DA VERSÃO
                            $('#widget-detalhe-versao').widget_box('show');
                            // console.log("mostrou nova-versao");
                        }
                    <!--### AÇÕES QUANDO FOR UMA NOVA VERSÃO ###-->








                    <!--### AÇÕES QUANDO FOR PARA EXCLUIR UMA VERSÃO ###-->
                    else if(acao=="excluir")
                        {

                            <!--### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A EXCLUSÃO ###-->
                            bootbox.dialog({
                                message: "<span class='bigger-110'>A versão será excluída. Confirma?</span>",
                                buttons:
                                {
                                    "danger" :
                                    {
                                        "label" : "Excluir!",
                                        "className" : "btn-sm btn-danger",
                                        "callback": function() {

                                            <!--### MOSTRA A DIV DE CARREGANDO ###-->
                                            contentArea.css('opacity', 0.25)

                                            var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                                            var offset = contentArea.offset();
                                            loader.css({top: offset.top, left: offset.left})
                                            <!--### MOSTRA A DIV DE CARREGANDO ###-->




                                            <!--### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###-->
                                            $.ajax({
                                                type: "POST",
                                                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                                                url: '<? echo $sistema; ?>/acoes_registros.php',
                                                data: {
                                                    //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                                                    chave_primaria: id_versao,
                                                    operacao: "excluir-versao",
                                                    filial: "<? echo $_REQUEST['filial']; ?>"
                                                },
                                                success: function(data)
                                                {
                                                    <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
                                                    if (data === 'OK') {
                                                        //ABRE MENSAGEM DE OK
                                                        $.gritter.add({
                                                            title: '<i class="ace-icon fa fa-check"></i> OK!',
                                                            text: '<? echo $registro_excluido; ?>',
                                                            class_name: 'gritter-success gritter-center',
                                                            fade_out_speed:500,
                                                            time:2000,
                                                            before_open: function(e){
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                                                contentArea.css('opacity', 1)
                                                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->

                                                                //FADEOUT NA LINHA DO GRID QUE FOI EXCLUÍDO
                                                                $('#tabela-versoes #linha_doc_ver_'+id_versao).fadeOut();

                                                                //ATUALIZA O GRID DE DOCUMENTOS
                                                                oTable1.ajax.reload(null,false);
                                                            }
                                                        });
                                                    }
                                                    <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






                                                    <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
                                                    else {
                                                        $.gritter.add({
                                                            title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                                            //O TEXTO DA MENSAGEM DE ERRO SERÁ TUDO O QUE TIVER RETORNADO DE MENSAGEM NO ARQUIVO DE EXECUÇÃO
                                                            text: data,
                                                            class_name: 'gritter-error gritter-center',
                                                            sticky: false,
                                                            fade_out_speed:500,
                                                            after_close: function(e) {
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                                                contentArea.css('opacity', 1)
                                                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                            <!--### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###-->
                                        }
                                    },
                                    "button" :
                                    {
                                        "label" : "Cancelar",
                                        "className" : "btn-sm"
                                    }
                                }
                            });
                            <!--### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A EXCLUSÃO ###-->


                        }
                    <!--### AÇÕES QUANDO FOR PARA EXCLUIR UMA VERSÃO ###-->












                    <!--### AÇÕES QUANDO FOR PARA PUBLICAR OU REVOGAR UMA VERSÃO ###-->
                    else if((acao=="publicar")||(acao=="revogar"))
                        {
                            if(acao=="publicar") {

                                var acao_registro = "publicar-versao";
                                var texto_MSG = "Confirma a publicação da versão?";
                                var tipo_button = "success";
                                var label = "Publicar";
                                var texto_OK = "A versão foi publicada!";

                            } else if(acao=="revogar") {

                                var acao_registro = "revogar-versao";
                                var texto_MSG = "Esta ação é irreversível. Confirma a REVOAGAÇÃO da versão?";
                                var tipo_button = "danger";
                                var label = "Revogar";
                                var texto_OK = "A versão foi revogada!";
                            }

                            <!--### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A PUBLICAÇÃO/REVOGAÇÃO ###-->
                            bootbox.dialog({
                                message: "<span class='bigger-110'>"+texto_MSG+"</span>",
                                buttons:
                                {
                                    tipo_button :
                                    {
                                        "label" : label,
                                        "className" : "btn-sm btn-"+tipo_button,
                                        "callback": function() {

                                            <!--### MOSTRA A DIV DE CARREGANDO ###-->
                                            contentArea.css('opacity', 0.25)

                                            var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                                            var offset = contentArea.offset();
                                            loader.css({top: offset.top, left: offset.left})
                                            <!--### MOSTRA A DIV DE CARREGANDO ###-->




                                            <!--### AJAX PARA POSTAGEM DO PEDIDO PARA PUBLICAR/REVOGAR A VERSÃO ###-->
                                            $.ajax({
                                                method: "POST",
                                                dataType: "json",
                                                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                                                url: '<? echo $sistema; ?>/acoes_registros.php',
                                                data: {
                                                    //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                                                    doc_ver_id: id_versao,
                                                    operacao: acao_registro,
                                                    filial: "<? echo $_REQUEST['filial']; ?>"
                                                },
                                                success: function(data, textStatus, jqXHR)
                                                {
                                                    <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
                                                    if (data.salvo == 'OK') {
                                                        //ABRE MENSAGEM DE OK
                                                        $.gritter.add({
                                                            title: '<i class="ace-icon fa fa-check"></i> OK!',
                                                            text: texto_OK,
                                                            class_name: 'gritter-success gritter-center',
                                                            fade_out_speed:500,
                                                            time:2000,
                                                            before_open: function(e){
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                                                contentArea.css('opacity', 1)
                                                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->

                                                                //FECHAR O MODAL DE VERSÕES
                                                                $('#modal-form-versoes').modal("hide");

                                                                //ATUALIZA O GRID DE DOCUMENTOS
                                                                oTable1.ajax.reload(null,false);
                                                            }
                                                        });
                                                    }
                                                    <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






                                                    <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
                                                    else {
                                                        $.gritter.add({
                                                            title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                                            //O TEXTO DA MENSAGEM DE ERRO SERÁ TUDO O QUE TIVER RETORNADO DE MENSAGEM NO ARQUIVO DE EXECUÇÃO
                                                            text: data.msg_erro,
                                                            class_name: 'gritter-error gritter-center',
                                                            sticky: false,
                                                            fade_out_speed:500,
                                                            after_close: function(e) {
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                                                contentArea.css('opacity', 1)
                                                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                            <!--### AJAX PARA POSTAGEM DO PEDIDO PARA PUBLICAR/REVOGAR A VERSÃO ###-->
                                        }
                                    },
                                    "button" :
                                    {
                                        "label" : "Cancelar",
                                        "className" : "btn-sm"
                                    }
                                }
                            });
                            <!--### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A PUBLICAÇÃO/REVOGAÇÃO ###-->


                        }
                    <!--### AÇÕES QUANDO FOR PARA PUBLICAR OU REVOGAR UMA VERSÃO ###-->

                }
            <!--### FUNÇÃO QUE EXECUTA AS AÇÕES DOS BOTÕES DE DE CADA VERSÃO ###-->

































            <!--### FUNÇÃO QUE REALIZA AÇÕES CONFORME O STATUS DO DOCUMENTO ###-->
            function AcoesStatus(status, data, DocResult, origem, tinymce) {


                // console.log("origem");
                // console.log(origem);
                // console.log(tinymce);
                // console.log("tinymce.activeEditor");
                // console.log(tinymce.activeEditor);
                // console.log("tinymce get");
                // console.log(tinymce.get('doc_ver_texto'));



                <!--### LIBERA TODOS OS CAMPOS ###-->
                $("#form-versoes #doc_ver_tipo").prop("disabled", false).trigger('chosen:updated');
                if(tinymce!=null) tinymce.get('doc_ver_texto').setMode('design');
                $("#form-versoes #doc_ver_versao").prop("readonly", false);
                $("#form-versoes #doc_ver_vigencia").prop("readonly", false);
                $("#form-versoes #doc_ver_status").prop("disabled", false).trigger('chosen:updated');
                $("#form-versoes input[name='doc_ver_leitura']").prop("disabled", false);
                $("#form-versoes #doc_ver_observacoes").prop("readonly", false);
                $("#form-versoes #doc_ver_obs_aprovador").prop("readonly", false);
                <!--### LIBERA TODOS OS CAMPOS ###-->

                // console.log(status);

                <!--### AÇÕES REALIZADAS PARA CADA TIPO DE STATUS ###-->

                //STATUS: EM ELABORAÇÃO || AGUARDANDO APROVAÇÃO
                if(((status==0)||(status==10))&&(status!=''))
                    {
                        // console.log("0 ou 10");

                        //AÇÕES EXECUTADAS SOMENTE SE FOR NO CARREGAMENTO INICIAL DA VERSÃO
                        if(origem=='inicial')
                            {
                                //console.log("inicial");
                                //SE É O USUÁRIO CRIADOR E NÃO TEM FUNÇÃO DE APROVADOR, MANTÉM APENAS O STATUS EM ELABORAÇÃO
                                //E AGUARDANDO APROVAÇÃO E MOSTRA OS BOTÕES DE AÇÃO
                                if((status<=10)&&(data.doc_ver_criacao_usuario=="<?php echo $_SESSION['login']['id']; ?>")&&(DocResult["aprovador"]!="sim"))
                                    {
                                        // console.log("INICIAL (EM ELABORAÇÃO || AGUARDANDO APROVAÇÃO) && É O USUÁRIO CRIADOR && NÃO É APROVADOR")

                                        $.each($("#form-versoes #doc_ver_status option"), function(id, opcao) {
                                            if($(opcao).val()>10) $("#form-versoes #doc_ver_status option[value='"+$(opcao).val()+"']").remove().trigger('chosen:updated');
                                        });
                                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").show();
                                    }
                                //SE É USUÁRIO APROVADOR, MOSTRA TODAS AS OPÇÕES ATÉ O STATUS DE APROVAÇÃO
                                //E MOSTRA OS BOTÕES DE AÇÃO
                                if((status<=10)&&(DocResult["aprovador"]=="sim"))
                                    {
                                        // console.log("INICIAL (EM ELABORAÇÃO || AGUARDANDO APROVAÇÃO) && É APROVADOR")

                                        $.each($("#form-versoes #doc_ver_status option"), function(id, opcao) {
                                            if($(opcao).val()>40) $("#form-versoes #doc_ver_status option[value='"+$(opcao).val()+"']").remove().trigger('chosen:updated');
                                        });
                                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").show();
                                    }
                            }

                        if(origem=='change')
                            {
                                $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").show();
                            }

                        //SE O STATUS FOR AGUARDANDO APROVAÇÃO ESCONDE OS BOTÕES DE AÇÃO, DESABILITA O CAMPO OBSERVAÇÕES
                        //E ESCONDE/DESABILITA OBS APROVADOR
                        if(status==10)
                            {
                                // console.log("AGUARDANDO APROVAÇÃO");

                                $("#form-versoes #doc_ver_observacoes").prop("readonly", true);
                                $("#form-versoes #doc_ver_obs_aprovador").prop("readonly", true);
                                $("#campo_obs_aprovador").hide();
                                $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").hide();
                            }

                        //DESATIVA O STATUS AGUARDANDO APROVAÇÃO
                        $("#form-versoes #doc_ver_status option[value='10']").prop('disabled', true).trigger('chosen:updated');
                    }

                //STATUS: EM APROVAÇÃO
                else if(status==20)
                    {
                        // console.log("20");

                        //SE O STATUS FOR EM APROVAÇÃO, HABILITA OS BOTÕES DE AÇÃO, DESABILITA O CAMPO DE OBSERVAÇÕES,
                        //MOSTRA E HABILITA O CAMPO DE OBS APROVADOR E REMOVE AS OPÇÕES EM ELABORAÇÃO E AGUARDANDO APROVAÇÃO
                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").show();
                        $("#form-versoes #doc_ver_observacoes").prop("readonly", true);
                        $("#form-versoes #doc_ver_obs_aprovador").prop("readonly", false);
                        $("#campo_obs_aprovador").slideDown();
                        $("#form-versoes #doc_ver_status option[value='0']").remove().trigger('chosen:updated');
                        $("#form-versoes #doc_ver_status option[value='10']").remove().trigger('chosen:updated');

                        //AÇÕES QUANDO FOR CARREGAMENTO INICIAL
                        if(origem=='inicial')
                            {
                                <!--### SE NÃO FOR APROVADOR REMOVE TODOS OS STATUS, EXCETO O SELECIONADO E ESCONDE OS BOTÕES DE AÇÃO ###-->
                                if(DocResult["aprovador"]!="sim")
                                    {
                                        // console.log("NÃO É APROVADOR");
                                        $.each($("#form-versoes #doc_ver_status option"), function(id, opcao) {

                                            if($(opcao).val()!=status) $("#form-versoes #doc_ver_status option[value='"+$(opcao).val()+"']").remove().trigger('chosen:updated');
                                        });

                                        //ESCONDE OS BOTÕES DE AÇÃO
                                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").hide();
                                    }
                                <!--### SE NÃO FOR APROVADOR REMOVE TODOS OS STATUS, EXCETO O SELECIONADO E ESCONDE OS BOTÕES DE AÇÃO ###-->
                            }
                    }

                //STATUS: REVISAR/CORRIGIR || APROVADO || VIGENTE || REVOGADO || ARQUIVADO
                else if(status>=30)
                    {
                        // console.log(">=30");

                        //REMOVE OS BOTÕES DE AÇÃO
                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar, #form-versoes #btn-publicar, #form-versoes #btn-revogar").hide();

                        //MOSTRA O CAMPO DE OBS APROVADOR
                        $('#campo_obs_aprovador').show();

                        //AÇÕES QUANDO FOR CARREGAMENTO INICIAL
                        if(origem=='inicial')
                            {
                                <!--### REMOVE TODOS OS STATUS, EXCETO O SELECIONADO ###-->
                                $.each($("#form-versoes #doc_ver_status option"), function(id, opcao) {

                                    if($(opcao).val()!=status) $("#form-versoes #doc_ver_status option[value='"+$(opcao).val()+"']").remove().trigger('chosen:updated');
                                });
                                <!--### REMOVE TODOS OS STATUS, EXCETO O SELECIONADO ###-->

                                //SE O STATUS FOR APROVADO E SE FOR O USUÁRIO PUBLICADOR
                                //OU O USUÁRIO CRIADOS, HABILITA O BOTÃO DE PUBLICAR
                                if(status==40)
                                    {
                                        //MOSTRA O BOTÃO DE PUBLICAR
                                        if((DocResult["publicador"]=="sim")||(data.doc_ver_criacao_usuario=="<?php echo $_SESSION['login']['id']; ?>")) $("#form-versoes #btn-publicar").show();
                                    }

                                //SE O STATUS FOR VIGENTE E SE FOR O USUÁRIO PUBLICADOR
                                //HABILITA O BOTÃO DE REVOGAR
                                if(status==50)
                                    {
                                        //MOSTRA O BOTÃO DE REVOGAR
                                        if(DocResult["publicador"]=="sim") $("#form-versoes #btn-revogar").show();
                                    }

                                //SE O STATUS FOR APROVADO, VIGENTE OU REVOGADO, TORNA TODOS OS CAMPOS READONLY
                                if((status==40)||(status==50)||(status==60))
                                    {
                                        $("#form-versoes #doc_ver_tipo").prop("disabled", true).trigger('chosen:updated');
                                        if(tinymce!=null) tinymce.get('doc_ver_texto').setMode('readonly');
                                        $("#form-versoes #doc_ver_versao").prop("readonly", true);
                                        $("#form-versoes #doc_ver_vigencia").prop("readonly", true);
                                        $("#form-versoes #doc_ver_status").prop("disabled", true).trigger('chosen:updated');
                                        $("#form-versoes input[name='doc_ver_leitura']").prop("disabled", true);
                                        $("#form-versoes #doc_ver_observacoes").prop("readonly", true);
                                        $("#form-versoes #doc_ver_obs_aprovador").prop("readonly", true);
                                    }
                            }

                        if(origem=='change')
                            {
                                //MOSTRA O CAMPO OBS APROVADOR
                                $("#form-versoes #doc_ver_obs_aprovador").prop("readonly", false);

                                <!--### SE O STATUS FOR REVISAR/CORRIGIR OU APROVADO, TORNA CAMPO OBSERVAÇÕES READONLY ###-->
                                <!--### E MOSTRA OS BOTÕES DE AÇÃO ###-->
                                if((status==30)||(status==40))
                                    {
                                        //TORNA CAMPO DE OBSERVAÇÕES READONLY
                                        $("#form-versoes #doc_ver_observacoes").prop("readonly", true);

                                        //MOSTRA BOTÃO DE ENCAMINHAR
                                        $("#form-versoes #btn-encaminhar").show();
                                    }
                                <!--### SE O STATUS FOR REVISAR/CORRIGIR OU APROVADO, TORNA CAMPO OBSERVAÇÕES READONLY ###-->
                                <!--### E MOSTRA OS BOTÕES DE AÇÃO ###-->


                                //SE O STATUS FOR VIGENTE, MOSTRA O BOTÃO DE PUBLICAR
                                if(status==50) $("#form-versoes #btn-publicar").show();

                                //SE O STATUS FOR REVOGADO, MOSTRA O BOTÃO DE REVOGAR
                                if(status==60) $("#form-versoes #btn-revogar").show();
                            }
                    }
                //SE NÃO TEM STATUS SELECIONADO, ESCONDE TODOS OS BOTÕES DE AÇÃO
                else if(status=='')
                    {
                        // console.log("nada");
                        $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar, #form-versoes #btn-publicar, #form-versoes #btn-revogar").hide();
                    }


                //AÇÕES EXECUTADAS SOMENTE SE FOR NO CARREGAMENTO INICIAL DA VERSÃO
                if(origem=='inicial')
                    {
                        //SE O USUÁRIO NÃO FOR DE NÍVEL SUPERIOR OU NÃO FOR APROVADOR OU NÃO É O CRIADOR,
                        //REMOVE TODAS AS OPÇÕES, MANTÉM APENAS A OPÇÃO DO STATUS ESCOLHIDO E REMOVE OS BOTÕES DE AÇÃO
                        if((DocResult["pertence_nivel_superior"]!="sim")&&(DocResult["aprovador"]!="sim")&&(data.doc_ver_criacao_usuario!="<?php echo $_SESSION['login']['id']; ?>"))
                            {
                                // console.log("NÃO É DE NÍVEL SUPERIOR && NÃO É APROVADOR && NÃO É CRIADOR");

                                $.each($("#form-versoes #doc_ver_status option"), function(id, opcao) {
                                    if($(opcao).val()!=status) $("#form-versoes #doc_ver_status option[value='"+$(opcao).val()+"']").remove().trigger('chosen:updated');
                                });
                                $("#form-versoes #btn-salvar, #form-versoes #btn-encaminhar").hide();
                            }
                    }
                <!--### AÇÕES REALIZADAS PARA CADA TIPO DE STATUS ###-->

                //ATUALIZA O CHOSEN DO DOC_VER_STATUS
                $("#form-versoes #doc_ver_status option").trigger('chosen:updated');


            }
            <!--### FUNÇÃO QUE REALIZA AÇÕES CONFORME O STATUS DO DOCUMENTO ###-->




























		<!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR ###-->
		$('[data-id="btn-form-inserir"]').on('click', function() {
			//verifica se o usuário tem permissão para a operação
			if("<? echo $auth[$sistema]["inserir"]; ?>"=="sim")
				{
					//TROCA O TÍTULO DO FORMULÁRIO PARA A VARIÁVEL title QUE ESTÁ NO BOTÃO
					$('#form-titulo-acao').html( $(this).attr('title') );

					//ESCONDE O BOTÃO DE ALTERAR E MOSTRA O DE INSERIR
					$('#btn-inserir').show();
					$('#btn-alterar').hide();

					//ESCONDE A DIV COM INFORMAÇÃO DE ID E HISTÓRICO DE ATUALIZAÇÕES DO REGISTRO
					$('#info_reg_user').hide();

                    //REMOVE A CLASSE DO ÍCONE FA
                    $("#preview_fa").removeClass();

                    <!--### ZERAR O FORMULÁRIO ###-->
                    $('#form-cadastros').trigger("reset");
                    $('#form-cadastros .chosen-select').trigger("chosen:updated");
                    $('.ace-file-container, .ace-file-input').remove();
                    $("#form-cadastros #thumb_da_foto").attr('src','<?php echo $http_imagens; ?>/spacer.gif');
                    $("#form-cadastros #btn-remover-foto").hide();
                    $("#form-cadastros #thumb_da_foto").removeClass('col-sm-3');
                    $("#form-cadastros .id_da_foto").val('');
                    $("#form-cadastros #titulo_da_foto").html('');
                    $("#form-cadastros #btn-escolher-foto").html('<i class="ace-icon fa fa-photo bigger-120 green"></i> Escolher a foto');
                    <!--### ZERAR O FORMULÁRIO ###-->

                    <!--### DEIXA JÁ MARCADO O STATUS DE ATIVO E GUARDA NO CAMPO HIDDEN ###-->
                    $("input[name=doc_status][value='1']").prop("checked",true);
                    $("#doc_status_hidden").val(1);
                    <!--### DEIXA JÁ MARCADO O STATUS DE ATIVO E GUARDA NO CAMPO HIDDEN ###-->

                    <!--### DEIXA JÁ MARCADO O DOC_FORM DE ATIVO E GUARDA NO CAMPO HIDDEN ###-->
                    $("input[name=doc_form][value='0']").prop("checked",true);
                    $("#doc_form_hidden").val(0);
                    <!--### DEIXA JÁ MARCADO O DOC_FORM DE ATIVO E GUARDA NO CAMPO HIDDEN ###-->

                    //ESCONDE OS CAMPOS DE DOC_FORM
                    $(".campos_doc_form").hide();

                    //ABRE O MODAL DE CADASTRO
                    $('#modal-form').modal("show");
				}
			else
				{
					<!--### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###-->
					$.gritter.add({
						title: '<i class="ace-icon fa fa-times"></i> Erro!',
						text: '<? echo $sistema_nao_autorizado; ?>',
						class_name: 'gritter-error gritter-center',
						sticky: false,
						fade_out_speed:500,
						before_open: function(e){
							$('#modal-form').modal("hide");
						}
					});
					<!--### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###-->
				}
		 })
		<!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR ###-->












		<!--### FUNÇÃO QUE ARMAZENA QUAL BOTÃO SUBMIT FOI PRESSIONADO NOS FORMS ###-->
		var btnSubmit;
        $('#form-cadastros :submit').on('click', function() {
            $('#form-cadastros #operacao').val( $(this).attr("data-id") );
            $('#form-cadastros #filial').val("<? echo $_REQUEST['filial']; ?>");
        });

		$('#form-versoes :submit').on('click', function() {
			$('#form-versoes #operacao').val( $(this).attr("data-id") );
			$('#form-versoes #filial').val("<? echo $_REQUEST['filial']; ?>");

            //DESABILITA A OPÇÃO DE EM APROVAÇÃO CASO A AÇÃO SEJA SALVAR-ENCAMINHAR
            if(($('#form-versoes #operacao').val()=='salvar-encaminhar')&&($('#form-versoes #doc_ver_status').find(":selected").val()==20)) $("#form-versoes #doc_ver_status option[value='20']").prop('disabled', true).trigger('chosen:updated');
            else $("#form-versoes #doc_ver_status option[value='20']").prop('disabled', false).trigger('chosen:updated');
		});
		<!--### FUNÇÃO QUE ARMAZENA QUAL BOTÃO SUBMIT FOI PRESSIONADO NOS FORMS ###-->



		<!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###-->
		<!--### MÉTODO ADICIONAL DE VALIDAÇÃO DE DATA EM FORMATO DD/MM/AAAA ###-->
		jQuery.validator.addMethod("dataBR",function(value,element) {
				return this.optional(element)||/^\d{1,2}[\/-]\d{1,2}[\/-]\d{4}$/.test(value);
			}, "Digite a data no formato DD/MM/AAAA");
        jQuery.validator.addMethod("datahoraBR",function(value,element) {
                return this.optional(element)||/^\d{1,2}[\/-]\d{1,2}[\/-]\d{4}[\/ ]\d{1,2}[\/:]\d{1,2}$/.test(value);
            }, "Digite a data no formato DD/MM/AAAA hh:mm");
		<!--### MÉTODO ADICIONAL DE VALIDAÇÃO DE DATA EM FORMATO DD/MM/AAAA ###-->


		$('#form-cadastros').validate({
			errorElement: 'div',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				<!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
				'doc_secao[]': {
					required: true
				},
                doc_titulo: {
                    required: true
                },
                doc_categoria: {
                    required: true
                },
                doc_ordem: {
                    required: true,
                    number: true
                },
                doc_endereco: {
                    required: true,
                    url: true
                },
                doc_destino: {
                    required: true
                },
                doc_status: {
                    required: true
                }
				<!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
			},

			messages: {
				<!--### MENSAGENS DE VALIDAÇÃO ###-->
				'doc_secao[]': {
					required: "Escolha pelo menos uma seção"
				},
                doc_titulo: {
                    required: "Digite o título do documento"
                },
                doc_categoria: {
                    required: "Escolha a categoria do documento"
                },
                doc_ordem: {
                    required: "Digite a ordem do documento",
                    number: "Digite a ordem em formato numérico",
                },
                doc_endereco: {
                    required: "Digite o endereço do documento",
                    url: "Digite uma URL válida"
                },
                doc_destino: {
                    required: "Digite o destino do documento"
                },
                doc_status: {
                    required: "Escolha o status do texto"
                }
				<!--### MENSAGENS DE VALIDAÇÃO ###-->
			},


			highlight: function (e) {
				$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
			},

			success: function (e) {
				$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
				$(e).remove();
			},

			errorPlacement: function (error, element) {
				if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
					var controls = element.closest('div[class*="col-"]');
					if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
					else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
				}
				else if(element.is('.select2')) {
					error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
				}
				else if(element.is('.chosen-select')) {
					error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
				}
				else error.insertAfter(element.parent());
			},

			submitHandler: function (form) {
				//console.log(btnSubmit);

				<!--### MOSTRA A DIV DE CARREGANDO ###-->
				var contentArea = $('body');
				contentArea.css('opacity', 0.25)

				var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
				var offset = contentArea.offset();
				loader.css({top: offset.top, left: offset.left})
				<!--### MOSTRA A DIV DE CARREGANDO ###-->

				<!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->
				var formData = new FormData($("#form-cadastros")[0]);
				$.ajax({
					type: "POST",
					//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
					url: '<? echo $sistema; ?>/acoes_registros.php',
					//ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
					data: formData,
					contentType: false,
					enctype: 'multipart/form-data',
					processData: false,
					success: function(data)
					{

						<!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
						if (data === 'OK') {

							if( $('#form-cadastros #operacao').val() == "inserir") var texto_OK = '<? echo $registro_inserido; ?>';
							else if( $('#form-cadastros #operacao').val() == "alterar") var texto_OK = '<? echo $registro_alterado; ?>';

                            //FECHA O MODAL DE CADASTRO
                            $('#modal-form').modal("hide");

							$.gritter.add({
								title: '<i class="ace-icon fa fa-check"></i> OK!',
								text: texto_OK,
								class_name: 'gritter-success gritter-center',
								fade_out_speed:500,
								time:2000,
								before_open: function(e){
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
									oTable1.ajax.reload(null,false);
								}
							});
						}
						<!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






						<!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
						else {
							$.gritter.add({
								title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
								text: data,
								class_name: 'gritter-error gritter-center',
								sticky: false,
								fade_out_speed:500,
								after_close: function(e) {
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
								}
							});
						}
						<!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->


					}
				});
				<!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->

			},
			invalidHandler: function (form) {
			}
		});
		<!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###-->












        //FUNÇÃO QUE ATIVA O PROPOVER DO HISTÓRICO DO REGISTRO
        $('[data-rel=reg_user_popover]').popover({container:'body', html:true});





		<!--### FUNÇÃO PARA FORMATAR OS BOTÕES DE CONTROLE DAS AÇÕES NO GRID ###-->
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();

			var off2 = $source.offset();
			//var w2 = $source.width();

			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		<!--### FUNÇÃO PARA FORMATAR OS BOTÕES DE CONTROLE DAS AÇÕES NO GRID ###-->



		<!--### FUNÇÃO AUTOSIZE DO INPUT TIPO TEXTAREA ###-->
		$('textarea[class*=autosize]').autosize({append: "\n"});
		<!--### FUNÇÃO AUTOSIZE DO INPUT TIPO TEXTAREA ###-->

        <!--### ATUALIZAR A ALTURA DO AUTOSIZE DOS TEXTAREAS ###-->
        $('textarea.js-auto-size').each( function(index, element) {
            textarea = $(this);
            textarea.trigger('input');
        });
        <!--### ATUALIZAR A ALTURA DO AUTOSIZE DOS TEXTAREAS ###-->


        <!--### FUNÇÃO DO INPUT TIPO DATEPICKER ###-->
        //datepicker plugin
        //link
        $('.date-picker').datepicker({
            language: "pt-BR",
            autoclose: true,
            todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
            $(this).prev().focus();
        });
        <!--### FUNÇÃO DO INPUT TIPO DATEPICKER ###-->




		<!--### FUNÇÃO DO INPUT TIPO DATE-TIME-PICKER ###-->
		if(!ace.vars['old_ie']) $('.date-timepicker').datetimepicker({
		 language: 'pt-BR'
		}).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
		<!--### FUNÇÃO DO INPUT TIPO DATE-TIME-PICKER ###-->






        <!--### FUNÇÃO DO INPUT TIPO DATE-RANGE-PICKER ###-->
        $('#filtro_periodo').daterangepicker({
            'applyClass' : 'btn-sm btn-success',
            'cancelClass' : 'btn-sm btn-default',
            format: 'DD/MM/YYYY',
            locale: {
                fromLabel: 'Início',
                toLabel: 'Término',
                applyLabel: 'Aplicar',
                cancelLabel: 'Limpar',
            }
        })
        .prev().on(ace.click_event, function(){
            $(this).next().focus();
        });
        <!--### FUNÇÃO DO INPUT TIPO DATE-RANGE-PICKER ###-->




        <!--### FUNÇÃO DO INPUT TIPO SPINNER +\- ###-->
        $('#doc_ordem').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
        .closest('.ace-spinner')
        .on('changed.fu.spinbox', function(){
            //console.log($('#spinner1').val())
        });
        <!--### FUNÇÃO DO INPUT TIPO SPINNER +\- ###-->











		<!--### FUNÇÃO PARA CORRIGIR O CHOSEN QUANDO ESTÁ DENTRO DE UM FORM MODAL ###-->
		//chosen plugin inside a modal will have a zero width because the select element is originally hidden
		//and its width cannot be determined.
		//so we set the width after modal is show
		$('#modal-form, #modal-form-permissoes, #modal-form-versoes').on('shown.bs.modal', function () {
			if(!ace.vars['touch']) {
				$(this).find('.chosen-container').each(function(){
					$(this).find('a:first-child').css('width' , '250px');
					$(this).find('.chosen-drop').css('width' , '250px');
					$(this).find('.chosen-search input').css('width' , '240px');
				});
			}
		})
		<!--### FUNÇÃO PARA CORRIGIR O CHOSEN QUANDO ESTÁ DENTRO DE UM FORM MODAL ###-->




        <!--### FUNÇÃO QUE FAZ O PREVIEW DOS ÍCONES ESCOLHIDOS ###-->
        $("#doc_icone").blur(function (){
            //alert(this.value);
            $("#preview_fa").removeClass();
            $("#preview_fa").addClass("ace-icon fa fa-2x fa-" + this.value);
        })
        <!--### FUNÇÃO QUE FAZ O PREVIEW DOS ÍCONES ESCOLHIDOS ###-->




        <!--### AÇÕES QUANDO CLICAR NO BOTÃO DE SUBIR/DESCER ORDEM DO TEXTO NO GRID ###-->
        $('#sample-table-2 tbody').on( 'click', '#txt-up, #txt-down', function () {
            //alert($(this).data('id'));
            //alert($(this).attr('id'));


            <!--### MOSTRA A DIV DE CARREGANDO ###-->
            var contentArea = $('body');
            contentArea
            .css('opacity', 0.25)

            var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
            var offset = contentArea.offset();
            loader.css({top: offset.top, left: offset.left})
            <!--### MOSTRA A DIV DE CARREGANDO ###-->


            <!--### AJAX PARA POSTAGEM DO PEDIDO DE MUDANÇA DA ORDEM ###-->
            $.ajax({
                type: "POST",
                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                url: '<? echo $sistema; ?>/acoes_registros.php',
                data: {
                    //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                    chave_primaria: $(this).data('id'),
                    operacao: $(this).attr('id'),
                    filial: "<? echo $_REQUEST['filial']; ?>"
                },
                success: function(data)
                {
                    <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
                    if (data === 'OK') {
                        <!--### ESCONDE A DIV DE CARREGANDO ###-->
                        contentArea.css('opacity', 1)
                        contentArea.prevAll('.ajax-loading-overlay').remove();
                        <!--### ESCONDE A DIV DE CARREGANDO ###-->

                        //ATUALIZA A TABELA DO GRID
                        oTable1.ajax.reload(null,false);
                    }
                    <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






                    <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
                    else {
                        $.gritter.add({
                            title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                            //O TEXTO DA MENSAGEM DE ERRO SERÁ TUDO O QUE TIVER RETORNADO DE MENSAGEM NO ARQUIVO DE EXECUÇÃO
                            text: data,
                            class_name: 'gritter-error gritter-center',
                            sticky: false,
                            fade_out_speed:500,
                            after_close: function(e) {
                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                                contentArea.css('opacity', 1)
                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                <!--### ESCONDE A DIV DE CARREGANDO ###-->
                            }
                        });
                    }
                }
            });
            <!--### AJAX PARA POSTAGEM DO PEDIDO DE MUDANÇA DA ORDEM ###-->

        });
        <!--### AÇÕES QUANDO CLICAR NO BOTÃO DE SUBIR/DESCER ORDEM DO TEXTO NO GRID ###-->





        <!--### CONJUNTO DE AÇÕES PARA LISTAGEM DAS FOTOS EXISTENTES E INSERÇÃO DE NOVA FOTO ###-->
         <?
            //INCLUI ARQUIVO DE JS
            // include("../banco-fotos/modal-fotos-js.inc.php");
        ?>
        <!--### CONJUNTO DE AÇÕES PARA LISTAGEM DAS FOTOS EXISTENTES E INSERÇÃO DE NOVA FOTO ###-->







        <!--### FUNÇÃO PARA LIMITAR OS CARACTERES DOS CAMPOS E MOSTRAR CONTADOR ###-->
        $('#doc_resumo').inputlimiter({
            limit: <?php echo $GLOBALS['limite_caracteres_resumo']; ?>,
            remText: 'faltam %n caractere%s',
            limitText: 'do máximo permitido de %n.'
        });
        <!--### FUNÇÃO PARA LIMITAR OS CARACTERES DOS CAMPOS E MOSTRAR CONTADOR ###-->



        <!--### FUNÇÃO PARA MOSTRAR OS CAMPOS DE DOC_FORM QUANDO CLICAR NO SIM ###-->
        $("input[name=doc_form]").on('click', function(event) {
            if($(this).val()==1)
                {
                    $(".campos_doc_form").slideDown();
                    $(".campos_doc_form_botao").hide();
                }
            else $(".campos_doc_form").slideUp();
        });;
        <!--### FUNÇÃO PARA MOSTRAR OS CAMPOS DE DOC_FORM QUANDO CLICAR NO SIM ###-->


        <!--### FUNÇÃO PARA MOSTRAR OS CAMPOS DE DOC_FORM_BOTAO QUANDO CLICAR NO SIM ###-->
        $("input[name=doc_form_imagem_botao]").on('click', function(event) {
            if($(this).val()==1) $(".campos_doc_form_botao").slideDown();
            else $(".campos_doc_form_botao").slideUp();
        });;
        <!--### FUNÇÃO PARA MOSTRAR OS CAMPOS DE DOC_FORM_BOTAO QUANDO CLICAR NO SIM ###-->













        <!--### FUNÇÕES DO GRID DE USUÁRIOS DENTRO DO DOCUMENTO ###-->
        <?php include("sistema-usuarios.cfg.php"); ?>

        var oTable1_usuarios =
        $('#tabela-usuarios')
        //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
        .DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                //URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
                "url": "<? echo $sistema; ?>/jquery.datatables.serverSide.php",
                "data": function ( d ) {
                    d.id_doc = $('#form-permissoes #doc_id').val();;
                    d.operacao = "selecionar_usuarios";
                    d.sistema = "<? echo $sistema; ?>";
                    d.filial = "<? echo $_REQUEST['filial']; ?>";
                }
            },
            //"ajax": "configuracoes/serverSide.php",
            //stateSave: true,
            bAutoWidth: false,
            "paging": true,
            "pageLength": 50,
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
            "columns": [
                <?
                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                        {
                            $num_coluna_grid_usuarios++;
                            echo '
                            { ';
                            echo '"name": "'.$key_coluna.'", ';
                            echo '"data": "'.$key_coluna.'", ';
                            echo '"title": "'.$config_coluna["title"].'", ';
                            echo '"width": "'.$config_coluna["width"].'", ';
                            echo '"visible": '.$config_coluna["visible"].', ';
                            echo '"searchable": '.$config_coluna["searchable"].', ';
                            if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
                            echo '"orderable": '.$config_coluna["orderable"];
                            echo ' }';
                            if($num_coluna_grid_usuarios<$total_colunas_grid) echo ',';
                        }
                ?>
            ],
            "order": [
                <?
                    //FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
                    foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
                        {
                            $num_ordenacao_grid_usuarios++;
                            echo '
                            [ ';
                            echo $array_colunas_key_grid[$coluna_nome].', ';
                            echo '"'.$coluna_ordenacao.'" ';
                            echo ' ]';
                            if($num_ordenacao_grid_usuarios<$total_ordenacao_grid) echo ',';
                        }
                ?>
            ],
            //TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "A busca não retornou resultados",
                "info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
                "infoEmpty": "0 registros",
                "infoFiltered": "de um total de _MAX_ cadastrados",
                "loadingRecords": "Carregando...",
                "sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
                "search":         "Buscar:",
                "paginate": {
                        "first":      "Primeira",
                        "last":       "Última",
                        "next":       "Próxima",
                        "previous":   "Anterior"
                }
            },
        } );
        <!--### FUNÇÕES DO GRID DE USUÁRIOS DENTRO DO DOCUMENTO ###-->














        <!--### AÇÕES QUANDO CLICAR NO CHECKBOX DE ASSOCIAÇÃO DO USUÁRIO NO DOCUMENTO ###-->
        $('#tabela-usuarios tbody, #div_associar_todos_usuarios').on( 'click', 'input[type=checkbox]', function () {

            var id_reg = $(this).val();
            var id_doc = $("#form-permissoes #doc_id").val();
            var selecionado = $(this).is(':checked');
            // console.log(id_reg);
            // console.log(id_doc);
            // console.log(selecionado);

            <!--### AJAX PARA POSTAGEM DA AÇÃO DE INCLUIR/REMOVER O USUÁRIO NO DOCUMENTO ###-->
            $.ajax({
                dataType: "json",
                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                url: '<? echo $sistema; ?>/acoes_registros.php',
                data: {
                    //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                    usuario: id_reg,
                    doc: id_doc,
                    selecionado: selecionado,
                    operacao: "associar_usuario",
                    filial: "<? echo $_REQUEST['filial']; ?>"
                },
                success: function(data, textStatus, jqXHR)
                {
                    //console.log("data: "+data+"\ntextStatus: "+textStatus);
                    if(data.resultado=="OK")
                        {
                            //ATUALIZA A TABELA DO GRID
                            oTable1_usuarios.ajax.reload(null,false);
                        }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                    //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                    $.gritter.add({
                        title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                        text: errorThrown,
                        class_name: 'gritter-error gritter-center',
                        sticky: false,
                        fade_out_speed:500
                    });
                }
            });
            <!--### AJAX PARA POSTAGEM DA AÇÃO DE INCLUIR/REMOVER O USUÁRIO NO DOCUMENTO ###-->

        });
        <!--### AÇÕES QUANDO CLICAR NO CHECKBOX DE ASSOCIAÇÃO DO USUÁRIO NO DOCUMENTO ###-->
























        <!--### FUNÇÕES DO GRID DE NÍVEIS DENTRO DO DOCUMENTO ###-->
        <?php include("sistema-niveis.cfg.php"); ?>

        var oTable1_niveis =
        $('#tabela-niveis')
        //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
        .DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                //URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
                "url": "<? echo $sistema; ?>/jquery.datatables.serverSide.php",
                "data": function ( d ) {
                    d.id_doc = $('#form-permissoes #doc_id').val();;
                    d.operacao = "selecionar_niveis";
                    d.sistema = "<? echo $sistema; ?>";
                    d.filial = "<? echo $_REQUEST['filial']; ?>";
                }
            },
            //"ajax": "configuracoes/serverSide.php",
            //stateSave: true,
            bAutoWidth: false,
            "paging": true,
            "pageLength": 50,
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
            "columns": [
                <?
                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                        {
                            $num_coluna_grid_niveis++;
                            echo '
                            { ';
                            echo '"name": "'.$key_coluna.'", ';
                            echo '"data": "'.$key_coluna.'", ';
                            echo '"title": "'.$config_coluna["title"].'", ';
                            echo '"width": "'.$config_coluna["width"].'", ';
                            echo '"visible": '.$config_coluna["visible"].', ';
                            echo '"searchable": '.$config_coluna["searchable"].', ';
                            if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
                            echo '"orderable": '.$config_coluna["orderable"];
                            echo ' }';
                            if($num_coluna_grid_niveis<$total_colunas_grid) echo ',';
                        }
                ?>
            ],
            "order": [
                <?
                    //FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
                    foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
                        {
                            $num_ordenacao_grid_niveis++;
                            echo '
                            [ ';
                            echo $array_colunas_key_grid[$coluna_nome].', ';
                            echo '"'.$coluna_ordenacao.'" ';
                            echo ' ]';
                            if($num_ordenacao_grid_niveis<$total_ordenacao_grid) echo ',';
                        }
                ?>
            ],
            //TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "A busca não retornou resultados",
                "info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
                "infoEmpty": "0 registros",
                "infoFiltered": "de um total de _MAX_ cadastrados",
                "loadingRecords": "Carregando...",
                "sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
                "search":         "Buscar:",
                "paginate": {
                        "first":      "Primeira",
                        "last":       "Última",
                        "next":       "Próxima",
                        "previous":   "Anterior"
                }
            },
        } );
        <!--### FUNÇÕES DO GRID DE NÍVEIS DENTRO DO DOCUMENTO ###-->














        <!--### AÇÕES QUANDO CLICAR NO CHECKBOX DE ASSOCIAÇÃO DO NÍVEL NO DOCUMENTO ###-->
        $('#tabela-niveis tbody, #div_associar_todos_niveis').on( 'click', 'input[type=checkbox]', function () {

            var id_reg = $(this).val();
            var id_doc = $("#form-permissoes #doc_id").val();
            var selecionado = $(this).is(':checked');
            // console.log(id_reg);
            // console.log(id_doc);
            // console.log(selecionado);

            <!--### AJAX PARA POSTAGEM DA AÇÃO DE INCLUIR/REMOVER O NÍVEL NO DOCUMENTO ###-->
            $.ajax({
                dataType: "json",
                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                url: '<? echo $sistema; ?>/acoes_registros.php',
                data: {
                    //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                    nivel: id_reg,
                    doc: id_doc,
                    selecionado: selecionado,
                    operacao: "associar_nivel",
                    filial: "<? echo $_REQUEST['filial']; ?>"
                },
                success: function(data, textStatus, jqXHR)
                {
                    //console.log("data: "+data+"\ntextStatus: "+textStatus);
                    if(data.resultado=="OK")
                        {
                            //ATUALIZA A TABELA DO GRID
                            oTable1_niveis.ajax.reload(null,false);
                        }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                    //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                    $.gritter.add({
                        title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                        text: errorThrown,
                        class_name: 'gritter-error gritter-center',
                        sticky: false,
                        fade_out_speed:500
                    });
                }
            });
            <!--### AJAX PARA POSTAGEM DA AÇÃO DE INCLUIR/REMOVER O NÍVEL NO DOCUMENTO ###-->

        });
        <!--### AÇÕES QUANDO CLICAR NO CHECKBOX DE ASSOCIAÇÃO DO NÍVEL NO DOCUMENTO ###-->





























        <!--### FUNÇÕES DO GRID DE GRUPOS DENTRO DO DOCUMENTO ###-->
        <?php include("sistema-grupos.cfg.php"); ?>

        var oTable1_grupos =
        $('#tabela-grupos')
        //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
        .DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                //URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
                "url": "<? echo $sistema; ?>/jquery.datatables.serverSide.php",
                "data": function ( d ) {
                    d.id_doc = $('#form-permissoes #doc_id').val();;
                    d.operacao = "selecionar_grupos";
                    d.sistema = "<? echo $sistema; ?>";
                    d.filial = "<? echo $_REQUEST['filial']; ?>";
                }
            },
            //"ajax": "configuracoes/serverSide.php",
            //stateSave: true,
            bAutoWidth: false,
            "paging": true,
            "pageLength": 50,
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
            "columns": [
                <?
                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                        {
                            $num_coluna_grid_grupos++;
                            echo '
                            { ';
                            echo '"name": "'.$key_coluna.'", ';
                            echo '"data": "'.$key_coluna.'", ';
                            echo '"title": "'.$config_coluna["title"].'", ';
                            echo '"width": "'.$config_coluna["width"].'", ';
                            echo '"visible": '.$config_coluna["visible"].', ';
                            echo '"searchable": '.$config_coluna["searchable"].', ';
                            if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
                            echo '"orderable": '.$config_coluna["orderable"];
                            echo ' }';
                            if($num_coluna_grid_grupos<$total_colunas_grid) echo ',';
                        }
                ?>
            ],
            "order": [
                <?
                    //FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
                    foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
                        {
                            $num_ordenacao_grid_grupos++;
                            echo '
                            [ ';
                            echo $array_colunas_key_grid[$coluna_nome].', ';
                            echo '"'.$coluna_ordenacao.'" ';
                            echo ' ]';
                            if($num_ordenacao_grid_grupos<$total_ordenacao_grid) echo ',';
                        }
                ?>
            ],
            //TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "A busca não retornou resultados",
                "info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
                "infoEmpty": "0 registros",
                "infoFiltered": "de um total de _MAX_ cadastrados",
                "loadingRecords": "Carregando...",
                "sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
                "search":         "Buscar:",
                "paginate": {
                        "first":      "Primeira",
                        "last":       "Última",
                        "next":       "Próxima",
                        "previous":   "Anterior"
                }
            },
        } );
        <!--### FUNÇÕES DO GRID DE GRUPOS DENTRO DO DOCUMENTO ###-->














        <!--### AÇÕES QUANDO CLICAR NO CHECKBOX DE ASSOCIAÇÃO DO GRUPO NO DOCUMENTO ###-->
        $('#tabela-grupos tbody, #div_associar_todos_grupos').on( 'click', 'input[type=checkbox]', function () {

            var id_reg = $(this).val();
            var id_doc = $("#form-permissoes #doc_id").val();
            var selecionado = $(this).is(':checked');
            // console.log(id_reg);
            // console.log(id_doc);
            // console.log(selecionado);

            <!--### AJAX PARA POSTAGEM DA AÇÃO DE INCLUIR/REMOVER O NÍVEL NO DOCUMENTO ###-->
            $.ajax({
                dataType: "json",
                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                url: '<? echo $sistema; ?>/acoes_registros.php',
                data: {
                    //VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
                    grupo: id_reg,
                    doc: id_doc,
                    selecionado: selecionado,
                    operacao: "associar_grupo",
                    filial: "<? echo $_REQUEST['filial']; ?>"
                },
                success: function(data, textStatus, jqXHR)
                {
                    //console.log("data: "+data+"\ntextStatus: "+textStatus);
                    if(data.resultado=="OK")
                        {
                            //ATUALIZA A TABELA DO GRID
                            oTable1_grupos.ajax.reload(null,false);
                        }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
                    //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                    $.gritter.add({
                        title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                        text: errorThrown,
                        class_name: 'gritter-error gritter-center',
                        sticky: false,
                        fade_out_speed:500
                    });
                }
            });
            <!--### AJAX PARA POSTAGEM DA AÇÃO DE INCLUIR/REMOVER O NÍVEL NO DOCUMENTO ###-->

        });
        <!--### AÇÕES QUANDO CLICAR NO CHECKBOX DE ASSOCIAÇÃO DO GRUPO NO DOCUMENTO ###-->













        <!--### AÇÕES QUANDO TROCA O TIPO DE DOCUMENTO ###-->
        $('#doc_ver_tipo').on('change', function() {

            $('#campo_texto').slideUp();
            $('#campo_file').slideUp();

            if($(this).val()=="arquivo") {
                $('#campo_file').slideDown();
            }
            else if($(this).val()=="texto") {
                $('#campo_texto').slideDown();
            }
        });
        <!--### AÇÕES QUANDO TROCA O TIPO DE DOCUMENTO ###-->













        <!--### AÇÕES QUANDO TROCA O STATUS DA VERSÃO ###-->
        $('#doc_ver_status').on('change', function() {

            // console.log(data);
            // console.log(DocResult);

            AcoesStatus($(this).val(), '', '', 'change', tinymce);

        });
        <!--### AÇÕES QUANDO TROCA O STATUS DA VERSÃO ###-->





































        <!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO DAS VERSÕES DE DOCUMENTO ###-->
        $('#form-versoes').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                <!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
                'doc_ver_tipo': {
                    required: true
                },
                doc_ver_versao: {
                    required: true
                },
                doc_ver_status: {
                    required: true
                },
                arquivo_enviado: {
                    required: function() {
                            if($('#form-versoes #doc_ver_tipo').val()=='arquivo') return true;
                            else return false;
                        },
                    number: true,
                    min: function() {
                            if($('#form-versoes #doc_ver_tipo').val()=='arquivo') return 1;
                            else return false;
                        }
                },
                doc_ver_texto: {
                    required: function() {
                            if($('#form-versoes #doc_ver_tipo').val()=='texto') return true;
                            else return false;
                        }
                },
                doc_ver_vigencia: {
                    required: function() {
                            if($('#form-versoes #doc_ver_status').val()==4) return true;
                            else return false;
                        },
                    dataBR: true
                },

                <!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
            },

            messages: {
                <!--### MENSAGENS DE VALIDAÇÃO ###-->
                doc_ver_tipo: {
                    required: "Escolha o tipo de documento"
                },
                doc_ver_versao: {
                    required: "Digite a versão do documento"
                },
                doc_ver_status: {
                    required: function() {
                            if(($('#form-versoes #doc_ver_status').find(":selected").val()==10)||
                                ($('#form-versoes #doc_ver_status').find(":selected").val()==20)) return "Verifique o status escolhido";
                            else return "Escolha o status da versão";
                        }
                },
                arquivo_enviado: {
                    required: "Escolha o arquivo para envio",
                    number: "Escolha o arquivo para envio",
                    min: "Escolha o arquivo para envio"
                },
                doc_ver_texto: {
                    required: "Digite o texto"
                },
                doc_ver_vigencia: {
                    required: "Digite a data de vigência",
                    dataBR: "Digite a data no formato DD/MM/AAAA"
                }
                <!--### MENSAGENS DE VALIDAÇÃO ###-->
            },


            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                //console.log(btnSubmit);

                <!--### MOSTRA A DIV DE CARREGANDO ###-->
                var contentArea = $('body');
                contentArea.css('opacity', 0.25)

                var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                loader.css({top: offset.top, left: offset.left})
                <!--### MOSTRA A DIV DE CARREGANDO ###-->

                <!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->
                var formData = new FormData($("#form-versoes")[0]);
                console.log(formData);
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                    url: '<? echo $sistema; ?>/acoes_registros.php',
                    //ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
                    data: formData,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    success: function(data, textStatus, jqXHR)
                    {

                        //console.log(data);

                        <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
                        if (data.salvo === 'OK') {

                            if( $('#form-versoes #operacao').val() == "salvar-versao") var texto_OK = 'A versão foi salva e poderá ser alterada posteriormente.';
                            else if( $('#form-versoes #operacao').val() == "salvar-encaminhar") var texto_OK = 'A versão foi salva e encaminhada.';
                            else if( $('#form-versoes #operacao').val() == "publicar-versao") var texto_OK = 'A versão foi publicada!';
                            else if( $('#form-versoes #operacao').val() == "revogar-versao") var texto_OK = 'A versão foi revogada!';

                            //FECHA O MODAL DE CADASTRO
                            $('#modal-form-versoes').modal("hide");

                            <!--### ENVIA OS E-MAILS DE AVISO ###-->
                            if(data.dados_email["envia_email"]=="sim")
                                {
                                    if(data["destino_email"]) {

                                        $.each(data["destino_email"], function(id, dados_email) {
                                            // console.log(id );
                                            // console.log(dados_email);
                                            $.ajax({
                                                type: "POST",
                                                //dataType: "json",
                                                url: '<? echo $sistema; ?>/enviar_email.php',
                                                data: {
                                                    remetente_nome: data.dados_email["remetente"],
                                                    destinatario_email: dados_email.email,
                                                    destinatario_nome: dados_email.nome,
                                                    assunto_email: data.dados_email["assunto"],
                                                    mensagem: dados_email.conteudo_mensagem,
                                                    msg_retorno_email: data.dados_email["msg_retorno_email"],
                                                    filial: "<? echo $_REQUEST['filial']; ?>"
                                                },
                                                success: function(data, textStatus, jqXHR) { }
                                            });
                                        });
                                    }
                                }
                            <!--### ENVIA OS E-MAILS DE AVISO ###-->

                            $.gritter.add({
                                title: '<i class="ace-icon fa fa-check"></i> OK!',
                                text: texto_OK,
                                class_name: 'gritter-success gritter-center',
                                fade_out_speed:500,
                                time:2000,
                                before_open: function(e){
                                    contentArea.css('opacity', 1)
                                    contentArea.prevAll('.ajax-loading-overlay').remove();
                                    contentArea.removeClass('modal-open');
                                    $("[class*='modal-backdrop']").remove();
                                    oTable1.ajax.reload(null,false);
                                }
                            });
                        }
                        <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






                        <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
                        else {
                            $.gritter.add({
                                title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                text: data.msg_erro,
                                class_name: 'gritter-error gritter-center',
                                sticky: false,
                                fade_out_speed:500,
                                before_open: function(e) {
                                    contentArea.css('opacity', 1)
                                    contentArea.prevAll('.ajax-loading-overlay').remove();
                                }
                            });
                        }
                        <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->


                    }
                });
                <!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->

            },
            invalidHandler: function (form) {
            }
        });
        <!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO DAS VERSÕES DE DOCUMENTO ###-->




















        <!--### FUNÇÕES QUANDO VIR SETADO NA URL O DOC_ID E DOC_VER_ID ###-->
        if(("<?php echo $_REQUEST['doc_id']; ?>">0)&&("<?php echo $_REQUEST['doc_ver_id']; ?>">0))
            {

                // console.log($(this).data("toggle"));
                // console.log($(this).data("id"));

                //GUARDA O ID DO DOCUMENTO
                var id_documento = "<?php echo $_REQUEST['doc_id']; ?>";

                //GUARDA O ID DA VERSÃO
                var id_versao = "<?php echo $_REQUEST['doc_ver_id']; ?>";

                //REMOVA TODAS AS LAYERS DE MODAL-OPEN
                $("[class*='modal-backdrop']").remove();

                <!--### MOSTRA A DIV DE CARREGANDO ###-->
                var contentArea = $('body');
                contentArea
                .css('opacity', 0.25)

                var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                loader.css({top: offset.top, left: offset.left})
                <!--### MOSTRA A DIV DE CARREGANDO ###-->

                //CHAMA A FUNÇÃO QUE ABRE OS DADOS DAS VERSÃO DO DOCUMENTO
                var DocResult = SelecionaVersaoDocumento(id_documento, id_versao, contentArea, 'externa', tinymce);
            }
        <!--### FUNÇÕES QUANDO VIR SETADO NA URL O DOC_ID E DOC_VER_ID ###-->




















        <!--### FUNÇÃO PARA CONTROLAR O UPLOAD DOS ARQUIVOS ###-->
        $('#form-versoes #files').fileinput({
            language: "pt-BR",
            uploadUrl: "documentos/upload.php",
            uploadAsync: true,
            overwriteInitial: true,
            showRemove: false,
            maxFileCount: 1,
            showUpload: false,
            allowedFileExtensions: ['jpg', 'png', 'jpeg', 'txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pps', 'zip', 'rar', 'tar', 'gz'],
            maxFileSize: <?php echo $limite_upload_php; ?>,
            uploadExtraData: function() {
                    var ExtraData = {};
                    ExtraData['doc_ver_pid'] = $("#form-versoes #doc_ver_pid").val();
                    ExtraData['doc_id'] = $("#form-versoes #doc_id").val();
                    ExtraData['operacao_upload'] = $("#form-versoes #operacao_upload").val();
                    return ExtraData;
                }
        }).on('fileuploaded', function(event, data, previewId, index) {
            $('#form-versoes #arquivo_enviado').val(parseInt(parseInt($('#form-versoes #arquivo_enviado').val()) + 1));
            $("#form-versoes #arquivo_filename").val(data.files[0]['name']);
            console.log("fileuploaded");

        }).on('filedeleted', function(event, key) {
            $('#form-versoes #arquivo_enviado').val(parseInt(parseInt($('#form-versoes #arquivo_enviado').val()) - 1));
            console.log("filedeleted");

        }).on('filebatchselected', function(event, files) {
            $('#form-versoes #files').fileinput('upload');
            console.log("filebatchselected");

        }).on('filesuccessremove', function(event, id) {
            $('#form-versoes #arquivo_enviado').val(parseInt(parseInt($('#form-versoes #arquivo_enviado').val()) - 1));
            console.log('filesuccessremove');

        });
        <!--### FUNÇÃO PARA CONTROLAR O UPLOAD DOS ARQUIVOS ###-->



	})
	});


    <!--### HACK PARA PERMITIR O USO DO TINYMCE DENTRO DE MODAL ###-->
    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });
    <!--### HACK PARA PERMITIR O USO DO TINYMCE DENTRO DE MODAL ###-->

</script>