<?php
/*###################################################################
|																	|
|	MÓDULO: doucmentos-categorias									|
|	DESCRIÇÃO: Arquivo padrão de configurações do módulo do 		|
|	sistema															|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 20/10/2016												|
|																	|
###################################################################*/


	/*###################################################################
	|																	|
	|	Variáveis de informações sobre o sistema, títulos e descrição 	|
	|	e informações das tabelas e campos.								|
	|	 																|
	###################################################################*/

	//NOME DO SISTEMA UTILIZADO COMO REFERÊNCIA PARA PERMISÕES DE ACESSO
	$sistema = "documentos-categorias";

	//INFORMAÇÕES DE IDENTIFICAÇÃO DO SISTEMA
	$sistema_titulo = "Categorias de Documentos";
	$sistema_titulo_item = "Categoria";
	$sistema_descricao = "Gerencie as categorias dos documentos que são disponibilizados na Intranet";

	//NOME DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ GERENCIAR
	$sistema_nome_da_tabela = "documentos_categorias";

	//NOME DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ EXIBIR NO GRID
	$sistema_nome_da_tabela_grid = "visao_documentos_categorias";

	//NOME DO CAMPO NA TABELA DO BANCO DE DADOS QUE É CHAVE-PRIMÁRIA PARA ALTERAÇÃO/EXCLUSÃO DOS REGISTROS
	$sistema_chave_primaria = "doc_categoria_id";

	//NOME DO CAMPO NA TABELA DO BANCO DE DADOS QUE É CHAVE-PRIMÁRIA E ID PARA O DT_RowID
	$sistema_chave_primaria_grid = "vis_doc_cat_id";

	//PREFIXO DOS CAMPOS DA TABELA DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ GERENCIAR
	$sistema_prefixo_campos = "doc_categoria_";

	//PREFIXO DOS CAMPOS DA TABELA/VIEW DO BANCO DE DADOS ONDE ESTÃO AS INFORMÇÕES QUE O SISTEMA IRÁ EXIBIR NO GRID
	$sistema_prefixo_campos_grid = "vis_doc_cat_";

	//NOME COMPLETO DO CAMPO NO BD QUE SERÁ O CONTROLE PARA NÃO EXIBIR OS EXCLUÍDOS
	$sistema_campo_excluido = "";






	/*###################################################################
	|																	|
	|	Variável $sistema_sql_adicional armazena condições WHERE 		|
	|	adicionais que poderão ser utilizadas para limitar as 			|
	|	pesquisas. Esta condição é aplicada em todas as consultas do 	|
	|	GRID. 															|
	|																	|
	|	Formato: usar sintaxe padrão SQL condicional, após o WHERE, 	|
	|	iniciando com "( " e terminando com " ) AND ".					|
	|	Ex: campo = 'valor' AND campo <> 'valor', etc					|
	|	 																|
	|	Se não houver necessidade de SQL adicional, deixar em branco.	|
	|	 																|
	###################################################################*/

	//$sistema_sql_adicional = " ( cli_cart_cliente_situacao = 'A' ) AND ";




	/*###################################################################
	|																	|
	|	Variável $array_campos_filtragem_permissoes é um array 			|
	|	multidimensional que contém as informações dos campos de 		|
	|	filtragem com base nas permissões, para o sistema não exibir 	|
	|	as informações que o usuário não tem acesso.					|
	|	 																|
	|	Fomato: "nome_do_campo_grid" => "array de permissões"			|
	|			array de permissões do $auth_filial						|
	|	 																|
	###################################################################*/

	//$array_campos_filtragem_permissoes = array("vis_doc_secao" => $auth_filial[$_REQUEST['filial']][$sistema."-secao"]);





	/*###################################################################
	|																	|
	|	Variável $array_colunas_grid é array multidimensional que 		|
	|	contém todas as configucações das colunas que constam no GRID	|
	| 	da tabela.														|
	|																	|
	|	Formato: NOME_DO_CAMPO_BD => (title,							|
	|								width,								|
	|								visible,							|
	|								searchable,							|
	|								orderable,							|
	|								array(formatacao)					|
	|	 																|
	|	NOME_DO_CAMPO_BD: deve ser escritos SEM O PREFIXO 				|
	|																	|
	|	title: título da coluna pode ser escrito em UTF8 acentudo		|
	|																	|
	|	width: tamanho da coluna no GRID. O somatório total das 		|
	|			colunas não pode ser maior do que 90%					|
	|																	|
	|	visible: "true" se deseja mostrar a coluna no GRID, "false" 	|
	|			para não mostrar	 									|
	|																	|
	|	searchable: "true" se deseja permitir busca no valor dessa 		|
	|				coluna, "false" para não mostrar	 				|
	|																	|
	|	className: "nome_da_class" indicar o nome da classe CSS para	|
	|				uso nas colunas quando desejar formatação diferente	|
	|				da padrão, uso opcional								|
	|																	|
	|	orderable: "true" se deseja permitir busca no valor dessa 		|
	|				coluna, "false" para não mostrar	 				|
	|																	|
	|	hidden: indicar o nome da classe hidden da coluna conforme		|
	|			a resolução do dispositivo. Opcional	 				|
	|																	|
	|	array(formatacao): array que contém a informações de formatação	|
	|						do valor a ser exibido no GRID,incluindo a 	|
	|						aplicação de funções de formatação de data,	|
	|						moeda, ou outros tratamentos que precisam 	|
	|						ser feitos com o valor. Só utilizar quendo	|
	|						exigir formatação. A formatação deverá 		|
	|						ser colocada após o return do function		|
	|	 																|
	###################################################################*/

	$array_colunas_grid = array("id" => array("title" => "ID",
											"width" => "3%",
											"visible" => "true",
											"searchable" => "true",
											"orderable" => "false"),

								"titulo" => array("title" => "Título",
												"width" => "28%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true"),

								"nivel_titulo" => array("title" => "Nível Superior",
												"width" => "28%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true"),

								"status" => array("title" => "Status",
												"width" => "14%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true",
												"formatacao" => array("formatacao" => function($formatacao) { return $_SESSION["guarda_config"]["titulo"]["todas"]["status"][$formatacao]; })),

								"ordem" => array("title" => "Ordem",
												"width" => "10%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "true"),

								"ordem_max" => array("title" => "",
												"width" => "5%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "false",
												"formatacao" => array("formatacao" => function($formatacao) {
													$separa_dados = explode('$', $formatacao);
													return (($separa_dados[2]<$separa_dados[0])||($separa_dados[2]>=0)) ? "<a id='txt-up' data-id='".$separa_dados[3]."' title='Aumentar ordem'><i class='ace-icon fa fa-arrow-circle-down bigger-130'></i></a>" : "";
												})),

								"ordem_min" => array("title" => "",
												"width" => "5%",
												"visible" => "true",
												"searchable" => "true",
												"orderable" => "false",
												"formatacao" => array("formatacao" => function($formatacao) {
													$separa_dados = explode('$', $formatacao);
													return (($separa_dados[2]>$separa_dados[0])||($separa_dados[2]>0)) ? "<a id='txt-down' data-id='".$separa_dados[3]."' title='Reduzir ordem'><i class='ace-icon fa fa-arrow-circle-up bigger-130'></i></a>" : "";
												})),

								"nivel" => array("title" => "NIVEL",
												"width" => "",
												"visible" => "false",
												"searchable" => "true",
												"orderable" => "true"),

								"nivel_filtrar" => array("title" => "NIVEL_FILTRAR",
												"width" => "",
												"visible" => "false",
												"searchable" => "true",
												"orderable" => "true")
												);


	/*###################################################################
	|																	|
	|	Variável $array_colunas_grid é array multidimensional que 		|
	|	contém as informações de como será a ordenação padrão do GRID. 	|
	|	Colocar a array na sequencia que deseja que seja feita a 		|
	|	ordenação. A primeira key é o nome do campo igual ao utilizado 	|
	|	no $array_colunas_grid e o valor indica se será ASC ou DESC		|
	|																	|
	###################################################################*/

	$array_ordenacao_grid = array("nivel_titulo" => "asc",
									"ordem" => "asc");


	//INCLUI O ARQUIVO DE FUNÇÕES ADICIONAIS PARA TODOS OS SISTEMAS
	include($pasta_includes."/functions.sistema.inc.php");

?>