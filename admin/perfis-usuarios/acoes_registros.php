<?php
/*###################################################################
|																	|
|	MÓDULO: usuários												|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 07/12/2015												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if($_REQUEST['operacao']=="selecionar")
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			// echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];

					//SELECIONA AS PERMISSÕES
					$sql_permissoes = "SELECT * FROM visao_perfis_permissoes WHERE vis_perf_perm_perfil = '".$_REQUEST['chave_primaria']."'";
					$exe_permissoes = mysql_query($sql_permissoes, $con) or die("Erro do MySQL[exe_permissoes]: ".mysql_error());
					while($ver_permissoes = mysql_fetch_array($exe_permissoes))
						{
							//ARMAZENA OS CAMPOS DE PERMISSÕES DE SISTEMAS E OPERAÇÕES
							$rows[$num]["permissao_".$ver_permissoes["vis_perf_perm_secao"]."_".$ver_permissoes["vis_perf_perm_operacao"]] = $ver_permissoes["vis_perf_perm_valor"];
							if($ver_permissoes["vis_perf_perm_filial"]<>"") $rows[$num]["perm_fi_".$ver_permissoes["vis_perf_perm_filial"]."_".$ver_permissoes["vis_perf_perm_secao"]."_".$ver_permissoes["vis_perf_perm_valor"]] = "sim";

							//ARMAZENA AS PERMISSÕES ESPECIAIS
							$rows[$num]["ignorar_perm_doc"] = (($ver_permissoes["vis_perf_perm_secao"]=="especial")&&($ver_permissoes["vis_perf_perm_operacao"]=="ignorar_perm_doc")) ? $ver_permissoes["vis_perf_perm_valor"] : "";
						}


				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			$_POST["perfil_status"] = $_POST["perfil_status_hidden"];
			unset($_POST["perfil_status_hidden"]);



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			// echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_perfil = mysql_insert_id($con);

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir)
				{

					$sistema_nome_da_tabela = "perfis_permissoes";


					/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI E EXECUTA A QUERY PARA INSERIR CADA PERMISSÃO ####*/
					if(count($_POST["permissao"])>0)
						{
							/*### INSERE AS PERMISSÕES GERAIS QUE NÃO ESTÃO RELACIOADAS A FILIAIS ###*/
							foreach($_POST["permissao"] as $nome_sistema => $array_permissoes_sistema)
								{
									//echo "<br>".$nome_sistema;
									foreach($array_permissoes_sistema as $nome_operacao => $valor_permissao)
										{
											//echo "<br>".$nome_operacao." => ".$valor_permissao;
											$sql_permissao[$nome_sistema][$nome_operacao] = "INSERT INTO ".$sistema_nome_da_tabela." (perfil_permissao_perfil,
																																				perfil_permissao_secao,
																																				perfil_permissao_operacao,
																																				perfil_permissao_valor)
																																			VALUES
																																				('".$id_perfil."',
																																				'".$nome_sistema."',
																																				'".$nome_operacao."',
																																				'".$valor_permissao."')";
											// echo "<br>sql_permissao: ".$sql_permissao[$nome_sistema][$nome_operacao];
											$exe_permissao[$nome_sistema][$nome_operacao] = mysql_query($sql_permissao[$nome_sistema][$nome_operacao], $con) or die("Erro do MySQL[exe_permissao[$nome_sistema][$nome_operacao]]: ".mysql_error());
										}
								}
							/*### INSERE AS PERMISSÕES GERAIS QUE NÃO ESTÃO RELACIOADAS A FILIAIS ###*/



							/*### INSERE AS PERMISSÕES QUE ESTÃO RELACIOADAS A FILIAIS ###*/
							foreach($_POST["permissao_filial"] as $filial => $array_secoes_usuario)
								{
									//echo "<br>1".$filial." => ";
									//print_r($array_secoes_usuario);
									foreach($array_secoes_usuario as $secao_usuario => $array_secao_valor)
										{
											//echo "<br>2".$secao_usuario." => ";
											//print_r($array_secao_valor);
											foreach($array_secao_valor as $secao_permitida => $sim)
												{
													//echo "<br>3".$secao_permitida." => ".$sim;
													$sql_permissao_filial[$filial][$secao_usuario][$secao_permitida] = "INSERT INTO ".$sistema_nome_da_tabela." (perfil_permissao_perfil,
																																						perfil_permissao_filial,
																																						perfil_permissao_operacao,
																																						perfil_permissao_secao,
																																						perfil_permissao_valor)
																																					VALUES
																																						('".$id_perfil."',
																																						'".$filial."',
																																						'secoes',
																																						'".$secao_usuario."',
																																						'".$secao_permitida."')";
													// echo "<br>sql_permissao_filial: ".$sql_permissao_filial[$filial][$secao_usuario][$secao_permitida];
													$exe_permissao_filial[$filial][$secao_usuario][$secao_permitida] = mysql_query($sql_permissao_filial[$filial][$secao_usuario][$secao_permitida], $con) or die("Erro do MySQL[exe_permissao_filial[$filial][$secao_usuario][$secao_permitida]]: ".mysql_error());
												}
										}
								}
							/*### INSERE AS PERMISSÕES QUE ESTÃO RELACIOADAS A FILIAIS ###*/





							/*### INSERE AS PERMISSÕES ESPECIAIS ###*/
							if($_POST["ignorar_perm_doc"]<>"")
								{
									//echo "<br>".$nome_operacao." => ".$valor_permissao;
									$sql_permissao_especial = "INSERT INTO ".$sistema_nome_da_tabela." (perfil_permissao_perfil,
																										perfil_permissao_secao,
																										perfil_permissao_operacao,
																										perfil_permissao_valor)
																									VALUES
																										('".$id_perfil."',
																										'especial',
																										'ignorar_perm_doc',
																										'".$_POST['ignorar_perm_doc']."')";
									//echo "<br>sql_permissao_especial: ".$sql_permissao_especial;
									$exe_permissao_especial = mysql_query($sql_permissao_especial, $con) or die("Erro do MySQL[exe_permissao_especial]: ".mysql_error());
								}
							/*### INSERE AS PERMISSÕES ESPECIAIS ###*/


						}
					/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI E EXECUTA A QUERY PARA INSERIR CADA PERMISSÃO ####*/


					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/












	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);


			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			$_POST["perfil_status"] = $_POST["perfil_status_hidden"];
			unset($_POST["perfil_status_hidden"]);



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/





			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar)
				{
					$id_perfil = $_POST[$sistema_chave_primaria];

					$sistema_prefixo_campos = "perfil_permissao_";
					$sistema_nome_da_tabela = "perfis_permissoes";

					/*#### APAGA TODAS AS PERMISSÕES ATUAIS DO PERFIL ####*/
					$sql_excluir_permissoes = "DELETE FROM ".$sistema_nome_da_tabela." WHERE perfil_permissao_perfil = '".$id_perfil."'";
					//echo "<br>sql_excluir_permissoes: ".$sql_excluir_permissoes;
					$exe_excluir_permissoes = mysql_query($sql_excluir_permissoes, $con) or die("Erro do MySQL[exe_excluir_permissoes]: ".mysql_error());
					/*#### APAGA TODAS AS PERMISSÕES ATUAIS DO PERFIL ####*/



					/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI E EXECUTA A QUERY PARA INSERIR CADA PERMISSÃO ####*/
					if(count($_POST["permissao"])>0)
						{
							/*### INSERE AS PERMISSÕES GERAIS QUE NÃO ESTÃO RELACIOADAS A FILIAIS ###*/
							foreach($_POST["permissao"] as $nome_sistema => $array_permissoes_sistema)
								{
									//echo "<br>".$nome_sistema;
									foreach($array_permissoes_sistema as $nome_operacao => $valor_permissao)
										{
											//echo "<br>".$nome_operacao." => ".$valor_permissao;
											$sql_permissao[$nome_sistema][$nome_operacao] = "INSERT INTO ".$sistema_nome_da_tabela." (perfil_permissao_perfil,
																																				perfil_permissao_secao,
																																				perfil_permissao_operacao,
																																				perfil_permissao_valor)
																																			VALUES
																																				('".$id_perfil."',
																																				'".$nome_sistema."',
																																				'".$nome_operacao."',
																																				'".$valor_permissao."')";
											// echo "<br>sql_permissao: ".$sql_permissao[$nome_sistema][$nome_operacao];
											$exe_permissao[$nome_sistema][$nome_operacao] = mysql_query($sql_permissao[$nome_sistema][$nome_operacao], $con) or die("Erro do MySQL[exe_permissao[$nome_sistema][$nome_operacao]]: ".mysql_error());
										}
								}
							/*### INSERE AS PERMISSÕES GERAIS QUE NÃO ESTÃO RELACIOADAS A FILIAIS ###*/



							/*### INSERE AS PERMISSÕES QUE ESTÃO RELACIOADAS A FILIAIS ###*/
							if(count($_POST["permissao_filial"])>0)
								{
									foreach($_POST["permissao_filial"] as $filial => $array_secoes_usuario)
										{
											//echo "<br>1".$filial." => ";
											//print_r($array_secoes_usuario);
											foreach($array_secoes_usuario as $secao_usuario => $array_secao_valor)
												{
													//echo "<br>2".$secao_usuario." => ";
													//print_r($array_secao_valor);
													foreach($array_secao_valor as $secao_permitida => $sim)
														{
															//echo "<br>3".$secao_permitida." => ".$sim;
															$sql_permissao_filial[$filial][$secao_usuario][$secao_permitida] = "INSERT INTO ".$sistema_nome_da_tabela." (perfil_permissao_perfil,
																																								perfil_permissao_filial,
																																								perfil_permissao_operacao,
																																								perfil_permissao_secao,
																																								perfil_permissao_valor)
																																							VALUES
																																								('".$id_perfil."',
																																								'".$filial."',
																																								'secoes',
																																								'".$secao_usuario."',
																																								'".$secao_permitida."')";
															// echo "<br>sql_permissao_filial: ".$sql_permissao_filial[$filial][$secao_usuario][$secao_permitida];
															$exe_permissao_filial[$filial][$secao_usuario][$secao_permitida] = mysql_query($sql_permissao_filial[$filial][$secao_usuario][$secao_permitida], $con) or die("Erro do MySQL[exe_permissao_filial[$filial][$secao_usuario][$secao_permitida]]: ".mysql_error());
														}
												}
										}
								}
							/*### INSERE AS PERMISSÕES QUE ESTÃO RELACIOADAS A FILIAIS ###*/





							/*### INSERE AS PERMISSÕES ESPECIAIS ###*/
							if($_POST["ignorar_perm_doc"]<>"")
								{
									//echo "<br>".$nome_operacao." => ".$valor_permissao;
									$sql_permissao_especial = "INSERT INTO ".$sistema_nome_da_tabela." (perfil_permissao_perfil,
																										perfil_permissao_secao,
																										perfil_permissao_operacao,
																										perfil_permissao_valor)
																									VALUES
																										('".$id_perfil."',
																										'especial',
																										'ignorar_perm_doc',
																										'".$_POST['ignorar_perm_doc']."')";
									//echo "<br>sql_permissao_especial: ".$sql_permissao_especial;
									$exe_permissao_especial = mysql_query($sql_permissao_especial, $con) or die("Erro do MySQL[exe_permissao_especial]: ".mysql_error());
								}
							/*### INSERE AS PERMISSÕES ESPECIAIS ###*/


						}
					/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI E EXECUTA A QUERY PARA INSERIR CADA PERMISSÃO ####*/

					if($_REQUEST['modificar_usuarios']=="sim")
						{
							$sql_usuarios_do_perfil = "SELECT DISTINCT vis_usuario_id FROM visao_usuarios WHERE vis_usuario_perfil_id = ".$id_perfil;
							// echo "<br>sql_usuarios_do_perfil: ".$sql_usuarios_do_perfil;
							$exe_usuarios_do_perfil = mysql_query($sql_usuarios_do_perfil, $con) or die("Erro do MySQL[exe_usuarios_do_perfil]: ".mysql_error());
							while($ver_usuarios_do_perfil = mysql_fetch_array($exe_usuarios_do_perfil))
								{
									$num_user++;
									$usuarios_do_perfil[] = $ver_usuarios_do_perfil["vis_usuario_id"];
									$sql_apaga_permissao = "DELETE FROM usuarios_permissoes WHERE usuario_permissao_usuario = ".$ver_usuarios_do_perfil["vis_usuario_id"];
									// echo "<br>sql_apaga_permissao: ".$sql_apaga_permissao;
									$exe_apaga_permissao = mysql_query($sql_apaga_permissao, $con) or die("Erro do MySQL[exe_apaga_permissao]: ".mysql_error());
								}
							if($num_user>0)
								{
									foreach($usuarios_do_perfil as $key => $id_user)
										{
											$sql_insere_permissoes = "INSERT INTO
																				usuarios_permissoes
																					(usuario_permissao_usuario,
																						usuario_permissao_secao,
																						usuario_permissao_operacao,
																						usuario_permissao_filial,
																						usuario_permissao_valor)
																					(SELECT
																						'".$id_user."' AS user_id,
																						vis_perf_perm_secao,
																						vis_perf_perm_operacao,
																						vis_perf_perm_filial,
																						vis_perf_perm_valor
																					FROM
																						visao_perfis_permissoes
																					WHERE
																						vis_perf_perm_perfil = ".$id_perfil.")";
											// echo "<br>sql_insere_permissoes: ".$sql_insere_permissoes;
											$exe_insere_permissoes = mysql_query($sql_insere_permissoes, $con) or die("Erro do MySQL[exe_insere_permissoes]: ".mysql_error());
										}
								}


						}

					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			// print_r($_REQUEST);

			/*#### CONSTRÓI A QUERY QUE EXCLUI O PERFIL ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY QUE EXCLUI O PERFIL ####*/

			//EXECUTA A QUERY QUE EXCLUI O PERFIL
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			if($_REQUEST['novo_perfil']>0)
				{
					/*#### BUSCA TODOS OS USUÁRIOS DO PERFIL QUE VAI SER EXCLUÍDO E ALTERA AS PERMISSÕES PARA O NOVO PERFIL ####*/
					$sql_usuarios_do_perfil = "SELECT DISTINCT vis_usuario_id FROM visao_usuarios WHERE vis_usuario_perfil_id = ".$_REQUEST['chave_primaria'];
					// echo "<br>sql_usuarios_do_perfil: ".$sql_usuarios_do_perfil;
					$exe_usuarios_do_perfil = mysql_query($sql_usuarios_do_perfil, $con) or die("Erro do MySQL[exe_usuarios_do_perfil]: ".mysql_error());
					while($ver_usuarios_do_perfil = mysql_fetch_array($exe_usuarios_do_perfil))
						{
							$num_user++;
							$usuarios_do_perfil[] = $ver_usuarios_do_perfil["vis_usuario_id"];
							$sql_apaga_permissao = "DELETE FROM usuarios_permissoes WHERE usuario_permissao_usuario = ".$ver_usuarios_do_perfil["vis_usuario_id"];
							// echo "<br>sql_apaga_permissao: ".$sql_apaga_permissao;
							$exe_apaga_permissao = mysql_query($sql_apaga_permissao, $con) or die("Erro do MySQL[exe_apaga_permissao]: ".mysql_error());
						}
					if($num_user>0)
						{
							foreach($usuarios_do_perfil as $key => $id_user)
								{
									$sql_insere_permissoes = "INSERT INTO
																		usuarios_permissoes
																			(usuario_permissao_usuario,
																				usuario_permissao_secao,
																				usuario_permissao_operacao,
																				usuario_permissao_filial,
																				usuario_permissao_valor)
																			(SELECT
																				'".$id_user."' AS user_id,
																				vis_perf_perm_secao,
																				vis_perf_perm_operacao,
																				vis_perf_perm_filial,
																				vis_perf_perm_valor
																			FROM
																				visao_perfis_permissoes
																			WHERE
																				vis_perf_perm_perfil = ".$_REQUEST['novo_perfil'].")";
									// echo "<br>sql_insere_permissoes: ".$sql_insere_permissoes;
									$exe_insere_permissoes = mysql_query($sql_insere_permissoes, $con) or die("Erro do MySQL[exe_insere_permissoes]: ".mysql_error());
								}
						}
					/*#### BUSCA TODOS OS USUÁRIOS DO PERFIL QUE VAI SER EXCLUÍDO E ALTERA AS PERMISSÕES PARA O NOVO PERFIL ####*/


					/*#### MODIFICA OS USUÁRIOS DO PERFIL EXCLUÍDO PARA O NOVO PERFIL ####*/
					$sql_atualizar = "UPDATE usuarios SET usuario_perfil = ".$_REQUEST['novo_perfil'].",
															usuario_atualizacao_data = '".$hoje_data_us."',
															usuario_atualizacao_usuario = '".$_SESSION["login"]["id"]."'
														WHERE
															usuario_perfil = '".$_REQUEST['chave_primaria']."'
															AND usuario_excluido <> 'sim'";
					// echo "<br>sql_atualizar: ".$sql_atualizar;

					$exe_atualizar = mysql_query($sql_atualizar, $con) or die("Erro do MySQL[exe_atualizar]: ".mysql_error());
					/*#### MODIFICA OS USUÁRIOS DO PERFIL EXCLUÍDO PARA O NOVO PERFIL ####*/
				}

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/











	/*#### AÇÕES QUANDO A OPERAÇÃO FOR CHECAR SE O PERFIL TEM USUÁRIO ####*/
	if($_REQUEST['operacao']=="checa_usuarios_perfil")
		{
			/*#### CONSTRÓI A QUERY DOS USUÁRIOS DO PERFIL ####*/
			$sql_usuarios = "SELECT
									COUNT(*) AS total_usuarios,
									GROUP_CONCAT(DISTINCT vis_usuario_nome ORDER BY vis_usuario_nome ASC SEPARATOR ', ') AS usuarios
								FROM
									visao_usuarios
								WHERE
									vis_usuario_perfil_id = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_usuarios: ".$sql_usuarios;
			/*#### CONSTRÓI A QUERY DOS USUÁRIOS DO PERFIL ####*/

			//EXECUTA A QUERY DOS USUÁRIOS DO PERFIL
			$exe_usuarios = mysql_query($sql_usuarios, $con) or die("Erro do MySQL[exe_usuarios]: ".mysql_error());

			//RETORNA OS DADOS DOS USUÁRIOS DO PERFIL
			$ver_usuarios = mysql_fetch_array($exe_usuarios);

			$rows["num_usuarios"] = $ver_usuarios["total_usuarios"];
			$rows["nome_usuarios"] = $ver_usuarios["usuarios"];


			/*#### CONSTRÓI A QUERY DA LISTA DE PERFIS ATUAIS ####*/
			$sql_perfis = "SELECT
									vis_perfil_id,
									vis_perfil_titulo
								FROM
									visao_perfis
								WHERE
									vis_perfil_id <> '".$_REQUEST['chave_primaria']."'
									AND vis_perfil_status = 1";
			//echo "<br>sql_perfis: ".$sql_perfis;
			/*#### CONSTRÓI A QUERY DA LISTA DE PERFIS ATUAIS ####*/

			//EXECUTA A QUERY DOS PERFIS
			$exe_perfis = mysql_query($sql_perfis, $con) or die("Erro do MySQL[exe_perfis]: ".mysql_error());

			//RETORNA OS DADOS DOS PERFIS
			while($ver_perfis = mysql_fetch_array($exe_perfis))
				{
					$rows["perfis"][$ver_perfis["vis_perfil_id"]] = $ver_perfis["vis_perfil_titulo"];
				}


			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_usuarios) $result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR CHECAR SE O PERFIL TEM USUÁRIO ####*/




















	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
