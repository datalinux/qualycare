<?
	$include_functions = "nao";
	include("../includes/configure.inc.php");

	if($_REQUEST['acao']=='sair')
		{
			session_start();
			session_destroy();
		}

	// echo "<pre>SESSION: "; print_r($_SESSION); echo "</pre>";
?>
<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><? echo $titulo_admin; ?></title>

		<meta name="description" content="Login do usuário" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="../lib/ace/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="../lib/font-awesome-4.7.0/css/font-awesome.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="../lib/ace/assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="../lib/ace/assets/css/ace.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../lib/ace/assets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="../lib/ace/assets/css/ace-rtl.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="../lib/ace/assets/css/ace-ie.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="../lib/ace/assets/css/ace.onpage-help.css" />

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="../lib/ace/assets/js/html5shiv.js"></script>
		<script src="../lib/ace/assets/js/respond.js"></script>
		<![endif]-->

		<!-- INCLUI ESTILOS DOS AVISOS GRITTER -->
		<link rel="stylesheet" href="../lib/ace/assets/css/jquery.gritter.css" />

		<!-- INCLUI ESTILOS PERSONALIZADOS PARA O SISTEMA, ESPECÍFICOS PARA PÁGINA HOME.PHP -->
		<link rel="stylesheet" href="../includes/estilos.admin.index.css" />
	</head>

	<body class="login-layout">

		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<img src="../images/logo_entrada.png" style="margin-bottom:15px;"><br><span class="white">Administração do Site</span>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="icon-user light-orange"></i>
												Digite seu usuário e senha
											</h4>

											<div class="space-6"></div>

											<form name="formulario-login" id="formulario-login">
												<fieldset>
                                                    <div class="form-group">
                                                        <label class="block clearfix">
                                                            <span class="block input-icon input-icon-right">
                                                                <input name="login" id="login" type="text" class="form-control" placeholder="Login" />
                                                                <i class="ace-icon fa fa-user"></i>
                                                            </span>
                                                        </label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="block clearfix">
                                                            <span class="block input-icon input-icon-right">
                                                                <input name="senha" id="senha" type="password" class="form-control" placeholder="Senha" />
                                                                <i class="ace-icon fa fa-lock"></i>
                                                            </span>
                                                        </label>
                                                    </div>

													<div class="space"></div>

													<div class="clearfix">
														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
                                                <input name="pagina_http" id="pagina_http" type="hidden" value="<? if(substr_count($_SERVER['QUERY_STRING'],"pagina_http=")>0) echo str_replace("pagina_http=","http://",$_SERVER['QUERY_STRING']); ?>" />
                                                <input name="acao" id="acao" type="hidden" value="<? echo $_REQUEST['acao']; ?>" />
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="#" data-target="#forgot-box" class="forgot-password-link">
													<i class="ace-icon fa fa-arrow-left"></i>
													Esqueci minha senha
												</a>
											</div>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<div id="forgot-box" class="forgot-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Enviar senha
											</h4>

											<div class="space-6"></div>
											<p>
												Digite seu e-mail para receber as instruções para resetar a senha
											</p>

											<form name="formulario-senha" id="formulario-senha">
												<fieldset>
                                                    <div class="form-group">
                                                        <label class="block clearfix">
                                                            <span class="block input-icon input-icon-right">
                                                                <input name="email_senha" id="email_senha" type="email" class="form-control" placeholder="E-mail" />
                                                                <i class="ace-icon fa fa-envelope"></i>
                                                            </span>
                                                        </label>
                                                    </div>

													<div class="clearfix">
														<button type="submit" class="width-35 pull-right btn btn-sm btn-danger">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Enviar!</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar center">
											<a href="#" data-target="#login-box" class="back-to-login-link">
												Voltar ao login
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->

							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../lib/ace/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='../lib/ace/assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='../lib/ace/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>


		<!--#### Adicionar js de funções gerais bootstrap ###-->
		<script src="../lib/ace/assets/js/bootstrap.js"></script>

		<!--#### Adicionar js de funções de validação de form ###-->
		<script src="../lib/ace/assets/js/jquery.validate.js"></script>

		<!--#### Adicionar js de funções gritter ###-->
		<script src="../lib/ace/assets/js/jquery.gritter.js"></script>


		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});

			<? if(($_SESSION["login"]["usuario"]<>"")&&
				($_SESSION["login"]["id"]>0)&&
				($_SESSION["login"]["sessao_registrada"]==true)&&
				($_SESSION["login"]["session_name"]=="admin".$key_senha_login)&&
				($_REQUEST['acao']<>"sair")) echo "window.location.replace('home.php');"; ?>


			<!--### focus() no login quando carrega ###-->
			$("#formulario-login [name='login']").focus();


			jQuery(function($) {

				//documentation : http://docs.jquery.com/Plugins/Validation/validate


				<!--### Função que faz a validação do form de Login ###-->
				$('#formulario-login').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						login: {
							required: true
						},
						senha: {
							required: true,
							minlength: 5
						}
					},

					messages: {
						login: {
							required: "Digite o login"
						},
						senha: {
							required: "Digite a senha",
							minlength: "A senha deve ter no mínimo 5 caracteres"
						}
					},

					invalidHandler: function (event, validator) { //display error alert on form submit
						$('.alert-danger', $('.login-form')).show();
					},

					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},

					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error').addClass('has-info');
						$(e).remove();
					},

					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},

					submitHandler: function (form) {
						//form.submit();

						<!--### MOSTRA A DIV DE CARREGANDO ###-->
						var contentArea = $('.main-content');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						<!--### MOSTRA A DIV DE CARREGANDO ###-->


						<!--### AJAX para postagem do formulário de Login ###-->
						$.ajax({
							type: "POST",
							url: 'login.php',
							data: {
								login: $("#login").val(),
								senha: $("#senha").val(),
								pagina_http: $("#pagina_http").val(),
							},
							success: function(data)
							{
								<!--### ESCONDE A DIV DE CARREGANDO ###-->
								contentArea.css('opacity', 1);
								contentArea.prevAll('.ajax-loading-overlay').remove();
								<!--### ESCONDE A DIV DE CARREGANDO ###-->

								<!--### Se o login.php retornar texto OK vai para home.php ###-->
								if (data === 'OK') {
									window.location.replace('home.php');
								}
								<!--### Se o login.php retornar texto NAO mostra mensagem de erro ###-->
								else if (data === 'NOK')  {
									$.gritter.add({
										title: '<i class="ace-icon fa fa-times"></i> Erro!',
										text: 'Usuário ou senha digitados incorretamente.',
										class_name: 'gritter-error gritter-center',
										fade_out_speed:500
									});

								}
								<!--### Se o login.php retornar texto de erro mostra mensagem de debug ###-->
								else {
									$.gritter.add({
										title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
										text: data,
										class_name: 'gritter-error gritter-center',
										sticky: false,
										fade_out_speed:500
									});
								}
							}
						});


					},
					invalidHandler: function (form) {
					}
				});
				<!--### Função que faz a validação do form de Login ###-->









				<!--### Função que faz a validação do form de Enviar Senha ###-->
				$('#formulario-senha').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					rules: {
						email_senha: {
							required: true,
							email: true
						}
					},

					messages: {
						email_senha: {
							required: "Digite o e-mail",
							email: "E-mail inválido"
						}
					},

					invalidHandler: function (event, validator) { //display error alert on form submit
						$('.alert-danger', $('.login-form')).show();
					},

					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},

					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error').addClass('has-info');
						$(e).remove();
					},

					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},

					submitHandler: function (form) {

						<!--### MOSTRA A DIV DE CARREGANDO ###-->
						var contentArea = $('.forgot-box');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						<!--### MOSTRA A DIV DE CARREGANDO ###-->

						<!--### AJAX para postagem do formulário de Enviar Senha ###-->
						$.ajax({
							type: "POST",
							url: 'lembrar_senha.php',
							data: {
								email_senha: $("#email_senha").val()
							},
							success: function(data)
							{
								<!--### ESCONDE A DIV DE CARREGANDO ###-->
								contentArea.css('opacity', 1);
								contentArea.prevAll('.ajax-loading-overlay').remove();
								<!--### ESCONDE A DIV DE CARREGANDO ###-->

								<!--### Se o enviar_email.php retornar texto OK mostra aviso de OK ###-->
								if (data === 'OK') {
									$.gritter.add({
										title: '<i class="ace-icon fa fa-check"></i> OK!',
										text: 'E-mail do lembrete de senha enviado com sucesso!',
										class_name: 'gritter-success gritter-center',
										fade_out_speed:500,
										time:2000
									});
								}
								<!--### Se o enviar_email.php NÃO retornar texto Correct mostra erro ###-->
								else if (data === 'NOK') {
									$.gritter.add({
										title: '<i class="ace-icon fa fa-times"></i> Erro!',
										text: 'E-mail digitado não existe no cadastro.',
										class_name: 'gritter-error gritter-center',
										fade_out_speed:500
									});
								}
								<!--### Se o enviar_email.php retornar texto de erro mostra mensagem de debug ###-->
								else {
									$.gritter.add({
										title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
										text: data,
										class_name: 'gritter-error gritter-center',
										sticky: false,
										fade_out_speed:500
									});
								}
							}
						});

					},
					invalidHandler: function (form) {
					}
				});

				<?
					if($_REQUEST['acao']=="logout")
						{
				?>
							$.gritter.add({
								title: '<i class="ace-icon fa fa-check"></i> OK!',
								text: 'Logout efetuado com sucesso!',
								class_name: 'gritter-info gritter-center',
								fade_out_speed:500,
								time:2000
							});
				<?
						}
				?>

			})

		</script>
	</body>
</html>
