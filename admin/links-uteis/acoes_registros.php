<?php
/*###################################################################
|																	|
|	MÓDULO: arquivos												|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 07/12/2015												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if($_REQUEST['operacao']=="selecionar")
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									LEFT JOIN
										fotos ON fotos.foto_id = ".$sistema_nome_da_tabela.".".$sistema_prefixo_campos."foto
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//MONTA O ARRAY ORDENADO COM AS SEÇÕES DO REGISTRO
					$separa_secoes = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."secao"]);
					foreach($separa_secoes as $secao_nome) {
						if($secao_nome<>"") {
							$num_secao++;
							$rows[$num][$sistema_prefixo_campos."secao[]"][$num_secao] = $secao_nome;
						}
					}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];
					$rows[$num]["thumb_da_foto"] = retorna_url_foto($ver_selecionar[$sistema_prefixo_campos."foto"], "G", "url")."?".md5(uniqid(microtime(),1)).getmypid();
					$rows[$num]["titulo_da_foto"] = $ver_selecionar["foto_titulo"];
				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{
			//print_r($_REQUEST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['link_secao'] as $secao_key => $secao_marcada)
				{
					$link_secao .= $separador_string.$secao_marcada;
				}
			$link_secao .= $separador_string;
			$_POST['link_secao'] = $link_secao;
			$_POST["link_status"] = $_POST["link_status_hidden"];
			unset($_POST['link_status_hidden']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/






			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			//echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_registro = mysql_insert_id($con);

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['link_secao'] as $secao_key => $secao_marcada)
				{
					$link_secao .= $separador_string.$secao_marcada;
				}
			$link_secao .= $separador_string;
			$_POST['link_secao'] = $link_secao;
			$_POST["link_status"] = $_POST["link_status_hidden"];
			unset($_POST['link_status_hidden']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/









	/*#### AÇÕES QUANDO A OPERAÇÃO FOR AUMENTAR/REDUZIR A ORDEM DO TEXTO ####*/
	if(($_REQUEST['operacao']=="txt-up")||($_REQUEST['operacao']=="txt-down"))
		{
			//print_r($_POST);

			$incremento = ($_REQUEST['operacao']=="txt-up") ? "+1" : "-1";

			/*#### CONSTRÓI A QUERY ####*/
			$sql_incremento = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."ordem = ".$sistema_prefixo_campos."ordem ".$incremento.", ".
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_incremento: ".$sql_incremento;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_incremento = mysql_query($sql_incremento, $con) or die("Erro do MySQL[exe_incremento]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_incremento) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR AUMENTAR/REDUZIR A ORDEM DO TEXTO ####*/


















	/*#### AÇÕES FOR PARA RETORNAR A LISTA DE CONTEÚDOS DO TIPO DE LINK ####*/
	if($_REQUEST['operacao']=="conteudo-tipo-link")
		{
			if($_REQUEST['link_tipo']=="noticias") {

				$sql_noticias = "SELECT * FROM visao_noticias ORDER BY vis_noticia_secao ASC, vis_noticia_data DESC";
				$exe_noticias = mysql_query($sql_noticias, $con) or die("Erro do MySQL[]: ".mysql_error());
				while($ver_noticias = mysql_fetch_array($exe_noticias)) {

					$separa_secoes = explode($GLOBALS['separador_string'],$ver_noticias["vis_noticia_secao"]);
					foreach($separa_secoes as $nome_secao) {
						if($nome_secao<>"") {
							$titulo_secoes .= $_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']]["noticias-secao"][$nome_secao].", ";
						}
					}

					$rows["data"][]["id"] = $ver_noticias["vis_noticia_id"];
					$rows["data"][]["value"] = substr($titulo_secoes,0,-2)." - ".mostra_data_completa_br($ver_noticias["vis_noticia_data"])." - ".$ver_noticias["vis_noticia_titulo"];
				}

				$rows["placeholder"] = "Escolha a notícia";
			}

			elseif($_REQUEST['link_tipo']=="textos-institucionais") {

				$sql_textos_institucionais = "SELECT * FROM visao_textos_institucionais ORDER BY vis_txt_inst_secao ASC, vis_txt_inst_ordem ASC";
				$exe_textos_institucionais = mysql_query($sql_textos_institucionais, $con) or die("Erro do MySQL[]: ".mysql_error());
				while($ver_textos_institucionais = mysql_fetch_array($exe_textos_institucionais)) {

					$separa_secoes = explode($GLOBALS['separador_string'],$ver_textos_institucionais["vis_txt_inst_secao"]);
					foreach($separa_secoes as $nome_secao) {
						if($nome_secao<>"") {
							$titulo_secoes .= $_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']]["textos-institucionais-secao"][$nome_secao].", ";
						}
					}

					$subtitulo = ($ver_textos_institucionais["vis_txt_inst_subtitulo"]<>"") ? " - ".$ver_textos_institucionais["vis_txt_inst_subtitulo"] : "";

					$rows["data"][]["id"] = $ver_textos_institucionais["vis_txt_inst_id"];
					$rows["data"][]["value"] = substr($titulo_secoes,0,-2)." - ".$ver_textos_institucionais["vis_txt_inst_ordem"]." - ".$ver_textos_institucionais["vis_txt_inst_titulo"].$subtitulo;
				}

				$rows["placeholder"] = "Escolha o texto institucional";
			}

			echo json_encode($rows);
		}
	/*#### AÇÕES FOR PARA RETORNAR A LISTA DE CONTEÚDOS DO TIPO DE LINK ####*/

















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/


	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
