<?php
/*###################################################################
|																	|
|	MÓDULO: destaques-topo											|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 17/01/2016												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if($_REQUEST['operacao']=="selecionar")
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//MONTA O ARRAY ORDENADO COM AS SEÇÕES DO REGISTRO
					$separa_secoes = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."secao"]);
					foreach($separa_secoes as $secao_nome) {
						if($secao_nome<>"") {
							$num_secao++;
							$rows[$num][$sistema_prefixo_campos."secao[]"][$num_secao] = $secao_nome;
						}
					}

					//MONTA O HTML COM O LINK DO ARQUIVO, SE EXISTIR
					if($ver_selecionar[$sistema_prefixo_campos."arquivo"]<>"") $rows[$num]["destaque_topo_existente"] .= "<div style='margin-bottom:10px;'><i class='fa fa-file-image-o'></i> Destaque existente: <a target='_blank' href='".$GLOBALS['http_destaques_topo']."/".$ver_selecionar[$sistema_prefixo_campos."arquivo"]."' data-rel='img_popover' data-trigger='hover' data-placement='top' data-content='<img src=\"".$GLOBALS['http_destaques_topo']."/".$ver_selecionar[$sistema_prefixo_campos."arquivo"]."?".$hoje_data_mk."\" class=\"img-responsive\">'  title='Pré-visualização'>".$ver_selecionar[$sistema_prefixo_campos."arquivo"]."</a></div><br>";

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."data_inicio"] = mostra_data_completa_br_timepicker($ver_selecionar[$sistema_prefixo_campos."data_inicio"]);
					$rows[$num][$sistema_prefixo_campos."data_termino"] = mostra_data_completa_br_timepicker($ver_selecionar[$sistema_prefixo_campos."data_termino"]);
				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{
			//print_r($_REQUEST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['destaque_topo_secao'] as $secao_key => $secao_marcada)
				{
					$destaque_topo_secao .= $separador_string.$secao_marcada;
				}
			$destaque_topo_secao .= $separador_string;
			$_POST['destaque_topo_secao'] = $destaque_topo_secao;
			$_POST['destaque_topo_data_inicio'] = mostra_data_completa_us($_POST['destaque_topo_data_inicio']);
			$_POST['destaque_topo_data_termino'] = mostra_data_completa_us($_POST['destaque_topo_data_termino']);
			$_POST["destaque_topo_status"] = $_POST["destaque_topo_status_hidden"];
			$_POST['destaque_topo_arquivo'] = t5f_sanitize_filename($_POST['destaque_topo_filename']);
			unset($_POST['destaque_topo_filename'], $_POST['destaque_topo_enviado'], $_POST['destaque_topo_status_hidden']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/






			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			//echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_registro = mysql_insert_id($con);

			if($exe_inserir)
				{
					//ATUALIZA O CAMPO DO NOME DE ARQUIVO
					$sql_atualiza_arquivo = "UPDATE
													".$sistema_nome_da_tabela."
												SET
													".$sistema_prefixo_campos."arquivo = CONCAT('".str_pad($id_registro,3,0,STR_PAD_LEFT)."','_',".$sistema_prefixo_campos."arquivo)
												WHERE
													".$sistema_prefixo_campos."id = ".$id_registro;
					//echo "<br>sql_atualiza_arquivo: ".$sql_atualiza_arquivo;
					$exe_atualiza_arquivo = mysql_query($sql_atualiza_arquivo, $con) or die("Erro do MySQL[exe_atualiza_arquivo]: ".mysql_error());

					//MOVE O ARQUIVO PARA A PASTA FINAL, JÁ RENOMEANDO
					if($exe_atualiza_arquivo)
						{
							rename($pasta_destaques_topo."/".$_POST['destaque_topo_pid'],$pasta_destaques_topo."/".str_pad($id_registro,3,0,STR_PAD_LEFT)."_".$_POST['destaque_topo_arquivo']);
						}
				}
			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['destaque_topo_secao'] as $secao_key => $secao_marcada)
				{
					$destaque_topo_secao .= $separador_string.$secao_marcada;
				}
			$destaque_topo_secao .= $separador_string;
			$_POST['destaque_topo_secao'] = $destaque_topo_secao;
			$_POST['destaque_topo_data_inicio'] = mostra_data_completa_us($_POST['destaque_topo_data_inicio']);
			$_POST['destaque_topo_data_termino'] = mostra_data_completa_us($_POST['destaque_topo_data_termino']);
			$_POST["destaque_topo_status"] = $_POST["destaque_topo_status_hidden"];
			if(($_POST['destaque_topo_enviado']>1)||(($_POST['destaque_topo_enviado']==1)&&($_POST['destaque_topo_filename']<>"")))
				{
					@unlink($GLOBALS['pasta_destaques_topo']."/".$_POST['destaque_topo_arquivo']);
					$_POST['destaque_topo_arquivo'] = t5f_sanitize_filename($_POST['destaque_topo_filename']);
				}
			else
				{
					unset($_POST['destaque_topo_arquivo']);
				}
			unset($_POST['destaque_topo_filename'], $_POST['destaque_topo_enviado'], $_POST['destaque_topo_status_hidden']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar)
				{
					//ATUALIZA O CAMPO DO NOME DE ARQUIVO
					if($_POST['destaque_topo_arquivo']<>"")
						{
							$nome_novo_arquivo = str_pad($_POST[$sistema_chave_primaria],3,0,STR_PAD_LEFT)."_".$_POST['destaque_topo_arquivo'];

							$sql_atualiza_arquivo = "UPDATE
															".$sistema_nome_da_tabela."
														SET
															".$sistema_prefixo_campos."arquivo = '".$nome_novo_arquivo."'
														WHERE
															".$sistema_prefixo_campos."id = ".$_POST[$sistema_chave_primaria];
							//echo "<br>sql_atualiza_arquivo: ".$sql_atualiza_arquivo;
							$exe_atualiza_arquivo = mysql_query($sql_atualiza_arquivo, $con) or die("Erro do MySQL[exe_atualiza_arquivo]: ".mysql_error());

							//MOVE O ARQUIVO PARA A PASTA FINAL, JÁ RENOMEANDO
							if($exe_atualiza_arquivo)
								{
									rename($pasta_destaques_topo."/".$_POST['destaque_topo_pid'],$pasta_destaques_topo."/".$nome_novo_arquivo);
								}
						}

					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/









	/*#### AÇÕES QUANDO A OPERAÇÃO FOR AUMENTAR/REDUZIR A ORDEM DO DESTAQUE ####*/
	if(($_REQUEST['operacao']=="dest-up")||($_REQUEST['operacao']=="dest-down"))
		{
			//print_r($_POST);

			$incremento = ($_REQUEST['operacao']=="dest-up") ? "+1" : "-1";

			/*#### CONSTRÓI A QUERY ####*/
			$sql_incremento = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."ordem = ".$sistema_prefixo_campos."ordem ".$incremento.", ".
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_incremento: ".$sql_incremento;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_incremento = mysql_query($sql_incremento, $con) or die("Erro do MySQL[exe_incremento]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_incremento) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR AUMENTAR/REDUZIR A ORDEM DO TEXTO ####*/











	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir)
				{
					//BUSCA O NOME DO ARQUIVO
					$sql_arquivo = "SELECT destaque_topo_arquivo FROM ".$sistema_nome_da_tabela." WHERE ".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
					$exe_arquivo = mysql_query($sql_arquivo, $con) or die("Erro do MySQL[exe_arquivo]: ".mysql_error());
					$ver_arquivo = mysql_fetch_array($exe_arquivo);
					//APAGA O ARQUIVO
					@unlink($GLOBALS['pasta_destaques_topo']."/".$ver_arquivo["destaque_topo_arquivo"]);
					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/









	/*#### AÇÕES QUANDO A OPERAÇÃO FOR RECORTAR UMA FOTO ####*/
	if($_REQUEST['operacao']=="recortar")
		{
			// print_r($_REQUEST);

			//FILTRA OS DADOS DO CAMPO DE IMAGEM, BUSCANDO SOMENTE A IMAGEM
			$conteudoImagem = substr($_REQUEST['foto_recortada'], strpos($_REQUEST['foto_recortada'], ",")+1);

			//DECODIFICA O CONTEÚDO DA IMAGEM
			$ImagemDecodificada = base64_decode($conteudoImagem);

			//SALVA A IMAGEM
			$imagem = fopen($GLOBALS['pasta_destaques_topo']."/".$_REQUEST['ImagePid'], 'wb' );
			fwrite( $imagem, $ImagemDecodificada);
			fclose( $imagem );

			$result = "OK";

		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR RECORTAR UMA FOTO ####*/



	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
