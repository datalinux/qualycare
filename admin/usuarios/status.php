<?
/*###################################################################
|																	|
|	Arquivo para demonstrar o status de alguns serviços e versões	|
|																	|
|	Autor: Guilherme Moreira de Castro					       		|
|	E-mail: guicastro@gmail.com									    |
|	Data: 10/08/2016												|
|																	|
###################################################################*/



	//INCLUSÃO DOS ARQUIVOS DE CONFIGURAÇÃO SEM FUNÇÕES JS
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO QUE PROTEGE A PÁGINA
	include($pasta_includes."/login.inc.php");
?>


<link rel="stylesheet" href="../lib/ace/assets/css/chosen.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/jquery.gritter.css" />
<link rel="stylesheet" href="../includes/estilos.admin.sistemas.css" />

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		Status
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Verifique o status e configurações de alguns serviços do sistema
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">


	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS --><!-- /.row -->





        <div class="row">
            <div class="col-xs-8">
                <table id="tabela-ambiente" class="table table-striped table-bordered table-hover">
                    <caption><strong>Servidor e Ambiente</strong></caption>
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Host</td>
                            <td><? echo $_SERVER['HTTP_HOST']; ?></td>
                        </tr>
                        <tr>
                            <td>SO do Servidor</td>
                            <td><? echo PHP_OS; ?></td>
                        </tr>
                        <tr>
                            <td>Versão Apache/PHP</td>
                            <td><? echo $_SERVER['SERVER_SOFTWARE']; ?></td>
                        </tr>
                        <tr>
                            <td>Navegador</td>
                            <td><? echo $_SERVER['HTTP_USER_AGENT']; ?></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>





		<!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->








<!-- page specific plugin scripts -->
<script type="text/javascript">

	var scripts = [null]
	$('[data-ajax-content=true]').ace_ajax('loadScripts', scripts, function() {


	  //inline scripts related to this page
		 jQuery(function($) {



	})
	});
</script>