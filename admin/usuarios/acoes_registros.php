<?php
/*###################################################################
|																	|
|	MÓDULO: usuários												|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 07/12/2015												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if($_REQUEST['operacao']=="selecionar")
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." AS tbl_user WHERE tbl_user.".$tabela_usuarios_prefixo_campo."id = ".$tabela_usuarios.".".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." AS tbl_user WHERE tbl_user.".$tabela_usuarios_prefixo_campo."id = ".$tabela_usuarios.".".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//MONTA O ARRAY ORDENADO COM AS FILIAIS DO USUÁRIO
					$separa_filiais = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."filiais"]);
					foreach($separa_filiais as $filial_nome) {
						if($filial_nome<>"") {
							$num_filial++;
							$rows[$num][$sistema_prefixo_campos."filiais[]"][$num_filial] = $filial_nome;
						}
					}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];

					$rows[$num][$sistema_prefixo_campos."login_antigo"] = $ver_selecionar[$sistema_prefixo_campos."login"];
					$rows[$num][$sistema_prefixo_campos."perfil_antigo"] = $ver_selecionar[$sistema_prefixo_campos."perfil"];
				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{
			//print_r($_POST);

			/*#### CRIPTOGRAFA A SENHA ####*/
			require($pasta_lib."/crypt/class_crypt.php");
			$crypt_class = new CRYPT_CLASS;
			$crypt_class->set_cipher('twofish');
			$crypt_class->set_mode('cfb');
			$crypt_class->set_key($key_senha_login);
			$_POST['usuario_senha'] = $crypt_class->encrypt($_POST['nova_senha']);
			/*#### CRIPTOGRAFA A SENHA ####*/



			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['usuario_filiais'] as $secao_key => $secao_marcada)
				{
					$usuario_filiais .= $separador_string.$secao_marcada;
				}
			$usuario_filiais .= $separador_string."geral".$separador_string;
			$_POST['usuario_filiais'] = $usuario_filiais;
			$_POST["usuario_status"] = $_POST["usuario_status_hidden"];
			$usuario_perfil_antigo = $_POST['usuario_perfil_antigo'];
			unset($_POST["usuario_status_hidden"], $_POST['usuario_login_antigo'], $_POST['usuario_perfil_antigo']);



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			//echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_registro = mysql_insert_id($con);

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir)
				{
					$result = "OK";


					/*#### INSERE AS PERMISSÕES COM BASE NO PERFIL ####*/
					$sql_insere_permissoes = "INSERT INTO
														usuarios_permissoes
															(usuario_permissao_usuario,
																usuario_permissao_secao,
																usuario_permissao_operacao,
																usuario_permissao_filial,
																usuario_permissao_valor)
															(SELECT
																'".$id_registro."' AS user_id,
																vis_perf_perm_secao,
																vis_perf_perm_operacao,
																vis_perf_perm_filial,
																vis_perf_perm_valor
															FROM
																visao_perfis_permissoes
															WHERE
																vis_perf_perm_perfil = ".$_POST['usuario_perfil'].")";
					// echo "<br>sql_insere_permissoes: ".$sql_insere_permissoes;
					$exe_insere_permissoes = mysql_query($sql_insere_permissoes, $con) or die("Erro do MySQL[exe_insere_permissoes]: ".mysql_error());
					/*#### INSERE AS PERMISSÕES COM BASE NO PERFIL ####*/


				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/












	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);

			/*#### VERIFICA SE EXISTE OUTRO USUÁRIO JÁ CADASTRADO QUANDO MUDAR O LOGIN ####*/
			if($_POST["usuario_login"]<>$_POST["usuario_login_antigo"])
				{
					$sql_usuario_existente = "SELECT ".$tabela_usuarios_prefixo_campo."id FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."excluido <> '".$_POST["usuario_login"]."' AND ".$tabela_usuarios_prefixo_campo."login = 'sim'";
					//echo "<br>sql_usuario_existente: ".$sql_usuario_existente;
					$exe_usuario_existente = mysql_query($sql_usuario_existente, $con) or die("Erro do MySQL[exe_usuario_existente]: ".mysql_error());
					if(mysql_num_rows($exe_usuario_existente)>0)
						{
							echo "O usuário digitado já existe, tente outra opção.";
							exit();
						}
				}
			unset($_POST["usuario_login_antigo"]);
			/*#### VERIFICA SE EXISTE OUTRO USUÁRIO JÁ CADASTRADO QUANDO MUDAR O LOGIN ####*/


			/*#### CRIPTOGRAFA A NOVA SENHA, SE FOI DIGITIADO ####*/
			if(($_POST['nova_senha']<>"")&&($_POST['nova_senha']==$_POST['repete_nova_senha']))
				{
					require($pasta_lib."/crypt/class_crypt.php");
					$crypt_class = new CRYPT_CLASS;
					$crypt_class->set_cipher('twofish');
					$crypt_class->set_mode('cfb');
					$crypt_class->set_key($key_senha_login);
					$_POST['usuario_senha'] = $crypt_class->encrypt($_POST['nova_senha']);
				}
			/*#### CRIPTOGRAFA A NOVA SENHA, SE FOI DIGITIADO ####*/



			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['usuario_filiais'] as $secao_key => $secao_marcada)
				{
					$usuario_filiais .= $separador_string.$secao_marcada;
				}
			$usuario_filiais .= $separador_string."geral".$separador_string;
			$_POST['usuario_filiais'] = $usuario_filiais;
			$_POST["usuario_status"] = $_POST["usuario_status_hidden"];
			$usuario_perfil_antigo = $_POST['usuario_perfil_antigo'];
			unset($_POST["usuario_status_hidden"],$_POST['usuario_perfil_antigo']);



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/








			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar)
				{

					/*#### SE HOUVE ALTERAÇÃO DE PERFIL, APAGA AS PERMISSÕES ATUAIS E ATUALIZA COM O NOVO PERFIL ####*/
					if($_REQUEST['usuario_perfil']<>$usuario_perfil_antigo)
						{
							$sql_apaga_permissao = "DELETE FROM usuarios_permissoes WHERE usuario_permissao_usuario = ".$_POST[$sistema_chave_primaria];
							// echo "<br>sql_apaga_permissao: ".$sql_apaga_permissao;
							$exe_apaga_permissao = mysql_query($sql_apaga_permissao, $con) or die("Erro do MySQL[exe_apaga_permissao]: ".mysql_error());

							$sql_insere_permissoes = "INSERT INTO
																usuarios_permissoes
																	(usuario_permissao_usuario,
																		usuario_permissao_secao,
																		usuario_permissao_operacao,
																		usuario_permissao_filial,
																		usuario_permissao_valor)
																	(SELECT
																		'".$_POST[$sistema_chave_primaria]."' AS user_id,
																		vis_perf_perm_secao,
																		vis_perf_perm_operacao,
																		vis_perf_perm_filial,
																		vis_perf_perm_valor
																	FROM
																		visao_perfis_permissoes
																	WHERE
																		vis_perf_perm_perfil = ".$_REQUEST['usuario_perfil'].")";
							// echo "<br>sql_insere_permissoes: ".$sql_insere_permissoes;
							$exe_insere_permissoes = mysql_query($sql_insere_permissoes, $con) or die("Erro do MySQL[exe_insere_permissoes]: ".mysql_error());
						}
					/*#### SE HOUVE ALTERAÇÃO DE PERFIL, APAGA AS PERMISSÕES ATUAIS E ATUALIZA COM O NOVO PERFIL ####*/





					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/











	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR PERMISSÕES DO USUÁRIO ####*/
	if($_REQUEST['operacao']=="selecionar_permissoes")
		{
			$sistema_prefixo_campos = "user_perm_";
			$sistema_nome_da_tabela = "visao_usuarios_permissoes";
			$sistema_chave_primaria = "user_perm_usuario_id";


			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					$num = 1;

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];

					$rows[$num]["user_perm_usuario_id"] = $ver_selecionar["user_perm_usuario_id"];
					$rows[$num]["user_perm_nome_login"] = $ver_selecionar["user_perm_nome_login"];

					//ARMAZENA OS CAMPOS DE PERMISSÕES DE SISTEMAS E OPERAÇÕES
					$rows[$num]["permissao_".$ver_selecionar["user_perm_secao"]."_".$ver_selecionar["user_perm_operacao"]] = $ver_selecionar["user_perm_valor"];
					if($ver_selecionar["user_perm_filial"]<>"") $rows[$num]["perm_fi_".$ver_selecionar["user_perm_filial"]."_".$ver_selecionar["user_perm_secao"]."_".$ver_selecionar["user_perm_valor"]] = "sim";

					//ARMAZENA AS PERMISSÕES ESPECIAIS
					$rows[$num]["ignorar_perm_doc"] = (($ver_selecionar["user_perm_secao"]=="especial")&&($ver_selecionar["user_perm_operacao"]=="ignorar_perm_doc")) ? $ver_selecionar["user_perm_valor"] : "";
				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR PERMISSÕES DO USUÁRIO ####*/














	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR AS PERMISSÕES DO USUÁRIO ####*/
	if($_REQUEST['operacao']=="alterar_permissoes")
		{
			//print_r($_POST);

			$sistema_prefixo_campos = "usuario_permissao_";
			$sistema_nome_da_tabela = "usuarios_permissoes";


			/*#### APAGA TODAS AS PERMISSÕES ATUAIS DO USUÁRIO ####*/
			$sql_excluir_permissoes = "DELETE FROM ".$sistema_nome_da_tabela." WHERE usuario_permissao_usuario = '".$_POST["user_perm_usuario_id"]."'";
			//echo "<br>sql_excluir_permissoes: ".$sql_excluir_permissoes;
			$exe_excluir_permissoes = mysql_query($sql_excluir_permissoes, $con) or die("Erro do MySQL[exe_excluir_permissoes]: ".mysql_error());
			/*#### APAGA TODAS AS PERMISSÕES ATUAIS DO USUÁRIO ####*/









			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI E EXECUTA A QUERY PARA INSERIR CADA PERMISSÃO ####*/
			if(count($_POST["permissao"])>0)
				{
					/*### INSERE AS PERMISSÕES GERAIS QUE NÃO ESTÃO RELACIOADAS A FILIAIS ###*/
					foreach($_POST["permissao"] as $nome_sistema => $array_permissoes_sistema)
						{
							//echo "<br>".$nome_sistema;
							foreach($array_permissoes_sistema as $nome_operacao => $valor_permissao)
								{
									//echo "<br>".$nome_operacao." => ".$valor_permissao;
									$sql_permissao[$nome_sistema][$nome_operacao] = "INSERT INTO ".$sistema_nome_da_tabela." (usuario_permissao_usuario,
																																		usuario_permissao_secao,
																																		usuario_permissao_operacao,
																																		usuario_permissao_valor)
																																	VALUES
																																		('".$_POST["user_perm_usuario_id"]."',
																																		'".$nome_sistema."',
																																		'".$nome_operacao."',
																																		'".$valor_permissao."')";
									//echo "<br>sql_permissao: ".$sql_permissao[$nome_sistema][$nome_operacao];
									$exe_permissao[$nome_sistema][$nome_operacao] = mysql_query($sql_permissao[$nome_sistema][$nome_operacao], $con) or die("Erro do MySQL[exe_permissao[$nome_sistema][$nome_operacao]]: ".mysql_error());
								}
						}
					/*### INSERE AS PERMISSÕES GERAIS QUE NÃO ESTÃO RELACIOADAS A FILIAIS ###*/



					/*### INSERE AS PERMISSÕES QUE ESTÃO RELACIOADAS A FILIAIS ###*/
					if(count($_POST["permissao_filial"])>0)
						{
							foreach($_POST["permissao_filial"] as $filial => $array_secoes_usuario)
								{
									//echo "<br>1".$filial." => ";
									//print_r($array_secoes_usuario);
									foreach($array_secoes_usuario as $secao_usuario => $array_secao_valor)
										{
											//echo "<br>2".$secao_usuario." => ";
											//print_r($array_secao_valor);
											foreach($array_secao_valor as $secao_permitida => $sim)
												{
													//echo "<br>3".$secao_permitida." => ".$sim;
													$sql_permissao_filial[$filial][$secao_usuario][$secao_permitida] = "INSERT INTO ".$sistema_nome_da_tabela." (usuario_permissao_usuario,
																																						usuario_permissao_filial,
																																						usuario_permissao_operacao,
																																						usuario_permissao_secao,
																																						usuario_permissao_valor)
																																					VALUES
																																						('".$_POST["user_perm_usuario_id"]."',
																																						'".$filial."',
																																						'secoes',
																																						'".$secao_usuario."',
																																						'".$secao_permitida."')";
													//echo "<br>sql_permissao_filial: ".$sql_permissao_filial[$filial][$secao_usuario][$secao_permitida];
													$exe_permissao_filial[$filial][$secao_usuario][$secao_permitida] = mysql_query($sql_permissao_filial[$filial][$secao_usuario][$secao_permitida], $con) or die("Erro do MySQL[exe_permissao_filial[$filial][$secao_usuario][$secao_permitida]]: ".mysql_error());
												}
										}
								}
						}
					/*### INSERE AS PERMISSÕES QUE ESTÃO RELACIOADAS A FILIAIS ###*/




					/*### INSERE AS PERMISSÕES ESPECIAIS ###*/
					if($_POST["ignorar_perm_doc"]<>"")
						{
							//echo "<br>".$nome_operacao." => ".$valor_permissao;
							$sql_permissao_especial = "INSERT INTO ".$sistema_nome_da_tabela." (usuario_permissao_usuario,
																								usuario_permissao_secao,
																								usuario_permissao_operacao,
																								usuario_permissao_valor)
																							VALUES
																								('".$_POST["user_perm_usuario_id"]."',
																								'especial',
																								'ignorar_perm_doc',
																								'".$_POST['ignorar_perm_doc']."')";
							//echo "<br>sql_permissao_especial: ".$sql_permissao_especial;
							$exe_permissao_especial = mysql_query($sql_permissao_especial, $con) or die("Erro do MySQL[exe_permissao_especial]: ".mysql_error());
						}
					/*### INSERE AS PERMISSÕES ESPECIAIS ###*/



				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI E EXECUTA A QUERY PARA INSERIR CADA PERMISSÃO ####*/








			/*#### CONSTRÓI A QUERY DE ATUALIZAÇÃO DO REGISTRO ####*/
			$sql_alterar = "UPDATE ".$tabela_usuarios." SET ".$tabela_usuarios_prefixo_campo."atualizacao_data = '".$hoje_data_us."', ".
																	$tabela_usuarios_prefixo_campo."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$tabela_usuarios_prefixo_campo."id = '".$_POST["user_perm_usuario_id"]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY DE ATUALIZAÇÃO DO REGISTRO ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR AS PERMISSÕES DO USUÁRIO ####*/















	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
