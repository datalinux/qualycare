<?
/*###################################################################
|																			|
|	Arquivo que executa as ações nos registros do perfil, chamado	|
|	pelo $.ajax															|
|																			|
|	Autor: Guilherme Moreira de Castro									|
|	E-mail: guicastro@gmail.com											|
|	Data: 06/08/2014														|
|																			|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");




	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			$sistema_chave_primaria = $tabela_usuarios_prefixo_campo."id";

			//print_r($_POST);

			/*#### VERIFICA SE EXISTE OUTRO USUÁRIO JÁ CADASTRADO QUANDO MUDAR O LOGIN ####*/
			if($_POST["usuario_login"]<>$_POST["usuario_login_antigo"])
				{
					$sql_usuario_existente = "SELECT ".$tabela_usuarios_prefixo_campo."id FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."excluido <> '".$_POST["usuario_login"]."' AND ".$tabela_usuarios_prefixo_campo."login = 'sim'";
					//echo "<br>sql_usuario_existente: ".$sql_usuario_existente;
					$exe_usuario_existente = mysql_query($sql_usuario_existente, $con) or die("Erro do MySQL[exe_usuario_existente]: ".mysql_error());
					if(mysql_num_rows($exe_usuario_existente)>0)
						{
							echo "O usuário digitado já existe, tente outra opção.";
							exit();
						}
				}
			unset($_POST["usuario_login_antigo"]);
			/*#### VERIFICA SE EXISTE OUTRO USUÁRIO JÁ CADASTRADO QUANDO MUDAR O LOGIN ####*/


			/*#### CRIPTOGRAFA A NOVA SENHA, SE FOI DIGITIADO ####*/
			if(($_POST['nova_senha']<>"")&&($_POST['nova_senha']==$_POST['repete_nova_senha']))
				{
					require($pasta_lib."/crypt/class_crypt.php");
					$crypt_class = new CRYPT_CLASS;
					$crypt_class->set_cipher('twofish');
					$crypt_class->set_mode('cfb');
					$crypt_class->set_key($key_senha_login);
					$_POST['usuario_senha'] = $crypt_class->encrypt($_POST['nova_senha']);
				}
			/*#### CRIPTOGRAFA A NOVA SENHA, SE FOI DIGITIADO ####*/



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $tabela_usuarios_prefixo_campo)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/





			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$tabela_usuarios." SET ".$sql_atualiza_campos.
																	$tabela_usuarios_prefixo_campo."atualizacao_data = '".$hoje_data_us."', ".
																	$tabela_usuarios_prefixo_campo."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/












	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
