<?
/*###################################################################
|																			|
|	Arquivo principal de telas e gerenciamento do módulo				|
|																			|
|	Autor: Guilherme Moreira de Castro									|
|	E-mail: guicastro@gmail.com											|
|	Data: 05/08/2014														|
|																			|
###################################################################*/



	//INCLUSÃO DOS ARQUIVOS DE CONFIGURAÇÃO SEM FUNÇÕES JS
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");
?>

<link rel="stylesheet" href="../lib/ace/assets/css/chosen.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/jquery.gritter.css" />
<link rel="stylesheet" href="../includes/estilos.admin.sistemas.css" />

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		Perfil
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Altere seus dados pessoais e senha
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">


	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS --><!-- /.row -->



		<div class="row">
			<div class="col-xs-12">


                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                            	<?
									$sql_usuario = "SELECT *,
																(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." AS tbl_user WHERE tbl_user.".$tabela_usuarios_prefixo_campo."id = ".$tabela_usuarios.".usuario_criacao_usuario) AS reg_criacao_usuario,
																(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." AS tbl_user WHERE tbl_user.".$tabela_usuarios_prefixo_campo."id = ".$tabela_usuarios.".usuario_atualizacao_usuario) AS reg_atualizacao_usuario
									 						FROM
																".$tabela_usuarios."
															WHERE
																".$GLOBALS['tabela_usuarios_prefixo_campo']."id = '".$_SESSION["login"]["id"]."'";
									//echo "<br>sql_usuario: ".$sql_usuario;
									$exe_usuario = mysql_query($sql_usuario, $con) or die("Erro do MySQL[exe_usuario]: ".mysql_error());
									$ver_usuario = mysql_fetch_array($exe_usuario);

									$historico_registro = "<strong>Criação:</strong> <br> ".mostra_data_completa_br($ver_usuario[$GLOBALS['tabela_usuarios_prefixo_campo']."criacao_data"])." - ".$ver_usuario["reg_criacao_usuario"];
									if(!is_null($ver_usuario["reg_atualizacao_usuario"])) $historico_registro .= "<br><br><strong>Última atualização:</strong> <br> ".mostra_data_completa_br($ver_usuario[$GLOBALS['tabela_usuarios_prefixo_campo']."atualizacao_data"])." - ".$ver_usuario["reg_atualizacao_usuario"];
								?>


                            <form class="form-horizontal" role="form" id="form-cadastros" name="form-cadastros">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">
                                          <div id="info_reg_user" class="form-group">
                                                    <div class="col-sm-3 no-padding-right" style="height: 22px; line-height:22px; text-align:right;">ID</div>

                                                    <div class="col-sm-9">
                                                        <span id="txt_id"><? echo str_pad($ver_usuario["usuario_id"],3,0,STR_PAD_LEFT); ?> </span> &nbsp;&nbsp;<i id="txt_id_reg" class="ace-icon fa fa-history bigger-120" data-rel="reg_user_popover" data-trigger="hover" data-placement="right" data-content="<? echo $historico_registro; ?>" title="Histórico do registro"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-3 control-label no-padding-right" for="usuario_nome">Nome</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario_nome" id="usuario_nome" class="col-xs-10 col-sm-9" value="<? echo $ver_usuario["usuario_nome"]; ?>" />
                                                    </div>
                                          		</div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="usuario_setor">Setor</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario_setor" id="usuario_setor" class="col-xs-10 col-sm-8" value="<? echo $ver_usuario["usuario_setor"]; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="usuario_cargo">Cargo</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario_cargo" id="usuario_cargo" class="col-xs-10 col-sm-7" value="<? echo $ver_usuario["usuario_cargo"]; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="usuario_telefone">Telefone</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario_telefone" id="usuario_telefone" class="col-xs-10 col-sm-6" value="<? echo $ver_usuario["usuario_telefone"]; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="usuario_email">E-mail</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario_email" id="usuario_email" class="col-xs-10 col-sm-9" value="<? echo $ver_usuario["usuario_email"]; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="usuario_login">Login</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario_login" id="usuario_login" class="col-xs-10 col-sm-5" value="<? echo $ver_usuario["usuario_login"]; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="nova_senha">Nova senha</label>

                                                    <div class="col-sm-9">
                                                        <input type="password" name="nova_senha" id="nova_senha" class="col-xs-10 col-sm-5" placeholder="Digite a nova senha" />
                                                        <input type="password" name="repete_nova_senha" id="repete_nova_senha" class="col-xs-10 col-sm-5" placeholder="Repita a senha" style="margin-top:5px; clear:left;" />
                                                    </div>
                                                </div>

                                                <input type="hidden" name="usuario_id" id="usuario_id" value="<? echo $ver_usuario["usuario_id"]; ?>" />
                                                <input type="hidden" name="usuario_login_antigo" id="usuario_login_antigo" value="<? echo $ver_usuario["usuario_login"]; ?>" />
                                                <input type="hidden" name="operacao" id="operacao" value="alterar" />
                                                <input type="hidden" name="filial" id="filial" value="<? echo $_REQUEST["filial"]; ?>" />




                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" id="btn-alterar" class="btn btn-sm btn-success" data-id="alterar">
                                        <i class="ace-icon fa fa-check"></i>
                                        Alterar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->


			</div>
		</div>






		<!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->








<!-- page specific plugin scripts -->
<script type="text/javascript">


	var scripts = [null]
	$('[data-ajax-content=true]').ace_ajax('loadScripts', scripts, function() {


	  //inline scripts related to this page
		 jQuery(function($) {

		<!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###-->
		<!--### MÉTODO ADICIONAL DE VALIDAÇÃO DOS CAMPOS DE SENHA ###-->
		jQuery.validator.addMethod("senha",function(value,element) {
				//console.log(value +" => "+ element);
				if(value == $('#repete_nova_senha').val()) return true;
				else return false;
			}, "As senhas não são iguais, digite novamente");
		<!--### MÉTODO ADICIONAL DE VALIDAÇÃO DOS CAMPOS DE SENHA ###-->

		$('#form-cadastros').validate({
			errorElement: 'div',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				<!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
				usuario_nome: {
					required: true
				},
				usuario_email: {
					required: true,
					email: true
				},
				usuario_login: {
					required: true,
					minlength: 8
				},
				nova_senha: {
					minlength: 6,
					senha: true
				}
				<!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
			},

			messages: {
				<!--### MENSAGENS DE VALIDAÇÃO ###-->
				usuario_nome: {
					required: "Digite o nome"
				},
				usuario_email: {
					required: "Digite o e-mail",
					email: "Digite um e-mail válido"
				},
				usuario_login: {
					required: "Digite o usuário",
					minlength: "O usuário deve ter no mínimo 8 caracteres"
				},
				nova_senha: {
					minlength: "A senha deve ter no mínimo 6 caracteres",
					senha: "As senhas não são iguais, digite novamente"
				}
				<!--### MENSAGENS DE VALIDAÇÃO ###-->
			},


			highlight: function (e) {
				$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
			},

			success: function (e) {
				$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
				$(e).remove();
			},

			errorPlacement: function (error, element) {
				if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
					var controls = element.closest('div[class*="col-"]');
					if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
					else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
				}
				else if(element.is('.select2')) {
					error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
				}
				else if(element.is('.chosen-select')) {
					error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
				}
				else error.insertAfter(element.parent());
			},

			submitHandler: function (form) {
				//console.log(btnSubmit);

				<!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->
				$.ajax({
					type: "POST",
					//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
					url: 'perfil/acoes_registros.php',
					//ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
					data: $("#form-cadastros").serialize(),
					success: function(data)
					{
						<!--### MOSTRA A DIV DE CARREGANDO ###-->
						var contentArea = $('body');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						<!--### MOSTRA A DIV DE CARREGANDO ###-->


						<!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
						if (data === 'OK') {

							if( $('#form-cadastros #operacao').val() == "alterar") var texto_OK = '<? echo $registro_alterado; ?>';

							$('#modal-form').modal("toggle");

							$.gritter.add({
								title: '<i class="ace-icon fa fa-check"></i> OK!',
								text: texto_OK,
								class_name: 'gritter-success gritter-center',
								fade_out_speed:500,
								time:2000,
								before_open: function(e){
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
								}
							});
						}
						<!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






						<!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
						else {
							$.gritter.add({
								title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
								text: data,
								class_name: 'gritter-error gritter-center',
								sticky: false,
								fade_out_speed:500,
								after_close: function(e) {
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
								}
							});
						}
						<!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->


					}
				});
				<!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->

			},
			invalidHandler: function (form) {
			}
		});
		<!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###-->





		//FUNÇÃO QUE ATIVA O PROPOVER DO HISTÓRICO DO REGISTRO
		$('[data-rel=reg_user_popover]').popover({container:'body', html:true});






	})
	});
</script>