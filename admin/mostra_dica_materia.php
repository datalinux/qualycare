<?
  header('Content-Type: text/html; charset=utf-8');
  $include_functions_js = "nao";
  include("../includes/configure.inc.php");
  $include_sistema = "nao";
  include($pasta_includes."/auth.inc.php");

  $sql_dica_materia = "SELECT * FROM visao_dicas WHERE vis_dica_id = ".anti_injection($_REQUEST['dica']);
  $exe_dica_materia = mysql_query($sql_dica_materia, $con) or die("Erro do MySQL[exe_dica_materia]: ".mysql_error());
  $ver_dica_materia = mysql_fetch_array($exe_dica_materia);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Visualizar Dica/Matéria</title>

    <!-- Bootstrap -->
    <link href="../lib/ace/assets/css/bootstrap.css" rel="stylesheet">

    <!-- text fonts -->
    <link rel="stylesheet" href="../lib/ace/assets/css/ace-fonts.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="../lib/ace/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div class="container-fluid">

    <div class="row">
      <div class="col-xs-12">
        <h3 class="header smaller lighter blue"><?php echo $ver_dica_materia["vis_dica_titulo"]; ?></h3>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <?php echo $ver_dica_materia["vis_dica_descricao"]; ?>
      </div>
    </div>

  </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../lib/ace/assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../lib/ace/assets/js/bootstrap.js"></script>
  </body>
</html>