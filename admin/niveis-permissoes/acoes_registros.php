<?php
/*###################################################################
|																	|
|	MÓDULO: usuários												|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 07/12/2015												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if($_REQUEST['operacao']=="selecionar")
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			// echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];
					$rows[$num]["sql_selecionar"] = $sql_selecionar;


				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			$_POST["nivel_status"] = $_POST["nivel_status_hidden"];
			unset($_POST["nivel_status_hidden"]);



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			// echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_perfil = mysql_insert_id($con);

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir)
				{
					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/












	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);


			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			$_POST["nivel_status"] = $_POST["nivel_status_hidden"];
			unset($_POST["nivel_status_hidden"]);



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/





			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar)
				{
					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'
																	OR ".$sistema_prefixo_campos."superior = '".$_REQUEST['chave_primaria']."'";
			// echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir)
				{
					//EXCLUI TODOS OS RELACIONAMENTOS NA TABELA DOCUMENTOS_NIVEIS DO NÍVEL EXCLUÍDO
					$sql_excluir_documentos_niveis = "DELETE FROM documentos_niveis WHERE doc_nivel_nivel = ".$_REQUEST['chave_primaria'];
					$exe_excluir_documentos_niveis = mysql_query($sql_excluir_documentos_niveis, $con) or die("Erro do MySQL[exe_excluir_documentos_niveis]: ".mysql_error());

					//EXCLUI TODOS OS RELACIONAMENTOS NA TABELA NIVEIS_GRUPOS DO NÍVEL EXCLUÍDO
					$sql_excluir_niveis_grupos = "DELETE FROM niveis_grupos WHERE nivel_grupo_nivel = ".$_REQUEST['chave_primaria'];
					$exe_excluir_niveis_grupos = mysql_query($sql_excluir_niveis_grupos, $con) or die("Erro do MySQL[exe_excluir_niveis_grupos]: ".mysql_error());

					//EXCLUI TODOS OS RELACIONAMENTOS NA TABELA USUARIOS_NIVEIS DO NÍVEL EXCLUÍDO
					$sql_excluir_usuarios_niveis = "DELETE FROM usuarios_niveis WHERE usuario_nivel_nivel = ".$_REQUEST['chave_primaria'];
					$exe_excluir_usuarios_niveis = mysql_query($sql_excluir_usuarios_niveis, $con) or die("Erro do MySQL[exe_excluir_usuarios_niveis]: ".mysql_error());

					//EXCLUI TODOS OS RELACIONAMENTOS NA TABELA LINKS_NIVEIS DO NÍVEL EXCLUÍDO
					$sql_excluir_links_niveis = "DELETE FROM links_niveis WHERE link_nivel_nivel = ".$_REQUEST['chave_primaria'];
					$exe_excluir_links_niveis = mysql_query($sql_excluir_links_niveis, $con) or die("Erro do MySQL[exe_excluir_links_niveis]: ".mysql_error());

					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/












	/*### AÇÕES PARA RETORNAR OS PERFIS ATUAIS ####*/
	if($_REQUEST['operacao']=="perfis_atuais")
		{
			if($_REQUEST['nivel_atual']>0) $linha_nivel_atual = "AND vis_nivel_id <> ".$_REQUEST['nivel_atual'];

			$sql_niveis_atuais = "SELECT * FROM
												visao_niveis
											WHERE
												vis_nivel_id > 0
												".$linha_nivel_atual."
											ORDER BY
												vis_nivel_titulo";
			$exe_niveis_atuais = mysql_query($sql_niveis_atuais) or die("Erro do MySQL[exe_niveis_atuais]: ".mysql_error());
			while($ver_niveis_atuais = mysql_fetch_array($exe_niveis_atuais))
				{
					$num_niveis++;
					$niveis_atuais[$num_niveis]["id_nivel"] = $ver_niveis_atuais["vis_nivel_id"];
					$niveis_atuais[$num_niveis]["titulo"] = $ver_niveis_atuais["vis_nivel_titulo"];
					$niveis_atuais[$num_niveis]["nivel_superior"] = $_REQUEST['nivel_superior'];
				}

			$result = json_encode($niveis_atuais);
		}
	/*### AÇÕES PARA RETORNAR OS PERFIS ATUAIS ####*/













	/*#### AÇÕES QUANDO A OPERAÇÃO FOR VERIFICAR SE O USUÁRIO TEM OUTRA ASSOCIAÇÃO ####*/
	if($_REQUEST['operacao']=="verificar_associacao")
		{
			// print_r($_REQUEST);

			$sql_associcao = "SELECT * FROM visao_usuarios_niveis WHERE vis_user_nv_usuario_id = ".$_REQUEST['usuario'];

			$exe_associcao = mysql_query($sql_associcao, $con) or die("Erro do MySQL[exe_associcao]: ".mysql_error());
			while($ver_associacao = mysql_fetch_array($exe_associcao))
				{
					$rows["nivel_titulo"] = $ver_associacao["vis_user_nv_nivel_titulo"];
					$rows["nivel_id"] = $ver_associacao["vis_user_nv_nivel_id"];
					$rows["usuario_id"] = $ver_associacao["vis_user_nv_usuario_id"];
				}

			$rows["usuario"] = $_REQUEST['usuario'];
			$rows["nivel"] = $_REQUEST['nivel'];
			$rows["selecionado"] = $_REQUEST['selecionado'];

			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR VERIFICAR SE O USUÁRIO TEM OUTRA ASSOCIAÇÃO ####*/















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM NÍVEL A UM USUÁRIO ####*/
	if($_REQUEST['operacao']=="associar_usuario")
		{
			// print_r($_REQUEST);

			$sql_remover = "DELETE FROM usuarios_niveis WHERE usuario_nivel_usuario = ".$_REQUEST['usuario'];
			$exe_remover = mysql_query($sql_remover, $con) or die("Erro do MySQL[exe_remover]: ".mysql_error());

			if($_REQUEST['selecionado']=="true")
				{
					$sql_associar = "INSERT INTO usuarios_niveis (usuario_nivel_nivel, usuario_nivel_usuario) VALUES (".$_REQUEST['nivel'].", ".$_REQUEST['usuario'].")";
					$exe_associar = mysql_query($sql_associar, $con) or die("Erro do MySQL[exe_associar]: ".mysql_error());
				}

			$rows["resultado"] = "OK";
			$rows["usuario"] = $_REQUEST['usuario'];
			$rows["nivel"] = $_REQUEST['nivel'];
			$rows["selecionado"] = $_REQUEST['selecionado'];

			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ASSOCIAR UM NÍVEL A UM USUÁRIO ####*/

















	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
