        <?php
            //INLCUIR O ARQUIVO DE CONFIGURAÇÕES DO SISTEMA DE FOTOS
            include($GLOBALS['pasta_root']."/admin/banco-fotos/sistema.cfg.php");
            unset($num_coluna_grid);
        ?>
        /*###################################################################
        |                                                                   |
        |   A variável (objeto) oTable2 é a que recebe o GRID e todas as    |
        |   configurações. Sempre que necessário referenciar o GRID         |
        |   utilizar este nome deste objeto.                                |
        |                                                                   |
        ###################################################################*/
        var oTableFotos =
        $('#sample-table-fotos')
        .DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                //URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
                "url": "<? echo $http_includes; ?>/jquery.datatables.serverSide.php",
                "data": function ( d ) {
                    d.sistema = "<? echo $sistema; ?>";
                    d.filial = "<? echo $_REQUEST['filial']; ?>";
                }
            },
            bAutoWidth: false,
            "paging": true,
            "columns": [
                <?
                    //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                    foreach($array_colunas_grid as $key_coluna => $config_coluna)
                        {
                            $num_coluna_grid++;
                            echo '
                            { ';
                            echo '"name": "'.$key_coluna.'", ';
                            echo '"data": "'.$key_coluna.'", ';
                            echo '"title": "'.$config_coluna["title"].'", ';
                            echo '"width": "'.$config_coluna["width"].'", ';
                            echo '"visible": '.$config_coluna["visible"].', ';
                            echo '"searchable": '.$config_coluna["searchable"].', ';
                            if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
                            echo '"orderable": '.$config_coluna["orderable"];
                            echo ' }';
                            if($num_coluna_grid<$total_colunas_grid) echo ',';
                        }
                ?>,
                {
                    "name": "acoes",
                    "width": "10%",
                    "visible": true,
                    "searchable": false,
                    "orderable": false,
                    "data": null,

                    /*###################################################################
                    |                                                                   |
                    |   defaultContent é a variável que contém o conteúdo da última     |
                    |   coluna do GRID onde existem os botões de controle das ações     |
                    |   para cada registro. Há dois grupos de botões, um para mobile e  |
                    |   outro para telas normais. Observar os elementns                 |
                    |   <i class='ace-icon fa fa-pencil' que contém os botões.          |
                    |                                                                   |
                    |   Propriedades                                                    |
                    |   data-id: é a operação do botão (alterar, excluir, etc)          |
                    |   title: texto da descrição do botão                              |
                    |   data-toggle: padrão modal para chamada do form                  |
                    |   data-target: ID da janela modal que será aberta                 |
                    |                                                                   |
                    ###################################################################*/
                    "defaultContent": "<div class='hidden-sm hidden-xs action-buttons'><a class='green'><i class='ace-icon fa fa-plus-square bigger-130' title='Escolher foto' data-id='escolher'></i></a></div><div class='hidden-md hidden-lg'><div class='inline position-relative'><button class='btn btn-minier btn-yellow dropdown-toggle' data-toggle='dropdown' data-position='auto'><i class='ace-icon fa fa-caret-down icon-only bigger-120'></i></button><ul class='dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'><li><a class='tooltip-success' data-rel='tooltip'><span class='green'><i class='ace-icon fa fa-plus-square bigger-120' title='Escolher foto' data-id='escolher'></i></span></a></li></ul></div></div>"
                }
            ],
            "order": [
                <?
                    //FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
                    foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
                        {
                            $num_ordenacao_grid++;
                            echo '
                            [ ';
                            echo $array_colunas_key_grid[$coluna_nome].', ';
                            echo '"'.$coluna_ordenacao.'" ';
                            echo ' ]';
                            if($num_ordenacao_grid<$total_ordenacao_grid) echo ',';
                        }
                ?>
            ],
            //TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "A busca não retortnou resultados",
                "info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
                "infoEmpty": "0 registros",
                "infoFiltered": "de um total de _MAX_ cadastrados",
                "loadingRecords": "Carregando...",
                "sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
                "search":         "Buscar:",
                "paginate": {
                        "first":      "Primeira",
                        "last":       "Última",
                        "next":       "Próxima",
                        "previous":   "Anterior"
                }
            },
        });

        //HACK PARA QUANDO PASSAR O MOUSE SOBRE OS BOTÕES O CURSOR FICAR TIPO pointer
        $('#sample-table-fotos tbody').on( 'mouseover', 'tr td:last-child i', function () { $(this).css("cursor","pointer") } );


        $('#sample-table-fotos tbody').on( 'click', 'tr td:last-child i', function () {

                //REFERÊNCIA PARA INDICAR AS POSIÇÕES PADRÃO DO ID DO REGISTRO NO GRID
                //eq(5) class: hidden-md hidden-lg => eq(7) id: 37 = REDUZIDO => (hidden-md eq(5): 0 / hidden-sm eq(1): -1)
                //eq(1) class: hidden-sm hidden-xs action-buttons => eq(3) id: 27 = GRANDE (hidden-md eq(5): -1 / hidden-sm eq(1): 0)


                //CONDIÇÃO PARA DEFINIR A VARIÁVEL id_reg QUE ARMAZENA O ID DO REGISTRO NO GRID
                if($(this).parents().eq(5).attr("class").indexOf('hidden-md')==0) var id_reg = $(this).parents().eq(7).attr("id");
                else if($(this).parents().eq(1).attr("class").indexOf('hidden-sm')==0) var id_reg = $(this).parents().eq(3).attr("id");

                //ARMAZENA A EXTENSAO DA FOTO PARA MONTAR O CAMINHO
                if($(this).parents().eq(5).attr("class").indexOf('hidden-md')==0) var extensao_foto = oTableFotos.row($(this).parents().eq(7)).data()['extensao'];
                else if($(this).parents().eq(1).attr("class").indexOf('hidden-sm')==0) var extensao_foto = oTableFotos.row($(this).parents().eq(3)).data()['extensao'];

                //ARMAZENA O NOME DA FOTO PARA MOSTRAR DEPOIS DA ESCOLHA
                if($(this).parents().eq(5).attr("class").indexOf('hidden-md')==0) var nome_foto = oTableFotos.row($(this).parents().eq(7)).data()['titulo'];
                else if($(this).parents().eq(1).attr("class").indexOf('hidden-sm')==0) var nome_foto = oTableFotos.row($(this).parents().eq(3)).data()['titulo'];


                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ESCOLHER NO GRID ###-->
                if($(this).attr('data-id')=="escolher")
                    {
                        //ADICIONA CLASSE 3 COLUNAS, ATRIBUI VALORES DO ID, NOME E SRC DA FOTO ESCOLHIDA
                        $("#form-cadastros #thumb_da_foto").addClass('col-sm-3');
                        $("#form-cadastros .id_da_foto").val(id_reg);
                        $("#form-cadastros #titulo_da_foto").html(nome_foto);
                        $("#form-cadastros #thumb_da_foto").attr('src', '<?php echo $http_fotos."/G"; ?>'+id_reg+'.'+extensao_foto);

                        //MOSTRA BOTÃO DE REMOVER
                        $("#form-cadastros #btn-remover-foto").show();

                        //ESCONDE O MODAL DE ESCOLHA DE FOTOS
                        $("#modal-form-fotos").modal("hide");

                        //MARCA O CHECKBOX DE MOSTRA A FOTO NA NOTÍCIA
                        $('#form-cadastros #noticia_mostra_foto_1').prop('checked',true);

                        //MOSTRA O MODAL DO CADASTRO INICIAL
                        $("#modal-form").modal("show");
                        $('body').addClass('modal-open');
                    }
                <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ESCOLHER NO GRID ###-->
            });


        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE REMOVER FOTO ###-->
        $("#btn-remover-foto").on('click', function() {

                $("#form-cadastros #thumb_da_foto").attr('src','<?php echo $http_imagens; ?>/spacer.gif');
                $("#form-cadastros #btn-remover-foto").hide();
                $("#form-cadastros #thumb_da_foto").removeClass('col-sm-3');
                $("#form-cadastros .id_da_foto").val(0);
                $("#form-cadastros #titulo_da_foto").html('');
                $("#form-cadastros #btn-escolher-foto").html('<i class="ace-icon fa fa-photo bigger-120 green"></i> Escolher a foto');

                //DESMARCA O CHECKBOX DE MOSTRA A FOTO NA NOTÍCIA
                $('#form-cadastros #noticia_mostra_foto_1').prop('checked',false);
            });
        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE REMOVER FOTO ###-->




        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ESCOLHER FOTO ###-->
        $("#btn-escolher-foto").on('click', function() {

                $("#modal-form").modal("hide");
                $("#modal-form-fotos").modal("show");
                $('body').addClass('modal-open');
                $("#form-cadastros #btn-escolher-foto").html('<i class="ace-icon fa fa-photo bigger-120 green"></i> Trocar a foto');
            });
        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ESCOLHER FOTO ###-->




        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE FECHAR MODAL DE FOTOS OU NOVA FOTO ###-->
        $("#btn-fechar-modal-form-fotos, #btn-fechar-modal-form-nova-foto, #btn-cancelar-modal-form-nova-foto").on('click', function() {

                $("#modal-form-fotos").modal("hide");
                $("#modal-form-nova-foto").modal("hide");
                $("#modal-form").modal("show");
                $('body').addClass('modal-open');

            });
        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE FECHAR MODAL DE FOTOS ###-->



        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE ENVIAR NOVA FOTO ###-->
        $("#btn-abrir-modal-form-nova-foto").on('click', function() {
            //verifica se o usuário tem permissão para a operação
            if("<? echo $auth["banco-fotos"]["inserir"]; ?>"=="sim")
                {

                    /*### VARIÁVEIS DE CONFIGURAÇÃO DO UPLOAD DO ARQUIVO DE FOTOS ###*/
                    var $FotosNovasPermitidas = ['jpg', 'png', 'jpeg'];
                    var $FotosNovasExtraData = function() {
                             var ExtraData = {};
                             ExtraData['foto_pid'] = $("#form-nova-foto #foto_pid").val();
                             ExtraData['foto_id'] = $("#form-nova-foto #foto_id").val();
                             ExtraData['seq'] = $("#form-nova-foto #arquivo_enviado").val();
                             ExtraData['operacao_upload'] = $("#form-nova-foto #operacao_upload").val();
                             return ExtraData;
                         };
                    /*### VARIÁVEIS DE CONFIGURAÇÃO DO UPLOAD DO ARQUIVO DE FOTOS ###*/

                    //CHAMA A FUNÇÃO DE UPLOAD DE ARQUIVOS
                    BootstrapFileInput('form-nova-foto', 'files-fotos', 'banco-fotos', 1, $FotosNovasPermitidas, '<?php echo $limite_upload_php; ?>', $FotosNovasExtraData);

                    //ESCONDE O FORM DE BUSCA DE FOTOS
                    $("#modal-form-fotos").modal("hide");

                    //ESCONDE O MODAL DO CADASTRO PRINCIPAL
                    $("#modal-form").modal("hide");

                    <!--### ZERAR O FORMULÁRIO ###-->
                    $('#form-nova-foto #arquivo_enviado').val(0);
                    $('#form-nova-foto').trigger("reset");
                    $('#form-nova-foto .chosen-select').trigger("chosen:updated");
                    $('#form-nova-foto #foto_pid').val(((new Date().getTime()).toString(16)) + (Math.random().toString(36).substr(2)));
                    $('#form-nova-foto .ace-file-container, #form-nova-foto .ace-file-input').remove();
                    <!--### ZERAR O FORMULÁRIO ###-->

                    <!--### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###-->
                    $('#form-nova-foto #operacao_upload').val('inserir');
                    $('#form-nova-foto #files-fotos').fileinput('refresh');
                    <!--### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###-->

                    //MOSTRA O MODAL DE ENVIAR NOVA FOTO
                    $('#modal-form-nova-foto').modal({keyboard: false, "show": true});
                }
            else
                {
                    <!--### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###-->
                    $.gritter.add({
                        title: '<i class="ace-icon fa fa-times"></i> Erro!',
                        text: '<? echo $sistema_nao_autorizado; ?>',
                        class_name: 'gritter-error gritter-center',
                        sticky: false,
                        fade_out_speed:500,
                        before_open: function(e){
                            $("#modal-form-fotos").modal("hide");
                            $("#modal-form-nova-foto").modal("hide");
                            $("#modal-form").modal("show");
                            $('body').addClass('modal-open');
                        }
                    });
                    <!--### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###-->
                }
         })
        <!--### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR NOVA FOTO ###-->



        <!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE ENVIO DE NOVA FOTO ###-->
        $('#form-nova-foto').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                <!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
                'foto_secao[]': {
                    required: true
                },
                foto_titulo: {
                    required: true
                },
                arquivo_enviado: {
                    required: true,
                    number: true,
                    min: 1,
                    max: 1
                }
                <!--### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###-->
            },

            messages: {
                <!--### MENSAGENS DE VALIDAÇÃO ###-->
                'foto_secao[]': {
                    required: "Escolha pelo menos uma seção"
                },
                foto_titulo: {
                    required: "Digite o título da foto"
                },
                arquivo_enviado: {
                    required: "Escolha o arquivo para envio",
                    number: "Escolha o arquivo para envio",
                    min: "Escolha o arquivo para envio",
                    max: "Só é permitido enviar 1 arquivo"
                }
                <!--### MENSAGENS DE VALIDAÇÃO ###-->
            },


            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
                //console.log(btnSubmit);

                <!--### MOSTRA A DIV DE CARREGANDO ###-->
                var contentArea = $('body');
                contentArea.css('opacity', 0.25)

                var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                var offset = contentArea.offset();
                loader.css({top: offset.top, left: offset.left})
                <!--### MOSTRA A DIV DE CARREGANDO ###-->

                <!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->
                var formData = new FormData($("#form-nova-foto")[0]);
                $.ajax({
                    type: "POST",
                    //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                    url: '<? echo $sistema; ?>/acoes_registros.php',
                    //ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
                    data: formData,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    success: function(data, textStatus, jqXHR)
                    {
                        //REALIZA O PARSE DO data
                        result = JSON.parse(data);

                        <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->
                        if (result[1]["id_da_foto"] > 0) {


                            //ADICIONA CLASSE 3 COLUNAS, ATRIBUI VALORES DO ID, NOME E SRC DA FOTO ESCOLHIDA
                            $("#form-cadastros #thumb_da_foto").addClass('col-sm-3');
                            $("#form-cadastros .id_da_foto").val(result[1]["id_da_foto"]);
                            $("#form-cadastros #titulo_da_foto").html(result[1]["titulo_da_foto"]);
                            $("#form-cadastros #thumb_da_foto").attr('src', '<?php echo $http_fotos."/G"; ?>'+result[1]["id_da_foto"]+'.'+result[1]["extensao"]);

                            //MOSTRA BOTÃO DE REMOVER
                            $("#form-cadastros #btn-remover-foto").show();

                            //ESCONDE O MODAL DE ESCOLHA DE FOTOS E ENVIO DE FOTOS
                            $('#modal-form-nova-foto').modal("hide");
                            $("#modal-form-fotos").modal("hide");

                            //MARCA O CHECKBOX DE MOSTRA A FOTO NA NOTÍCIA
                            $('#form-cadastros #noticia_mostra_foto_1').prop('checked',true);

                            //MOSTRA O MODAL DO CADASTRO PRINICIPAL
                            $("#modal-form").modal("show");
                            $('body').addClass('modal-open');


                            $.gritter.add({
                                title: '<i class="ace-icon fa fa-check"></i> OK!',
                                text: 'A foto foi inserida e atribuída ao cadastro. Se desejar, utilize a seção Banco de Fotos para fazer o recorte das miniaturas.',
                                class_name: 'gritter-success gritter-center',
                                fade_out_speed:500,
                                time:2000,
                                before_open: function(e){
                                    contentArea.css('opacity', 1)
                                    contentArea.prevAll('.ajax-loading-overlay').remove();
                                    oTableFotos.ajax.reload(null,false);
                                    $("#modal-form").modal("show");
                                }
                            });
                        }
                        <!--### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###-->






                        <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->
                        else {
                            $.gritter.add({
                                title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                text: data,
                                class_name: 'gritter-error gritter-center',
                                sticky: false,
                                fade_out_speed:500,
                                after_close: function(e) {
                                    contentArea.css('opacity', 1)
                                    contentArea.prevAll('.ajax-loading-overlay').remove();
                                    $("#modal-form").modal("show");
                                }
                            });
                        }
                        <!--### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###-->


                    }
                });
                <!--### AJAX PARA POSTAGEM DO FORMULÁRIO ###-->

            },
            invalidHandler: function (form) {
            }
        });
        <!--### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###-->