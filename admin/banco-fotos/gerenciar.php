<?php
/*###################################################################
|                                                                   |
|   MÓDULO: banco de fotos                                          |
|   DESCRIÇÃO: Arquivo principal de telas e gerenciamento do        |
|   módulo                                                          |
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 07/12/2015                                                |
|                                                                   |
###################################################################*/


	//INCLUSÃO DOS ARQUIVOS DE CONFIGURAÇÃO SEM FUNÇÕES JS
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//DEFINIÇÃO DO NOME DA OPERAÇÃO PRINCIPAL DE VISUALIZAÇÃO DE REGISTROS
	$operacao = "gerenciar";

	//INCLUSÃO DO ARQUIVO QUE CARREGA AS PERMISSÕES DO USUÁRIO
	include($pasta_includes."/auth.inc.php");
	//echo "<br>autorizado: ".$autorizado;
?>


<link rel="stylesheet" href="../lib/ace/assets/css/chosen.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/jquery.gritter.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/datepicker.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/bootstrap-fileinput/fileinput.css" />
<link rel="stylesheet" href="../lib/cropit/dist/cropit.css" />
<link rel="stylesheet" href="../includes/estilos.admin.sistemas.css" />


<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		<? echo $sistema_titulo; ?>
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			<? echo $sistema_descricao; ?>
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">


	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS --><!-- /.row -->

















        <!--### FILTROS DO GRID PRINCIPAL ###-->
        <div class="row">
            <div class="col-xs-12">
                <h3 class="header smaller lighter blue">Filtros</h3>
            </div>
            <div class="col-xs-4">
                <label for="filtro_secao">Filtrar por Seção</label><br />
                <select class="chosen-select" name="filtro_secao" id="filtro_secao" data-id="8" data-placeholder="Escolha a seção...">
                    <option value="">Todas as seções</option>
                    <?
                        //ORDENA O ARRAY DE SEÇÕES POR ORDEM ALFABETICA DE TÍTULO
                        @ksort($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"]);

                        foreach($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"] as $secao_value => $secao_title)
                            {
                                if(in_array($secao_value, $auth_filial[$_REQUEST['filial']][$sistema."-secao"]))
                                    {
                    ?>
                                        <option value="<?php echo $separador_string.$secao_value.$separador_string; ?>"><?php echo $secao_title; ?></option>
                    <?
                                    }
                            } 
                    ?>
                </select>
            </div>
        </div>
        <!--### FILTROS DO GRID PRINCIPAL ###-->










        <!--### GRID PRINCIPAL ###-->
		<div class="row">
			<div class="col-xs-12">
                <h3 class="header smaller lighter blue">
                    <button class="btn btn-white btn-default btn-round" title="Inserir <? echo $sistema_titulo_item; ?>" data-id="btn-form-inserir">
                        <i class="ace-icon fa fa-plus-square bigger-120 green"></i>
                        Inserir <? echo $sistema_titulo_item; ?>
                    </button>
                    <button class="btn btn-white btn-default btn-round" title="Atualizar" id="btn-atualizar-grid">
                        <i class="ace-icon fa fa-refresh bigger-120 blue"></i>
                        Atualizar
                    </button>
                </h3>


				<div class="table-header">
					Lista de <? echo strtolower($sistema_titulo); ?> existentes
				</div>

				<!-- <div class="table-responsive"> -->

				<!-- <div class="dataTables_borderWrap"> -->
				<div>
					<table id="sample-table-2" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
                            	<?
									//FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
									foreach($array_colunas_grid as $key_coluna => $config_coluna)
										{
											echo "<th";
											if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
											echo ">";
											echo $config_coluna["title"];
											echo "</th>";
										}
								?>
                                <th></th>
							</tr>
						</thead>


					</table>
				</div>
			</div>
		</div>
        <!--### GRID PRINCIPAL ###-->












		<div class="row">
			<div class="col-xs-12">




                <!--### MODAL DO FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                <div id="modal-form" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="form-titulo-acao" class="blue bigger"></h4>
                            </div>

                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                            <form class="form-horizontal" role="form" id="form-cadastros" name="form-cadastros">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">
                                                <div id="info_reg_user" class="form-group">
                                                    <div class="col-sm-3 no-padding-right" style="height: 22px; line-height:22px; text-align:right;">ID</div>

                                                    <div class="col-sm-9">
                                                        <span id="txt_id"></span> &nbsp;&nbsp;<i id="txt_id_reg" class="ace-icon fa fa-history bigger-120" data-rel="reg_user_popover" data-trigger="hover" data-placement="right" data-content="" title="Histórico do registro"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_secao">Seções</label>




													<div class="col-sm-9">
 														<?php

                                                            //ORDENA O ARRAY DE SEÇÕES POR ORDEM ALFABETICA DE TÍTULO
                                                            @ksort($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"]);

                                                            foreach($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"] as $secao_value => $secao_title)
                                                                {
                                                                	$num_secao++;
                                                        ?>
                                                                    <label class="col-md-3 col-sm-6 col-xs-12" <?php if(!in_array($secao_value, $auth_filial[$_REQUEST['filial']][$sistema."-secao"])) echo "style='display: none;'"; ?>>
                                                                        <input name="foto_secao[]" type="checkbox" id="foto_secao_<?php echo $num_secao; ?>" value="<?php echo $secao_value; ?>" class="ace" />
                                                                        <span class="lbl"> <?php echo $secao_title; ?></span>
                                                                    </label>
                                                        <?php
                                                                }
                                                        ?>
													</div>



                                                </div>

                                                <div class="form-group">
                                                	<label class="col-sm-3 control-label no-padding-right" for="foto_titulo">Título</label>

                                                  <div class="col-sm-9">
                                                        <input type="text" name="foto_titulo" id="foto_titulo" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_legenda">Legenda</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="foto_legenda" id="foto_legenda" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_autor">Autor</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="foto_autor" id="foto_autor" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_participantes">Participantes</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="foto_participantes" id="foto_participantes" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                	<label class="col-sm-3 control-label no-padding-right" for="files-fotos">Enviar imagem</label>

                                                  <div class="col-sm-9">
                                                    <input id="files-fotos" name="files-fotos[]" multiple type="file" lang="NaoAtribuir" />
                                                    <div id="imagens_atuais"></div>
    	                                            <input type="hidden" name="arquivo_enviado" id="arquivo_enviado" value="0" lang="NaoAtribuir" />
                                                  </div>
                                                </div>

                                                <input type="hidden" name="foto_id" id="foto_id" />
                                                <input type="hidden" name="foto_pid" id="foto_pid" />
                                                <input type="hidden" name="operacao" id="operacao" />
                                                <input type="hidden" name="operacao_upload" id="operacao_upload" />
                                                <input type="hidden" name="filial" id="filial" />

                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Cancelar
                                    </button>

                                    <button type="submit" id="btn-inserir" class="btn btn-sm btn-primary" data-id="inserir">
                                        <i class="ace-icon fa fa-check"></i>
                                        Inserir
                                    </button>

                                    <button type="submit" id="btn-alterar" class="btn btn-sm btn-success" data-id="alterar">
                                        <i class="ace-icon fa fa-check"></i>
                                        Alterar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->

                        </div>
                    </div>
                </div>
                <!--### MODAL DO FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->




























               <!--### MODAL DE RECORTE DE FOTOS ###-->
               <div id="modal-form-recorte" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="blue bigger">Recortar <? echo $sistema_titulo_item; ?></h4>
                            </div>







                            <!--### FORMULÁRIO RECORTE DE FOTOS ###-->
                            <form class="form-horizontal" role="form" id="form-recorte" name="form-recorte">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">

                                                <div class="form-group">
                                                	<label class="col-sm-3 control-label no-padding-right" for="foto_titulo">Título</label>

                                                  <div class="col-sm-9">
                                                        <span id="foto_titulo" class="col-xs-10 col-sm-10" style="padding-top:8px;" /></span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_autor">Autor</label>

                                                  <div class="col-sm-9">
                                                        <span id="foto_autor" class="col-xs-10 col-sm-10" style="padding-top:8px;" /></span>
                                                    </div>
                                                </div>

                                                <div id="area_tamanho_recorte" class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="tamanho_recorte">Tamanho do recorte</label>

                                                    <div class="col-sm-9">
                                                        <select class="chosen-select" name="tamanho_recorte" id="tamanho_recorte" data-placeholder="Escolha o tamanho para o recorte">
                                                            <option value=""></option>
                                                            <?php
                                                                foreach($_SESSION["guarda_config"]["valor"]["todas"]["miniaturas_prefixo"] as $miniatura_prefixo => $miniatura_nome)
                                                                    {
                                                                        if(($miniatura_prefixo<>"A")&&($miniatura_prefixo<>"G"))
                                                                            {
                                                            ?>
                                                                                <option value="<?php echo $miniatura_prefixo; ?>"><?php echo $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos"][$miniatura_nome]; ?></option>
                                                            <?php
                                                                            }
                                                                    } 
                                                            ?>
                                                        </select>
                                                    <span id="erro_tamanho_recorte" class="help-block">Escolha o tamanho para recortar</span>
                                                    </div>
                                                </div>

                                                <div><p>&nbsp;</p></div>

                                                <div id="area_recorte_foto">
                                                    <div class="image-editor">
                                                        <div class="cropit-image-preview-container">
                                                        <div class="cropit-image-preview"></div>
                                                    </div>

                                                    <div class="image-size-label">Zoom da imagem</div>
                                                        <input type="range" class="cropit-image-zoom-input">
                                                    </div>
                                                </div>


                                                <input type="hidden" name="foto_recortada" id="foto_recortada" value="" />
                                                <input type="hidden" name="foto_id" id="foto_id" value="" />
                                                <input type="hidden" name="foto_extensao" id="foto_extensao" value="" />
                                                <input type="hidden" name="operacao" id="operacao" value="recortar" />
                                                <input type="hidden" name="filial" id="filial" value="<?php echo $_REQUEST['filial']; ?>" />

                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Cancelar
                                    </button>

                                    <button type="button" id="btn-recortar" class="btn btn-sm btn-success" data-id="recortar">
                                        <i class="ace-icon fa fa-cut"></i>
                                        Recortar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO RECORTE DE FOTOS ###-->








                        </div>
                    </div>
               </div>
               <!--### MODAL DE RECORTE DE FOTOS ###-->




            </div>
		</div>








		<!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->








<!-- page specific plugin scripts -->
<script type="text/javascript">



    var scripts = [null]
	$('[data-ajax-content=true]').ace_ajax('loadScripts', scripts, function() {


	  //inline scripts related to this page
		 jQuery(function($) {


		  moment.locale('pt_BR');

	<?
		//SE NÃO FOR AUTORIZADO O ACESSO AO ARQUIVO INFORMA MENSAGEM DE ERRO USANDO GRITTER
		if($autorizado=="nao")
			{
	?>
				$(".col-xs-12").hide();
				$.gritter.add({
					title: '<i class="ace-icon fa fa-times"></i> Erro!',
					text: '<? echo $sistema_nao_autorizado; ?>',
					class_name: 'gritter-error gritter-center',
					sticky: false,
					fade_out_speed:500
				});
	<?
			}
	?>


		/*###################################################################
		|																			|
		|	A variável (objeto) oTable1 é a que recebe o GRID e todas as 	|
		|	configurações. Sempre que necessário referenciar o GRID 			|
		|	utilizar este nome deste objeto.										|
		|																			|
		###################################################################*/
		var oTable1 =
		$('#sample-table-2')
		//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
		.DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": {
				//URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
				"url": "<? echo $http_includes; ?>/jquery.datatables.serverSide.php",
				"data": function ( d ) {
					d.sistema = "<? echo $sistema; ?>";
					d.filial = "<? echo $_REQUEST['filial']; ?>";
				}
			},
			//"ajax": "configuracoes/serverSide.php",
			//stateSave: true,
			bAutoWidth: false,
			"paging": true,
			"columns": [
				<?
					//FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
					foreach($array_colunas_grid as $key_coluna => $config_coluna)
						{
							$num_coluna_grid++;
							echo '
							{ ';
							echo '"name": "'.$key_coluna.'", ';
							echo '"data": "'.$key_coluna.'", ';
							echo '"title": "'.$config_coluna["title"].'", ';
							echo '"width": "'.$config_coluna["width"].'", ';
							echo '"visible": '.$config_coluna["visible"].', ';
							echo '"searchable": '.$config_coluna["searchable"].', ';
							if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
							echo '"orderable": '.$config_coluna["orderable"];
							echo ' }';
							if($num_coluna_grid<$total_colunas_grid) echo ',';
						}
				?>,
				{
                	"name": "acoes",
                	"width": "10%",
                	"visible": true,
                	"searchable": false,
                	"orderable": false,
					"data": null,

					/*###################################################################
					|																			|
					|	defaultContent é a variável que contém o conteúdo da última 		|
					|	coluna do GRID onde existem os botões de controle das ações 		|
					|	para cada registro. Há dois grupos de botões, um para mobile e 	|
					|	outro para telas normais. Observar os elementns 					|
					|	<i class='ace-icon fa fa-pencil que contém os botões. 			|
					|																			|
					|	Propriedades															|
					|	data-id: é a operação do botão (alterar, excluir, etc) 			|
					|	title: texto da descrição do botão									|
					|	data-toggle: padrão modal para chamada do form						|
					|	data-target: ID da janela modal que será aberta					|
					|																			|
					###################################################################*/
					"defaultContent": "<div class='hidden-sm hidden-xs action-buttons'>\
											<a class='blue'><i class='ace-icon fa fa-cut bigger-130' title='Recortar <? echo $sistema_titulo_item; ?>' data-id='recortar'></i></a>\
											<a class='green'><i class='ace-icon fa fa-edit bigger-130' title='Alterar <? echo $sistema_titulo_item; ?>' data-id='alterar'></i></a>\
											<a class='red'><i class='ace-icon fa fa-trash-o bigger-130' title='Excluir <? echo $sistema_titulo_item; ?>' data-id='excluir'></i></a>\
										</div>\
										<div class='hidden-md hidden-lg'>\
											<div class='inline position-relative'>\
												<button class='btn btn-minier btn-yellow dropdown-toggle' data-toggle='dropdown' data-position='auto'><i class='ace-icon fa fa-caret-down icon-only bigger-120'></i></button>\
												<ul class='dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>\
													<li><a class='tooltip-success' data-rel='tooltip'><span class='blue'><i class='ace-icon fa fa-cut bigger-120' title='Recortar <? echo $sistema_titulo_item; ?>' data-id='recortar'></i></span></a></li>\
													<li><a class='tooltip-success' data-rel='tooltip'><span class='green'><i class='ace-icon fa fa-pencil-square-o bigger-120' title='Alterar <? echo $sistema_titulo_item; ?>' data-id='alterar'></i></span></a></li>\
													<li><a class='tooltip-error' data-rel='tooltip'><span class='red'><i class='ace-icon fa fa-trash-o bigger-120' title='Excluir <? echo $sistema_titulo_item; ?>' data-id='excluir'></i></span></a></li>\
												</ul>\
											</div>\
										</div>"
				}
			],
			"order": [
				<?
					//FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
					foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
						{
							$num_ordenacao_grid++;
							echo '
							[ ';
							echo $array_colunas_key_grid[$coluna_nome].', ';
							echo '"'.$coluna_ordenacao.'" ';
							echo ' ]';
							if($num_ordenacao_grid<$total_ordenacao_grid) echo ',';
						}
				?>
			],
			//TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"zeroRecords": "A busca não retortnou resultados",
				"info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
				"infoEmpty": "0 registros",
				"infoFiltered": "de um total de _MAX_ cadastrados",
				"loadingRecords": "Carregando...",
				"sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
				"search":         "Buscar:",
				"paginate": {
						"first":      "Primeira",
						"last":       "Última",
						"next":       "Próxima",
						"previous":   "Anterior"
				}
			},
	    });



		/*### CONTROLES DO FILTROS EXTERNOS NO GRID ###*/
		$('#filtro_secao').on( 'change', function () {
			oTable1
				.columns( $(this).attr('data-id') )
				.search( this.value )
				.draw();
		} );
		/*### CONTROLES DO FILTROS EXTERNOS NO GRID ###*/




		/*### BOTÃO DE ATUALIZAR GRID ###*/
		$('#btn-atualizar-grid').on('click', function () {
			oTable1.ajax.reload(null,false);
		});
		/*### BOTÃO DE ATUALIZAR GRID ###*/

															


		/*### CONTROLES DO INPUT CHOSEN ###*/
		$('.chosen-select').chosen({allow_single_deselect:true, search_contains:true});
		//resize the chosen on window resize

		//FUNÇÃO QUE CORRIGE BUG NO VALIDATE() PARA VALIDAR CAMPOS HIDDEN DO CHOSEN
		jQuery.validator.setDefaults({
		  ignore: []
		});

		$(window)
		.off('resize.chosen')
		.on('resize.chosen', function() {
			$('.chosen-select').each(function() {
				 var $this = $(this);
				 $this.next().css({'width': $this.parent().width()});
			})
		}).trigger('resize.chosen');
		/*### CONTROLES DO INPUT CHOSEN ###*/



		//HACK PARA QUANDO PASSAR O MOUSE SOBRE OS BOTÕES O CURSOR FICAR TIPO pointer
		$('#sample-table-2 tbody').on( 'mouseover', 'tr td:last-child i', function () { $(this).css("cursor","pointer") } );



		/*###################################################################
		|																			|
		|	A função abaixo é aque controla todas as ações dos botões de 	|
		|	ação de cada registro da tabela. Sempre que for necessário 		|
		|	adicionar novos botões será nesta função que as as operações 	|
		|	serão definidas, incluindo operações de selecionar registro, 	|
		|	comandos de alterar, comandos de excluir e demais ações do 		|
		|	módulo.																	|
		|																			|
		###################################################################*/
		$('#sample-table-2 tbody').on( 'click', 'tr td:last-child i', function () {

				//REFERÊNCIA PARA INDICAR AS POSIÇÕES PADRÃO DO ID DO REGISTRO NO GRID
				//eq(5) class: hidden-md hidden-lg => eq(7) id: 37 = REDUZIDO => (hidden-md eq(5): 0 / hidden-sm eq(1): -1)
				//eq(1) class: hidden-sm hidden-xs action-buttons => eq(3) id: 27 = GRANDE (hidden-md eq(5): -1 / hidden-sm eq(1): 0)


				//$('.senhamask').on( 'click', function () { alert("over"); } );
				//$('.senhamask').on('click', function () { alert("over"); } );




				//CONDIÇÃO PARA DEFINIR A VARIÁVEL id_reg QUE ARMAZENA O ID DO REGISTRO NO GRID
				if($(this).parents().eq(5).attr("class").indexOf('hidden-md')==0) var id_reg = $(this).parents().eq(7).attr("id");
				else if($(this).parents().eq(1).attr("class").indexOf('hidden-sm')==0) var id_reg = $(this).parents().eq(3).attr("id");





				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE RECORTAR NO GRID ###*/
				if(($(this).attr('data-id')=="recortar")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
					{
                        //ESCONDE A ÁREA DE RECORTE DE FOTOS
                        $("#area_recorte_foto").hide();

                        //ESCONDE O ERRO DE NÃO ESCOLHER TAMANHO DO RECORTE
                        $("#area_tamanho_recorte").removeClass('has-error');
                        $("#erro_tamanho_recorte").hide();

                        //REMOVE A OPÇÃO ESCOLHIDA DO TAMANHO DO RECORTE
                        $("#tamanho_recorte").val('').trigger('chosen:updated');

						/*### MOSTRA A DIV DE CARREGANDO ###*/
						var contentArea = $('body');
						contentArea	.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						/*### MOSTRA A DIV DE CARREGANDO ###*/



						/*### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###*/
						$.ajax({
							dataType: "json",
							//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
							url: '<? echo $sistema; ?>/acoes_registros.php',
							data: {
								//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
								chave_primaria: id_reg,
								operacao: "selecionar",
								filial: "<? echo $_REQUEST['filial']; ?>"
							},
							success: function(data, textStatus, jqXHR)
							{
								/*### ESCONDE A DIV DE CARREGANDO ###*/
								contentArea.css('opacity', 1);
								contentArea.prevAll('.ajax-loading-overlay').remove();
								/*### ESCONDE A DIV DE CARREGANDO ###*/

                                //CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
                                $('#modal-form-recorte').modal("show");

                                //CARREGA OS DADOS DE ID, TÍTULO E AUTOR DA FOTO
                                $("#form-recorte #foto_id").val(data[1]['foto_id']);
                                $("#form-recorte #foto_extensao").val(data[1]['foto_extensao']);
                                $("#form-recorte #foto_titulo").html(data[1]['foto_titulo']);
                                $("#form-recorte #foto_autor").html(data[1]['foto_autor']);


                                /*### FUNÇÃO QUE MODIFICA A LARGURA E ALTURA DO RECORTE DA IMAGEM CONFORME ESCOLHIDO ###*/
                                $('#tamanho_recorte').on( 'change', function () {

                                    //CRIA O ARRAY COM AS LARGURAS E ALTURAS DAS MINIATURAS, CONFORME CONFIGURAÇÕES
                                    var largura_fotos = [];
                                    var altura_fotos = [];
                                    <?php
                                        foreach($_SESSION["guarda_config"]["valor"]["todas"]["miniaturas_prefixo"] as $minuatura_prefixo => $miniatura_nome)
                                            {
                                                echo "largura_fotos['".$minuatura_prefixo."'] = ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_largura"][$miniatura_nome]."; \n";
                                                echo "altura_fotos['".$minuatura_prefixo."'] = ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_altura"][$miniatura_nome]."; \n";
                                            }
                                    ?>

                                    //SE ESCOLHEU UM TAMANHO, DEFINE O TAMANHO DA ÁREA DO CROPIT E MOSTRA A ÁREA DE RECORTE
                                    if($('#tamanho_recorte').val()!="") {
                                        //DEFINE A LARGURA E ALTURA INICIAL DO CROPIT COMO 50% DO TAMANHO DA IMAGEM GRANDE
                                        $('.image-editor').cropit('previewSize', { width: largura_fotos[$('#tamanho_recorte').val()], height: altura_fotos[$('#tamanho_recorte').val()] });
                                        $("#area_recorte_foto").slideDown();

                                        //AJUSTA A ÁREA DE IMAGEM PARA O CENTRO DO MODAL
                                        $('.cropit-image-preview-container').css('left','50%');
                                        $('.cropit-image-preview-container').css('margin-left','-'+$('.cropit-image-background-container').width()/2+'px');

                                        //DEFINE O SRC ESCOLHIDO DO CROPIT
                                        $('.image-editor').cropit('imageSrc', data[1]['src_foto_grande']);
                                    }
                                } );
                                /*### FUNÇÃO QUE MODIFICA A LARGURA E ALTURA DO RECORTE DA IMAGEM CONFORME ESCOLHIDO ###*/


                                $('.image-editor').cropit({
                                    imageBackground: true,
                                    imageBackgroundBorderWidth: 20,
                                    initialZoom: 'image',
                                });

                                //DEFINE O SRC INICIAL DO CROPIT
                                $('.image-editor').cropit('imageSrc', data[1]['src_foto_grande']);

                                //DEFINE A LARGURA E ALTURA INICIAL DO CROPIT COMO 50% DO TAMANHO DA IMAGEM GRANDE
                                $('.image-editor').cropit('previewSize', { width: Math.round(data[1]['largura_foto_grande']*0.5), height: Math.round(data[1]['altura_foto_grande']*0.5) });

                                //AJUSTA A ÁREA INICIAL DE IMAGEM PARA O CENTRO DO MODAL
                                $('.cropit-image-preview-container').css('left','50%');
                                $('.cropit-image-preview-container').css('margin-left','-'+$('.cropit-image-background-container').width()/2+'px');


                                /*### AÇÃO QUANDO PRESSIONA O BOTÃO RECORTAR ###*/
                                $('#btn-recortar').on('click', function() {
                                    var imageData = $('.image-editor').cropit('export');
                                    $("#foto_recortada").val(imageData);

                                    /*### VERIFICA SE FOI ESCOLHIDO O TAMANHO DO RECORTE ###*/
                                    if($("#tamanho_recorte").val()!="")
                                        {
                                            /*### MOSTRA A DIV DE CARREGANDO ###*/
                                            var contentArea = $('body');
                                            contentArea .css('opacity', 0.25)

                                            var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
                                            var offset = contentArea.offset();
                                            loader.css({top: offset.top, left: offset.left})
                                            /*### MOSTRA A DIV DE CARREGANDO ###*/

                                            //REMOVE O ERRO POR NÃO TES ESCOLHIDO O TAMANHO DO RECORTE
                                            $("#area_tamanho_recorte").removeClass('has-error');
                                            $("#erro_tamanho_recorte").hide();


                                            /*### AJAX POSTAGEM DO RECORTE DA FOTO ###*/
                                            var formData = new FormData($("#form-recorte")[0]);
                                            $.ajax({
                                                type: "POST",
                                                //URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
                                                url: '<? echo $sistema; ?>/acoes_registros.php',
                                                //ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
                                                data: formData,
                                                contentType: false,
                                                enctype: 'multipart/form-data',
                                                processData: false,
                                                success: function(data)
                                                {
                                                    if (data === 'OK') {
                                                        //MENSAGEM DE OK QUANDO O RECORTE FOI EFETUADO
                                                        $.gritter.add({
                                                            title: '<i class="ace-icon fa fa-check"></i> OK!',
                                                            text: 'A foto foi recortada com sucesso!',
                                                            class_name: 'gritter-success gritter-center',
                                                            fade_out_speed:500,
                                                            time:2000,
                                                            before_open: function(e){
                                                                contentArea.css('opacity', 1);
                                                                contentArea.prevAll('.ajax-loading-overlay').remove();

                                                                //FECHA O MODAL DE RECORTE
                                                                $('#modal-form-recorte').modal("hide");

                                                                //REMOVE A OPÇÃO ESCOLHIDA DO TAMANHO DO RECORTE
                                                                $("#tamanho_recorte").val('').trigger('chosen:updated');
                                                            }
                                                        });


                                                    }
                                                    else {
                                                        //MENSAGEM DE ERRO QUANDO O RECORTE NÃO DER CERTO
                                                        //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                                                        $.gritter.add({
                                                            title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                                            text: data,
                                                            class_name: 'gritter-error gritter-center',
                                                            sticky: false,
                                                            fade_out_speed:500,
                                                            after_close: function(e) {
                                                                contentArea.css('opacity', 1)
                                                                contentArea.prevAll('.ajax-loading-overlay').remove();
                                                            }
                                                        });
                                                    }


                                                },
                                                error: function(jqXHR, textStatus, errorThrown) {
                                                    //MENSAGEM DE ERRO QUANDO O RECORTE NÃO DER CERTO
                                                    //console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
                                                    $.gritter.add({
                                                        title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
                                                        text: errorThrown,
                                                        class_name: 'gritter-error gritter-center',
                                                        sticky: false,
                                                        fade_out_speed:500,
                                                        after_close: function(e) {
                                                            contentArea.css('opacity', 1)
                                                            contentArea.prevAll('.ajax-loading-overlay').remove();
                                                        }
                                                    });
                                                }
                                            });
                                            /*### AJAX POSTAGEM DO RECORTE DA FOTO ###*/
                                        }
                                    else
                                        {
                                            //DEMOSTRA O ERRO POR NÃO TES ESCOLHIDO O TAMANHO DO RECORTE
                                            $("#area_tamanho_recorte").addClass('has-error');
                                            $("#erro_tamanho_recorte").show();
                                        }
                                    /*### VERIFICA SE FOI ESCOLHIDO O TAMANHO DO RECORTE ###*/
                                });
                                /*### AÇÃO QUANDO PRESSIONA O BOTÃO RECORTAR ###*/



							},
							error: function(jqXHR, textStatus, errorThrown) {
								//MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
								//console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
								$.gritter.add({
									title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
									text: errorThrown,
									class_name: 'gritter-error gritter-center',
									sticky: false,
									fade_out_speed:500,
									after_close: function(e) {
										contentArea.css('opacity', 1)
										contentArea.prevAll('.ajax-loading-overlay').remove();
									}
								});
							}
						});
						/*### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###*/
					}
				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE RECORTAR NO GRID ###*/
















				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ALTERAR NO GRID ###*/
				else if(($(this).attr('data-id')=="alterar")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
					{
						//alert("alterar "+id_reg);

						//TROCA O TÍTULO DO FORMULÁRIO PARA A VARIÁVEL title QUE ESTÁ NO BOTÃO
						$('#form-titulo-acao').html( $(this).attr('title') );

						//ESCONDE O BOTÃO DE INSERIR E MOSTRA O DE ALTERAR
						$('#btn-inserir').hide();
						$('#btn-alterar').show();

						/*### MOSTRA A DIV DE CARREGANDO ###*/
						var contentArea = $('body');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						/*### MOSTRA A DIV DE CARREGANDO ###*/




						/*### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###*/
						$.ajax({
							dataType: "json",
							//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
							url: '<? echo $sistema; ?>/acoes_registros.php',
							data: {
								//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
								chave_primaria: id_reg,
								operacao: "selecionar",
								filial: "<? echo $_REQUEST['filial']; ?>"
							},
							success: function(data, textStatus, jqXHR)
							{
								//console.log("data: "+data+"\ntextStatus: "+textStatus);

								/*### EACH PARA PERCORRER TODO O FORMULÁRIO DE CADASTRO E PREENCHER COM OS DADOS DO BANCO ###*/
								$.each($('#form-cadastros :input'), function(index, element) {
									/*console.log("type: "+index+" => "+element.type);
									console.log("name: "+index+" => "+element.name);
									console.log("id: "+index+" => "+element.id);
									console.log("value: "+index+" => "+element.value);
									console.log("lang: "+index+" => "+element.lang);
									console.log("data[id]: "+data[1][element.id]);
									console.log("data[name]: "+data[1][element.name]);
									console.log("this.val(): "+$(this).val());
									console.log("--------\n");*/

									if(element.type=="radio")
										{
											if($(this).val() == data[1][element.name])
												{
													$(this).prop('checked',true);
													$(':input[name='+element.name+'_hidden]').val(data[1][element.name]);
												}
											else $(this).prop('checked',false);
										}
									/*###################################################################
									|																	|
									|	O padrão de nomenclatura dos campos checkbox sempre será o 		|
									|	name='campo[]' e o id='campo_X' onde X é um numeral incremental	|
									|	e o JSON deve retornar um array com a chave sendo o nome, em  	|
									| 	formato vetor, do campo do checkbox (ex: foto_secao[])			|
									|																	|
									###################################################################*/
									if(element.type=="checkbox")
                                        {
                                            /*console.log("\n-------");
                                            console.log("element.name: "+element.name);
                                            console.log("element.id: "+element.id);
                                            console.log("this.val: "+$(this).val());
                                            console.log("this.id: "+$(this).attr('id'));*/
                                            $(this).prop('checked', false);
                                            if(data[1][element.name] != null)
                                                {
                                                    $.each(data[1][element.name], function(key, value) {
                                                        //console.log("value do bd: "+value);
                                                        if($("#"+element.id).val() == value)
                                                            {
                                                                //console.log("#"+element.id+" é true");
                                                                $("#"+element.id).prop('checked', true);
                                                            }
                                                    });
                                                }
                                        }
									/*###################################################################
									|																			|
									|	Para contornar o bug de serialize() do jquery com campo tipo 		|
									|	radio é necessário incluir no form um campo tipo hidden com o 	|
									|	memso nome do radio acrescido de _hidden e colocar como 			|
									|	lang="NaoAtribuir" para não processar no $.each					|
									|																			|
									###################################################################*/
									else if((element.lang!="NaoAtribuir")&&(element.type!="radio")&&(element.type!="checkbox"))
										{
											$(this).val(data[1][element.id]).trigger('chosen:updated');
										}
								});
								/*### EACH PARA PERCORRER TODO O FORMULÁRIO DE CADASTRO E PREENCHER COM OS DADOS DO BANCO ###*/



								/*### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###*/
								$('#txt_id').html( data[1]["<? echo $sistema_prefixo_campos; ?>id_reg_user"] );
								var texto_id_reg = "<strong>Criação:</strong> <br> "+data[1]["<? echo $sistema_prefixo_campos; ?>criacao_data"]+" - "+data[1]["reg_criacao_usuario"];
								if(data[1]["reg_atualizacao_usuario"] != null) texto_id_reg += "<br><br><strong>Última atualização:</strong> <br> "+data[1]["<? echo $sistema_prefixo_campos; ?>atualizacao_data"]+" - "+data[1]["reg_atualizacao_usuario"];
								$('#txt_id_reg').attr("data-content", texto_id_reg);
								$('#info_reg_user').show();
								/*### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###*/

								/*### PREENCHE AS INFORMAÇÕES DA ÁREA DE UPLOAD DE ARQUIVOS ###*/
								$('#imagens_atuais').html(data[1]["imagens_atuais"]);
								$('#form-cadastros #operacao_upload').val('alterar');
                                $('#form-cadastros #arquivo_enviado').val(0);
								$('#files-fotos').fileinput('refresh');
								$('label[for=files-fotos]').text('Enviar nova imagem');
								/*### PREENCHE AS INFORMAÇÕES DA ÁREA DE UPLOAD DE ARQUIVOS ###*/

								/*### ESCONDE A DIV DE CARREGANDO ###*/
								contentArea.css('opacity', 1);
								contentArea.prevAll('.ajax-loading-overlay').remove();
								/*### ESCONDE A DIV DE CARREGANDO ###*/

								//CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
								$('#modal-form').modal("show");

							},
							error: function(jqXHR, textStatus, errorThrown) {
								//MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
								//console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
								$.gritter.add({
									title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
									text: errorThrown,
									class_name: 'gritter-error gritter-center',
									sticky: false,
									fade_out_speed:500,
									after_close: function(e) {
										contentArea.css('opacity', 1)
										contentArea.prevAll('.ajax-loading-overlay').remove();
									}
								});
							}
						});
						/*### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###*/
					}
				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ALTERAR NO GRID ###*/

























				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE EXCLUIR NO GRID ###*/
				else if(($(this).attr('data-id')=="excluir")&&("<? echo $auth[$sistema]["excluir"]; ?>"=="sim"))
					{
						//alert("excluir "+id_reg);

						/*### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A EXCLUSÃO ###*/
						bootbox.dialog({
							message: "<span class='bigger-110'>O registro será excluído. Confirma?</span>",
							buttons:
							{
								"danger" :
								{
									"label" : "Excluir!",
									"className" : "btn-sm btn-danger",
									"callback": function() {

										/*### MOSTRA A DIV DE CARREGANDO ###*/
										var contentArea = $('body');
										contentArea
										.css('opacity', 0.25)

										var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
										var offset = contentArea.offset();
										loader.css({top: offset.top, left: offset.left})
										/*### MOSTRA A DIV DE CARREGANDO ###*/




										/*### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###*/
										$.ajax({
											type: "POST",
											//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
											url: '<? echo $sistema; ?>/acoes_registros.php',
											data: {
												//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
												chave_primaria: id_reg,
												operacao: "excluir",
												filial: "<? echo $_REQUEST['filial']; ?>"
											},
											success: function(data)
											{
												/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/
												if (data === 'OK') {
													//ABRE MENSAGEM DE OK
													$.gritter.add({
														title: '<i class="ace-icon fa fa-check"></i> OK!',
														text: '<? echo $registro_excluido; ?>',
														class_name: 'gritter-success gritter-center',
														fade_out_speed:500,
														time:2000,
														before_open: function(e){
															/*### ESCONDE A DIV DE CARREGANDO ###*/
															contentArea.css('opacity', 1)
															contentArea.prevAll('.ajax-loading-overlay').remove();
															/*### ESCONDE A DIV DE CARREGANDO ###*/

															//FADEOUT NA LINHA DO GRID QUE FOI EXCLUÍDO
															$('#sample-table-2 #'+id_reg).fadeOut();

															//ATUALIZA A TABELA DO GRID
															oTable1.ajax.reload(null,false);
														}
													});
												}
												/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/






												/*### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###*/
												else {
													$.gritter.add({
														title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
														//O TEXTO DA MENSAGEM DE ERRO SERÁ TUDO O QUE TIVER RETORNADO DE MENSAGEM NO ARQUIVO DE EXECUÇÃO
														text: data,
														class_name: 'gritter-error gritter-center',
														sticky: false,
														fade_out_speed:500,
														after_close: function(e) {
															/*### ESCONDE A DIV DE CARREGANDO ###*/
															contentArea.css('opacity', 1)
															contentArea.prevAll('.ajax-loading-overlay').remove();
															/*### ESCONDE A DIV DE CARREGANDO ###*/
														}
													});
												}
											}
										});
										/*### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###*/
									}
								},
								"button" :
								{
									"label" : "Cancelar",
									"className" : "btn-sm"
								}
							}
						});
					}
				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE EXCLUIR NO GRID ###*/














				/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM AUTORIZAÇÃO PARA EXECUTAR A OPERAÇÃO ###*/
				else
					{
						$.gritter.add({
							title: '<i class="ace-icon fa fa-times"></i> Erro!',
							text: '<? echo $sistema_nao_autorizado; ?>',
							class_name: 'gritter-error gritter-center',
							sticky: false,
							fade_out_speed:500,
							before_open: function(e){
								$('#modal-form').modal("hide");
							}
						});
					}
				/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM AUTORIZAÇÃO PARA EXECUTAR A OPERAÇÃO ###*/
			} );












		/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR ###*/
		$('[data-id="btn-form-inserir"]').on('click', function() {
			//verifica se o usuário tem permissão para a operação
			if("<? echo $auth[$sistema]["inserir"]; ?>"=="sim")
				{
					//TROCA O TÍTULO DO FORMULÁRIO PARA A VARIÁVEL title QUE ESTÁ NO BOTÃO
					$('#form-titulo-acao').html( $(this).attr('title') );

					//ESCONDE O BOTÃO DE ALTERAR E MOSTRA O DE INSERIR
					$('#btn-inserir').show();
					$('#btn-alterar').hide();

					//ESCONDE A DIV COM INFORMAÇÃO DE ID E HISTÓRICO DE ATUALIZAÇÕES DO REGISTRO
					$('#info_reg_user').hide();

					/*### ZERAR O FORMULÁRIO ###*/
					$('#form-cadastros #arquivo_enviado').val(0);
					$('#form-cadastros').trigger("reset");
					$('#form-cadastros .chosen-select').trigger("chosen:updated");
					$('#form-cadastros #foto_pid').val(((new Date().getTime()).toString(16)) + (Math.random().toString(36).substr(2)));
					$("#imagens_atuais").html('');
					$('.ace-file-container, .ace-file-input').remove();
					/*### ZERAR O FORMULÁRIO ###*/

					/*### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###*/
					$('#form-cadastros #operacao_upload').val('inserir');
					$('#files-fotos').fileinput('refresh');
					$('label[for=files-fotos]').text('Enviar imagem');
					/*### VARIÁVEIS DE USO NO UPLOAD DE ARQUIVOS ###*/

                    //ABRE O MODAL DE CADASTRO
                    $('#modal-form').modal("show");
				}
			else
				{
					/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###*/
					$.gritter.add({
						title: '<i class="ace-icon fa fa-times"></i> Erro!',
						text: '<? echo $sistema_nao_autorizado; ?>',
						class_name: 'gritter-error gritter-center',
						sticky: false,
						fade_out_speed:500,
						before_open: function(e){
							$('#modal-form').modal("hide");
						}
					});
					/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###*/
				}
		 })
		/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR ###*/












		/*### FUNÇÃO QUE ARMAZENA QUAL BOTÃO SUBMIT FOI PRESSIONADO NO FORM ###*/
		var btnSubmit;
		$('#form-cadastros :submit').on('click', function() {
			$('#form-cadastros #operacao').val( $(this).attr("data-id") );
			$('#form-cadastros #filial').val("<? echo $_REQUEST['filial']; ?>");
		});
		/*### FUNÇÃO QUE ARMAZENA QUAL BOTÃO SUBMIT FOI PRESSIONADO NO FORM ###*/



		/*### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###*/
		/*### MÉTODO ADICIONAL DE VALIDAÇÃO DE DATA EM FORMATO DD/MM/AAAA ###*/
		jQuery.validator.addMethod("dataBR",function(value,element) {
				return this.optional(element)||/^\d{1,2}[\/-]\d{1,2}[\/-]\d{4}$/.test(value);
			}, "Digite a data no formato DD/MM/AAAA");
		/*### MÉTODO ADICIONAL DE VALIDAÇÃO DE DATA EM FORMATO DD/MM/AAAA ###*/


		$('#form-cadastros').validate({
			errorElement: 'div',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				/*### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###*/
				'foto_secao[]': {
					required: true
				},
				foto_titulo: {
					required: true
				},
				arquivo_enviado: {
					required: function() {
							if($('#form-cadastros #operacao').val()=='inserir') return true;
							else return false;
						},
					number: true,
                    min: function() {
                            if($('#form-cadastros #operacao').val()=='inserir') return 1;
                            else return false;
                        },
					max: 1
				}
				/*### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###*/
			},

			messages: {
				/*### MENSAGENS DE VALIDAÇÃO ###*/
				'foto_secao[]': {
					required: "Escolha pelo menos uma seção"
				},
				foto_titulo: {
					required: "Digite o título da foto"
				},
				arquivo_enviado: {
					required: "Escolha o arquivo para envio",
					number: "Escolha o arquivo para envio",
                    min: "Escolha o arquivo para envio",
					max: "Só é permitido enviar 1 arquivo"
				}
				/*### MENSAGENS DE VALIDAÇÃO ###*/
			},


			highlight: function (e) {
				$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
			},

			success: function (e) {
				$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
				$(e).remove();
			},

			errorPlacement: function (error, element) {
				if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
					var controls = element.closest('div[class*="col-"]');
					if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
					else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
				}
				else if(element.is('.select2')) {
					error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
				}
				else if(element.is('.chosen-select')) {
					error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
				}
				else error.insertAfter(element.parent());
			},

			submitHandler: function (form) {
				//console.log(btnSubmit);

				/*### MOSTRA A DIV DE CARREGANDO ###*/
				var contentArea = $('body');
				contentArea.css('opacity', 0.25)

				var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
				var offset = contentArea.offset();
				loader.css({top: offset.top, left: offset.left})
				/*### MOSTRA A DIV DE CARREGANDO ###*/

				/*### AJAX PARA POSTAGEM DO FORMULÁRIO ###*/
				var formData = new FormData($("#form-cadastros")[0]);
				$.ajax({
					type: "POST",
					//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
					url: '<? echo $sistema; ?>/acoes_registros.php',
					//ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
					data: formData,
					contentType: false,
					enctype: 'multipart/form-data',
					processData: false,
					success: function(data)
					{

						/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/
						if (data === 'OK') {

							if( $('#form-cadastros #operacao').val() == "inserir") var texto_OK = '<? echo $registro_inserido; ?>';
							else if( $('#form-cadastros #operacao').val() == "alterar") var texto_OK = '<? echo $registro_alterado; ?>';

                            //FECHA O MODAL DE CADASTRO
                            $('#modal-form').modal("hide");

							$.gritter.add({
								title: '<i class="ace-icon fa fa-check"></i> OK!',
								text: texto_OK,
								class_name: 'gritter-success gritter-center',
								fade_out_speed:500,
								time:2000,
								before_open: function(e){
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
									oTable1.ajax.reload(null,false);
								}
							});
						}
						/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/






						/*### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###*/
						else {
							$.gritter.add({
								title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
								text: data,
								class_name: 'gritter-error gritter-center',
								sticky: false,
								fade_out_speed:500,
								after_close: function(e) {
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
								}
							});
						}
						/*### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###*/


					}
				});
				/*### AJAX PARA POSTAGEM DO FORMULÁRIO ###*/

			},
			invalidHandler: function (form) {
			}
		});
		/*### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###*/









		//FUNÇÃO QUE ATIVA O PROPOVER DO HISTÓRICO DO REGISTRO
		$('[data-rel=reg_user_popover]').popover({container:'body', html:true});




		/*### FUNÇÃO PARA FORMATAR OS BOTÕES DE CONTROLE DAS AÇÕES NO GRID ###*/
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();

			var off2 = $source.offset();
			//var w2 = $source.width();

			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		/*### FUNÇÃO PARA FORMATAR OS BOTÕES DE CONTROLE DAS AÇÕES NO GRID ###*/



		/*### FUNÇÃO AUTOSIZE DO INPUT TIPO TEXTAREA ###*/
		$('textarea[class*=autosize]').autosize({append: "\n"});
		/*### FUNÇÃO AUTOSIZE DO INPUT TIPO TEXTAREA ###*/



		/*### FUNÇÃO PARA CORRIGIR O CHOSEN QUANDO ESTÁ DENTRO DE UM FORM MODAL ###*/
		//chosen plugin inside a modal will have a zero width because the select element is originally hidden
		//and its width cannot be determined.
		//so we set the width after modal is show
		$('#modal-form, #modal-form-recorte').on('shown.bs.modal', function () {
			if(!ace.vars['touch']) {
				$(this).find('.chosen-container').each(function(){
					$(this).find('a:first-child').css('width' , '250px');
					$(this).find('.chosen-drop').css('width' , '250px');
					$(this).find('.chosen-search input').css('width' , '240px');
				});
			}
		})
		/*### FUNÇÃO PARA CORRIGIR O CHOSEN QUANDO ESTÁ DENTRO DE UM FORM MODAL ###*/




        /*### VARIÁVEIS DE CONFIGURAÇÃO DO UPLOAD DO ARQUIVO DE FOTOS ###*/
        var FotosPermitidas = ['jpg', 'png', 'jpeg'];
        var FotosExtraData = function() {
                 var ExtraData = {};
                 ExtraData['foto_pid'] = $("#form-cadastros #foto_pid").val();
                 ExtraData['foto_id'] = $("#form-cadastros #foto_id").val();
                 ExtraData['seq'] = $("#form-cadastros #arquivo_enviado").val();
                 ExtraData['operacao_upload'] = $("#form-cadastros #operacao_upload").val();
                 return ExtraData;
             };
        /*### VARIÁVEIS DE CONFIGURAÇÃO DO UPLOAD DO ARQUIVO DE FOTOS ###*/

        //CHAMA A FUNÇÃO DE UPLOAD DE ARQUIVOS
        BootstrapFileInput('form-cadastros', 'files-fotos', 'banco-fotos', 1, FotosPermitidas, '<?php echo $limite_upload_php; ?>', FotosExtraData);


	})
	});
</script>