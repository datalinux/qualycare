<?php
/*###################################################################
|                                                                   |
|   MÓDULO: banco-fotos		                                        |
|   DESCRIÇÃO: Arquivo que realiza as ações de envio de imagens 	|
|	do plugin FileInput 											|
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 07/12/2015                                                |
|                                                                   |
###################################################################*/

	include("../../includes/configure.inc.php");

	// print_r($_FILES);
	// print_r($_REQUEST);

	if($_REQUEST['acao']=="DELETE")
		{
			//REMOVE O ARQUIVO TEMPORÁRIO
			@unlink($pasta_fotos."/tmp/".$_REQUEST['pid']);
		}
	elseif($_FILES["files-fotos"]["tmp_name"][0]<>"")
		{
			$seq = $_REQUEST['seq'];
			$nome_arquivo = "tmp/".$_REQUEST['foto_pid']."-".$seq;
			move_uploaded_file($_FILES["files-fotos"]["tmp_name"][0],$GLOBALS['pasta_fotos']."/".$nome_arquivo);
			$id_arquivo = $_REQUEST['foto_pid']."-".$seq;
			$caption_arquivo = $_FILES["files-fotos"]["name"][0];

			/*#### ROTACIONA A IMAGEM CASO ESTEJA COM ORIENTAÇÃO VIRADA ####*/
			$extensao_arquivo = (getimagesize($pasta_fotos."/tmp/".$id_arquivo)["mime"]=="image/jpeg") ? "jpg" : "png";
			//echo "<br>extensao_arquivo: ".$extensao_arquivo;
			$nova_imagem = ($extensao_arquivo=="png") ? imagecreatefrompng($pasta_fotos."/".$nome_arquivo) : imagecreatefromjpeg($pasta_fotos."/".$nome_arquivo);

			$exif = @exif_read_data($pasta_fotos."/".$nome_arquivo);
			//echo "<br>exif: ";
			//print_r($exif);
			if(!empty($exif['Orientation'])) {
			    switch($exif['Orientation']) {
			        case 8:
			            $imagem_rotacionada = imagerotate($nova_imagem,90,10);
			            break;
			        case 3:
			            $imagem_rotacionada = imagerotate($nova_imagem,180,10);
			            break;
			        case 6:
			            $imagem_rotacionada = imagerotate($nova_imagem,-90,10);
			            break;
			    }
			    if($imagem_rotacionada<>"") {
					$salvar_imagem = ($extensao_arquivo=="png") ? imagepng($imagem_rotacionada, $pasta_fotos."/".$nome_arquivo, 100) : imagejpeg($imagem_rotacionada, $pasta_fotos."/".$nome_arquivo, 100);
			    }
			}
			/*#### ROTACIONA A IMAGEM CASO ESTEJA COM ORIENTAÇÃO VIRADA ####*/

		}

	if(substr_count($_FILES["files-fotos"]["type"][0],"image")>0) $initialPreview = "<img src='".$http_fotos."/".$nome_arquivo."?".$hoje_data_mk."' class='file-preview-image' style='width:auto; height:160px;'>";
	else $initialPreview = "<div class='file-preview-text'><h2><i class='glyphicon glyphicon-file'></i></h2></div>";


	echo json_encode([
		'error' => $msg_erro,
		'apagado' => $msg_apaga,
		'initialPreview' => [$initialPreview,],
		'initialPreviewConfig' => [
			['caption' => $caption_arquivo, 'width' => '100px', 'url' => "banco-fotos/upload.php?acao=DELETE&pid=".$id_arquivo."&seq=".$seq, 'key' => $id_arquivo],
		],
		'append' => true,
		'extra' => ['filename' => $filename, 'seq' => $seq]]);

?>