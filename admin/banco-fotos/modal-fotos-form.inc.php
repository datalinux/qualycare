
                <!--### MODAL DO FORMULÁRIO BUSCAR FOTO ###-->
                <?
                  //INLCUIR O ARQUIVO DE CONFIGURAÇÕES DO SISTEMA DE FOTOS
                  include($GLOBALS['pasta_root']."/admin/banco-fotos/sistema.cfg.php");
                ?>
                <div id="modal-form-fotos" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" id="btn-fechar-modal-form-fotos" class="close">&times;</button>
                                <h4 class="blue bigger">Buscar foto</h4>
                                <br>
                                <button class="btn btn-white btn-default btn-round" id="btn-abrir-modal-form-nova-foto" title="Enviar nova foto" >
                                    <i class="ace-icon fa fa-plus-square bigger-120 green"></i>
                                    Enviar nova foto
                                </button>
                            </div>





                            <div>
                                <table id="sample-table-fotos" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <?
                                                //FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
                                                foreach($array_colunas_grid as $key_coluna => $config_coluna)
                                                    {
                                                        echo "<th";
                                                        if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
                                                        echo ">";
                                                        echo $config_coluna["title"];
                                                        echo "</th>";
                                                    }
                                            ?>
                                            <th></th>
                                        </tr>
                                    </thead>


                                </table>
                            </div>




                        </div>
                    </div>
                </div>
                <!--### MODAL DO FORMULÁRIO BUSCAR FOTO ###-->


















                <!--### MODAL DO FORMULÁRIO ENVIAR NOVA FOTO ###-->
                <div id="modal-form-nova-foto" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" id="btn-fechar-modal-form-nova-foto" class="close">&times;</button>
                                <h4 id="form-titulo-acao" class="blue bigger">Enviar nova foto</h4>
                            </div>

                            <!--### FORMULÁRIO DE ENVIO DE NOVA FOTO ###-->

                            <form class="form-horizontal" role="form" id="form-nova-foto" name="form-nova-foto">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_secao">Seções</label>




                                                    <div class="col-sm-9">
                                                        <?php

                                                            //ORDENA O ARRAY DE SEÇÕES POR ORDEM ALFABETICA DE TÍTULO
                                                            @ksort($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"]);

                                                            foreach($_SESSION["guarda_config"]["titulo"][$_REQUEST['filial']][$sistema."-secao"] as $secao_value => $secao_title)
                                                                {
                                                                    $num_secao++;
                                                        ?>
                                                                    <label class="col-md-3 col-sm-6 col-xs-12" <?php if(!in_array($secao_value, $auth_filial[$_REQUEST['filial']][$sistema."-secao"])) echo "style='display: none;'"; ?>>
                                                                        <input name="foto_secao[]" type="checkbox" id="foto_secao_<?php echo $num_secao; ?>" value="<?php echo $secao_value; ?>" class="ace" />
                                                                        <span class="lbl"> <?php echo $secao_title; ?></span>
                                                                    </label>
                                                        <?php
                                                                }
                                                        ?>
                                                    </div>



                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_titulo">Título</label>

                                                  <div class="col-sm-9">
                                                        <input type="text" name="foto_titulo" id="foto_titulo" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_legenda">Legenda</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="foto_legenda" id="foto_legenda" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_autor">Autor</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="foto_autor" id="foto_autor" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="foto_participantes">Participantes</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="foto_participantes" id="foto_participantes" class="typeahead col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="files-fotos">Enviar imagem</label>

                                                  <div class="col-sm-9">
                                                    <input id="files-fotos" name="files-fotos[]" type="file" multiple lang="NaoAtribuir" />
                                                    <div id="imagens_atuais"></div>
                                                    <input type="hidden" name="arquivo_enviado" id="arquivo_enviado" lang="NaoAtribuir" />
                                                  </div>
                                                </div>

                                                <input type="hidden" name="foto_id" id="foto_id" />
                                                <input type="hidden" name="foto_pid" id="foto_pid" />
                                                <input type="hidden" name="operacao" id="operacao" value="enviar_nova_foto" />
                                                <input type="hidden" name="operacao_upload" id="operacao_upload" />
                                                <input type="hidden" name="filial" id="filial" value="<?php echo $_REQUEST['filial']; ?>" />

                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm" id="btn-cancelar-modal-form-nova-foto">
                                        <i class="ace-icon fa fa-times"></i>
                                        Cancelar
                                    </button>

                                    <button type="submit" id="btn-inserir" class="btn btn-sm btn-primary" data-id="inserir">
                                        <i class="ace-icon fa fa-check"></i>
                                        Inserir
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE ENVIO DE NOVA FOTO ###-->

                        </div>
                    </div>
                </div>
             <!--### MODAL DO FORMULÁRIO ENVIAR NOVA FOTO ###-->
