<?php
/*###################################################################
|																	|
|	MÓDULO: banco de fotos											|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 07/12/2015												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);

	//print_r($_REQUEST);

	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if($_REQUEST['operacao']=="selecionar")
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//MONTA O ARRAY ORDENADO COM AS SEÇÕES DO REGISTRO
					$separa_secoes = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."secao"]);
					foreach($separa_secoes as $secao_nome) {
						if($secao_nome<>"") {
							$num_secao++;
							$rows[$num][$sistema_prefixo_campos."secao[]"][$num_secao] = $secao_nome;
						}
					}

					//MONTA O HTML COM AS IMAGENS
					$rows[$num]["imagens_atuais"] .= "<strong>Grande:</strong>";
					$rows[$num]["imagens_atuais"] .= "<img src='".$GLOBALS['http_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"]["grande"].$ver_selecionar[$sistema_prefixo_campos."id"].".".$ver_selecionar[$sistema_prefixo_campos."extensao"]."?".md5(uniqid(microtime(),1)).getmypid()."' class='img-responsive' style='padding:4px;' /><br/>";
					foreach($_SESSION["guarda_config"]["valor"]["todas"]["miniaturas_prefixo"] as $miniatura_nome) {
						if(($miniatura_nome<>"alta")&&($miniatura_nome<>"grande")) {
							$rows[$num]["imagens_atuais"] .= "<strong>".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos"][$miniatura_nome].":</strong>";
							$rows[$num]["imagens_atuais"] .= "<img src='".$GLOBALS['http_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"][$miniatura_nome].$ver_selecionar[$sistema_prefixo_campos."id"].".".$ver_selecionar[$sistema_prefixo_campos."extensao"]."?".md5(uniqid(microtime(),1)).getmypid()."' class='img-responsive' style='padding:4px;' /><br/>";
						}
					}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];
					$rows[$num]["src_foto_grande"] = $GLOBALS['http_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"]["grande"].$ver_selecionar[$sistema_prefixo_campos."id"].".".$ver_selecionar[$sistema_prefixo_campos."extensao"]."?".md5(uniqid(microtime(),1)).getmypid();
					$rows[$num]["largura_foto_grande"] = @getimagesize($GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"]["grande"].$ver_selecionar[$sistema_prefixo_campos."id"].".".$ver_selecionar[$sistema_prefixo_campos."extensao"])[0];
					$rows[$num]["altura_foto_grande"] = @getimagesize($GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"]["grande"].$ver_selecionar[$sistema_prefixo_campos."id"].".".$ver_selecionar[$sistema_prefixo_campos."extensao"])[1];
				}

			//echo "<br>rows: ";
			//print_r($rows);


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if(($_REQUEST['operacao']=="inserir")||($_REQUEST['operacao']=="enviar_nova_foto"))
		{

			//print_r($_REQUEST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['foto_secao'] as $secao_key => $secao_marcada)
				{
					$foto_secao .= $separador_string.$secao_marcada;
				}
			$foto_secao .= $separador_string;
			$_POST['foto_secao'] = $foto_secao;
			$arquivo_foto = getFileUploadedPid($_POST['foto_pid'],$GLOBALS['pasta_fotos']."/tmp");
			$_POST['foto_extensao'] = (getimagesize($pasta_fotos."/tmp/".$arquivo_foto)["mime"]=="image/jpeg") ? "jpg" : "png";



			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/






			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			//echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_registro = mysql_insert_id($con);

			if($exe_inserir)
				{
					//CHAMA A FUNÇÃO QUE CRIA AS FOTOS DE ALTA RESOLUÇÃO, GRANDE E MINIATURAS
					criar_miniaturas($arquivo_foto, $id_registro, $_POST['foto_extensao']);
				}
			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir) $result = "OK";

			if(($exe_inserir)&&($_REQUEST['operacao']=="enviar_nova_foto"))
				{
					$rows[1]["id_da_foto"] = $id_registro;
					$rows[1]["extensao"] = $_POST['foto_extensao'];
					$rows[1]["titulo_da_foto"] = $_POST['foto_titulo'];
					$result = json_encode($rows);
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['foto_secao'] as $secao_key => $secao_marcada)
				{
					$foto_secao .= $separador_string.$secao_marcada;
				}
			$foto_secao .= $separador_string;
			$_POST['foto_secao'] = $foto_secao;
			if($_POST['arquivo_enviado']>0) {

				$arquivo_foto = getFileUploadedPid($_POST['foto_pid'],$GLOBALS['pasta_fotos']."/tmp");
				$_POST['foto_extensao'] = (@getimagesize($pasta_fotos."/tmp/".$arquivo_foto)["mime"]=="image/jpeg") ? "jpg" : "png";
			}


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar)
				{
					if($_POST['arquivo_enviado']>0)
						{
							//CHAMA A FUNÇÃO QUE CRIA AS FOTOS DE ALTA RESOLUÇÃO, GRANDE E MINIATURAS
							criar_miniaturas($arquivo_foto, $_POST[$sistema_chave_primaria], $_POST['foto_extensao']);
						}
					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/














	/*#### AÇÕES QUANDO A OPERAÇÃO FOR RECORTAR UMA FOTO ####*/
	if($_REQUEST['operacao']=="recortar")
		{
			// print_r($_REQUEST);

			//FILTRA OS DADOS DO CAMPO DE IMAGEM, BUSCANDO SOMENTE A IMAGEM
			$conteudoImagem = substr($_REQUEST['foto_recortada'], strpos($_REQUEST['foto_recortada'], ",")+1);

			//DECODIFICA O CONTEÚDO DA IMAGEM
			$ImagemDecodificada = base64_decode($conteudoImagem);

			//SALVA A IMAGEM
			$imagem = fopen($GLOBALS['pasta_fotos']."/".$_REQUEST['tamanho_recorte'].$_REQUEST['foto_id'].".".$_REQUEST['foto_extensao'], 'wb' );
			fwrite( $imagem, $ImagemDecodificada);
			fclose( $imagem );

			$result = "OK";


		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR RECORTAR UMA FOTO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir)
				{
					//APAGA TODAS AS FOTOS, EXCETO A GRANDE
					$extensao_arquivo = (is_file($GLOBALS['pasta_fotos']."/G".$_REQUEST['chave_primaria'].".jpg")) ? ".jpg" : ".png";
					foreach($_SESSION["guarda_config"]["valor"]["todas"]["miniaturas_prefixo"] as $miniatura_nome)
						{
							if($miniatura_nome<>"grande") @unlink($GLOBALS['pasta_fotos']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_prefixo"][$miniatura_nome].$_REQUEST['chave_primaria'].$extensao_arquivo);
						}

					$result = "OK";
				}
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/


	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
