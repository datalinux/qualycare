<?php
/*###################################################################
|                                                                   |
|   MÓDULO: banco de fotos                                          |
|   DESCRIÇÃO: Arquivo que gera a lista de fotos externas		    |
|	no formato JSON para utilização no TinyMCE						|
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 31/12/2015                                                |
|                                                                   |
###################################################################*/


	//INCLUSÃO DOS ARQUIVOS DE CONFIGURAÇÃO SEM FUNÇÕES JS
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//BUSCA EXISTENTES AS FOTOS NO BANCO DE DADOS
	$sql_fotos = "SELECT * FROM fotos WHERE foto_excluido <> 'sim' ORDER BY foto_titulo ASC";
	$exe_fotos = mysql_query($sql_fotos, $con) or die("Erro do MySQL[exe_fotos]: ".mysql_error());
	$total_fotos = mysql_num_rows($exe_fotos);
	$fotos = "[";
	while($ver_fotos = mysql_fetch_array($exe_fotos))
		{
			$num_foto++;
			$fotos .= "{title:'".$ver_fotos["foto_titulo"]."', value:'".retorna_url_foto($ver_fotos["foto_id"], "G", "url")."'}";
			if($num_foto<$total_fotos) $fotos .= ", ";
		}
	$fotos .= "]";

	//RETORNA A VARIÁVEL COM O JSON DAS FOTOS
	echo $fotos;
?>