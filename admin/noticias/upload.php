<?php
/*###################################################################
|                                                                   |
|	MÓDULO: galerias-fotos											|
|   DESCRIÇÃO: Arquivo que realiza as ações de envio de arquivos 	|
|	do plugin FileInput 											|
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 23/01/2016                                                |
|                                                                   |
###################################################################*/

	// error_reporting(E_ALL);

	include("../../includes/configure.inc.php");

	// print_r($_FILES);
	// print_r($_REQUEST);

	if($_REQUEST['acao']=="DELETE")
		{
			//REMOVE O ARQUIVO
			@unlink($GLOBALS['pasta_fotos_rodape']."/tmp/".$_REQUEST['arquivo']);
		}
	else
		{
			$abre_dir_fotos_rodape_tmp = new DirectoryIterator($GLOBALS['pasta_fotos_rodape']."/tmp");
			foreach($abre_dir_fotos_rodape_tmp as $le_dir_fotos_rodape_tmp) {

				// echo "<br>".$le_dir_fotos_rodape_tmp->__toString();
				if(($le_dir_fotos_rodape_tmp->__toString() <> ".")&&
					($le_dir_fotos_rodape_tmp->__toString() <> "..")&&
					($le_dir_fotos_rodape_tmp->__toString() <> "index.php")&&
					(substr_count($le_dir_fotos_rodape_tmp->__toString(),$_REQUEST['noticia_id'].".") > 0))
					{
						$arquivos_fotos_rodape[] = $le_dir_fotos_rodape_tmp->getFilename();
					}
			}
			if(count($arquivos_fotos_rodape)>0)
				{
					sort($arquivos_fotos_rodape);
					$ultimo_arquivo = str_replace($_REQUEST['noticia_id'].".","",end($arquivos_fotos_rodape));
				}

			// echo "<br>arquivos_fotos_rodape: "; print_r($arquivos_fotos_rodape);
			// echo "<br>ultimo_arquivo: ".$ultimo_arquivo;
			
			//INCREMENTA O NÚMERO DO ÚLTIMO ARQUIVO
			$ultimo_arquivo++;
			
			$nome_arquivo = $_REQUEST['noticia_id'].".".$ultimo_arquivo;
			// echo "<br>nome_arquivo: ".$nome_arquivo;

			//NOMEIA O NOVO ARQUIVO, MOVE PARA DIETÓRIO TEMPORÁRIO E ARMAZENA VARIÁVEL COM O NOME REAL DO ARQUIVO
			move_uploaded_file($_FILES["files"]["tmp_name"][0],$GLOBALS['pasta_fotos_rodape']."/tmp/".$nome_arquivo);

			/*#### ROTACIONA SE FOR IMAGEM E ESTEJA COM ORIENTAÇÃO VIRADA ####*/
			if(substr_count($_FILES["files"]["type"][0],"image")>0)
				{
					$extensao_arquivo = (getimagesize($GLOBALS['pasta_fotos_rodape']."/tmp/".$nome_arquivo)["mime"]=="image/jpeg") ? "jpg" : "png";
					// echo "<br>extensao_arquivo: ".$extensao_arquivo;
					$nova_imagem = ($extensao_arquivo=="png") ? imagecreatefrompng($GLOBALS['pasta_fotos_rodape']."/tmp/".$nome_arquivo) : imagecreatefromjpeg($GLOBALS['pasta_fotos_rodape']."/tmp/".$nome_arquivo);

					$exif = @exif_read_data($GLOBALS['pasta_fotos_rodape']."/".$nome_arquivo);
					// echo "<pre>exif: "; print_r($exif); echo "</pre>";
					if(!empty($exif['Orientation'])) {
					    switch($exif['Orientation']) {
					        case 8:
					            $imagem_rotacionada = imagerotate($nova_imagem,90,10);
					            break;
					        case 3:
					            $imagem_rotacionada = imagerotate($nova_imagem,180,10);
					            break;
					        case 6:
					            $imagem_rotacionada = imagerotate($nova_imagem,-90,10);
					            break;
					    }
					    if($imagem_rotacionada<>"") {
							$salvar_imagem = ($extensao_arquivo=="png") ? imagepng($imagem_rotacionada, $GLOBALS['pasta_fotos_rodape']."/tmp/".$nome_arquivo, 100) : imagejpeg($imagem_rotacionada, $GLOBALS['pasta_fotos_rodape']."/tmp/".$nome_arquivo, 100);
					    }
					}
				}
			/*#### ROTACIONA SE FOR IMAGEM E ESTEJA COM ORIENTAÇÃO VIRADA ####*/
			
			
		}
		
	/*#### RETORNA PREVIEW DA IMAGEM ####*/
	if(substr_count($_FILES["files"]["type"][0],"image")>0) $initialPreview = "<img src='".$GLOBALS['http_fotos_rodape']."/tmp/".$nome_arquivo."?".$hoje_data_mk."' class='file-preview-image'>";
	else $initialPreview = "<div class='file-preview-text'><h2><i class='glyphicon glyphicon-file'></i></h2></div>";
	/*#### RETORNA PREVIEW DA IMAGEM ####*/

	echo json_encode([
		'error' => $msg_erro,
		'apagado' => $msg_apaga,
		'initialPreview' => [$initialPreview,],
		'initialPreviewConfig' => [
			['caption' => $_FILES["files"]["name"][0], 'width' => '100px', 'url' => "noticias/upload.php?acao=DELETE&noticia=".$_REQUEST['noticia_id']."&arquivo=".$nome_arquivo, 'key' => $nome_arquivo],
		],
		'append' => true]);

?>