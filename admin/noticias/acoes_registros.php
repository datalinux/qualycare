<?php
/*###################################################################
|																	|
|	MÓDULO: noticias												|
|	DESCRIÇÃO: Arquivo que executa as ações nos registros do 		|
|	módulo, chamado	pelo $.ajax										|
|																	|
|	Autor: Guilherme Moreira de Castro								|
|	E-mail: guilherme@datalinux.com.br								|
|	Data: 17/01/2016												|
|																	|
###################################################################*/


	//INCLUSAO DO ARQUIVO GERAL DE CONFIGURAÇÕES E PERMISSÕES
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//INCLUSÃO DO ARQUIVO PADRÃO DE CONFIGURAÇÕES DO MÓDULO
	include("sistema.cfg.php");

	//DECOFIFICA A HASH PARA SQL CUSTOMIZADO
	$hash = base64_decode($_REQUEST['hash']);





	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/
	if(($_REQUEST['operacao']=="selecionar")||
		($_REQUEST['operacao']=="selecionar_fotos")||
		($_REQUEST['operacao']=="selecionar_fotos_upload")||
		($_REQUEST['operacao']=="leitura"))
		{

			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/
			$sql_describe = "DESCRIBE ".$sistema_nome_da_tabela;
			$exe_describe = mysql_query($sql_describe, $con) or die("Erro do MySQL[exe_describe]: ".mysql_error());
			while($ver_describe = mysql_fetch_array($exe_describe))
				{
					$campos_tabela[$ver_describe["Field"]] = 1;
				}
			/*#### SELECIONA TODOS OS CAMPOS DA TABELA E ARMAZENA EM ARRAY ####*/

			//echo "<br>campos_tabela: ";
			//print_r($campos_tabela);

			/*#### CONSTRÓI A QUERY ####*/
			$sql_selecionar = "SELECT *,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."criacao_usuario) AS reg_criacao_usuario,
										(SELECT ".$tabela_usuarios_prefixo_campo."nome FROM ".$tabela_usuarios." WHERE ".$tabela_usuarios_prefixo_campo."id = ".$sistema_prefixo_campos."atualizacao_usuario) AS reg_atualizacao_usuario
									FROM ".
										$sistema_nome_da_tabela."
									LEFT JOIN
										fotos ON fotos.foto_id = ".$sistema_nome_da_tabela.".".$sistema_prefixo_campos."foto
									WHERE ".
										$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_selecionar: ".$sql_selecionar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_selecionar = mysql_query($sql_selecionar, $con) or die("Erro do MySQL[exe_selecionar]: ".mysql_error());
			while($ver_selecionar = mysql_fetch_array($exe_selecionar))
				{
					//ARMAZENA TODAS AS INFORMAÇÕES DE TODOS OS CAMPOS EM UM ARRAY
					$num++;
					foreach($campos_tabela as $nome_campo => $value)
						{
							$rows[$num][$nome_campo] = $ver_selecionar[$nome_campo];
						}

					//MONTA O ARRAY ORDENADO COM AS SEÇÕES DO REGISTRO
					$separa_secoes = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."secao"]);
					foreach($separa_secoes as $secao_nome) {
						if($secao_nome<>"") {
							$num_secao++;
							$rows[$num][$sistema_prefixo_campos."secao[]"][$num_secao] = $secao_nome;
						}
					}

					//MONTA O ARRAY ORDENADO COM OS DESTAQUES DO REGISTRO
					$separa_destaques = explode($separador_string, $ver_selecionar[$sistema_prefixo_campos."destaque"]);
					foreach($separa_destaques as $destaque_nome) {
						if($destaque_nome<>"") {
							$num_destaque++;
							$rows[$num][$sistema_prefixo_campos."destaque[]"][$num_destaque] = $destaque_nome;
						}
					}

					//FORMATAÇÃO DE CAMPOS ESPECÍFICOS, CRIAÇÃO DE CAMPOS NOVOS OU APLICAÇÃO DE MÁSCARAS
					$rows[$num][$sistema_prefixo_campos."id_reg_user"] = str_pad($ver_selecionar[$sistema_prefixo_campos."id"],3,0,STR_PAD_LEFT);
					$rows[$num][$sistema_prefixo_campos."criacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."criacao_data"]);
					$rows[$num]["reg_criacao_usuario"] = $ver_selecionar["reg_criacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."atualizacao_data"] = mostra_data_completa_br($ver_selecionar[$sistema_prefixo_campos."atualizacao_data"]);
					$rows[$num]["reg_atualizacao_usuario"] = $ver_selecionar["reg_atualizacao_usuario"];
					$rows[$num][$sistema_prefixo_campos."data"] = mostra_data_completa_br_timepicker($ver_selecionar[$sistema_prefixo_campos."data"]);
					$rows[$num][$sistema_prefixo_campos."data_atualizacao"] = mostra_data_completa_br_timepicker($ver_selecionar[$sistema_prefixo_campos."data_atualizacao"]);
					$rows[$num]["thumb_da_foto"] = retorna_url_foto($ver_selecionar[$sistema_prefixo_campos."foto"], "G", "url")."?".md5(uniqid(microtime(),1)).getmypid();
					$rows[$num]["titulo_da_foto"] = $ver_selecionar["foto_titulo"];

					//FORMATA O CAMPO NOTICIA_MOSTRA_FOTO COMO OBJETO CHECKBOX
					$rows[$num]["noticia_mostra_foto[]"][1] = $ver_selecionar["noticia_mostra_foto"];


					/*#### SEPARA AS TAGS E CRIA UMA ARRAY ####*/
					if($ver_selecionar["noticia_tags"]<>"")
						{
							$separa_tags = explode(",",$ver_selecionar["noticia_tags"]);
							foreach($separa_tags as $tag) {
								$num_tag++;
								$rows[$num]["tags"][$num_tag] = trim($tag);
							}
						}
					/*#### SEPARA AS TAGS E CRIA UMA ARRAY ####*/

				}



			/*#### RETORNA O CONTEUDO DO <UL> DA LISTA DE FOTOS DA GALERUA ####*/
			if($_REQUEST['operacao']=="selecionar_fotos")
				{
					if(is_dir($GLOBALS['pasta_fotos_rodape_fotos']."/".$rows[1][$sistema_prefixo_campos."id"]))
						{
							$abre_dir_fotos_rodape = new DirectoryIterator($GLOBALS['pasta_fotos_rodape_fotos']."/".$rows[1][$sistema_prefixo_campos."id"]);
							foreach($abre_dir_fotos_rodape as $le_diretorio_fotos_rodape) {

								if(($le_diretorio_fotos_rodape->__toString() <> ".")&&
									($le_diretorio_fotos_rodape->__toString() <> "..")&&
									($le_diretorio_fotos_rodape->__toString() <> ".DS_Store")&&
									(substr_count($le_diretorio_fotos_rodape->__toString(),$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_prefixo"]["grande"]) == 0))
									{
										//echo "<br>".$le_diretorio_fotos_rodape->getFilename();
										$thumbs_da_galeria[] = $le_diretorio_fotos_rodape->getFilename();
									}
							}
						}


				if(count($thumbs_da_galeria)>0)
					{
						//ORDENA O ARRAY DE FOTOS EM ORDEM CRESCENTE
						sort($thumbs_da_galeria);

						foreach($thumbs_da_galeria as $key => $thumb)
							{
								$lista_de_fotos .= '<li data-id="'.$thumb.'">';
		                        $lista_de_fotos .= '<a>';
		                        $lista_de_fotos .= '<img src='.$GLOBALS['http_fotos_rodape_fotos']."/".$rows[1][$sistema_prefixo_campos."id"].'/'.$thumb.'?'.$hoje_data_mk.' />';
		                        $lista_de_fotos .= '</a>';
		                        $lista_de_fotos .= '<div class="tools tools-bottom">';
		                        $lista_de_fotos .= '<a data-id="'.$thumb.'" data-rel="excluir" title="Excluir foto"><i class="ace-icon fa fa-times red"></i></a>';
		                        $lista_de_fotos .= '</div>';
		                        $lista_de_fotos .= '</li>';
							}

						//RETORNA OBJETO DA LISTA DE FOTOS
						$rows[$num]["lista_de_fotos"] = $lista_de_fotos;
					}
				else
					{
						$rows[$num]["lista_de_fotos"] = "Não há fotos cadastradas nesta notícia";
					}

				}
			/*#### RETORNA O CONTEUDO DO <UL> DA LISTA DE FOTOS DA GALERUA ####*/
			//echo "<br>rows: ";
			//print_r($rows);




			/*### HISTÓRICO DE LEITURA DO REGISTRO ###*/
			if($_REQUEST['operacao']=="leitura")
				{
					$sql_historico = "SELECT DISTINCT
												noticias_leitura.noticia_leitura_data,
												arquivos_usuarios.arq_usuario_nome,
												arquivos_usuarios.arq_usuario_email
											FROM
												noticias_leitura
											INNER JOIN
												galerias_fotos ON galerias_fotos.galeria_id = noticias_leitura.noticia_leitura_noticia
														AND galerias_fotos.galeria_excluido <> 'sim'
											INNER JOIN
												arquivos_usuarios ON arquivos_usuarios.arq_usuario_id = noticias_leitura.noticia_leitura_usuario
																	AND arquivos_usuarios.arq_usuario_excluido <> 'sim'
											WHERE
												noticias_leitura.noticia_leitura_noticia = ".$_REQUEST['chave_primaria'];
					$exe_historico = mysql_query($sql_historico, $con) or die("Erro do MySQL[exe_historico]: ".mysql_error());
					if(mysql_num_rows($exe_historico)>0)
						{
							$rows[$num]["historico"] = '<table id="simple-table" class="table  table-bordered table-hover">
															<thead>
																<tr>
																	<th width="20%">Data</th>
																	<th width="40%">Usuário</th>
																	<th width="40%">E-mail</th>
																</tr>
															</thead>

															<tbody>';

							while($ver_historico = mysql_fetch_array($exe_historico))
								{
									$rows[$num]["historico"] .= "<tr>";
									$rows[$num]["historico"] .= "<td>";
									$rows[$num]["historico"] .= mostra_data_completa_br($ver_historico["noticia_leitura_data"]);
									$rows[$num]["historico"] .= "</td>";
									$rows[$num]["historico"] .= "<td>";
									$rows[$num]["historico"] .= $ver_historico["arq_usuario_nome"];
									$rows[$num]["historico"] .= "</td>";
									$rows[$num]["historico"] .= "<td>";
									$rows[$num]["historico"] .= $ver_historico["arq_usuario_email"];
									$rows[$num]["historico"] .= "</td>";
									$rows[$num]["historico"] .= "</tr>";
								}

							$rows[$num]["historico"] .= '</tbody></table>';
						}
					else
						{
							$rows[$num]["historico"] = "<h3>Não há registros de leitura deste arquivo</h3>";
						}

						$rows[$num]["sql_historico"] = $sql_historico;
				}
			/*### HISTÓRICO DE LEITURA DO REGISTRO ###*/


			//RETORNA O ARRAY EM FORMATO JSON
			$result = json_encode($rows);
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR SELECIONAR UM REGISTRO ESPECÍFICO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/
	if($_REQUEST['operacao']=="inserir")
		{
			//print_r($_REQUEST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['noticia_secao'] as $secao_key => $secao_marcada)
				{
					$noticia_secao .= $separador_string.$secao_marcada;
				}
			$noticia_secao .= $separador_string;
			$_POST['noticia_secao'] = $noticia_secao;
			if(count($_POST['noticia_destaque'])>0)
				{
					foreach($_POST['noticia_destaque'] as $secao_key => $secao_marcada)
						{
							$noticia_destaque .= $separador_string.$secao_marcada;
						}
					$noticia_destaque .= $separador_string;
					$_POST['noticia_destaque'] = $noticia_destaque;
				}
			$_POST['noticia_data'] = mostra_data_completa_us($_POST['noticia_data']);
			$_POST['noticia_data_atualizacao'] = mostra_data_completa_us($_POST['noticia_data_atualizacao']);
			$_POST["noticia_status"] = $_POST["noticia_status_hidden"];
			$_POST["noticia_mostra_foto"] = $_POST["noticia_mostra_foto"][0];
			unset($_POST['noticia_status_hidden']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_campos .= $nome_campo.", ";
							$sql_valores .= "'".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/






			/*#### CONSTRÓI A QUERY ####*/
			$sql_inserir = "INSERT INTO ".$sistema_nome_da_tabela." ( ".$sql_campos.
																	$sistema_prefixo_campos."criacao_data, ".
																	$sistema_prefixo_campos."criacao_usuario )
																VALUES
																	( ".$sql_valores.
																	"'".$hoje_data_us."', ".
																	"'".$_SESSION["login"]["id"]."' )";
			//echo "<br>sql_inserir: ".$sql_inserir;
			/*#### CONSTRÓI A QUERY ####*/


			//EXECUTA A QUERY
			$exe_inserir = mysql_query($sql_inserir, $con) or die("Erro do MySQL[exe_inserir]: ".mysql_error());
			$id_registro = mysql_insert_id($con);

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_inserir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR INSERIR UM NOVO REGISTRO ####*/










	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="alterar")
		{
			//print_r($_POST);

			//APLICAÇÃO DE MÁSCARAS E FORMATAÇÕES NOS CAMPOS
			foreach($_POST['noticia_secao'] as $secao_key => $secao_marcada)
				{
					$noticia_secao .= $separador_string.$secao_marcada;
				}
			$noticia_secao .= $separador_string;
			$_POST['noticia_secao'] = $noticia_secao;
			if(count($_POST['noticia_destaque'])>0)
				{
					foreach($_POST['noticia_destaque'] as $secao_key => $secao_marcada)
						{
							$noticia_destaque .= $separador_string.$secao_marcada;
						}
					$noticia_destaque .= $separador_string;
					$_POST['noticia_destaque'] = $noticia_destaque;
				}
			else $_POST['noticia_destaque'] = "";
			$_POST['noticia_data'] = mostra_data_completa_us($_POST['noticia_data']);
			$_POST['noticia_data_atualizacao'] = mostra_data_completa_us($_POST['noticia_data_atualizacao']);
			$_POST["noticia_status"] = $_POST["noticia_status_hidden"];
			$_POST["noticia_mostra_foto"] = $_POST["noticia_mostra_foto"][0];
			unset($_POST['noticia_status_hidden']);


			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/
			foreach($_POST as $nome_campo => $valor_campo)
				{
					if((substr_count($nome_campo, $sistema_prefixo_campos)>0)&&($nome_campo<>$sistema_chave_primaria))
						{
							//echo $nome_campo." => ".$valor_campo;
							$sql_atualiza_campos .= $nome_campo." = '".addslashes($valor_campo)."', ";
						}
				}
			/*#### PERCORRE TODOS O ARRAY $_POST E CONSTRÓI A QUERY SÓ COM OS CAMPOS DO MÓDULO ####*/




			/*#### CONSTRÓI A QUERY ####*/
			$sql_alterar = "UPDATE ".$sistema_nome_da_tabela." SET ".$sql_atualiza_campos.
																	$sistema_prefixo_campos."atualizacao_data = '".$hoje_data_us."', ".
																	$sistema_prefixo_campos."atualizacao_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_POST[$sistema_chave_primaria]."'";
			//echo "<br>sql_alterar: ".$sql_alterar;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_alterar = mysql_query($sql_alterar, $con) or die("Erro do MySQL[exe_alterar]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_alterar) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR ALTERAR UM REGISTRO ####*/






















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR PROCESSAR AS FOTOS ENVIADAS ####*/
	if($_REQUEST['operacao']=="processar_fotos")
		{
			//print_r($_REQUEST);

			if(!is_dir($GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia_id']))
				{
					$oldumask = umask(0);
					mkdir($GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia_id'],0777);
					umask($oldumask);
				}

			unset($ultimo_arquivo, $abre_dir_tmp_fotos_rodape, $le_dir_tmp_fotos_rodape);

			//echo "\n\ngaleria: ".$_REQUEST['noticia_id'].".";
			//echo "\ndiretorio: ".$GLOBALS['pasta_fotos_rodape']."/tmp";

			/*#### LE O DIRETORIO TMP E CRIA ARRAY COM ARQUIVOS E TIPOS MIME ####*/
			$abre_dir_tmp_fotos_rodape = new DirectoryIterator($GLOBALS['pasta_fotos_rodape']."/tmp");
			foreach($abre_dir_tmp_fotos_rodape as $le_dir_tmp_fotos_rodape) {

				if(($le_dir_tmp_fotos_rodape->__toString() <> ".")&&
					($le_dir_tmp_fotos_rodape->__toString() <> "..")&&
					($le_dir_tmp_fotos_rodape->__toString() <> ".DS_Store")&&
					(substr_count($le_dir_tmp_fotos_rodape->__toString(),$_REQUEST['noticia_id'].".") > 0))
					{
						//echo "\n\nle_dir_tmp_fotos_rodape: ".$le_dir_tmp_fotos_rodape->getFilename();
						//echo "\n\nsubstr_count: ".substr_count($le_dir_tmp_fotos_rodape->getFilename(), $_REQUEST['noticia_id'].".");
						$arquivos_da_galeria[] = $le_dir_tmp_fotos_rodape->getFilename();
						$mime_arquivos_da_galeria[$le_dir_tmp_fotos_rodape->getFilename()] = mime_content_type($le_dir_tmp_fotos_rodape->getPathname());
						$path_arquivos_da_galeria[$le_dir_tmp_fotos_rodape->getFilename()] = $le_dir_tmp_fotos_rodape->getPathname();
					}
			}

			//echo "\narquivos_da_galeria: ";
			//print_r($arquivos_da_galeria);
			//echo "\nmime_arquivos_da_galeria: ";
			//print_r($mime_arquivos_da_galeria);
			//echo "\npath_arquivos_da_galeria: ";
			//print_r($path_arquivos_da_galeria);
			//ORDENA OS ARQUIVOS EM ORDEM CRESCENTE
			sort($arquivos_da_galeria);


			//GUARDA O NÚMERO DO NOME DO ÚLTIMO ARQUIVO
			$ultimo_arquivo = str_replace($_REQUEST['noticia_id'].".","",end($arquivos_da_galeria));
			//echo "\nultimo_arquivo: ".$ultimo_arquivo;
			/*#### LE O DIRETORIO TMP E CRIA ARRAY COM ARQUIVOS E TIPOS MIME ####*/






			/*#### LE O ARRAY DE ARQUIVOS E DESCOMPACTA TODOS OS ARQUIVOS ZIP ####*/
			foreach($arquivos_da_galeria as $key => $arquivo) {

				if(substr_count($mime_arquivos_da_galeria[$arquivo],"zip") > 0)
					{
						$oldumask = umask(0);
						$zip_destino = $GLOBALS['pasta_fotos_rodape']."/tmp/zip-".$arquivo;
						mkdir($zip_destino,0777);
						umask($oldumask);

						$zip = new ZipArchive;
						$zip->open($path_arquivos_da_galeria[$arquivo]);
						$zip->extractTo($zip_destino);
						for($i = 0; $i <= $zip->numFiles; $i++) {
							//echo "\n\n-----";
							$filename = $zip->getNameIndex($i);
							$ultimo_arquivo++;
							//echo "\n\norigem: ".$zip_destino."/".$filename;
							//echo "\ndestino: ".$GLOBALS['pasta_fotos_rodape']."/tmp/".$_REQUEST['noticia_id'].".".$ultimo_arquivo;
							//echo "\nsubstr: ".substr_count($filename,"__MACOSX");
							if((!is_dir($zip_destino."/".$filename))&&(substr_count($filename,"__MACOSX")==0)) 
								{

									//MOVE O ARQUIVO PARA A RAIZ DO TMP COM O NOME SEQUENCIAL
									rename($zip_destino."/".$filename, $GLOBALS['pasta_fotos_rodape']."/tmp/".$_REQUEST['noticia_id'].".".$ultimo_arquivo);

									//ADICIONA O ARQUIVO DESCOMPACTADO NOS ARRAYS DE ARQUIVOS DO DIRETÓRIO
									array_push($arquivos_da_galeria, $_REQUEST['noticia_id'].".".$ultimo_arquivo);
									$mime_arquivos_da_galeria[$_REQUEST['noticia_id'].".".$ultimo_arquivo] = mime_content_type($GLOBALS['pasta_fotos_rodape']."/tmp/".$_REQUEST['noticia_id'].".".$ultimo_arquivo);
									$path_arquivos_da_galeria[$_REQUEST['noticia_id'].".".$ultimo_arquivo] = $GLOBALS['pasta_fotos_rodape']."/tmp/".$_REQUEST['noticia_id'].".".$ultimo_arquivo;
								}
						}
						$zip->close();

						//APAGA O DIRETÓRIO CRIADO PARA DESCOMPACTAR
						apaga_diretorio($zip_destino);

						//APAGA O ARQUIVO ZIP
						@unlink($path_arquivos_da_galeria[$arquivo]);

						//REMOVE O ARQUIVO ZIP DO ARRAY DE ARQUIVOS DO DIRETÓRIO
						unset($arquivos_da_galeria[$key]);
					}
			}
			/*#### LE O ARRAY DE ARQUIVOS E DESCOMPACTA TODOS OS ARQUIVOS ZIP ####*/







			/*#### LE O DIRETORIO DESTINO E GUARDA O NÚMERO DO ÚLTIMO ARQUIVO ####*/
			$abre_dir_galeria = new DirectoryIterator($GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia_id']);
			foreach($abre_dir_galeria as $le_dir_galeria) {

				if(($le_dir_galeria->__toString() <> ".")&&
					($le_dir_galeria->__toString() <> "..")&&
					($le_dir_galeria->__toString() <> ".DS_Store")&&
					(substr_count($le_dir_galeria->__toString(),$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_prefixo"]["grande"]) > 0))
					{
						$arquivos_da_galeria_destino[] = $le_dir_galeria->getFilename();
					}
			}

			if(count($arquivos_da_galeria_destino)>0)
			{
				//ORDENA OS ARQUIVOS EM ORDEM CRESCENTE
				sort($arquivos_da_galeria_destino);

				//GUARDA O NÚMERO DO NOME DO ÚLTIMO ARQUIVO
				$ultimo_arquivo_destino = str_replace($_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_prefixo"]["grande"],"",str_replace(".jpg","",str_replace(".png","",end($arquivos_da_galeria_destino))));
			}
			//echo "\nultimo_arquivo_destino: ".$ultimo_arquivo_destino;
			/*#### LE O DIRETORIO DESTINO E GUARDA O NÚMERO DO ÚLTIMO ARQUIVO ####*/









			/*#### LE O ARRAY DE ARQUIVOS CRIANDO AS FOTOS FINAIS DA GALERIA ####*/
			foreach($arquivos_da_galeria as $key => $arquivo) {

				//INCREMENTA O NÚMERO DO ÚLTIMO ARQUIVO NA PASTA DESTINO
				$ultimo_arquivo_destino++;

				/*#### GUARDA AS INFORMAÇÕES E DIMENSÕES DA IMAGEM TEMPORÁRIA ####*/
				$foto_temporaria = $GLOBALS['pasta_fotos_rodape']."/tmp/".$arquivo;
				$tamanho_imagem = @getimagesize($foto_temporaria);
				$foto_extensao = ($tamanho_imagem["mime"]=="image/png") ? "png" : "jpg";
				/*#### GUARDA AS INFORMAÇÕES E DIMENSÕES DA IMAGEM TEMPORÁRIA ####*/



				foreach($_SESSION["guarda_config"]["valor"]["todas"]["miniaturas_fotos_rodape_prefixo"] as $miniatura_nome)
					{
						// echo "\n\n".$miniatura_nome;
						// echo "\nlargura: ".$tamanho_imagem[0];
						// echo "\naltura: ".$tamanho_imagem[1];
						// echo "\nlargura SESS: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_largura"][$miniatura_nome];
						// echo "\naltura SESS: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome];

						/*#### VERIFICA AS DIMENSÕES DA IMAGEM E CRIA A VERSÃO EM NOVO TAMANHO, SE AS DIMENSÕES DA FOTO ENVIADA FOREM MAIORES DO QUE O LIMITE DO DEFINIDO ####*/
						if(($tamanho_imagem[0]>=$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_largura"][$miniatura_nome])
							||($tamanho_imagem[1]>=$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome]))
							{
								$porcentagem_largura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_largura"][$miniatura_nome] / $tamanho_imagem[0];
								$porcentagem_altura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome] / $tamanho_imagem[1];
								$nova_altura_largura = $tamanho_imagem[1] * $porcentagem_largura;
								$nova_largura_altura = $tamanho_imagem[0] * $porcentagem_altura;

								// echo "\nporcentagem_largura: ".$porcentagem_largura;
								// echo "\nporcentagem_altura: ".$porcentagem_altura;
								// echo "\nnova_largura[largura]: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_largura"][$miniatura_nome];
								// echo "\nnova_altura[largura]: ".$nova_altura_largura;
								// echo "\nnova_largura[altura]: ".$nova_largura_altura;
								// echo "\nnova_altura[altura]: ".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome];

								if($miniatura_nome=="grande")
									{
										if($nova_altura_largura > $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome])
											{
												$nova_largura = ceil($nova_largura_altura);
												$nova_altura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome];
											}
										else
											{
												$nova_largura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_largura"][$miniatura_nome];
												$nova_altura = ceil($nova_altura_largura);
											}
									}
								else
									{
										$nova_largura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_largura"][$miniatura_nome];
										$nova_altura = $_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_altura"][$miniatura_nome];

										if($porcentagem_largura>$porcentagem_altura)
											{
												$nova_altura = $tamanho_imagem[1] * $porcentagem_altura;
												$nova_largura = $tamanho_imagem[0] * $porcentagem_altura;
											}
									}

								// echo "\nnova_largura: ".$nova_largura;
								// echo "\nnova_altura: ".$nova_altura;


								if($tamanho_imagem["mime"]=="image/png")
									{
										//cria uma nova imagem grande com o tamanho reduzido de acordo com os limites do sistema
										$imagem_atual = imagecreatefrompng($foto_temporaria);
										$largura_imagem_atual = imagesx($imagem_atual);
										$altura_imagem_atual = imagesy($imagem_atual);
										imagealphablending($imagem_atual, true);
										$nova_imagem = imagecreatetruecolor($nova_largura,$nova_altura);
										imagealphablending($nova_imagem, false);
										imagesavealpha($nova_imagem, true);
										$transparente = imagecolorallocatealpha($nova_imagem, 255, 255, 255, 127);
										imagefilledrectangle($nova_imagem, 0, 0, $nova_largura, $nova_altura, $transparente);
										imagecopyresampled($nova_imagem, $imagem_atual, 0, 0, 0, 0, $nova_largura, $nova_altura, $tamanho_imagem[0], $tamanho_imagem[1]);
										imagepng($nova_imagem, $GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia_id']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_prefixo"][$miniatura_nome].str_pad($ultimo_arquivo_destino,5,0,STR_PAD_LEFT).'.'.$foto_extensao, 0);
										imagedestroy($imagem_atual);
										imagedestroy($nova_imagem);
										//echo "<br>imagem PNG criada";
									}
								else
									{
										//cria uma nova imagem grande com o tamanho reduzido de acordo com os limites do sistema
										$imagem_atual = imagecreatefromjpeg($foto_temporaria);
										$largura_imagem_atual = imagesx($imagem_atual);
										$altura_imagem_atual = imagesy($imagem_atual);
										$nova_imagem = imagecreatetruecolor($nova_largura,$nova_altura);
										imagecopyresampled($nova_imagem, $imagem_atual, 0, 0, 0, 0, $nova_largura, $nova_altura, $tamanho_imagem[0], $tamanho_imagem[1]);
										imagejpeg($nova_imagem, $GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia_id']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_prefixo"][$miniatura_nome].str_pad($ultimo_arquivo_destino,5,0,STR_PAD_LEFT).'.'.$foto_extensao, 100);
										imagedestroy($imagem_atual);
										imagedestroy($nova_imagem);
										//echo "<br>imagem JPG criada";
									}
							}
						/*#### VERIFICA AS DIMENSÕES DA IMAGEM E CRIA A VERSÃO EM NOVO TAMANHO, SE AS DIMENSÕES DA FOTO ENVIADA FOREM MAIORES DO QUE O LIMITE DO DEFINIDO ####*/



						/*#### SE NÃO, APENAS COPIA A IMAGEM PARA O NOVO DESTINO ####*/
						else
							{
								//echo "<br>copiar";
								@copy($foto_temporaria, $GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia_id']."/".$_SESSION["guarda_config"]["titulo"]["todas"]["miniaturas_fotos_rodape_prefixo"][$miniatura_nome].str_pad($ultimo_arquivo_destino,5,0,STR_PAD_LEFT).'.'.$foto_extensao);
							}
						/*#### SE NÃO, APENAS COPIA A IMAGEM PARA O NOVO DESTINO ####*/
					}


				//APAGAR A FOTO TEMPORÁRIA
				@unlink($foto_temporaria);

			}
			/*#### LE O ARRAY DE ARQUIVOS CRIANDO AS FOTOS FINAIS DA GALERIA ####*/









			//RETORNA TEXTO OK
			$result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR PROCESSAR AS FOTOS ENVIADAS ####*/














	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UMA FOTO ####*/
	if($_REQUEST['operacao']=="excluir_foto")
		{
			//print_r($_POST);

			//APAGA O ARQUIVO THUMB
			@unlink($GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia'].'/'.$_REQUEST['arquivo']);

			//APAGA O ARQUIVO IMAGE
			@unlink($GLOBALS['pasta_fotos_rodape_fotos']."/".$_REQUEST['noticia'].'/'.str_replace("thumb","image",$_REQUEST['arquivo']));

			//RETORNA TEXTO OK
			$result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UMA FOTO ####*/
















	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/
	if($_REQUEST['operacao']=="excluir")
		{
			/*#### CONSTRÓI A QUERY ####*/
			$sql_excluir = "UPDATE ".$sistema_nome_da_tabela." SET ".$sistema_prefixo_campos."excluido = 'sim',
																	".$sistema_prefixo_campos."excluido_data = '".$hoje_data_us."',
																	".$sistema_prefixo_campos."excluido_usuario = '".$_SESSION["login"]["id"]."'
																WHERE
																	".$sistema_chave_primaria." = '".$_REQUEST['chave_primaria']."'";
			//echo "<br>sql_excluir: ".$sql_excluir;
			/*#### CONSTRÓI A QUERY ####*/

			//EXECUTA A QUERY
			$exe_excluir = mysql_query($sql_excluir, $con) or die("Erro do MySQL[exe_excluir]: ".mysql_error());

			//SE TIVER EXECUTADO, RETORNA TEXTO OK
			if($exe_excluir) $result = "OK";
		}
	/*#### AÇÕES QUANDO A OPERAÇÃO FOR EXCLUIR UM REGISTRO ####*/


	//RETORNA O RESULTADO DAS ACOES
	echo $result;
?>
