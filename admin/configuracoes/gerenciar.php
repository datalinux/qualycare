<?php
/*###################################################################
|                                                                   |
|   MÓDULO: configurações                                           |
|   DESCRIÇÃO: Arquivo principal de telas e gerenciamento do        |
|   módulo                                                          |
|                                                                   |
|   Autor: Guilherme Moreira de Castro                              |
|   E-mail: guilherme@datalinux.com.br                              |
|   Data: 07/12/2015                                                |
|                                                                   |
###################################################################*/



	//INCLUSÃO DOS ARQUIVOS DE CONFIGURAÇÃO SEM FUNÇÕES JS
	$include_functions_js = "nao";
	include("../../includes/configure.inc.php");

	//DEFINIÇÃO DO NOME DA OPERAÇÃO PRINCIPAL DE VISUALIZAÇÃO DE REGISTROS
	$operacao = "gerenciar";

	//INCLUSÃO DO ARQUIVO QUE CARREGA AS PERMISSÕES DO USUÁRIO
	include($pasta_includes."/auth.inc.php");
	//echo "<br>autorizado: ".$autorizado;
?>

<link rel="stylesheet" href="../lib/ace/assets/css/chosen.css" />
<link rel="stylesheet" href="../lib/ace/assets/css/jquery.gritter.css" />
<link rel="stylesheet" href="../includes/estilos.admin.sistemas.css" />

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		<? echo $sistema_titulo; ?>
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			<? echo $sistema_descricao; ?>
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">


	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS --><!-- /.row -->



        <!--### FILTROS DO GRID PRINCIPAL ###-->
		<div class="row">
            <div class="col-xs-12">
                <h3 class="header smaller lighter blue">Filtros</h3>
            </div>
            <div class="col-xs-4">
                <label for="form-field-select-3">Filtrar por Categoria</label>
                <br />
                <select class="chosen-select" id="filtro_categoria" data-id="2" data-placeholder="Escolha a categoria...">
                    <option value="">Todas as categorias</option>
					<?
                        $sql_categorias = "SELECT DISTINCT configuracao_categoria FROM configuracoes WHERE configuracao_excluido <> 'sim' ORDER BY configuracao_categoria ASC";
                        $exe_categorias = mysql_query($sql_categorias, $con) or die("Erro do MySQL[exe_categorias]: ".mysql_error());
                        while($ver_categorias = mysql_fetch_array($exe_categorias))
                            { 
                    ?>
                                <option value="<? echo $ver_categorias["configuracao_categoria"]; ?>"><? echo $ver_categorias["configuracao_categoria"]; ?></option>
                    <?
                            } 
                    ?>
                </select>
            </div>
        </div>
        <!--### FILTROS DO GRID PRINCIPAL ###-->












        <!--### GRID PRINCIPAL ###-->
        <br />
		<div class="row">
			<div class="col-xs-12">
                <h3 class="header smaller lighter blue">
                    <button class="btn btn-white btn-default btn-round" title="Inserir <? echo $sistema_titulo_item; ?>" data-id="btn-form-inserir">
                        <i class="ace-icon fa fa-plus-square bigger-120 green"></i>
                        Inserir <? echo $sistema_titulo_item; ?>
                    </button>
                    <button class="btn btn-white btn-default btn-round" title="Atualizar" id="btn-atualizar-grid">
                        <i class="ace-icon fa fa-refresh bigger-120 blue"></i>
                        Atualizar
                    </button>
                </h3>


				<div class="table-header">
					Lista de <? echo strtolower($sistema_titulo); ?> existentes
				</div>

				<!-- <div class="table-responsive"> -->

				<!-- <div class="dataTables_borderWrap"> -->
				<div>
					<table id="sample-table-2" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
                            	<?
									//FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
									foreach($array_colunas_grid as $key_coluna => $config_coluna)
										{
											echo "<th";
											if($config_coluna["hidden"]<>"") echo " class='".$config_coluna["hidden"]."'";
											echo ">";
											echo $config_coluna["title"];
											echo "</th>";
										}
								?>
                                <th></th>
							</tr>
						</thead>


					</table>
				</div>
			</div>
		</div>
        <!--### GRID PRINCIPAL ###-->










		<div class="row">
			<div class="col-xs-12">



                <!--### MODAL DO FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                <div id="modal-form" class="modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 id="form-titulo-acao" class="blue bigger"></h4>
                            </div>







                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->
                            <form class="form-horizontal" role="form" id="form-cadastros" name="form-cadastros">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="col-xs-12">
                                                <div id="info_reg_user" class="form-group">
                                                    <div class="col-sm-3 no-padding-right" style="height: 22px; line-height:22px; text-align:right;">ID</div>

                                                    <div class="col-sm-9">
                                                        <span id="txt_id"></span> &nbsp;&nbsp;<i id="txt_id_reg" class="ace-icon fa fa-history bigger-120" data-rel="reg_user_popover" data-trigger="hover" data-placement="right" data-content="" title="Histórico do registro"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="configuracao_filial">Filial</label>

                                                    <div class="col-sm-9">
                                                        <select class="chosen-select" id="exitentes_configuracao_filial" data-placeholder="Filiais já existentes" onchange="this.form.configuracao_filial.value = this.options[this.options.selectedIndex].value">
                                                   			<option value=""></option>
															<?
                                                                $sql_existentes = "SELECT configuracao_filial FROM configuracoes WHERE configuracao_excluido <> 'sim' GROUP BY configuracao_filial ORDER BY configuracao_filial ASC";
                                                                $exe_existentes = mysql_query($sql_existentes, $con) or die("Erro do MySQL[exe_existentes]: ".mysql_error());
                                                                while($ver_existentes = mysql_fetch_array($exe_existentes))
                                                                    { 
                                                            ?>
                                                            			<option value="<? echo $ver_existentes["configuracao_filial"]; ?>"><? echo $ver_existentes["configuracao_filial"]; ?></option>
                                                            <?
																	} 
                                                            ?>
                                                        </select>
                                                        <br />

                                                        <input type="text" name="configuracao_filial" id="configuracao_filial" class="col-xs-10 col-sm-6" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="configuracao_categoria">Categoria</label>

                                                    <div class="col-sm-9">
                                                        <select class="chosen-select" id="exitentes_configuracao_filial" data-placeholder="Categorias já existentes" onchange="this.form.configuracao_categoria.value = this.options[this.options.selectedIndex].value">
                                                   			<option value=""></option>
															<?
                                                                $sql_existentes = "SELECT configuracao_categoria FROM configuracoes WHERE configuracao_excluido <> 'sim' GROUP BY configuracao_categoria ORDER BY configuracao_categoria ASC";
                                                                $exe_existentes = mysql_query($sql_existentes, $con) or die("Erro do MySQL[exe_existentes]: ".mysql_error());
                                                                while($ver_existentes = mysql_fetch_array($exe_existentes))
                                                                    { 
                                                            ?>
                                                            			<option value="<? echo $ver_existentes["configuracao_categoria"]; ?>"><? echo $ver_existentes["configuracao_categoria"]; ?></option>
                                                            <?
																	} 
                                                            ?>
                                                        </select>
                                                        <br />
                                                        <input type="text" name="configuracao_categoria" id="configuracao_categoria" class="col-xs-10 col-sm-8" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="configuracao_titulo">Título</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="configuracao_titulo" id="configuracao_titulo" class="col-xs-10 col-sm-8" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="configuracao_valor">Valor</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="configuracao_valor" id="configuracao_valor" class="col-xs-10 col-sm-8" />
                                                    </div>
                                                </div>

                                                <div>
                                                    <label for="configuracao_observacoes">Observações</label>

                                                    <textarea class="form-control" name="configuracao_observacoes" id="configuracao_observacoes" placeholder="Escreva a descrição dessa configuração"></textarea>
                                                  	<input type="hidden" name="configuracao_id" id="configuracao_id" />
                                                  	<input type="hidden" name="operacao" id="operacao" />
                                                  	<input type="hidden" name="filial" id="filial" />
                                                </div>




                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times"></i>
                                        Cancelar
                                    </button>

                                    <button type="submit" id="btn-inserir" class="btn btn-sm btn-primary" data-id="inserir">
                                        <i class="ace-icon fa fa-check"></i>
                                        Inserir
                                    </button>

                                    <button type="submit" id="btn-alterar" class="btn btn-sm btn-success" data-id="alterar">
                                        <i class="ace-icon fa fa-check"></i>
                                        Alterar
                                    </button>
                                </div>
                            </form>
                            <!--### FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->








                        </div>
                    </div>
                </div>
                <!--### MODAL DO FORMULÁRIO DE CADASTRO E ALTERAÇÃO ###-->


            </div>
		</div>








		<!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->








<!-- page specific plugin scripts -->
<script type="text/javascript">


    var scripts = [null]
	$('[data-ajax-content=true]').ace_ajax('loadScripts', scripts, function() {


	  //inline scripts related to this page
		 jQuery(function($) {


		  moment.locale('pt_BR');

	<?
		//SE NÃO FOR AUTORIZADO O ACESSO AO ARQUIVO INFORMA MENSAGEM DE ERRO USANDO GRITTER
		if($autorizado=="nao")
			{
	?>
				$(".col-xs-12").hide();
				$.gritter.add({
					title: '<i class="ace-icon fa fa-times"></i> Erro!',
					text: '<? echo $sistema_nao_autorizado; ?>',
					class_name: 'gritter-error gritter-center',
					sticky: false,
					fade_out_speed:500
				});
	<?
			}
	?>



		/*###################################################################
		|																	|
		|	A variável (objeto) oTable1 é a que recebe o GRID e todas as 	|
		|	configurações. Sempre que necessário referenciar o GRID 		|
		|	utilizar este nome deste objeto.								|
		|																	|
		###################################################################*/
		var oTable1 =
		$('#sample-table-2')
		//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
		.DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": {
				//URL E INFORMAÇÕES PARA CARREGAMENTO DOS DADOS NO GRID
				"url": "<? echo $http_includes; ?>/jquery.datatables.serverSide.php",
				"data": function ( d ) {
					d.sistema = "<? echo $sistema; ?>";
					d.filial = "<? echo $_REQUEST['filial']; ?>";
				}
			},
			//"ajax": "configuracoes/serverSide.php",
			//stateSave: true,
			bAutoWidth: false,
			"paging": true,
			"columns": [
				<?
					//FOREACH QUE RETORNA TODAS AS COLUNAS DO GRID CONFORME INFORMAÇÃO DO ARQUIVO sistema.cfg.php
					foreach($array_colunas_grid as $key_coluna => $config_coluna)
						{
							$num_coluna_grid++;
							echo '
							{ ';
							echo '"name": "'.$key_coluna.'", ';
							echo '"data": "'.$key_coluna.'", ';
							echo '"title": "'.$config_coluna["title"].'", ';
							echo '"width": "'.$config_coluna["width"].'", ';
							echo '"visible": '.$config_coluna["visible"].', ';
							echo '"searchable": '.$config_coluna["searchable"].', ';
							if($config_coluna["className"]<>"") echo '"className": "'.$config_coluna["className"].'", ';
							echo '"orderable": '.$config_coluna["orderable"];
							echo ' }';
							if($num_coluna_grid<$total_colunas_grid) echo ',';
						}
				?>,
				{
                	"name": "acoes",
                	"width": "10%",
                	"visible": true,
                	"searchable": false,
                	"orderable": false,
					"data": null,

					/*###################################################################
					|																	|
					|	defaultContent é a variável que contém o conteúdo da última 	|
					|	coluna do GRID onde existem os botões de controle das ações 	|
					|	para cada registro. Há dois grupos de botões, um para mobile e 	|
					|	outro para telas normais. Observar os elementns 				|
					|	<i class='ace-icon fa fa-pencil que contém os botões. 			|
					|																	|
					|	Propriedades													|
					|	data-id: é a operação do botão (alterar, excluir, etc) 			|
					|	title: texto da descrição do botão								|
					|	data-toggle: padrão modal para chamada do form					|
					|	data-target: ID da janela modal que será aberta					|
					|																	|
					###################################################################*/
					"defaultContent": "<div class='hidden-sm hidden-xs action-buttons'>\
											<a class='green'><i class='ace-icon fa fa-edit bigger-130' title='Alterar <? echo $sistema_titulo_item; ?>' data-id='alterar'></i></a>\
											<a class='red'><i class='ace-icon fa fa-trash-o bigger-130' title='Excluir <? echo $sistema_titulo_item; ?>' data-id='excluir'></i></a>\
										</div>\
										<div class='hidden-md hidden-lg'>\
											<div class='inline position-relative'>\
												<button class='btn btn-minier btn-yellow dropdown-toggle' data-toggle='dropdown' data-position='auto'><i class='ace-icon fa fa-caret-down icon-only bigger-120'></i></button>\
												<ul class='dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>\
													<li><a class='tooltip-success' data-rel='tooltip'><span class='green'><i class='ace-icon fa fa-edit bigger-120' title='Alterar <? echo $sistema_titulo_item; ?>' data-id='alterar'></i></span></a></li>\
													<li><a class='tooltip-error' data-rel='tooltip'><span class='red'><i class='ace-icon fa fa-trash-o bigger-120' title='Excluir <? echo $sistema_titulo_item; ?>' data-id='excluir'></i></span></a></li>\
												</ul>\
											</div>\
										</div>"
				}
			],
			"order": [
				<?
					//FOREACH DA VARIÁVEL NO sistema.cfg.php PARA DEFINIR A ORDENAÇÃO INICIAL DO GRID
					foreach($array_ordenacao_grid as $coluna_nome => $coluna_ordenacao)
						{
							$num_ordenacao_grid++;
							echo '
							[ ';
							echo $array_colunas_key_grid[$coluna_nome].', ';
							echo '"'.$coluna_ordenacao.'" ';
							echo ' ]';
							if($num_ordenacao_grid<$total_ordenacao_grid) echo ',';
						}
				?>
			],
			//TRADUÇÃO DOS ITENS DE CONTROLE DO GRID
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"zeroRecords": "A busca não retortnou resultados",
				"info": "Página _PAGE_ de _PAGES_ | Total de registros: _TOTAL_",
				"infoEmpty": "0 registros",
				"infoFiltered": "de um total de _MAX_ cadastrados",
				"loadingRecords": "Carregando...",
				"sProcessing": "<i class='ace-icon fa fa-spin fa-cog blue bigger-160'></i> Processado...",
				"search":         "Buscar:",
				"paginate": {
						"first":      "Primeira",
						"last":       "Última",
						"next":       "Próxima",
						"previous":   "Anterior"
				}
			},
	    } );





		/*### CONTROLES DOS FILTROS ADICIONAIS NO GRID ###*/
		$('#filtro_categoria').on( 'change', function () {
			oTable1
                .columns( $(this).attr('data-id') )
				.search( this.value )
				.draw();
		} );
		/*### CONTROLES DOS FILTROS ADICIONAIS NO GRID ###*/



		/*### BOTÃO DE ATUALIZAR GRID ###*/
		$('#btn-atualizar-grid').on('click', function () {
			oTable1.ajax.reload(null,false);
		});
		/*### BOTÃO DE ATUALIZAR GRID ###*/



		/*### CONTROLES DO INPUT CHOSEN ###*/
		$('.chosen-select').chosen({allow_single_deselect:true, search_contains:true});
		//resize the chosen on window resize

		//FUNÇÃO QUE CORRIGE BUG NO VALIDATE() PARA VALIDAR CAMPOS HIDDEN DO CHOSEN
		jQuery.validator.setDefaults({
		  ignore: []
		});

		$(window)
		.off('resize.chosen')
		.on('resize.chosen', function() {
			$('.chosen-select').each(function() {
				 var $this = $(this);
				 $this.next().css({'width': $this.parent().width()});
			})
		}).trigger('resize.chosen');
		/*### CONTROLES DO INPUT CHOSEN ###*/



		//HACK PARA QUANDO PASSAR O MOUSE SOBRE OS BOTÕES O CURSOR FICAR TIPO pointer
		$('#sample-table-2 tbody').on( 'mouseover', 'tr td:last-child i', function () { $(this).css("cursor","pointer") } );




		/*###################################################################
		|																	|
		|	A função abaixo é aque controla todas as ações dos botões de 	|
		|	ação de cada registro da tabela. Sempre que for necessário 		|
		|	adicionar novos botões será nesta função que as as operações 	|
		|	serão definidas, incluindo operações de selecionar registro, 	|
		|	comandos de alterar, comandos de excluir e demais ações do 		|
		|	módulo.															|
		|																	|
		###################################################################*/
		$('#sample-table-2 tbody').on( 'click', 'tr td:last-child i', function () {

				//REFERÊNCIA PARA INDICAR AS POSIÇÕES PADRÃO DO ID DO REGISTRO NO GRID
				//eq(5) class: hidden-md hidden-lg => eq(7) id: 37 = REDUZIDO => (hidden-md eq(5): 0 / hidden-sm eq(1): -1)
				//eq(1) class: hidden-sm hidden-xs action-buttons => eq(3) id: 27 = GRANDE (hidden-md eq(5): -1 / hidden-sm eq(1): 0)



				//CONDIÇÃO PARA DEFINIR A VARIÁVEL id_reg QUE ARMAZENA O ID DO REGISTRO NO GRID
				if($(this).parents().eq(5).attr("class").indexOf('hidden-md')==0) var id_reg = $(this).parents().eq(7).attr("id");
				else if($(this).parents().eq(1).attr("class").indexOf('hidden-sm')==0) var id_reg = $(this).parents().eq(3).attr("id");



				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ALTERAR NO GRID ###*/
				if(($(this).attr('data-id')=="alterar")&&("<? echo $auth[$sistema]["alterar"]; ?>"=="sim"))
					{
						//alert("alterar "+id_reg);

						//TROCA O TÍTULO DO FORMULÁRIO PARA A VARIÁVEL title QUE ESTÁ NO BOTÃO
						$('#form-titulo-acao').html( $(this).attr('title') );

						//ESCONDE O BOTÃO DE INSERIR E MOSTRA O DE ALTERAR
						$('#btn-inserir').hide();
						$('#btn-alterar').show();

						/*### MOSTRA A DIV DE CARREGANDO ###*/
						var contentArea = $('body');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						/*### MOSTRA A DIV DE CARREGANDO ###*/




						/*### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###*/
						$.ajax({
							dataType: "json",
							//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
							url: '<? echo $sistema; ?>/acoes_registros.php',
							data: {
								//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
								chave_primaria: id_reg,
								operacao: "selecionar",
								filial: "<? echo $_REQUEST['filial']; ?>"
							},
							success: function(data, textStatus, jqXHR)
							{
								//console.log("data: "+data+"\ntextStatus: "+textStatus);

								/*### EACH PARA PERCORRER TODO O FORMULÁRIO DE CADASTRO E PREENCHER COM OS DADOS DO BANCO ###*/
								$.each($('#form-cadastros :input'), function(index, element) {
									/*console.log("type: "+index+" => "+element.type);
									console.log("name: "+index+" => "+element.name);
									console.log("id: "+index+" => "+element.id);
									console.log("value: "+index+" => "+element.value);
									console.log("lang: "+index+" => "+element.lang);
									console.log("data[id]: "+data[1][element.id]);
									console.log("data[name]: "+data[1][element.name]);
									console.log("this.val(): "+$(this).val());
									console.log("--------\n");*/

									if(element.type=="radio")
										{
											if($(this).val() == data[1][element.name])
												{
													$(this).prop('checked',true);
													$(':input[name='+element.name+'_hidden]').val(data[1][element.name]);
												}
											else $(this).prop('checked',false);
										}
									/*###################################################################
									|																			|
									|	O padrão de nomenclatura dos campos checkbox sempre será o 		|
									|	name='campo[]' e o id='campo_X' onde X é um numeral incremental	|
									|																			|
									###################################################################*/
									if(element.type=="checkbox")
                                        {
                                            /*console.log("\n-------");
                                            console.log("element.name: "+element.name);
                                            console.log("element.id: "+element.id);
                                            console.log("this.val: "+$(this).val());
                                            console.log("this.id: "+$(this).attr('id'));*/
                                            $(this).prop('checked', false);
                                            if(data[1][element.name] != null)
                                                {
                                                    $.each(data[1][element.name], function(key, value) {
                                                        //console.log("value do bd: "+value);
                                                        if($("#"+element.id).val() == value)
                                                            {
                                                                //console.log("#"+element.id+" é true");
                                                                $("#"+element.id).prop('checked', true);
                                                            }
                                                    });
                                                }
                                        }
									/*###################################################################
									|																			|
									|	Para contornar o bug de serialize() do jquery com campo tipo 		|
									|	radio é necessário incluir no form um campo tipo hidden com o 	|
									|	memso nome do radio acrescido de _hidden e colocar como 			|
									|	lang="NaoAtribuir" para não processar no $.each					|
									|																			|
									###################################################################*/
									else if((element.lang!="NaoAtribuir")&&(element.type!="radio"))
										{
											$(this).val(data[1][element.id]).trigger('chosen:updated');
										}
								});
								/*### EACH PARA PERCORRER TODO O FORMULÁRIO DE CADASTRO E PREENCHER COM OS DADOS DO BANCO ###*/



								/*### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###*/
								$('#txt_id').html( data[1]["<? echo $sistema_prefixo_campos; ?>id_reg_user"] );
								var texto_id_reg = "<strong>Criação:</strong> <br> "+data[1]["<? echo $sistema_prefixo_campos; ?>criacao_data"]+" - "+data[1]["reg_criacao_usuario"];
								if(data[1]["reg_atualizacao_usuario"] != null) texto_id_reg += "<br><br><strong>Última atualização:</strong> <br> "+data[1]["<? echo $sistema_prefixo_campos; ?>atualizacao_data"]+" - "+data[1]["reg_atualizacao_usuario"];
								$('#txt_id_reg').attr("data-content", texto_id_reg);
								$('#info_reg_user').show();
								/*### PREENCHE AS INFORMAÇÕES DE ID DO REGISTRO E HISTÓRICO DE ATUALIZAÇÕES ###*/


								/*### ESCONDE A DIV DE CARREGANDO ###*/
								contentArea.css('opacity', 1);
								contentArea.prevAll('.ajax-loading-overlay').remove();
								/*### ESCONDE A DIV DE CARREGANDO ###*/

								//CHAMADA PARA ABRIR O FORMULÁRIO DE CADASTRO
								$('#modal-form').modal("show");

							},
							error: function(jqXHR, textStatus, errorThrown) {
								//MENSAGEM DE ERRO QUANDO A EXECUÇÃO DA SQL NÃO DER CERTO
								//console.log("textStatus: "+textStatus+"\nerrorThrown: "+errorThrown);
								$.gritter.add({
									title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
									text: errorThrown,
									class_name: 'gritter-error gritter-center',
									sticky: false,
									fade_out_speed:500,
									after_close: function(e) {
										contentArea.css('opacity', 1)
										contentArea.prevAll('.ajax-loading-overlay').remove();
									}
								});
							}
						});
						/*### AJAX PARA POSTAGEM DO PEDIDO DE SELEÇÃO DO REGISTRO ###*/
					}
				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE ALTERAR NO GRID ###*/

























				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE EXCLUIR NO GRID ###*/
				else if(($(this).attr('data-id')=="excluir")&&("<? echo $auth[$sistema]["excluir"]; ?>"=="sim"))
					{
						//alert("excluir "+id_reg);

						/*### ABRE CAIXA DE DIÁLOGO PARA CONFIRMAR A EXCLUSÃO ###*/
						bootbox.dialog({
							message: "<span class='bigger-110'>O registro será excluído. Confirma?</span>",
							buttons:
							{
								"danger" :
								{
									"label" : "Excluir!",
									"className" : "btn-sm btn-danger",
									"callback": function() {

										/*### MOSTRA A DIV DE CARREGANDO ###*/
										var contentArea = $('body');
										contentArea
										.css('opacity', 0.25)

										var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
										var offset = contentArea.offset();
										loader.css({top: offset.top, left: offset.left})
										/*### MOSTRA A DIV DE CARREGANDO ###*/




										/*### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###*/
										$.ajax({
											type: "POST",
											//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
											url: '<? echo $sistema; ?>/acoes_registros.php',
											data: {
												//VARIÁVEIS PADRÃO PARA MANDAR PARA O ARQUIVO DE EXECUÇÃO
												chave_primaria: id_reg,
												operacao: "excluir",
												filial: "<? echo $_REQUEST['filial']; ?>"
											},
											success: function(data)
											{
												/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/
												if (data === 'OK') {
													//ABRE MENSAGEM DE OK
													$.gritter.add({
														title: '<i class="ace-icon fa fa-check"></i> OK!',
														text: '<? echo $registro_excluido; ?>',
														class_name: 'gritter-success gritter-center',
														fade_out_speed:500,
														time:2000,
														before_open: function(e){
															/*### ESCONDE A DIV DE CARREGANDO ###*/
															contentArea.css('opacity', 1)
															contentArea.prevAll('.ajax-loading-overlay').remove();
															/*### ESCONDE A DIV DE CARREGANDO ###*/

															//FADEOUT NA LINHA DO GRID QUE FOI EXCLUÍDO
															$('#sample-table-2 #'+id_reg).fadeOut();

															//ATUALIZA A TABELA DO GRID
															oTable1.ajax.reload(null,false);
														}
													});
												}
												/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/






												/*### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###*/
												else {
													$.gritter.add({
														title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
														//O TEXTO DA MENSAGEM DE ERRO SERÁ TUDO O QUE TIVER RETORNADO DE MENSAGEM NO ARQUIVO DE EXECUÇÃO
														text: data,
														class_name: 'gritter-error gritter-center',
														sticky: false,
														fade_out_speed:500,
														after_close: function(e) {
															/*### ESCONDE A DIV DE CARREGANDO ###*/
															contentArea.css('opacity', 1)
															contentArea.prevAll('.ajax-loading-overlay').remove();
															/*### ESCONDE A DIV DE CARREGANDO ###*/
														}
													});
												}
											}
										});
										/*### AJAX PARA POSTAGEM DO PEDIDO DE EXCLUSÃO DO REGISTRO ###*/
									}
								},
								"button" :
								{
									"label" : "Cancelar",
									"className" : "btn-sm"
								}
							}
						});
					}
				/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTAO DE EXCLUIR NO GRID ###*/














				/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM AUTORIZAÇÃO PARA EXECUTAR A OPERAÇÃO ###*/
				else
					{
						$.gritter.add({
							title: '<i class="ace-icon fa fa-times"></i> Erro!',
							text: '<? echo $sistema_nao_autorizado; ?>',
							class_name: 'gritter-error gritter-center',
							sticky: false,
							fade_out_speed:500,
							before_open: function(e){
								$('#modal-form').modal("hide");
							}
						});
					}
				/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM AUTORIZAÇÃO PARA EXECUTAR A OPERAÇÃO ###*/
			} );












		/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR ###*/
		$('[data-id="btn-form-inserir"]').on('click', function() {
			//verifica se o usuário tem permissão para a operação
			if("<? echo $auth[$sistema]["inserir"]; ?>"=="sim")
				{
					//TROCA O TÍTULO DO FORMULÁRIO PARA A VARIÁVEL title QUE ESTÁ NO BOTÃO
					$('#form-titulo-acao').html( $(this).attr('title') );

					//ESCONDE O BOTÃO DE ALTERAR E MOSTRA O DE INSERIR
					$('#btn-inserir').show();
					$('#btn-alterar').hide();

					//ESCONDE A DIV COM INFORMAÇÃO DE ID E HISTÓRICO DE ATUALIZAÇÕES DO REGISTRO
					$('#info_reg_user').hide();

					//ZERA O FORMULÁRIO
					$('#form-cadastros').trigger("reset");
					$('#form-cadastros .chosen-select').trigger("chosen:updated");

                    //ABRE O MODAL DE CADASTRO
                    $('#modal-form').modal("show");
				}
			else
				{
					/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###*/
					$.gritter.add({
						title: '<i class="ace-icon fa fa-times"></i> Erro!',
						text: '<? echo $sistema_nao_autorizado; ?>',
						class_name: 'gritter-error gritter-center',
						sticky: false,
						fade_out_speed:500,
						before_open: function(e){
							$('#modal-form').modal("hide");
						}
					});
					/*### MENSAGEM DE ERRO QUANDO O USUÁRIO NÃO TEM PERMISSÃO PAR EXECUTAR A OPERAÇÃO ###*/
				}
		 })
		/*### AÇÕES DISPARADAS QUANDO CLICAR NO BOTÃO PARA ABRIR O FORMULÁRIO DE INSERIR ###*/









		/*### FUNÇÃO QUE ARMAZENA QUAL BOTÃO SUBMIT FOI PRESSIONADO NO FORM ###*/
		var btnSubmit;
		$('#form-cadastros :submit').on('click', function() {
			$('#form-cadastros #operacao').val( $(this).attr("data-id") );
			$('#form-cadastros #filial').val("<? echo $_REQUEST['filial']; ?>");
		});
		/*### FUNÇÃO QUE ARMAZENA QUAL BOTÃO SUBMIT FOI PRESSIONADO NO FORM ###*/



		/*### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###*/
		$('#form-cadastros').validate({
			errorElement: 'div',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				/*### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###*/
				configuracao_filial: {
					required: true
				},
				configuracao_categoria: {
					required: true
				},
				configuracao_titulo: {
					required: true
				},
				configuracao_valor: {
					required: true
				}
				/*### CAMPOS OBRIGATÓRIOS E CONDIÇÕES ###*/
			},

			messages: {
				/*### MENSAGENS DE VALIDAÇÃO ###*/
				configuracao_filial: {
					required: "Digite a filial"
				},
				configuracao_categoria: {
					required: "Digite a categoria"
				},
				configuracao_titulo: {
					required: "Digite o título"
				},
				configuracao_valor: {
					required: "Digite o valor"
				}
				/*### MENSAGENS DE VALIDAÇÃO ###*/
			},


			highlight: function (e) {
				$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
			},

			success: function (e) {
				$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
				$(e).remove();
			},

			errorPlacement: function (error, element) {
				if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
					var controls = element.closest('div[class*="col-"]');
					if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
					else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
				}
				else if(element.is('.select2')) {
					error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
				}
				else if(element.is('.chosen-select')) {
					error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
				}
				else error.insertAfter(element.parent());
			},

			submitHandler: function (form) {
				//console.log(btnSubmit);

				/*### AJAX PARA POSTAGEM DO FORMULÁRIO ###*/
				$.ajax({
					type: "POST",
					//URL DO ARQUIVO QUE VAI EXECUTAR A AÇÃO E TRAZER O RESULTADO
					url: '<? echo $sistema; ?>/acoes_registros.php',
					//ARRAY PARA ARMAZENAR TODOS OS INPUTS DENTRO DO FORM
					data: $("#form-cadastros").serialize(),
					success: function(data)
					{
						/*### MOSTRA A DIV DE CARREGANDO ###*/
						var contentArea = $('body');
						contentArea
						.css('opacity', 0.25)

						var loader = $('<div style="position: fixed; z-index: 2000;" class="ajax-loading-overlay"><i class="ajax-loading-icon fa fa-spin fa-cog fa-2x blue icone-carregar"></i></div>').insertBefore(contentArea);
						var offset = contentArea.offset();
						loader.css({top: offset.top, left: offset.left})
						/*### MOSTRA A DIV DE CARREGANDO ###*/


						/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/
						if (data === 'OK') {

							if( $('#form-cadastros #operacao').val() == "inserir") var texto_OK = '<? echo $registro_inserido; ?>';
							else if( $('#form-cadastros #operacao').val() == "alterar") var texto_OK = '<? echo $registro_alterado; ?>';

                            //FECHA O MODAL DE CADASTRO
                            $('#modal-form').modal("hide");

							$.gritter.add({
								title: '<i class="ace-icon fa fa-check"></i> OK!',
								text: texto_OK,
								class_name: 'gritter-success gritter-center',
								fade_out_speed:500,
								time:2000,
								before_open: function(e){
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
									oTable1.ajax.reload(null,false);
								}
							});
						}
						/*### AÇÕES QUANDO O RESULTADO DA EXECUÇÃO RETORNAR TEXTO OK ###*/






						/*### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###*/
						else {
							$.gritter.add({
								title: '<i class="ace-icon fa fa-database"></i> Erro na consulta!',
								text: data,
								class_name: 'gritter-error gritter-center',
								sticky: false,
								fade_out_speed:500,
								after_close: function(e) {
									contentArea.css('opacity', 1)
									contentArea.prevAll('.ajax-loading-overlay').remove();
								}
							});
						}
						/*### AÇÕES QUANDO RETORNAR ERRO NO RESULTADO DA EXECUÇÃO ###*/


					}
				});
				/*### AJAX PARA POSTAGEM DO FORMULÁRIO ###*/

			},
			invalidHandler: function (form) {
			}
		});
		/*### FUNÇÕES QUE FAZEM A VALIDAÇÃO DO FORMULÁRIO DE CADASTRO/ALTERAÇÃO ###*/












		//FUNÇÃO QUE ATIVA O PROPOVER DO HISTÓRICO DO REGISTRO
		$('[data-rel=reg_user_popover]').popover({container:'body', html:true});




		/*### FUNÇÃO PARA FORMATAR OS BOTÕES DE CONTROLE DAS AÇÕES NO GRID ###*/
		$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		function tooltip_placement(context, source) {
			var $source = $(source);
			var $parent = $source.closest('table')
			var off1 = $parent.offset();
			var w1 = $parent.width();

			var off2 = $source.offset();
			//var w2 = $source.width();

			if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
			return 'left';
		}
		/*### FUNÇÃO PARA FORMATAR OS BOTÕES DE CONTROLE DAS AÇÕES NO GRID ###*/





		/*### FUNÇÃO PARA CORRIGIR O CHOSEN QUANDO ESTÁ DENTRO DE UM FORM MODAL ###*/
		//chosen plugin inside a modal will have a zero width because the select element is originally hidden
		//and its width cannot be determined.
		//so we set the width after modal is show
		$('#modal-form').on('shown.bs.modal', function () {
			if(!ace.vars['touch']) {
				$(this).find('.chosen-container').each(function(){
					$(this).find('a:first-child').css('width' , '210px');
					$(this).find('.chosen-drop').css('width' , '210px');
					$(this).find('.chosen-search input').css('width' , '200px');
				});
			}
		})
		/*### FUNÇÃO PARA CORRIGIR O CHOSEN QUANDO ESTÁ DENTRO DE UM FORM MODAL ###*/


	})
	});
</script>