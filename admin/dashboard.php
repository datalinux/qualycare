<?
	$include_functions_js = "nao";
	include("../includes/configure.inc.php");
	include($pasta_includes."/login.inc.php");
?>

<p>&nbsp;</p>
<p>Bem-vindo(a) <? echo $_SESSION["login"]["nome"]; ?>, utilize o menu ao lado para navegar no sistema.</p>
<?php
// echo "<pre>SESSION: "; print_r($_SESSION); echo "</pre>"; ?>
<!-- page specific plugin scripts -->
<script type="text/javascript">


	var scripts = [null,"../lib/ace/assets/js/dataTables/jquery.dataTables.js",
		"../lib/ace/assets/js/dataTables/jquery.dataTables.bootstrap.js",
		"../lib/ace/assets/js/jquery-ui.custom.js",
		"../lib/ace/assets/js/jquery.ui.touch-punch.js",
		"../lib/ace/assets/js/chosen.jquery.js",
		"../lib/ace/assets/js/fuelux/fuelux.spinner.js",
		"../lib/ace/assets/js/date-time/bootstrap-datepicker.js",
		"../lib/ace/assets/js/date-time/locales/bootstrap-datepicker.pt-BR.js",
		"../lib/ace/assets/js/date-time/bootstrap-timepicker.js",
        "../lib/ace/assets/js/date-time/moment-with-locales.min.js",
		"../lib/ace/assets/js/date-time/daterangepicker.js",
		"../lib/ace/assets/js/date-time/bootstrap-datetimepicker.js",
		"../lib/ace/assets/js/bootstrap-colorpicker.js",
		"../lib/ace/assets/js/jquery.validate.js",
		"../lib/ace/assets/js/additional-methods.js",
		"../lib/ace/assets/js/jquery.gritter.js",
		"../lib/ace/assets/js/bootbox.js",
		"../lib/ace/assets/js/jquery.knob.min.js",
		"../lib/ace/assets/js/jquery.autosize.min.js",
		"../lib/ace/assets/js/jquery.inputlimiter.1.3.1.js",
		"../lib/ace/assets/js/jquery.maskedinput.js",
		"../lib/ace/assets/js/select2.js",
		"../lib/ace/assets/js/bootstrap-tag.min.js",
		"../lib/ace/assets/js/bootstrap-fileinput/fileinput.js",
		"../lib/ace/assets/js/bootstrap-fileinput/fileinput_locale_pt-BR.js",
        "../lib/cropit/dist/jquery.cropit.js",
        "../lib/rotate/jQueryRotate.js",
		"../lib/ace/assets/js/typeahead.jquery.js", null]

	  $('[data-ajax-content=true]').ace_ajax('loadScripts', scripts, function() {


	  //inline scripts related to this page
		 jQuery(function($) {


		});

	});
</script>